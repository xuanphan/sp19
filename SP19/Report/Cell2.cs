﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Report
{
    public partial class Cell2
    {

        public static string GetCellSP(ICell cell)
        {
            if (cell == null)
            {
                return null;
            }
            string cellValue = null;
            switch (cell.CellType)
            {
                case CellType.String:
                    cellValue = cell.StringCellValue.Trim();
                    break;
                case CellType.Formula:
                    try
                    {
                        cellValue = cell.NumericCellValue.ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        cellValue = cell.StringCellValue.ToString().Trim();
                    }
                    break;
                case CellType.Numeric:
                    cellValue = DateUtil.IsCellDateFormatted(cell)
                        ? cell.DateCellValue.ToString().Trim()
                        : cell.NumericCellValue.ToString().Trim();
                    break;
                case CellType.Blank:
                    cellValue = "";
                    break;
                case CellType.Boolean:
                    cellValue = cell.BooleanCellValue.ToString().Trim();
                    break;
            }
            return cellValue;
        }
    }
}
