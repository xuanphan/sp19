﻿$.validator.addMethod("validDate", function (value) {
    var result = isDate(value);
    return result;
}, "Nhập sai kiểu ngày tháng (dd/mm/yyyy)");

$.validator.addMethod("validDateFromTo", function (value) {
    var dateto = $.datepicker.parseDate('dd/mm/yy', $("#DateTo").val());
    var datefrom = $.datepicker.parseDate('dd/mm/yy', $("#DateFrom").val());
    if (datefrom > dateto) {
        return false;
    }
    else
        return true;
}, "Từ ngày phải nhỏ hơn đến ngày");

function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    //Declare Regex  
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[3];
    dtDay = dtArray[1];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}
