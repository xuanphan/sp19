﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Report.Models
{
    public class ChangeOutletModel
    {
        public List<Outlet> listforuser { get; set; }
        public List<Outlet> listoutlet { get; set; }
    }
}