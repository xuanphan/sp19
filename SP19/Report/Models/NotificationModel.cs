﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Report.Models
{
    public class NotificationModel
    {
        [Display(Name = "Tiêu Đề")]
        public string NotificationTitle { get; set; }

        [Display(Name = "Nội Dung")]
        public string NotificationContent { get; set; }

        [UIHint("_ListOutletmulti")]
        [Display(Name = "Siêu Thị")]
        public string OutletNotification { get; set; }

        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionAttendance { get; set; }
    }
}