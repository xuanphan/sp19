﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;

namespace Report.Models
{
    public class AddUserModel
    {
        public string message { get; set; }
        public int status { get; set; }
        public List<User_App> listDataApp { get; set; }
        public List<User_Web> listDataWeb { get; set; }
    }
}