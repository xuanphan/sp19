﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Report.Models
{
    public class AssignGiftMegaModel
    {
        public List<Outlet> listoutlet { get; set; }
        public List<Lucky_Mega_Gift> listgift { get; set; }
        public List<Lucky_Mega_Set_Gift_To_Outlet> listgiftoutlet { get; set; }
    }
}