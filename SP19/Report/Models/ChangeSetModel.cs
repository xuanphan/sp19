﻿using Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Report.Models
{
    public class ChangeSetModel
    {
        public List<Outlet_Requirement_Change_Set> outlet_Requirement_Change_Set { get; set; }
        public List<Outlet_Current_Set> outlet_Current_Set { get; set; }
        public List<Brand_Set> brand_Set { get; set; }
        public string OutletName { get; set; }
        public int OutletID { get; set; }
        public long BrandSetCurrent { get; set; }
        public long BrandSetRequest { get; set; }

    }
}