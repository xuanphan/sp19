﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Report.Models
{
    public class UserModel
    {

        [Display(Name = "Tên Đăng Nhập")]
        public string UserName { get; set; }      

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật Khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Xác Nhận Mật Khẩu")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [UIHint("_RoleType")]
        [Display(Name = "Quyền")]
        public string RoleTypes { get; set; }

        [UIHint("_UserType")]
        [Display(Name = "Loại User")]
        public string UserType { get; set; }
    }
}