﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Report.Models
{
    public class HomeModel
    {
        public int TotalOutlet { get; set; }
        public int ToTalAttendance { get; set; }
        public int TotalProfile { get; set; }
        public int ToTalProfileDiscipline { get; set; }
        public int ToTalSet { get; set; }
        public int ToTalTakeOff { get; set; }

    }
}