﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using System.ComponentModel.DataAnnotations;

namespace Report.Models
{
    public class AddProfileModel
    {
        [Display(Name = "Mã Công Ty")]
        public string ProjectCode { get; set; }
        [Display(Name = "Mã Nhân Viên")]
        public string ProfileCode { get; set; }
        [Display(Name = "Tên Nhân Viên")]
        public string ProfileName { get; set; }
        [Display(Name = "Số Điện Thoại")]
        public string ProfilePhone { get; set; }
        [Display(Name = "Kinh Nghiệm")]
        public string Experience { get; set; }
        [Display(Name = "Mật Khẩu")]
        public string Pass { get; set; }

        [UIHint("_RegionName")]
        [Display(Name = "Tỉnh")]
        public string RegionProfile { get; set; }
    }
}