﻿using Business;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Report.Models
{
    public class RequirementSetModel
    {
        public List<Outlet_Current_Set> outlet_Current_Set { get; set; }
        public List<Warehouse_Requirement_Set_Detail> warehouse_Requirement_Set_Detail { get; set; }

    }
}