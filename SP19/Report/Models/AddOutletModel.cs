﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;

namespace Report.Models
{
    public class AddOutletModel
    {
        public string message { get; set; }
        public int status { get; set; }
        public List<Outlet> listData { get; set; }
    }
}