﻿using Report.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace Report
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                HttpCookie customAuthCookie = Request.Cookies["customauth"];
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(customAuthCookie.Value);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CustomPrincipalSerializeModel serializeModel = serializer.Deserialize<CustomPrincipalSerializeModel>(authTicket.UserData);

                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                newUser.UserID = serializeModel.UserID;
                newUser.SaleID = serializeModel.SaleID;
                newUser.SaleSupID = serializeModel.SaleSupID;
                newUser.ListSaleID = serializeModel.ListSaleID;
                newUser.ProjectID = serializeModel.ProjectID;
                newUser.OutletTypeID = serializeModel.OutletTypeID;
                newUser.UserRole = serializeModel.UserRole;
                newUser.UserName = serializeModel.UserName;
                newUser.FirstName = serializeModel.FirstName;
                newUser.LastName = serializeModel.LastName;
                newUser.UserRoles = serializeModel.Roles;
                newUser.UserMenus = serializeModel.Menus;
                HttpContext.Current.User = newUser;
            }
        }
    }
}