﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Report.Helpers
{
    interface ICustomPrincipal : IPrincipal
    {
        int UserID { get; set; }
        int ProjectID { get; set; }
        int SaleID { get; set; }
        string ListSaleID { get; set; }
        int SaleSupID { get; set; }
        int OutletTypeID { get; set; }
        int UserRole { get; set; }
        string UserName { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }

    public class CustomPrincipal : ICustomPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role)
        {
            return UserRoles.Any(p => p == role);
        }

        public int UserID { get; set; }
        public int SaleID { get; set; }
        public string ListSaleID { get; set; }
        public int SaleSupID { get; set; }
        public int ProjectID { get; set; }
        public int OutletTypeID { get; set; }
        public int UserRole { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<string> UserRoles { get; set; }
        public IEnumerable<string> UserMenus { get; set; }

        public CustomPrincipal(string userName)
        {
            this.Identity = new GenericIdentity(userName);
        }
    }

    public class CustomPrincipalSerializeModel
    {
        public int ProjectID { get; set; }
        public int SaleID { get; set; }
        public int SaleSupID { get; set; }
        public string ListSaleID { get; set; }
        public int OutletTypeID { get; set; }
        public int UserID { get; set; }
        public int UserRole { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public IEnumerable<string> Menus { get; set; }
    }
}