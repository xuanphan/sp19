﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcSiteMapProvider;

namespace Report.Helpers
{
    public class CustomVisibilityProvider : SiteMapNodeVisibilityProviderBase
    {
        public override bool IsVisible(ISiteMapNode node, IDictionary<string, object> sourceMetadata)
        {
            CustomPrincipal userData = HttpContext.Current.User as CustomPrincipal;
            if (userData.UserMenus.Contains(node.Key))
                return true;
            return false;
        }

        public override bool AppliesTo(string providerName)
        {
            return base.AppliesTo(providerName);
        }
    }
}