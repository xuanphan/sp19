﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Report.Helpers
{
    public static class CustomHelper
    {
        public static object RecurseType(NameValueCollection collection, Type type, string prefix)
        {
            try
            {
                var returnObject = Activator.CreateInstance(type);

                foreach (var property in type.GetProperties())
                {
                    foreach (var key in collection.AllKeys)
                    {
                        if (String.IsNullOrEmpty(prefix) || key.Length > prefix.Length)
                        {
                            var propertyNameToMatch = String.IsNullOrEmpty(prefix) ? key : key.Substring(property.Name.IndexOf(prefix) + prefix.Length + 1);

                            if (property.Name.ToLower() == propertyNameToMatch.ToLower())
                            {
                                if (property.PropertyType == typeof(DateTime))
                                {
                                    DateTime value = Convert.ToDateTime(Common.ConvertUtils.ConvertStringToShortDate(collection.Get(key)));
                                    property.SetValue(returnObject, value, null);
                                }
                                else
                                    property.SetValue(returnObject, Convert.ChangeType(collection.Get(key), property.PropertyType), null);
                            }
                        }
                    }
                }

                return returnObject;
            }
            catch (MissingMethodException)
            {

                return null;
            }
        }

        public static RouteValueDictionary ToRouteValues(this NameValueCollection col, Object obj)
        {
            var values = obj == null ? new RouteValueDictionary() : new RouteValueDictionary(obj);
            if (col != null)
            {
                foreach (string key in col)
                {

                    if (!values.ContainsKey(key)) values[key] = col[key];
                }
            }
            return values;
        }
        public static T TryMappingObjectFromQuerystring<T>(NameValueCollection queryString, string conditionKey) where T : class
        {
            try
            {
                var returnObject = Activator.CreateInstance(typeof(T));
                if (queryString.AllKeys.Contains(conditionKey))
                {
                    Mapper.CreateMap<NameValueCollection, T>()
                       .ConvertUsing(x =>
                       {
                           return (T)CustomHelper.RecurseType(x, typeof(T), "");
                       });

                    returnObject = Mapper.Map<NameValueCollection, T>(queryString);
                }
                return (T)returnObject;
            }
            catch
            {
                return (T)Activator.CreateInstance(typeof(T));
            }
        }
        public static T ParseQueryString<T>(ref T search, NameValueCollection queryString) where T : class
        {
            if (queryString.AllKeys.Contains("q"))
            {
                var _queryString = HttpUtility.ParseQueryString(queryString["q"]);
                while (!_queryString.AllKeys.Contains("Search"))
                {
                    _queryString = HttpUtility.ParseQueryString(_queryString["q"]);
                }
                search = CustomHelper.TryMappingObjectFromQuerystring<T>(_queryString, "Search");
            }
            else
            {
                search = CustomHelper.TryMappingObjectFromQuerystring<T>(queryString, "Search");
            }
            return search;
        }
    }
}