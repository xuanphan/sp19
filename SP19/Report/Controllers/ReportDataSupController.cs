﻿using Business;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Report.Models;

namespace Report.Controllers
{

    public class ReportDataSupController : BaseController
    {
        //
        // GET: /ReportPOSM/
        // báo cáo dữ liệu tại siêu thị :: app sup xem
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                Report_Data data = new Report_Data();
                List<ReportDataEXT> list = new List<ReportDataEXT>();
                ReportDataSE se = new ReportDataSE();
                //get outlet
                Outlet outlet = new Outlet();
                SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID,User.UserID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;

                //get area 
                Business.Region region = new Region();
                SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID,User.UserID), "ID", "Text");
                ViewBag.listRegion = listRegion;

                //lấy brand
                ViewBag.typeimagesup = Business.CodeDetail.GetAll("ImageTypeSup");
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = data.Search(ref _messageError, ref list,User.ProjectID, User.UserID, se);
                return View(list);
            }
         
        }
        //------------------------Get Outlet for district
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            string messageSystemError = String.Empty;
            Outlet outlet = new Business.Outlet();
            var List = outlet.GetOutletForRegion(_RegionID, User.ProjectID,User.UserID);
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search(int? page, string _RegionReportDataSE, string _OutletReportDataSE, string _Date)
        {
            
                string _messageError = String.Empty;
                Report_Data data = new Report_Data();
                List<ReportDataEXT> list = new List<ReportDataEXT>();
                ReportDataSE se = new ReportDataSE();

                se.Date = Common.ConvertUtils.ConvertStringToShortDate(_Date);
                se.OutletReportDataSE = int.Parse(_OutletReportDataSE);
                se.RegionReportDataSE = _RegionReportDataSE;
                Session["Date"] = _Date;
                Session["OutletReportDataSE"] = se.OutletReportDataSE;
                Session["RegionReportDataSE"] = se.RegionReportDataSE;
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = data.Search(ref _messageError, ref list, User.ProjectID, User.UserID, se);
                return PartialView("_List", list);
        }
    }
}
