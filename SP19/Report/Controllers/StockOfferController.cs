﻿using Business;
using Report.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPOI.HSSF.Util;
using Common;

namespace Report.Controllers
{
    public class StockOfferController : BaseController
    {
        //
        // GET: /StockManager/
        // quản lý, đề xuất nhập hàng dựa theo báo cáo tồn kho 
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //if (DateTime.Now.TimeOfDay >= TimeSpan.Parse("17:00") || DateTime.Now.TimeOfDay <= TimeSpan.Parse("10:10"))
                //{
                //    return RedirectToAction("TimeOut", "StockManagerForSale");
                //}
                //else
                //{
                string _messageError = String.Empty;
                Stock stock = new Stock();
                List<StockEXT> list = new List<StockEXT>();
                StockSE se = new StockSE();
                //danh sach product
                ViewBag.product = Business.Product.GetProduct(User.ProjectID);

                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    ViewBag.outlets = Business.Outlet.GetAllForSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID);
                    ViewBag.havestatus = false;
                    //get outlet
                    Outlet outlet = new Outlet();
                    SelectList listOutlet = new SelectList(outlet.GetAllForComboSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID), "ID", "Text");
                    ViewBag.listOutlet = listOutlet;

                    //get area 
                    Business.Region region = new Region();
                    SelectList listRegion = new SelectList(region.GetAllForComboSale(User.ProjectID, User.SaleSupID), "ID", "Text");
                    ViewBag.listRegion = listRegion;
                }
                else
                {

                    ViewBag.outlets = Business.Outlet.GetAllForSale(User.ProjectID, User.SaleID, User.OutletTypeID);
                    ViewBag.havestatus = false;
                    //get outlet
                    Outlet outlet = new Outlet();
                    SelectList listOutlet = new SelectList(outlet.GetAllForComboSale(User.ProjectID, User.SaleID), "ID", "Text");
                    ViewBag.listOutlet = listOutlet;

                    //get area 
                    Business.Region region = new Region();
                    SelectList listRegion = new SelectList(region.GetAllForComboSale(User.ProjectID, User.SaleID), "ID", "Text");
                    ViewBag.listRegion = listRegion;


                }
                //get statusstock
                CodeDetail codeDetail = new CodeDetail();
                SelectList listStatusStock = new SelectList(codeDetail.GetForCombo("StatusStock"), "ID", "Text");
                ViewBag.listStatusStock = listStatusStock;
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchOffer(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                return View(list);
                //}
            }
        }

        //------------------------Get Outlet for district
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    var List = outlet.GetOutletForRegionSaleSup(_RegionID, User.ProjectID, User.SaleSupID, User.ListSaleID);
                    return Json(List, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var List = outlet.GetOutletForRegionSale(_RegionID, User.ProjectID, User.OutletTypeID, User.SaleID);
                    return Json(List, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult Search(int? page, string _RegionStockSE, string _OutletStockSE, string _StatusStockSE, string _DateStockSE)
        {
            if (User != null)
            {
                string _messageError = String.Empty;
                Stock stock = new Stock();
                List<StockEXT> list = new List<StockEXT>();
                StockSE se = new StockSE();
                ViewBag.product = Business.Product.GetProduct(User.ProjectID);
                ViewBag.outlets = Business.Outlet.SearchOutletForSale(User.ProjectID, User.SaleID, int.Parse(_OutletStockSE), _RegionStockSE);
                if (_StatusStockSE == "0")
                {
                    ViewBag.havestatus = false;
                }
                else
                {
                    ViewBag.havestatus = true;
                }
                se.DateStockSE = Common.ConvertUtils.ConvertStringToShortDate(_DateStockSE);
                se.OutletStockSE = int.Parse(_OutletStockSE);
                se.RegionStockSE = _RegionStockSE;
                se.StatusStockSE = long.Parse(_StatusStockSE);
                Session["DateStockSE"] = _DateStockSE;
                Session["OutletStockSE"] = se.OutletStockSE;
                Session["RegionStockSE"] = se.RegionStockSE;
                Session["StatusStockSE"] = se.StatusStockSE;
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchOffer(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult TimeOut()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }
    }
}


