﻿using Report.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Business;
using Common;
using System.Web.Mvc;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
namespace Report.Controllers
{
    public class CustomerMegaController : BaseController
    {
        //
        // GET: /ManagerCustomer/
        public ActionResult Index(int? page)
        {

            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
               
                string _messageSystemError = String.Empty;
                List<CustomerEXT> list = new List<CustomerEXT>();
                Business.Customer customer = new Customer();
                CustomerSE se = new CustomerSE();
                //get outlet
                Outlet outlet = new Outlet();
                SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;
                //get area
                Business.Region region = new Region();
                SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listRegion = listRegion;


                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = customer.Search(ref _messageSystemError, User.ProjectID, User.OutletTypeID, se, ref list);

                return View(list);
            }
        }
        //------------------------Get Outlet for district
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        //Tim kiem
        public ActionResult Search(int? page, string _RegionCustomerSE, string _OutletCustomerSE, string _DateFromCustomerSE, string _DateToCustomerSE)
        {
            if (User != null)
            {
              
                string _messageSystemError = String.Empty;
                List<CustomerEXT> list = new List<CustomerEXT>();
                Customer customer = new Business.Customer();
                CustomerSE se = new CustomerSE();
                se.DateFromCustomerSE = Common.ConvertUtils.ConvertStringToShortDate(_DateFromCustomerSE);
                se.DateToCustomerSE = Common.ConvertUtils.ConvertStringToShortDate(_DateToCustomerSE);
                se.OutletCustomerSE = int.Parse(_OutletCustomerSE);
                se.RegionCustomerSE = _RegionCustomerSE;
                Session["OutletcommentSE"] = se.OutletCustomerSE;
                Session["DateFromcommentSE"] = _DateFromCustomerSE;
                Session["DateTocommentSE"] = _DateToCustomerSE;
                Session["RegionCustomerSE"] = se.RegionCustomerSE;
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = customer.Search(ref _messageSystemError, User.ProjectID, User.OutletTypeID, se, ref list);

                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageSystemError = String.Empty;
                Stock stock = new Stock();
                List<CustomerEXT> list = new List<CustomerEXT>();
                CustomerSE se = new CustomerSE();
                Customer customer = new Business.Customer();


                if (Session["OutletcommentSE"] == null || Session["DateFromcommentSE"] == null || Session["DateTocommentSE"] == null || Session["RegionCustomerSE"] == null)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = customer.Search(ref _messageSystemError, User.ProjectID, User.OutletTypeID, se, ref list);
                }
                else
                {
                    se.OutletCustomerSE = long.Parse(Session["OutletcommentSE"].ToString());
                    se.RegionCustomerSE = Session["RegionCustomerSE"].ToString();
                    se.DateToCustomerSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateTocommentSE"].ToString());
                    se.DateFromCustomerSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateFromcommentSE"].ToString());
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = customer.Search(ref _messageSystemError, User.ProjectID, User.OutletTypeID, se, ref list);
                }
                var product = Business.Product.GetProductgift(User.ProjectID);
                var gift = Business.Gift.GetAll(User.ProjectID);
                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\Report_Giff.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report
                int x = 1;
               
                for (int i = 0; i < list.Count(); i++)
                {
                    IRow row7 = sheet_Report.CreateRow(i+3);
                    row7.CreateCell(0).SetCellValue(list[i].CreateDate);
                    row7.GetCell(0).CellStyle = cellStyle;
                    row7.CreateCell(1).SetCellValue(list[i].OutletName);
                    row7.CreateCell(2).SetCellValue(list[i].NumberBill);
                    row7.CreateCell(3).SetCellValue(list[i].CustomerName);
                    row7.CreateCell(4).SetCellValue(list[i].CustomerPhone);
                    int countpro = product.Count();
                    int countgift = gift.Count();
                    List<int> _count = new List<int>();
                    int z = 5;
                    int k = 5 + countpro;
                    for (int e = 0; e < product.Count(); e++)
                    {
                        var _detailproduct = list[i].listproduct.Where(p => p.ProductCode == product[e].ProductCode);
                        if (_detailproduct.Count() > 0)
                        {
                            foreach (var y in _detailproduct)
                            {
                                if (y.ProductCode == product[e].ProductCode)
                                {
                                    row7.CreateCell(z).SetCellValue(y.Number);
                                }
                            }
                        }
                        z++;
                    }
                    for (int e = 0; e < gift.Count(); e++)
                    {
                        var _detailproduct = list[i].listgift.Where(p => p.GiftID == gift[e].ID);
                        if (_detailproduct.Count() > 0)
                        {
                            foreach (var y in _detailproduct)
                            {
                                if (y.GiftID == gift[e].ID)
                                {
                                    row7.CreateCell(k).SetCellValue(y.NumberGift);
                                }
                            }
                        }
                        k++;
                    }
                    var col = 5 + countpro + countgift;
                    row7.CreateCell(col).SetCellValue(list[i].listimage.FirstOrDefault().FilePath);
                }
               
                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "Report_CustomerPresent" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }
        public ActionResult ExportExcel2()
        {
            string _messageSystemError = String.Empty;
            Stock stock = new Stock();
            List<CustomerEXT> list = new List<CustomerEXT>();
            CustomerSE se = new CustomerSE();
            Customer customer = new Business.Customer();


            if (Session["OutletcommentSE"] == null || Session["DateFromcommentSE"] == null || Session["DateTocommentSE"] == null || Session["RegionCustomerSE"] == null)
            {
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = customer.Search(ref _messageSystemError, User.ProjectID, User.OutletTypeID, se, ref list);
            }
            else
            {
                se.OutletCustomerSE = long.Parse(Session["OutletcommentSE"].ToString());
                se.RegionCustomerSE = Session["RegionCustomerSE"].ToString();
                se.DateToCustomerSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateTocommentSE"].ToString());
                se.DateFromCustomerSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateFromcommentSE"].ToString());
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = customer.Search(ref _messageSystemError, User.ProjectID, User.OutletTypeID, se, ref list);
            }
            var product = Business.Product.GetProductgift(User.ProjectID);
            FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\Report_CustomerPresent.xls"), FileMode.Open, FileAccess.Read);
            HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
            HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
            ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
            ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
            IDataFormat format = templateWorkbook.CreateDataFormat();
            cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy");
            cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

            var font1 = templateWorkbook.CreateFont();
            font1.FontHeightInPoints = 11;
            font1.FontName = "Calibri";
            font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

            ICellStyle style2 = templateWorkbook.CreateCellStyle();
            style2.SetFont(font1);

            //excel sheet report
            int x = 1;
            int y = 1;
            int z = 1;
            for (int i = 0; i < list.Count(); i++)
            {
                x--;
                IRow row7 = sheet_Report.CreateRow(i + 2 + x);
                row7.CreateCell(0).SetCellValue(list[i].CreateDate);
                row7.GetCell(0).CellStyle = cellStyle;
                row7.CreateCell(1).SetCellValue(list[i].OutletName);
                row7.CreateCell(2).SetCellValue(list[i].NumberBill);
                row7.CreateCell(3).SetCellValue("Tên KH: " + list[i].CustomerName + "  Địa Chỉ: " + list[i].CustomerAddress + "  SĐT: " + list[i].CustomerPhone);
                int countpro = list[i].listproduct.Count();
                int countgift = list[i].listgift.Count();
                int countimage = list[i].listimage.Count();
                List<int> _count = new List<int>();
                _count.Add(countpro);
                _count.Add(countgift);
                _count.Add(countimage);
                int _max = _count.Max();

                if (_max > 0)
                {
                    for (int e = 0; e < _max; e++)
                    {
                        if (countpro != 0 && countpro > e)
                        {
                            row7.CreateCell(4).SetCellValue(list[i].listproduct.ToList()[e].ProductName);
                            row7.CreateCell(5).SetCellValue(list[i].listproduct.ToList()[e].Number);
                        }
                        else
                        {
                            row7.CreateCell(4).SetCellValue("");
                            row7.CreateCell(5).SetCellValue("");
                        }
                        if (countgift != 0 && countgift > e)
                        {
                            row7.CreateCell(6).SetCellValue(list[i].listgift.ToList()[e].GiftName);
                            row7.CreateCell(7).SetCellValue(list[i].listgift.ToList()[e].NumberGift);
                        }
                        else
                        {
                            row7.CreateCell(6).SetCellValue("");
                            row7.CreateCell(7).SetCellValue("");
                        }
                        if (countimage != 0 && countimage > e)
                        {
                            row7.CreateCell(8).SetCellValue(list[i].listimage.ToList()[e].FilePath);
                        }
                        else
                        {
                            row7.CreateCell(8).SetCellValue("");
                        }
                        x++;
                        row7 = sheet_Report.CreateRow(i + 2 + x);
                    }
                }
                else
                {
                    row7.CreateCell(4).SetCellValue("");
                    row7.CreateCell(5).SetCellValue("");
                    row7.CreateCell(6).SetCellValue("");
                    row7.CreateCell(7).SetCellValue("");
                    row7.CreateCell(8).SetCellValue("");
                    x++;
                    row7 = sheet_Report.CreateRow(i + 2 + x);
                }
            }
            sheet_Report.ForceFormulaRecalculation = true;
            MemoryStream output = new MemoryStream();
            string downloadFileName = "Report_CustomerPresent" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
            templateWorkbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
        }
        [HttpGet]
        public ActionResult _ImageGift(string filepath)
        {
            ViewBag.filepath = filepath;
            return PartialView();
        }
    }
}
