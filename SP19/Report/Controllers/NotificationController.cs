﻿using Business;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Report.Models;

namespace Report.Controllers
{
    // quản lý, thêm thông báo
    public class NotificationController : BaseController
    {
        //
        // GET: /Notification/

        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string Message = String.Empty;
                List<Notification> list = new List<Notification>();
                Notification notification = new Notification();

                //get outlet
                Outlet outlet = new Outlet();
                SelectList listOutlet = new SelectList(outlet.GetAllForComboNotifi(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;

                //get area
                Business.Region region = new Region();
                SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listRegion = listRegion;

                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT errCode = notification.SearchAll(ref Message, User.ProjectID, User.OutletTypeID, ref list);
                ViewBag.RoleUser = User.UserRoles.FirstOrDefault();

                return View(list);
            }
        }

        [HttpGet]
        public ActionResult AddNew(string _regionAttendance, string Title, string Description, string _listoutlet)
        {
                try
                {
                    if (User == null)
                    {
                        return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string MessageError = String.Empty;

                        List<Notification> list = new List<Notification>();
                        Notification notification = new Notification();
                        notification.NotificationCode = DateTime.Now.ToString("ddMMyyyyHHmmss");
                        notification.NotificationContent = Description;
                        notification.NotificationTitle = Title;
                        notification.CreatedDateTime = DateTime.Now;
                        Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW errCode = notification.AddNew(ref MessageError,_regionAttendance, _listoutlet, User.ProjectID, User.OutletTypeID, User.UserID);
                        Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT errCodeselect = notification.SearchAll(ref MessageError, User.ProjectID, User.OutletTypeID, ref list);
                        return PartialView("_List", list);
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                    return null;
                }
        }

        public ActionResult Result()
        {
            ViewBag.Message = TempData["message"];
            return PartialView();
        }
        [HttpGet]
        public ActionResult Update(string _listIDNotification)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string MessageError = String.Empty;

                List<Notification> list = new List<Notification>();
                Notification notification = new Notification();
                //update xoa thong bao
                Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE errCode1 = notification.Update(ref MessageError, _listIDNotification, User.UserID);
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT errCode = notification.SearchAll(ref MessageError, User.ProjectID, User.OutletTypeID, ref list);
                return PartialView("_List", list);
            }
        }

        public ActionResult Detail(string _NotificationID)
        {
            Notification notification = new Business.Notification();
            string MessageError = String.Empty;
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = notification.GetSimple(ref MessageError, _NotificationID, ref notification);
            ViewBag.NotificationID = _NotificationID;
            return PartialView("Detail", notification);
        }

        [HttpGet]
        public ActionResult UpdateDetail(string _content, string _idnotification, string _title)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Notification notification = new Business.Notification();
                string MessageError = String.Empty;
                List<Notification> list = new List<Notification>();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE errCode = notification.Update(ref MessageError, _content, _idnotification, _title, User.UserID);
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT errCodeselect = notification.SearchAll(ref MessageError, User.ProjectID, User.OutletTypeID, ref list);
                return PartialView("_List", list);
            }
        }
    }
}
