﻿using Business;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Report.Models;
using System.Text;
using System.Web.UI;

namespace Report.Controllers
{
    public class AssignGiftMegaController : BaseController
    {
        //
        // GET: /Notification/
        // phân quà mega trúng cho siêu thị
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string Message = String.Empty;
                Outlet outlet = new Outlet();
                LuckyAssignSE se = new LuckyAssignSE();
                Lucky_Mega_Gift gift = new Lucky_Mega_Gift();
                Lucky_Mega_Set_Gift_To_Outlet giftoutlet = new Lucky_Mega_Set_Gift_To_Outlet();
                //get outlet
                SelectList listOutlet = new SelectList(outlet.GetAllForComboMega(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;
                //quà
                SelectList listGift = new SelectList(gift.GetAllForComboMega(DateTime.Now), "ID", "Text");
                ViewBag.listGift = listGift;
                //đợt
                SelectList listTurn = new SelectList(gift.GetTurn(DateTime.Now), "ID", "Text");
                ViewBag.listturn = listTurn;
                //danh sách outlet Mega     
                List<LuckyAssignEXT> listgiftoutlet = Lucky_Mega_Set_Gift_To_Outlet.GetAllMega(DateTime.Now,User.ProjectID, User.OutletTypeID);               
                return View(listgiftoutlet);
            }
        }
        public ActionResult _Search()
        {
            LuckyAssignSE se = new LuckyAssignSE();
            return PartialView();
        }     
        public ActionResult AddNew( string _date, string _outletassign, string _gift)
        {
            if (User != null)
            {
                Outlet outlet = new Outlet();
                LuckyAssignSE se = new LuckyAssignSE();
                Lucky_Mega_Gift gift = new Lucky_Mega_Gift();
                string _messageSystemError = String.Empty;
                Lucky_Mega_Set_Gift_To_Outlet giftoutlet = new Lucky_Mega_Set_Gift_To_Outlet();
                List<Lucky_Mega_Set_Gift_To_Outlet> list = new List<Lucky_Mega_Set_Gift_To_Outlet>();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW errCode = Lucky_Mega_Set_Gift_To_Outlet.AddNew(ref _messageSystemError, _date, _outletassign, _gift, User.UserID);
                List<LuckyAssignEXT> listgiftoutlet = Lucky_Mega_Set_Gift_To_Outlet.GetAllMega(Common.ConvertUtils.ConvertStringToShortDate(_date), User.ProjectID, User.OutletTypeID);
                //return PartialView("_List", listgiftoutlet);
                //get outlet
                SelectList listOutlet = new SelectList(outlet.GetAllForComboMega(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;
                //quà
                SelectList listGift = new SelectList(gift.GetAllForComboMega(DateTime.Now), "ID", "Text");
                ViewBag.listGift = listGift;
                //đợt
                SelectList listTurn = new SelectList(gift.GetTurn(DateTime.Now), "ID", "Text");
                ViewBag.listturn = listTurn;
                var json = new { list1 = "Header", list2 = "Footer"};
                return CustomJson(json).AddPartialView("_Search").AddPartialView("_List", listgiftoutlet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Search(string _date, string _outletassign, string _gift)
        {
            if (User != null)
            {

                string _messageSystemError = String.Empty;
                Lucky_Mega_Set_Gift_To_Outlet giftoutlet = new Lucky_Mega_Set_Gift_To_Outlet();
                List<Lucky_Mega_Set_Gift_To_Outlet> list = new List<Lucky_Mega_Set_Gift_To_Outlet>();
                LuckyAssignSE se = new LuckyAssignSE();
                se.DateAssignSE = Common.ConvertUtils.ConvertStringToShortDate(_date);
                se.OutletAssignSE = int.Parse(_outletassign);
                se.GiftSE = int.Parse(_gift);
                List<LuckyAssignEXT> listgiftoutlet = Lucky_Mega_Set_Gift_To_Outlet.Search(se, User.ProjectID, User.OutletTypeID);
                return PartialView("_List", listgiftoutlet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SendOTP(string phone)
        {
            if (User != null)
            {

                string _messageSystemError = String.Empty;
                Lucky_Mega_Set_Gift_To_Outlet giftoutlet = new Lucky_Mega_Set_Gift_To_Outlet();
                int _status = 0;
                var listgiftoutlet = Lucky_Mega_Set_Gift_To_Outlet.SendOTP(ref _messageSystemError, ref _status, phone, User.UserID);
                return Json(new { ok = _messageSystemError, status = _status }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Update(string _listID)
        {
            if (User != null)
            {

                string _messageSystemError = String.Empty;
                Lucky_Mega_Set_Gift_To_Outlet giftoutlet = new Lucky_Mega_Set_Gift_To_Outlet();
                List<Lucky_Mega_Set_Gift_To_Outlet> list = new List<Lucky_Mega_Set_Gift_To_Outlet>();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE errCode = Lucky_Mega_Set_Gift_To_Outlet.Update(ref _messageSystemError, _listID, User.UserID);
                List<LuckyAssignEXT> listgiftoutlet = Lucky_Mega_Set_Gift_To_Outlet.GetAllMega(DateTime.Now, User.ProjectID, User.OutletTypeID);
                return PartialView("_List", listgiftoutlet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SendNotification(string ID)
        {
            if (User != null)
            {

                string _messageSystemError = String.Empty;
                Lucky_Mega_Set_Gift_To_Outlet giftoutlet = new Lucky_Mega_Set_Gift_To_Outlet();
                List<Lucky_Mega_Set_Gift_To_Outlet> list = new List<Lucky_Mega_Set_Gift_To_Outlet>();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE errCode = Lucky_Mega_Set_Gift_To_Outlet.SendMessage(ref _messageSystemError, ID, User.UserID);
                List<LuckyAssignEXT> listgiftoutlet = Lucky_Mega_Set_Gift_To_Outlet.GetAllMega(DateTime.Now, User.ProjectID, User.OutletTypeID);
                return PartialView("_List", listgiftoutlet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult GetGiftFordate(string _date, string _outletID)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string MessageError = String.Empty;
                Lucky_Mega_Gift gift = new Lucky_Mega_Gift();
                var listgift = gift.GetAllMega(Common.ConvertUtils.ConvertStringToShortDate(_date), _outletID);
                return Json(listgift, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetTurnFordate(string _date)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string MessageError = String.Empty;
                Lucky_Mega_Gift gift = new Lucky_Mega_Gift();
                var listTurn = gift.GetTurn(Common.ConvertUtils.ConvertStringToShortDate(_date));
                var listgift = gift.GetGift(Common.ConvertUtils.ConvertStringToShortDate(_date));
                return Json(new
                {
                    Turn = listTurn,
                    Gift = listgift
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetOutletNotGift(string _GiftID)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string MessageError = String.Empty;
                Lucky_Mega_Gift gift = new Lucky_Mega_Gift();
                var listOutlet = gift.GetOutletNotGift(_GiftID, User.ProjectID, User.OutletTypeID);
                return Json(listOutlet, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Login(string _phone, string _password)
        {
            Lucky_Mega_Set_Gift_To_Outlet giftoutlet = new Lucky_Mega_Set_Gift_To_Outlet();
            string MessageError = String.Empty;
            int status = 0;
            Outlet outlet = new Outlet();
            LuckyAssignSE se = new LuckyAssignSE();
            Lucky_Mega_Gift gift = new Lucky_Mega_Gift();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = giftoutlet.GetOPT(ref MessageError, _phone, _password, ref status);
            List<LuckyAssignEXT> listgiftoutlet = Lucky_Mega_Set_Gift_To_Outlet.GetAllMega(DateTime.Now, User.ProjectID, User.OutletTypeID);
            //outlet
            SelectList listOutlet = new SelectList(outlet.GetAllForComboMega(User.ProjectID, User.OutletTypeID), "ID", "Text");
            ViewBag.listOutlet = listOutlet;
            //quà
            SelectList listGift = new SelectList(gift.GetAllForComboMega(DateTime.Now), "ID", "Text");
            ViewBag.listGift = listGift;
            //đợt
            SelectList listTurn = new SelectList(gift.GetTurn(DateTime.Now), "ID", "Text");
            ViewBag.listturn = listTurn;
            if (status == 2)
            {
                var json = new { list1 = "Header", list2 = "Footer", list3 = "2" };
                return CustomJson(json).AddPartialView("_LoginPage").AddPartialView("_Result");
            }
            else
            {
                ViewBag.Theme = "~/Views/Shared/_ProfileLayout.cshtml";
                var json = new { list1 = "Header", list2 = "Footer", list3 = "1" };
                return CustomJson(json).AddPartialView("_Search").AddPartialView("_List", listgiftoutlet);
            }

        }

        public ActionResult _InfoCustomer(string _OutletID, string _GiftID)
        {
            if (User != null)
            {
                string _messageSystemError = String.Empty;
                List<View_Customer_Gift_Mega> list = Lucky_Mega_Set_Gift_To_Outlet.GetCustomerSimple(_OutletID, _GiftID);
                return PartialView(list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }


        protected internal virtual CustomJsonResult CustomJson(object json = null, bool allowGet = true)
        {
            return new CustomJsonResult(json)
            {
                JsonRequestBehavior = allowGet ? JsonRequestBehavior.AllowGet : JsonRequestBehavior.DenyGet
            };
        }
        public class CustomJsonResult : JsonResult
        {
            public CustomJsonResult(object json)
            {
                _json = json;
                _partials = new List<KeyValuePair<string, object>>();
                _results = new List<string>();
            }

            private readonly object _json;
            private readonly IList<KeyValuePair<string, object>> _partials;
            private readonly IList<string> _results;

            public CustomJsonResult AddPartialView(string partialViewName = null, object model = null)
            {
                _partials.Add(new KeyValuePair<string, object>(partialViewName, model));
                return this;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                foreach (var partial in _partials)
                {
                    var html = ControllerHelper.RenderPartialViewToString(context, partial.Key, partial.Value);
                    _results.Add(html);
                }
                base.Data = Data;
                base.ExecuteResult(context);
            }

            public new object Data
            {
                get
                {
                    return new
                    {
                        Html = _results,
                        Json = _json
                    };
                }
            }
        }
        public static class ControllerHelper
        {

            public static string RenderPartialViewToString(ControllerContext context, string viewName, object model)
            {
                var controller = context.Controller;

                var partialView = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);

                var stringBuilder = new StringBuilder();
                using (var stringWriter = new StringWriter(stringBuilder))
                {
                    using (var htmlWriter = new HtmlTextWriter(stringWriter))
                    {
                        controller.ViewData.Model = model;
                        partialView.View.Render(
                            new ViewContext(
                                controller.ControllerContext,
                                partialView.View,
                                controller.ViewData,
                                new TempDataDictionary(),
                                htmlWriter),
                            htmlWriter);
                    }
                }
                return stringBuilder.ToString();
            }


        }
    }
}
