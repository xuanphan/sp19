﻿using Business;
using Report.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPOI.HSSF.Util;
using Common;

namespace Report.Controllers
{
    public class ReportSaleVolumeController : BaseController
    {
        //
        // GET: /StockManager/
        // Báo cáo số lượng hàng bán : OffTakeVolume
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                ReportHome reportHome = new ReportHome();
                List<SaleVolumeEXT> list = new List<SaleVolumeEXT>();
                List<SaleVolumeEXT> simple = new List<SaleVolumeEXT>();
                StockSE se = new StockSE();
                //danh sach product
                ViewBag.product = Business.Product.GetProductReport(User.ProjectID);
                ViewBag.havestatus = false;
                if (User.UserRoles.FirstOrDefault().ToString() == "SaleSup")
                {
                    ViewBag.teams = Business.Team.GetAllForSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID);
                    ViewBag.havestatus = false;
                    //get outlet
                    Outlet outlet = new Outlet();
                    SelectList listOutlet = new SelectList(outlet.GetAllForComboSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID), "ID", "Text");
                    ViewBag.listOutlet = listOutlet;
                    Business.User user = new User();
                    SelectList listsale = new SelectList(user.GetUserSale(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listsale = listsale;
                    //get area 
                    Business.Region region = new Region();
                    SelectList listRegion = new SelectList(region.GetAllForComboSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID), "ID", "Text");
                    ViewBag.listRegion = listRegion;
                }
                else if (User.UserRoles.FirstOrDefault().ToString() == "sale")
                {
                    ViewBag.teams = Business.Team.GetAllForSale(User.ProjectID, User.SaleID, User.OutletTypeID);
                    ViewBag.havestatus = false;
                    //get outlet
                    Outlet outlet = new Outlet();
                    SelectList listOutlet = new SelectList(outlet.GetAllForComboSale(User.ProjectID, User.SaleID), "ID", "Text");
                    ViewBag.listOutlet = listOutlet;
                    Business.User user = new User();
                    SelectList listsale = new SelectList(user.GetUserSale(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listsale = listsale;
                    //get area 
                    Business.Region region = new Region();
                    SelectList listRegion = new SelectList(region.GetAllForComboSale(User.ProjectID, User.SaleID), "ID", "Text");
                    ViewBag.listRegion = listRegion;
                }
                else
                {
                    //get outlet
                    Outlet outlet = new Outlet();
                    SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listOutlet = listOutlet;
                    //get sale 
                    Business.User user = new User();
                    SelectList listsale = new SelectList(user.GetUserSale(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listsale = listsale;
                    //get area 
                    Business.Region region = new Region();
                    SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listRegion = listRegion;
                    ViewBag.teams = Business.Team.GetAll(User.ProjectID, User.OutletTypeID);
                }
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = reportHome.SearchSaleVolume(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list, ref simple);
                ViewBag.SaleVolumeEXT = simple;
                return View(list);
            }
        }

        //------------------------Get Outlet for district
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        } //------------------------Get Outlet for district
        public JsonResult GetSaleForRegion(string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                User user = new Business.User();
                var List = user.GetSaleForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        //------------------------Get Outlet for district
        public JsonResult GetOutletForSale(string _SaleID, string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForSale(int.Parse(_SaleID), _RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult Search(int? page, string _RegionStockSE, string _OutletStockSE, string _DateStockSE, string _SaleID)
        {

            string _messageError = String.Empty;
            ReportHome reportHome = new ReportHome();
            List<SaleVolumeEXT> list = new List<SaleVolumeEXT>();
            List<SaleVolumeEXT> simple = new List<SaleVolumeEXT>();
            StockSE se = new StockSE();
            ViewBag.product = Business.Product.GetProductReport(User.ProjectID);
            if (User.UserRoles.FirstOrDefault().ToString() == "SaleSup")
            {
                ViewBag.teams = Business.Team.SearchOutletForSaleSup(User.ProjectID, User.SaleSupID, int.Parse(_OutletStockSE), _RegionStockSE, User.ListSaleID);
            }
            else if (User.UserRoles.FirstOrDefault().ToString() == "sale")
            {
                ViewBag.teams = Business.Team.SearchOutletForSale(User.ProjectID, User.SaleID, int.Parse(_OutletStockSE), _RegionStockSE);
            }
            else
            {
                ViewBag.teams = Business.Team.SearchOutlet(User.ProjectID, User.OutletTypeID, int.Parse(_OutletStockSE), _RegionStockSE, _SaleID);
            }
            se.DateStockSE = Common.ConvertUtils.ConvertStringToShortDate(_DateStockSE);
            se.OutletStockSE = int.Parse(_OutletStockSE);
            se.RegionStockSE = _RegionStockSE;
            se.SaleSE = long.Parse(_SaleID);
            Session["DateStockSE"] = _DateStockSE;
            Session["OutletStockSE"] = se.OutletStockSE;
            Session["RegionStockSE"] = se.RegionStockSE;
            Session["SaleSE"] = se.SaleSE;
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = reportHome.SearchSaleVolume(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list, ref simple);
            ViewBag.UserRole = User.UserRoles.FirstOrDefault();
            ViewBag.SaleVolumeEXT = simple;
            return PartialView("_List", list);
        }

        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                ReportHome reportHome = new ReportHome();
                List<SaleVolumeEXT> list = new List<SaleVolumeEXT>();
                StockSE se = new StockSE();
                List<SaleVolumeEXT> listsimple = new List<SaleVolumeEXT>();

                if (Session["DateStockSE"] == null || Session["OutletStockSE"] == null || Session["RegionStockSE"] == null || Session["SaleSE"] == null)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = reportHome.SearchSaleVolume(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list, ref listsimple);
                }
                else
                {
                    se.OutletStockSE = int.Parse(Session["OutletStockSE"].ToString());
                    se.RegionStockSE = Session["RegionStockSE"].ToString();
                    se.SaleSE = long.Parse(Session["SaleSE"].ToString());
                    se.DateStockSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateStockSE"].ToString());
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = reportHome.SearchSaleVolume(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list, ref listsimple);
                }
                if (Session["RegionStockSE"] == null)
                { Session["RegionStockSE"] = "0"; }
                if (Session["OutletStockSE"] == null)
                { Session["OutletStockSE"] = "0"; }
                if (Session["SaleSE"] == null)
                { Session["SaleSE"] = "0"; }
                List<Team_Outlet> outletlist = new List<Team_Outlet>();
                if (User.UserRoles.FirstOrDefault().ToString() == "SaleSup")
                {
                   outletlist = Business.Team.SearchOutletForSaleSup(User.ProjectID, User.SaleSupID, int.Parse(Session["OutletStockSE"].ToString()),  Session["RegionStockSE"].ToString(), User.ListSaleID);
                }
                else if (User.UserRoles.FirstOrDefault().ToString() == "sale")
                {
                    outletlist = Business.Team.SearchOutletForSale(User.ProjectID, User.SaleID, int.Parse(Session["OutletStockSE"].ToString()),  Session["RegionStockSE"].ToString());
                }
                else
                {
                    outletlist =  Business.Team.SearchOutlet(User.ProjectID, User.OutletTypeID, int.Parse(Session["OutletStockSE"].ToString()), Session["RegionStockSE"].ToString(), Session["SaleSE"].ToString());
                }
                outletlist = outletlist.Where(p => (p.Outlet.Region.Type == se.RegionStockSE || se.RegionStockSE == "Tất Cả" || se.RegionStockSE == null) && (p.OutletID == se.OutletStockSE || se.OutletStockSE == null || se.OutletStockSE == 0)).ToList();
                List<Product> listproduct = Business.Product.GetProductReport(User.ProjectID);
                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\BaoCaoSanLuong.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");
                HSSFCellStyle hStyle22 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle22.FillBackgroundColor = HSSFColor.Grey40Percent.Index;
                hStyle22.FillPattern = FillPattern.SolidForeground;
                hStyle22.FillForegroundColor = HSSFColor.Grey40Percent.Index;
                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report
                int x = 1;
                for (int i = 0; i < outletlist.Count(); i++)
                {
                    var simple = list.Where(temp => temp.TeamOutletID == outletlist[i].ID).ToList();
                    if (simple.Count() > 0)
                    {
                        IRow row7 = sheet_Report.CreateRow(i + 4);
                        row7.CreateCell(0).SetCellValue(x);
                        row7.CreateCell(1).SetCellValue(outletlist[i].Outlet.Region.Type);
                        row7.CreateCell(2).SetCellValue(outletlist[i].Team.TeamName);
                        row7.CreateCell(3).SetCellValue(outletlist[i].Outlet.City);
                        int y = 4;
                        for (int item = 0; item < listproduct.Count(); item++)
                        {
                            var _detail = simple.Where(p => p.ProductCode == listproduct[item].ProductCode);
                            if (_detail.Count() > 0)
                            {
                                var temp = _detail.FirstOrDefault();
                                row7.CreateCell(y).SetCellValue(temp.Number);
                            }
                            else
                            {
                                if (listproduct[item].IsNaN != null)
                                {
                                    if (listproduct[item].IsNaN.Split('|').ToList().Where(p => p == outletlist[i].Outlet.GroupID.ToString()).Count() > 0)
                                    {
                                        row7.CreateCell(y).SetCellValue("");
                                        row7.GetCell(y).CellStyle = hStyle22;
                                    }
                                    else
                                    {
                                        row7.CreateCell(y).SetCellValue("");
                                    }
                                }
                                else
                                {
                                    row7.CreateCell(y).SetCellValue("");
                                }
                            }
                            y++;
                        }
                        x++;
                    }
                    else
                    {
                        IRow row7 = sheet_Report.CreateRow(i + 4);
                        row7.CreateCell(0).SetCellValue(x);
                        row7.CreateCell(1).SetCellValue(outletlist[i].Outlet.Region.Type);
                        row7.CreateCell(2).SetCellValue(outletlist[i].Team.TeamName);
                        row7.CreateCell(3).SetCellValue(outletlist[i].Outlet.City);
                        int y = 4;
                        for (int item = 0; item < listproduct.Count(); item++)
                        {
                            if (listproduct[item].IsNaN != null)
                            {
                                if (listproduct[item].IsNaN.Split('|').ToList().Where(p => p == outletlist[i].Outlet.GroupID.ToString()).Count() > 0)
                                {
                                    row7.CreateCell(y).SetCellValue("");
                                    row7.GetCell(y).CellStyle = hStyle22;
                                }
                                else
                                {
                                    row7.CreateCell(y).SetCellValue("");
                                }
                            }
                            else
                            {
                                row7.CreateCell(y).SetCellValue("");
                            }
                            y++;
                        }
                        x++;
                    }
                }
                IRow row8 = sheet_Report.CreateRow(outletlist.Count() + 4);
                row8.CreateCell(0).SetCellValue(x);
                row8.CreateCell(1).SetCellValue("");
                row8.CreateCell(2).SetCellValue("");
                row8.CreateCell(3).SetCellValue("Tổng Cộng");
                int j = 4;
                for (int item = 0; item < listproduct.Count(); item++)
                {
                    var _detailgift = listsimple.Where(p => p.ProductCode == listproduct[item].ProductCode);
                    if (_detailgift.Count() > 0)
                    {
                        row8.CreateCell(j).SetCellValue(_detailgift.FirstOrDefault().Number);
                    }
                    else
                    {
                        row8.CreateCell(j).SetCellValue("");
                    }
                    j++;

                }
                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "BaoCaoSanLuong" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }
    }
}


