﻿using Business;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Report.Models;

namespace Report.Controllers
{
    // quản lý , đổi set quà cho siêu thị
    public class ManagementSetController : BaseController
    {
        //
        // GET: /Notification/

        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                List<OutletCurrentSetEXT> list = new List<OutletCurrentSetEXT>();
                Outlet_Current_Set outlet_Current_Set = new Outlet_Current_Set();
                OutletCurrentSetSE se = new OutletCurrentSetSE();
                //get outlet
                Outlet outlet = new Outlet();
                SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;
                //get area
                Business.Region region = new Region();
                SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listRegion = listRegion;

                //lấy brand
                ViewBag.brand = Business.Brand.GetAll(User.ProjectID);
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT errCode = outlet_Current_Set.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                return View(list);
            }
        }
        [HttpGet]
        public ActionResult Search(int? page, string _RegionOutletCurrentSet, string _OutletOutletCurrentSet)
        {
            if (User != null)
            {
                string _messageError = String.Empty;
                List<OutletCurrentSetEXT> list = new List<OutletCurrentSetEXT>();
                Outlet_Current_Set outlet_Current_Set = new Outlet_Current_Set();
                OutletCurrentSetSE se = new OutletCurrentSetSE();
                ViewBag.brand = Business.Brand.GetAll(User.ProjectID);
                se.OutletCurrentSet = int.Parse(_OutletOutletCurrentSet);
                se.RegionCurrentSet = _RegionOutletCurrentSet;
                Session["OutletCurrentSet"] = se.OutletCurrentSet;
                Session["RegionCurrentSet"] = se.RegionCurrentSet;
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = outlet_Current_Set.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult _Update(string _OutletID)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string _messageError = String.Empty;
                ChangeSetModel model = new ChangeSetModel();
                Outlet_Current_Set outlet_Current_Set = new Outlet_Current_Set();
                List<Outlet_Current_Set> list_outlet_Current_Set = new List<Outlet_Current_Set>();
                //Lấy thông tin Outlet
                var simple = Outlet.GetSimple(int.Parse(_OutletID));
                //Lấy Set hiện tại
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = outlet_Current_Set.GetCurrent(ref _messageError, User.ProjectID, int.Parse(_OutletID), ref list_outlet_Current_Set);
                Outlet_Requirement_Change_Set outlet_Requirement_Change_Set = new Business.Outlet_Requirement_Change_Set();
                List<Outlet_Requirement_Change_Set> list_outlet_Requirement_Change_Set = new List<Outlet_Requirement_Change_Set>();
                //Lấy Set yêu cầu mới
                //get brandset
                Business.Brand_Set brand_Set = new Brand_Set();
                SelectList listbrandset = new SelectList(brand_Set.GetAllForCombo(_OutletID,User.ProjectID), "ID", "Text");
                ViewBag.listbrandset = listbrandset;
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error1 = outlet_Requirement_Change_Set.GetRequest(ref _messageError, User.ProjectID, int.Parse(_OutletID), ref list_outlet_Requirement_Change_Set);
                var brand = brand_Set.GetAll(User.ProjectID, User.OutletTypeID);
                model.outlet_Current_Set = list_outlet_Current_Set;
                model.outlet_Requirement_Change_Set = list_outlet_Requirement_Change_Set;
                model.OutletName = simple.OutletName;
                model.brand_Set = brand;
                model.OutletID = simple.ID;
                ViewBag.brand = Business.Brand.GetAll(User.ProjectID);
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                return View(model);
            }
        }
        //------------------------Get Outlet for district
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult Update(string _brandsetID, string _pOutletID)
        {
            try
            {
                if (User != null)
                {
                    string _messageError = String.Empty;
                    List<OutletCurrentSetEXT> list = new List<OutletCurrentSetEXT>();
                    Outlet_Current_Set outlet_Current_Set = new Outlet_Current_Set();
                    OutletCurrentSetSE se = new OutletCurrentSetSE();
                    ViewBag.brand = Business.Brand.GetAll(User.ProjectID);
                    //update
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE errCode = outlet_Current_Set.Update(ref _messageError, User.ProjectID, int.Parse(_pOutletID), _brandsetID, User.UserID);
                    //get
                    if (Session["OutletCurrentSet"] == null || Session["RegionCurrentSet"] == null)
                    {
                        Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT errCode1 = outlet_Current_Set.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                    }
                    else
                    {
                        se.OutletCurrentSet = long.Parse(Session["OutletCurrentSet"].ToString());
                        se.RegionCurrentSet = Session["RegionCurrentSet"].ToString();

                        Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT errCode1 = outlet_Current_Set.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                    }
                    ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                    return PartialView("_List", list);
                }
                else
                {
                    return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                return null;
            }
        }

        public ActionResult Result()
        {
            ViewBag.Message = TempData["message"];
            return PartialView();
        }
    }
}
