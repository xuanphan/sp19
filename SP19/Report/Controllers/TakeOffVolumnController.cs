﻿using Business;
using Report.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Report.Controllers
{
    public class TakeOffVolumnController : BaseController
    {
        //
        // GET: /ReportTakeOffVolume/
        // xem báo cáo hàng bán theo biểu đồ
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (User.UserRoles.FirstOrDefault() == "Master")
                {
                    User.ProjectID = 0;
                }
                string _messageError = String.Empty;
                Take_Off_Volumn takeoffvolumn = new Take_Off_Volumn();
                List<Take_Off_VolumnEXT> list = new List<Take_Off_VolumnEXT>();
                Take_Off_VolumnSE se = new Take_Off_VolumnSE();

                //get outlet
                Outlet outlet = new Outlet();
                SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;
                ViewBag.brand = Business.Brand.GetAllTakeOff(User.ProjectID);
                List<DateTime> listdate = new List<DateTime>();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = takeoffvolumn.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error1 = takeoffvolumn.SearchDate(ref _messageError, User.ProjectID, User.OutletTypeID, ref listdate, se);
                ViewBag.listdate = listdate;

                return View(list);
            }
        }

        public ActionResult Search(int? page, string _OutletTakeOffSE, string _dateformTakeOffSE, string _datetoTakeOffSE)
        {
            if (User != null)
            {
                if (User.UserRoles.FirstOrDefault() == "Master")
                {
                    User.ProjectID = 0;
                }
                string _messageError = String.Empty;
                Take_Off_Volumn takeoffvolumn = new Take_Off_Volumn();
                List<Take_Off_VolumnEXT> list = new List<Take_Off_VolumnEXT>();
                Take_Off_VolumnSE se = new Take_Off_VolumnSE();

                se.DateFromTakeOffSE = Common.ConvertUtils.ConvertStringToShortDate(_dateformTakeOffSE);
                se.DateToTakeOffSE = Common.ConvertUtils.ConvertStringToShortDate(_datetoTakeOffSE);
                se.OutletTakeOffSE = int.Parse(_OutletTakeOffSE);
                Session["OutletTakeOffSE"] = se.OutletTakeOffSE;
                Session["DateFromTakeOffSE"] = _dateformTakeOffSE;
                Session["DateToTakeOffSE"] = _datetoTakeOffSE;
                ViewBag.brand = Business.Brand.GetAllTakeOff(User.ProjectID);
                List<DateTime> listdate = new List<DateTime>();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = takeoffvolumn.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error1 = takeoffvolumn.SearchDate(ref _messageError, User.ProjectID, User.OutletTypeID, ref listdate, se);
                ViewBag.listdate = listdate;
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (User.UserRoles.FirstOrDefault() == "Master")
                {
                    User.ProjectID = 0;
                }
                string _messageError = String.Empty;
                Take_Off_Volumn takeoffvolumn = new Take_Off_Volumn();
                List<Take_Off_VolumnEXT> list = new List<Take_Off_VolumnEXT>();
                Take_Off_VolumnSE se = new Take_Off_VolumnSE();

                if (Session["OutletTakeOffSE"] == null || Session["DateFromTakeOffSE"] == null || Session["DateToTakeOffSE"] == null)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = takeoffvolumn.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                }
                else
                {
                    se.OutletTakeOffSE = long.Parse(Session["OutletTakeOffSE"].ToString());
                    se.DateFromTakeOffSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateFromTakeOffSE"].ToString());
                    se.DateToTakeOffSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateToTakeOffSE"].ToString());
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = takeoffvolumn.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                }

                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\Report_Take_Off_Volume.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report
                int x = 1;
                for (int i = 0; i < list.Count(); i++)
                {
                    IRow row7 = sheet_Report.CreateRow(i + 3);
                    row7.CreateCell(0).SetCellValue(x);
                    row7.CreateCell(1).SetCellValue(list[i].Dates.Date);
                    row7.GetCell(1).CellStyle = cellStyle;
                    row7.CreateCell(2).SetCellValue(list[i].TotalNumber);
                    x++;
                }
                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "Report_Off_Take_Volume" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }

    }
}

