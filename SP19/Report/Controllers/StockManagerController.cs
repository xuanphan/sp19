﻿using Business;
using Report.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPOI.HSSF.Util;
using Common;

namespace Report.Controllers
{
    public class StockManagerController : BaseController
    {
        //
        // GET: /StockManager/
        // báo cáo tồn kho  : admin xem
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                Stock stock = new Stock();
                List<StockEXT> list = new List<StockEXT>();
                StockSE se = new StockSE();
                //danh sach product
                ViewBag.product = Business.Product.GetProduct(User.ProjectID);
                ViewBag.productdelta = Business.Product.GetProductdelta(User.ProjectID);
                ViewBag.outlets = Business.Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                ViewBag.havestatus = false;
                //get outlet
                Outlet outlet = new Outlet();
                SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;

                //get area 
                Business.Region region = new Region();
                SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listRegion = listRegion;

                //get district
                SelectList listDistrict = new SelectList(region.GetDistrictForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listDistrict = listDistrict;

                //get sale 
                Business.User user = new User();
                SelectList listsale = new SelectList(user.GetUserSale(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listsale = listsale;

                //get statusstock
                CodeDetail codeDetail = new CodeDetail();
                SelectList listStatusStock = new SelectList(codeDetail.GetForCombo("StatusStock"), "ID", "Text");
                ViewBag.listStatusStock = listStatusStock;
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.Search(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                return View(list);
            }
        }
        // lấy tỉnh theo area
        public JsonResult GetDistrictForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Team team = new Business.Team();
                var List = team.GetDistrictForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        //------------------------Get Outlet for area
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        } 
        
        //------------------------Get Outlet for area
        public JsonResult GetSaleForRegion(string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                User user = new Business.User();
                var List = user.GetSaleForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        //------------------------Get Outlet for district
        public JsonResult GetOutletForSale(string _SaleID, string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForSale(int.Parse(_SaleID), _RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetOutletForDistrict(string _DistrictID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForDistrict(int.Parse(_DistrictID), User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Search(int? page,string _DistrictAttendance, string _RegionStockSE, string _OutletStockSE, string _StatusStockSE, string _DateStockSE, string _SaleSE)
        {
            if (User != null)
            {

                string _messageError = String.Empty;
                Stock stock = new Stock();
                List<StockEXT> list = new List<StockEXT>();
                StockSE se = new StockSE();
                ViewBag.product = Business.Product.GetProduct(User.ProjectID);
                ViewBag.outlets = Business.Outlet.SearchOutlet(int.Parse(_DistrictAttendance), User.ProjectID, User.OutletTypeID, int.Parse(_OutletStockSE), _RegionStockSE, _SaleSE);
                if (_StatusStockSE == "0")
                {
                    ViewBag.havestatus = false;
                }
                else
                {
                    ViewBag.havestatus = true;
                }

                se.DateStockSE = Common.ConvertUtils.ConvertStringToShortDate(_DateStockSE);
                se.OutletStockSE = int.Parse(_OutletStockSE);
                se.RegionStockSE = _RegionStockSE;
                se.StatusStockSE = long.Parse(_StatusStockSE);
                se.SaleSE = long.Parse(_SaleSE);
                se.DistrictAttendance = int.Parse(_DistrictAttendance);

                Session["DateStockSE"] = _DateStockSE;
                Session["OutletStockSE"] = se.OutletStockSE;
                Session["RegionStockSE"] = se.RegionStockSE;
                Session["StatusStockSE"] = se.StatusStockSE;
                Session["SaleSE"] = se.SaleSE;
                Session["DistrictStockSE"] = se.DistrictAttendance;
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.Search(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                Stock stock = new Stock();
                List<StockEXT> list = new List<StockEXT>();
                StockSE se = new StockSE();
                bool havestatus = false;
                if (Session["DistrictStockSE"] == null || Session["OutletStockSE"] == null || Session["RegionStockSE"] == null || Session["StatusStockSE"] == null || Session["DateStockSE"] == null || Session["SaleSE"] == null)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.Search(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                }
                else
                {
                    se.OutletStockSE = int.Parse(Session["OutletStockSE"].ToString());
                    se.RegionStockSE = Session["RegionStockSE"].ToString();
                    se.StatusStockSE = long.Parse(Session["StatusStockSE"].ToString());
                    se.SaleSE = long.Parse(Session["SaleSE"].ToString());
                    se.DateStockSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateStockSE"].ToString());
                    se.DistrictAttendance = int.Parse(Session["DistrictStockSE"].ToString());
                    if (Session["StatusStockSE"].ToString() != "0")
                    {
                        havestatus = true;
                    }
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.Search(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                }
                if (Session["RegionStockSE"] == null)
                { Session["RegionStockSE"] = "0"; }
                if (Session["OutletStockSE"] == null)
                { Session["OutletStockSE"] = "0"; }
                if (Session["SaleSE"] == null)
                { Session["SaleSE"] = "0"; }
                if (Session["DistrictStockSE"] == null)
                { Session["DistrictStockSE"] = "0"; }
                var product = Business.Product.GetProduct(User.ProjectID);
                var oulteList = Business.Outlet.SearchOutletExcel(int.Parse(Session["DistrictStockSE"].ToString()), User.ProjectID, User.OutletTypeID, int.Parse(Session["OutletStockSE"].ToString()), Session["RegionStockSE"].ToString(), Session["SaleSE"].ToString());
                if (havestatus == true)
                {
                    if (list.Count() == 0)
                    {
                        oulteList = new List<Business.Sale_Outlet>();
                    }
                    else
                    {
                        oulteList = oulteList.Where(p => list.Any(l => p.OutletID == int.Parse(l.OutletID))).ToList().OrderBy(p => p.Outlet.Chanel.ChanelName).ToList();
                    }
                }
                var kams = Business.View_Kam_Outlet.getList();
                var salesups = Business.View_SaleSup_Outlet.getList();

                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\Report_Stock.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.None;

                HSSFCellStyle hStyle = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle.SetFont(font1);
                hStyle.FillBackgroundColor = HSSFColor.Green.Index;
                hStyle.FillPattern = FillPattern.SolidForeground;
                hStyle.FillForegroundColor = HSSFColor.Green.Index;
                HSSFCellStyle hStyle1 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle1.FillBackgroundColor = HSSFColor.Yellow.Index;
                hStyle1.FillPattern = FillPattern.SolidForeground;
                hStyle1.FillForegroundColor = HSSFColor.Yellow.Index;
                HSSFCellStyle hStyle2 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle2.FillBackgroundColor = HSSFColor.Red.Index;
                hStyle2.FillPattern = FillPattern.SolidForeground;
                hStyle2.FillForegroundColor = HSSFColor.Red.Index;
                HSSFCellStyle hStyle22 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle22.FillBackgroundColor = HSSFColor.Grey40Percent.Index;
                hStyle22.FillPattern = FillPattern.SolidForeground;
                hStyle22.FillForegroundColor = HSSFColor.Grey40Percent.Index;
                HSSFCellStyle hStyle3 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report

                int x = 1;

                for (int item = 0; item < oulteList.Count(); item++)
                {

                    var simple = list.Where(temp => int.Parse(temp.OutletID) == oulteList[item].Outlet.ID).ToList();
                    var simpleKAM = kams.Where(p => p.OutletID == oulteList[item].Outlet.ID).FirstOrDefault();
                    var simpleSaleSup = salesups.Where(p => p.OutletID == oulteList[item].Outlet.ID).FirstOrDefault();
                    if (simple.Count() > 0)
                    {
                        IRow row7 = sheet_Report.CreateRow(item + 2);
                        row7.CreateCell(0).SetCellValue(x);
                        row7.CreateCell(1).SetCellValue(oulteList[item].Outlet.Region.Type);
                        row7.CreateCell(2).SetCellValue(oulteList[item].Outlet.City);
                        row7.CreateCell(3).SetCellValue(oulteList[item].Outlet.Chanel.ChanelName);
                        row7.CreateCell(4).SetCellValue(oulteList[item].Outlet.OutletName);
                        row7.CreateCell(5).SetCellValue(oulteList[item].User_Web.FullName);
                        row7.CreateCell(6).SetCellValue(simpleSaleSup != null ? simpleSaleSup.NameSaleSup : "");
                        row7.CreateCell(7).SetCellValue(simpleKAM != null ? simpleKAM.FullName : "");
                        int z = 8;
                        for (int i = 0; i < product.Count(); i++)
                        {
                            var _detailproduct = simple.Where(p => p.ProductID == product[i].ID);
                            if (_detailproduct.Count() > 0)
                            {
                                foreach (var y in _detailproduct)
                                {
                                    if (y.ProductID == product[i].ID)
                                    {
                                        if (y.StatusName == Business.Stock.StatusStock.OOS.ToString())
                                        {
                                            if (y.NumberSale == null)
                                            {
                                                row7.CreateCell(z).SetCellValue(y.NumberSP);
                                                row7.GetCell(z).CellStyle = hStyle3;
                                            }
                                            else
                                            {
                                                row7.CreateCell(z).SetCellValue(y.NumberSale.GetValueOrDefault());
                                                row7.GetCell(z).CellStyle = hStyle2;
                                            }
                                        }
                                        else if (y.StatusName == Business.Stock.StatusStock.ALERT.ToString())
                                        {
                                            if (y.NumberSale == null)
                                            {
                                                row7.CreateCell(z).SetCellValue(y.NumberSP);
                                                row7.GetCell(z).CellStyle = hStyle3;
                                            }
                                            else
                                            {
                                                row7.CreateCell(z).SetCellValue(y.NumberSale.GetValueOrDefault());
                                                row7.GetCell(z).CellStyle = hStyle1;
                                            }
                                        }
                                        else if (y.StatusName == Business.Stock.StatusStock.SAFE.ToString())
                                        {
                                            if (y.NumberSale == null)
                                            {
                                                row7.CreateCell(z).SetCellValue(y.NumberSP);
                                                row7.GetCell(z).CellStyle = hStyle3;
                                            }
                                            else
                                            {
                                                row7.CreateCell(z).SetCellValue(y.NumberSale.GetValueOrDefault());
                                                row7.GetCell(z).CellStyle = hStyle;
                                            }
                                        }
                                        else
                                        {

                                            if (product[i].GroupID.ToString().Contains(oulteList[item].Outlet.GroupID.ToString()) && oulteList[item].Outlet.GroupID.ToString().Length == 2)
                                            {
                                                row7.CreateCell(z).SetCellValue("");
                                                row7.GetCell(z).CellStyle = hStyle22;

                                            }
                                            else
                                            {
                                                row7.CreateCell(z).SetCellValue("");
                                            }
                                        }

                                    }
                                    else
                                    {
                                        if (product[i].IsNaN != null)
                                        {
                                            if (product[i].IsNaN.Split('|').ToList().Where(p => p == oulteList[item].Outlet.GroupID.ToString()).Count() > 0)
                                            {
                                                row7.CreateCell(z).SetCellValue("");
                                                row7.GetCell(z).CellStyle = hStyle22;
                                            }
                                            else
                                            {
                                                row7.CreateCell(z).SetCellValue("");
                                            }
                                        }
                                        else
                                        {
                                            row7.CreateCell(z).SetCellValue("");
                                        }
                                    }
                                }
                            }
                            else
                            {

                                if (product[i].IsNaN != null)
                                {
                                    if (product[i].IsNaN.Split('|').ToList().Where(p => p == oulteList[item].Outlet.GroupID.ToString()).Count() > 0)
                                    {
                                        row7.CreateCell(z).SetCellValue("");
                                        row7.GetCell(z).CellStyle = hStyle22;
                                    }
                                    else
                                    {
                                        row7.CreateCell(z).SetCellValue("");
                                    }
                                }
                                else
                                {
                                    row7.CreateCell(z).SetCellValue("");
                                }
                            }
                            z++;
                        }

                        row7.CreateCell(25).SetCellValue(simple.FirstOrDefault().Note);

                        //row7.CreateCell(22).SetCellValue(simple.FirstOrDefault().DateStock.Date);
                        //row7.GetCell(22).CellStyle = cellStyle;
                    }
                    else
                    {
                        IRow row7 = sheet_Report.CreateRow(item + 2);
                        row7.CreateCell(0).SetCellValue(x);
                        row7.CreateCell(1).SetCellValue(oulteList[item].Outlet.Region.Type);
                        row7.CreateCell(2).SetCellValue(oulteList[item].Outlet.City);
                        row7.CreateCell(3).SetCellValue(oulteList[item].Outlet.Chanel.ChanelName);
                        row7.CreateCell(4).SetCellValue(oulteList[item].Outlet.OutletName);
                        row7.CreateCell(5).SetCellValue(oulteList[item].User_Web.FullName);
                        row7.CreateCell(6).SetCellValue(simpleSaleSup != null ? simpleSaleSup.NameSaleSup : "");
                        row7.CreateCell(7).SetCellValue(simpleKAM != null ? simpleKAM.FullName : "");
                        int z = 8;
                        foreach (var k in product)
                        {
                            if (k.IsNaN != null)
                            {
                                if (k.IsNaN.Split('|').ToList().Where(p => p == oulteList[item].Outlet.GroupID.ToString()).Count() > 0)
                                {
                                    row7.CreateCell(z).SetCellValue("");
                                    row7.GetCell(z).CellStyle = hStyle22;
                                }
                                else
                                {
                                    row7.CreateCell(z).SetCellValue("");
                                }
                            }
                            else
                            {
                                row7.CreateCell(z).SetCellValue("");
                            }
                            z++;
                        }
                    }

                    x++;
                }
                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "Report_Stock_Daily" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }
    }
}


