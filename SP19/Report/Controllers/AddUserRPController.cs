﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Business;
using Report.Models;

namespace Report.Controllers
{
    // thêm user
    public class AddUserRPController : BaseController
    {
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                AddUserModel model = new AddUserModel();
                model.status = 0;
                model.message = "";
                model.listDataApp = Business.User.getApp(User.ProjectID, User.OutletTypeID);
                model.listDataWeb = Business.User.getWeb(User.ProjectID, User.OutletTypeID);

                return View(model);
            }
        }



        public ActionResult Import()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                AddUserModel model = new AddUserModel();
                try
                {
                    string _messagerError = String.Empty;
                    string _FilePath = String.Empty;
                    string imageUrl = String.Empty;
                    string imagePath = String.Empty;
                    string imageName = String.Empty;
                    string Link = String.Empty;
                    ISheet sheet = null;

                    imageName = Request.Files[0].FileName;
                    string loaiuser = Request.Form[0];
                    Business.Connection.CreateImagePathWeb(Request.PhysicalApplicationPath, Request.Url.Authority, "FILEOUTLET", _FilePath, out imageUrl);
                    imagePath = Request.PhysicalApplicationPath + "/FILEOUTLET/" + _FilePath;

                    Directory.CreateDirectory(imagePath);

                    if (System.IO.File.Exists(imagePath + "/" + imageName))
                    {
                        imageName = "exist" + Common.CodeMD5.MD5Hash(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss ttt")) + imageName;
                    }

                    Request.Files[0].SaveAs(imagePath + "/" + imageName);
                    Link = imageUrl + DateTime.Now.ToString("ddMMyyyy") + "/" + imageName;
                    imageUrl = imagePath + "/" + imageName;

                    List<User_App> listCurrentAPP = new List<User_App>();
                    List<User_Web> listCurrentWEB = new List<User_Web>();

                    if (loaiuser == "APP")
                    {
                        listCurrentAPP = Business.User.getApp(User.ProjectID, User.OutletTypeID);
                    }
                    else
                    {
                        listCurrentWEB = Business.User.getWeb(User.ProjectID, User.OutletTypeID);
                    }

                    List<User_App> listEr1 = listCurrentAPP;
                    List<User_Web> listEr2 = listCurrentWEB;

                    HSSFWorkbook xssfExcel = new HSSFWorkbook();
                    FileInfo fi = new FileInfo(Request.Files[0].FileName);
                    List<User_App> listRPAPP = new List<User_App>();
                    List<User_Web> listRPWEB = new List<User_Web>();
                    using (FileStream files = new FileStream(imageUrl, FileMode.Open, FileAccess.Read))
                    {
                        xssfExcel = new HSSFWorkbook(files);
                    }

                    sheet = xssfExcel.GetSheet("Data");

                    int indexStart = 2;
                    int countRow = 0;
                    if (loaiuser == "APP")
                    {

                        for (int i = indexStart; i <= sheet.LastRowNum; i++)
                        {
                            countRow = i;
                            try
                            {
                                User_App entity = new User_App();
                                entity.Code = Cell2.GetCellSP(sheet.GetRow(i).GetCell(1));
                                if (Cell2.GetCellSP(sheet.GetRow(i).GetCell(2)) != null && Cell2.GetCellSP(sheet.GetRow(i).GetCell(2)) != "")
                                {
                                    entity.TeamOutletID = int.Parse(Cell2.GetCellSP(sheet.GetRow(i).GetCell(2)));
                                }



                                entity.FullName = Cell2.GetCellSP(sheet.GetRow(i).GetCell(3));
                                entity.UserName = Cell2.GetCellSP(sheet.GetRow(i).GetCell(4));
                                entity.PassWord = Cell2.GetCellSP(sheet.GetRow(i).GetCell(5));
                                entity.UserType = Cell2.GetCellSP(sheet.GetRow(i).GetCell(6)).ToUpper();
                                string listOutlet = Cell2.GetCellSP(sheet.GetRow(i).GetCell(7)) != null ? Cell2.GetCellSP(sheet.GetRow(i).GetCell(7)) : "";
                                entity.CreatedBy = User.UserID;
                                entity.CreatedDateTime = DateTime.Now;
                                entity.Status = 1;
                                entity.RowVersion = 1;

                                if (entity.UserType == "SP" || entity.UserType == "KHO" || entity.UserType == "SUP")
                                {
                                    if (listCurrentAPP.Where(o => o.UserName.ToLower() == entity.UserName.ToLower()).Count() > 0 || entity.UserName == "")
                                    {
                                        model.status = 2;
                                        model.message = "Username ( " + entity.UserName + " ) rỗng hoặc đã tồn tại trong hệ thống hoặc file." + " Dòng: " + (countRow + 1);
                                        model.listDataApp = listEr1;
                                        model.listDataWeb = listEr2;
                                        return PartialView("_List", model);
                                    }
                                    else
                                    {
                                        switch (entity.UserType)
                                        {
                                            case "SP":
                                                if (entity.TeamOutletID == null)
                                                {
                                                    model.status = 2;
                                                    model.message = "Username ( " + entity.UserName + " ) chưa có thông tin cột TeamoutletID." + " Dòng: " + (countRow + 1);
                                                    model.listDataApp = listEr1;
                                                    model.listDataWeb = listEr2;
                                                    return PartialView("_List", model);
                                                }
                                                break;
                                            case "SUP":
                                                if (listOutlet == "")
                                                {
                                                    model.status = 2;
                                                    model.message = "Username ( " + entity.UserName + " ) chưa có thông tin cột ListOutlet." + " Dòng: " + (countRow + 1);
                                                    model.listDataApp = listEr1;
                                                    model.listDataWeb = listEr2;
                                                    return PartialView("_List", model);
                                                }
                                                else
                                                {
                                                    entity.listOutlet = new List<int>();
                                                    string[] splitID = listOutlet.Split(',');
                                                    foreach (var item in splitID)
                                                    {
                                                        entity.listOutlet.Add(int.Parse(item));
                                                    }
                                                }
                                                break;
                                            case "KHO":
                                                if (listOutlet == "")
                                                {
                                                    model.status = 2;
                                                    model.message = "Username ( " + entity.UserName + " ) chưa có thông tin cột ListOutlet." + " Dòng: " + (countRow + 1);
                                                    model.listDataApp = listEr1;
                                                    model.listDataWeb = listEr2;
                                                    return PartialView("_List", model);
                                                }
                                                else
                                                {
                                                    entity.listOutlet = new List<int>();
                                                    string[] splitID = listOutlet.Split(',');
                                                    foreach (var item in splitID)
                                                    {
                                                        entity.listOutlet.Add(int.Parse(item));
                                                    }
                                                }
                                                break;
                                        }
                                        listRPAPP.Add(entity);
                                        listCurrentAPP.Add(entity);
                                    }
                                }
                                else
                                {
                                    model.status = 2;
                                    model.message = "Username ( " + entity.UserName + " ) sai loại User. Dòng " + (countRow + 1);
                                    model.listDataApp = listEr1;
                                    model.listDataWeb = listEr2;
                                    return PartialView("_List", model);
                                }
                            }
                            catch (Exception ex)
                            {
                                model.status = 2;
                                model.message = "Lỗi: " + ex.Message + " Dòng: " + (countRow + 1);
                                model.listDataApp = listEr1;
                                model.listDataWeb = listEr2;
                                return PartialView("_List", model);
                            }
                        }
                    }
                    else
                    {
                        for (int i = indexStart; i <= sheet.LastRowNum; i++)
                        {
                            countRow = i;
                            try
                            {
                                User_Web entity = new User_Web();
                                entity.ProjectID = User.ProjectID;
                                entity.Code = Cell2.GetCellSP(sheet.GetRow(i).GetCell(1));
                                entity.FullName = Cell2.GetCellSP(sheet.GetRow(i).GetCell(2));
                                entity.UserName = Cell2.GetCellSP(sheet.GetRow(i).GetCell(3));
                                entity.PassWord = Cell2.GetCellSP(sheet.GetRow(i).GetCell(4));
                                entity.UserType = Cell2.GetCellSP(sheet.GetRow(i).GetCell(5)).ToUpper();
                                string listOutlet = Cell2.GetCellSP(sheet.GetRow(i).GetCell(6)) != null ? Cell2.GetCellSP(sheet.GetRow(i).GetCell(6)) : "";
                                entity.CreatedBy = User.UserID;
                                entity.CreatedDateTime = DateTime.Now;
                                entity.Status = 1;
                                entity.RowVersion = 1;
                                if (entity.UserType == "SALE")
                                {
                                    if (listCurrentWEB.Where(o => o.UserName.ToLower() == entity.UserName.ToLower()).Count() > 0 || entity.UserName == "")
                                    {
                                        model.status = 2;
                                        model.message = "Username ( " + entity.UserName + " ) rỗng hoặc đã tồn tại trong hệ thống hoặc file." + " Dòng: " + (countRow + 1);
                                        model.listDataApp = listEr1;
                                        model.listDataWeb = listEr2;
                                        return PartialView("_List", model);
                                    }
                                    else
                                    {
                                        if (listOutlet == "")
                                        {
                                            model.status = 2;
                                            model.message = "Username ( " + entity.UserName + " ) chưa có thông tin cột ListOutlet." + " Dòng: " + (countRow + 1);
                                            model.listDataApp = listEr1;
                                            model.listDataWeb = listEr2;
                                            return PartialView("_List", model);
                                        }
                                        else
                                        {
                                            entity.listOutlet = new List<int>();
                                            string[] splitID = listOutlet.Split(',');
                                            foreach (var item in splitID)
                                            {
                                                entity.listOutlet.Add(int.Parse(item));
                                            }
                                        }

                                        listRPWEB.Add(entity);
                                        listCurrentWEB.Add(entity);
                                    }

                                }
                                else
                                {
                                    model.status = 2;
                                    model.message = "Username ( " + entity.UserName + " ) sai loại User. Dòng " + (countRow + 1);
                                    model.listDataApp = listEr1;
                                    model.listDataWeb = listEr2;
                                    return PartialView("_List", model);
                                }


                            }
                            catch (Exception ex)
                            {
                                model.status = 2;
                                model.message = "Lỗi: " + ex.Message + " Dòng: " + (countRow + 1);
                                model.listDataApp = listEr1;
                                model.listDataWeb = listEr2;
                                return PartialView("_List", model);
                            }

                        }
                    }
                    if (loaiuser == "APP")
                    {
                        if (listRPAPP.Count() == 0)
                        {
                            model.status = 2;
                            model.message = "Không có dữ liệu cần import.";
                            model.listDataApp = listEr1;
                            model.listDataWeb = listEr2;
                            return PartialView("_List", model);
                        }
                        else
                        {
                            MainResult result = Business.User.addRPAPP(listRPAPP);
                            if (result.Status != 1)
                            {
                                model.status = 2;
                                model.listDataApp = listEr1;
                                model.listDataWeb = listEr2;
                            }
                            else
                            {
                                model.status = 1;
                                model.listDataApp = Business.User.getApp(User.ProjectID, User.OutletTypeID);
                                model.listDataWeb = Business.User.getWeb(User.ProjectID, User.OutletTypeID);
                            }

                            model.message = result.Description;
                            
                            return PartialView("_List", model);
                        }
                    }
                    else
                    {
                        if (listRPWEB.Count() == 0)
                        {
                            model.status = 2;
                            model.message = "Không có dữ liệu cần import.";
                            model.listDataApp = listEr1;
                            model.listDataWeb = listEr2;
                            return PartialView("_List", model);
                        }
                        else
                        {
                            MainResult result = Business.User.addRPWEB(listRPWEB);
                            if (result.Status != 1)
                            {
                                model.status = 2;
                                model.listDataApp = listEr1;
                                model.listDataWeb = listEr2;
                            }
                            else
                            {
                                model.status = 1;
                                model.listDataApp = Business.User.getApp(User.ProjectID, User.OutletTypeID);
                                model.listDataWeb = Business.User.getWeb(User.ProjectID, User.OutletTypeID);
                            }
                            model.message = result.Description;
                            
                            return PartialView("_List", model);
                        }
                    }

                }
                catch (Exception ex)
                {
                    model.status = 2;
                    model.message = "Lỗi. " + ex.Message;
                    model.listDataApp = new List<User_App>();
                    model.listDataWeb = new List<User_Web>();
                    return PartialView("_List", model);
                }
            }
        }

        public ActionResult ExportFormatA()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {

                string _messageError = String.Empty;

                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\FormatUserApp.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);

                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                List<View_TeamOutletNotAssign> listNotAssign = new List<View_TeamOutletNotAssign>();
                List<Outlet> listOutlet = new List<Outlet>();
                listNotAssign = Business.User.getTeamOutletNotAssign(User.ProjectID, User.OutletTypeID);
                listOutlet = Outlet.getByProjectAndType(User.ProjectID, User.OutletTypeID);
                HSSFSheet sheetTeamOutlet = templateWorkbook.GetSheet("TeamOutlet") as HSSFSheet;
                HSSFSheet sheetOutlet = templateWorkbook.GetSheet("Outlet") as HSSFSheet;

                //excel data teamoutlet
                int x = 1;
                int u = 2;
                for (int i = 0; i < listNotAssign.Count(); i++)
                {

                    IRow row7 = sheetTeamOutlet.CreateRow(u);
                    row7.CreateCell(1).SetCellValue(listNotAssign[i].ID);
                    row7.CreateCell(2).SetCellValue(listNotAssign[i].TeamName);
                    u++;
                }


                u = 2;

                for (int i = 0; i < listOutlet.Count(); i++)
                {

                    IRow row7 = sheetOutlet.CreateRow(u);
                    row7.CreateCell(1).SetCellValue(listOutlet[i].ID);
                    row7.CreateCell(2).SetCellValue(listOutlet[i].OutletName);
                    u++;
                }

                sheetTeamOutlet.ForceFormulaRecalculation = true;
                sheetOutlet.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "FormartUserAPP" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }

        public ActionResult ExportFormatW()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {

                string _messageError = String.Empty;

                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\FormatUserWeb.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);

                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                List<View_TeamOutletNotAssign> listNotAssign = new List<View_TeamOutletNotAssign>();
                List<Outlet> listOutlet = new List<Outlet>();

                listOutlet = Outlet.getByProjectAndType(User.ProjectID, User.OutletTypeID);

                HSSFSheet sheetOutlet = templateWorkbook.GetSheet("Outlet") as HSSFSheet;

                //excel data teamoutlet
                int x = 1;
                int u = 2;


                for (int i = 0; i < listOutlet.Count(); i++)
                {

                    IRow row7 = sheetOutlet.CreateRow(u);
                    row7.CreateCell(1).SetCellValue(listOutlet[i].ID);
                    row7.CreateCell(2).SetCellValue(listOutlet[i].OutletName);
                    u++;
                }

                sheetOutlet.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "FormartUserWeb" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }
    }
}
