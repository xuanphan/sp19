﻿using Business;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using MvcPaging;
namespace Report.Controllers
{
    // quản lý nhân viên
    public class ProfileController : BaseController
    {
        //
        // GET: /Profile/

        public static List<ProfileEXT> listlocal;
        public ActionResult Index(int? page)
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                //get area
                Business.Region region = new Region();

                if (User.OutletTypeID == -1)
                {
                    if (User.SaleSupID != -1 && User.SaleSupID != 0)
                    {
                        SelectList listRegion = new SelectList(region.GetAllForComboSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID), "ID", "Text");
                        ViewBag.listRegion = listRegion;
                    }
                    else
                    {
                        SelectList listRegion = new SelectList(region.GetAllForComboSale(User.ProjectID, User.SaleID), "ID", "Text");
                        ViewBag.listRegion = listRegion;
                    }
                }
                else
                {
                    SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listRegion = listRegion;
                }
                if (User.OutletTypeID == -1)
                {
                    if (User.SaleSupID != -1 && User.SaleSupID != 0)
                    {
                        SelectList listRegionName = new SelectList(region.GetAllForComboNameSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID), "ID", "Text");
                        ViewBag.listRegionName = listRegionName;
                    }
                    else
                    {
                        SelectList listRegionName = new SelectList(region.GetAllForComboNameSale(User.ProjectID, User.SaleID), "ID", "Text");
                        ViewBag.listRegionName = listRegionName;
                    }
                }
                else
                {
                    SelectList listRegionName = new SelectList(region.GetAllForComboName(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listRegionName = listRegionName;
                }

                Profile profile = new Business.Profile();
                string MessageError = String.Empty;
                List<ProfileEXT> list = new List<ProfileEXT>();
                ProfileSE se = new ProfileSE();
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.SearchSaleSup(ref MessageError, User.ProjectID, User.OutletTypeID, User.SaleID, User.ListSaleID, se, ref list);
                }
                else
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.Search(ref MessageError, User.ProjectID, User.OutletTypeID, User.SaleID, se, ref list);
                }
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                return View(list);
            }
        }
        //[HttpGet]
        //public ActionResult IndexPage(int? page)
        //{
        //    if (User != null)
        //    {
        //        string MessageError = String.Empty;
        //        ViewBag.UserRole = User.UserRoles.FirstOrDefault();
        //        Profile profile = new Business.Profile();
        //        IQueryable<ProfileEXT> list = null;
        //        ProfileSE se = new ProfileSE();
        //        if (Session["ProfileRegion"] == null || Session["ProfileCode"] == null || Session["ProfileRegionName"] == null)
        //        {
        //            se = new ProfileSE();
        //        }
        //        else
        //        {
        //            se.ProfileRegion = Session["ProfileRegion"].ToString();
        //            se.ProfileRegionName = int.Parse(Session["ProfileRegionName"].ToString());
        //            se.ProfileCode = Session["ProfileCode"].ToString();
        //        }
        //        if (User.SaleSupID != -1 && User.SaleSupID != 0)
        //        {
        //            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.SearchSaleSup(ref MessageError, User.ProjectID, User.OutletTypeID, User.SaleID, User.ListSaleID, se, page, ref list);
        //        }
        //        else
        //        {
        //            //Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.Search(ref MessageError, User.ProjectID, User.OutletTypeID, User.SaleID, se, page, ref list);
        //        }
        //        int currentPageIndex = page.HasValue ? page.Value : 1;
        //        var pageList = list.ToPagedList(currentPageIndex, 10);
        //        return PartialView("_List", pageList);
        //    }
        //    else
        //    {
        //        return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        [HttpGet]
        public ActionResult Search(int? page, string _profileRegion, string _profileCode, string _profileRegionName)
        {
            if (User != null)
            {
                Profile profile = new Business.Profile();
                string MessageError = String.Empty;
                List<ProfileEXT> list = new List<ProfileEXT>();
                ProfileSE se = new ProfileSE();
                se.ProfileRegionName = int.Parse(_profileRegionName);
                se.ProfileRegion = _profileRegion;
                se.ProfileCode = _profileCode;
                Session["ProfileRegion"] = _profileRegion;
                Session["ProfileRegionName"] = _profileRegionName;
                Session["ProfileCode"] = _profileCode;
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.SearchSaleSup(ref MessageError, User.ProjectID, User.OutletTypeID, User.SaleID, User.ListSaleID, se, ref list);
                }
                else
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.Search(ref MessageError, User.ProjectID, User.OutletTypeID, User.SaleID, se, ref list);
                }
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ProfileDetail(long _profileID)
        {
            Profile profile = new Business.Profile();
            string MessageError = String.Empty;
            ProfileEXT simple = new ProfileEXT();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.GetSimple(ref MessageError, _profileID, ref simple);
            return PartialView("ProfileDetail", simple);
        }
        public ActionResult DiscliplineProfile(long _profileID)
        {
            string MessageError = String.Empty;
            Profile_Discipline profile_Discipline = new Profile_Discipline();
            List<Profile_Discipline> list = new List<Profile_Discipline>();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile_Discipline.GetAll(ref MessageError, _profileID, ref list);
            return PartialView("DiscliplineProfile", list);
        }
        public ActionResult ProfileDetailEmergency(long _profileID)
        {
            Profile profile = new Business.Profile();
            string MessageError = String.Empty;
            List<Profile_Emergency> simple = new List<Profile_Emergency>();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.GetSimpleEmergency(ref MessageError, _profileID, ref simple);
            return PartialView("ProfileDetailEmergency", simple);
        }
        [HttpGet]
        public ActionResult Update(int? page,string _listID)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Profile profile = new Business.Profile();
                Profile_Discipline profile_Discipline = new Business.Profile_Discipline();
                string MessageError = String.Empty;
                List<ProfileEXT> list = new List<ProfileEXT>();
                ProfileSE se = new ProfileSE();
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                //update xóa kỷ luật
                Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE errCode = profile_Discipline.Update(ref MessageError, _listID, User.UserID);
                return null;
            }
        }
        [HttpPost]
        public ActionResult AddNew(int? page)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string _messagerError = String.Empty;
                string _FilePath = String.Empty;
                string imageUrl = String.Empty;
                string imagePath = String.Empty;
                string imageName = String.Empty;
                string Link = String.Empty;
                // Add file
                imageName = Request.Files[0].FileName;
                Business.Connection.CreateImagePathWeb(Request.PhysicalApplicationPath, Request.Url.Authority, "FileDiscipline", _FilePath, out imageUrl);
                imagePath = Request.PhysicalApplicationPath + "/FileDiscipline/" + _FilePath;

                Directory.CreateDirectory(imagePath);

                if (System.IO.File.Exists(imagePath + "/" + imageName))
                {
                    imageName = "exist" + Common.CodeMD5.MD5Hash(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss ttt")) + imageName;
                }

                Request.Files[0].SaveAs(imagePath + "/" + imageName);
                Link = imageUrl + "/" + imageName;
                imageUrl = imagePath + "/" + imageName;

                //

                string _Discipline = Request.Form["Discipline"];
                string _TitleDiscipline = Request.Form["TitleDiscipline"];

                Profile profile = new Business.Profile();
                Profile_Discipline profile_Discipline = new Business.Profile_Discipline();
                string MessageError = String.Empty;
                IQueryable<ProfileEXT> list = null;
                ProfileSE se = new ProfileSE();
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                if (Session["ProfileID"] != null)
                {
                    string _ProfileID = Session["ProfileID"].ToString();
                    //thêm kỷ luật
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE errCode = profile_Discipline.AddNew(ref MessageError, _ProfileID, _Discipline, _TitleDiscipline, Link, User.UserID);
                }
                if (Session["ProfileRegion"] == null || Session["ProfileCode"] == null || Session["ProfileRegionName"] == null)
                {
                    se = new ProfileSE();
                }
                else
                {
                    se.ProfileRegion = Session["ProfileRegion"].ToString();
                    se.ProfileRegionName = int.Parse(Session["ProfileRegionName"].ToString());
                    se.ProfileCode = Session["ProfileCode"].ToString();
                }
                return null;
            }
        }
        public ActionResult AddProfileDiscipline(string _profileID)
        {
            Session["ProfileID"] = _profileID;
            return PartialView("AddProfileDiscipline");
        }
        [HttpPost]
        public ActionResult Import(int? page)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                Profile profile = new Business.Profile();
                Profile_Discipline profile_Discipline = new Business.Profile_Discipline();
                string MessageError = String.Empty;
                List<ProfileEXT> list = new List<ProfileEXT>();
                ProfileSE se = new ProfileSE();
                string imageName = Request.Files[0].FileName;
                string _FilePath = String.Empty;
                string imageUrl = String.Empty;
                string imagePath = String.Empty;
                HttpPostedFileBase files = Request.Files[0]; //Read the Posted Excel File  
                ISheet sheet; //Create the ISheet object to read the sheet cell values  
                string filename = Path.GetFileName(Server.MapPath(files.FileName)); //get the uploaded file name  
                var fileExt = Path.GetExtension(filename); //get the extension of uploaded excel file
                //Lưu File
                Business.Connection.CreateImagePathWeb(Request.PhysicalApplicationPath, Request.Url.Authority, "FileProfile", _FilePath, out imageUrl);
                imagePath = Request.PhysicalApplicationPath + "/FileProfile/" + _FilePath;

                Directory.CreateDirectory(imagePath);

                if (System.IO.File.Exists(imagePath + "/" + imageName))
                {
                    imageName = "exist" + Common.CodeMD5.MD5Hash(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss ttt")) + imageName;
                }

                Request.Files[0].SaveAs(imagePath + "/" + imageName);
                //đọc file
                if (fileExt == ".xls")
                {
                    HSSFWorkbook hssfwb = new HSSFWorkbook(files.InputStream); //HSSWorkBook object will read the Excel 97-2000 formats  
                    sheet = hssfwb.GetSheetAt(0); //get first Excel sheet from workbook  
                }
                else
                {
                    XSSFWorkbook hssfwb = new XSSFWorkbook(files.InputStream); //XSSFWorkBook will read 2007 Excel format  
                    sheet = hssfwb.GetSheetAt(0); //get first Excel sheet from workbook   
                }
                for (int row = 1; row <= sheet.LastRowNum; row++) //Loop the records upto filled row  
                {
                    if (sheet.GetRow(row) != null) //null is when the row only contains empty cells   
                    {
                        string ProfileCode = Cell.GetCell(sheet.GetRow(row).GetCell(0));
                        string ProfileName = Cell.GetCell(sheet.GetRow(row).GetCell(1));
                        string ProfilePhone = Cell.GetCell(sheet.GetRow(row).GetCell(2));
                        string ProfileAddress = Cell.GetCell(sheet.GetRow(row).GetCell(3));
                        string Avatar = Cell.GetCell(sheet.GetRow(row).GetCell(4));
                        string Experience = Cell.GetCell(sheet.GetRow(row).GetCell(5));
                        string RegionID = Cell.GetCell(sheet.GetRow(row).GetCell(6));
                        string IsEmergency = Cell.GetCell(sheet.GetRow(row).GetCell(7));
                        string PassWord = Cell.GetCell(sheet.GetRow(row).GetCell(8));//Here for sample , I just save the value in "value" field, Here you can write your custom logics...
                        string Outlettype = Cell.GetCell(sheet.GetRow(row).GetCell(9));
                        string CompanyCode = Cell.GetCell(sheet.GetRow(row).GetCell(10));
                        profile.ProfileCode = ProfileCode;
                        profile.ProfileName = ProfileName;
                        profile.ProfilePhone = ProfilePhone;
                        profile.ProfileAddress = ProfileAddress;
                        profile.Avatar = Avatar;
                        profile.Experience = Experience;
                        profile.RegionID = int.Parse(RegionID);
                        profile.IsEmergency = Boolean.Parse(IsEmergency);
                        profile.OutletTypeID = int.Parse(Outlettype);
                        profile.PassWord = PassWord;
                        profile.ProjectID = User.ProjectID;
                        profile.CreatedBy = User.UserID;
                        profile.CreatedDateTime = DateTime.Now;
                        profile.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                        profile.RowVersion = 1;
                        profile.ID = 1;
                        profile.ProjectCode = CompanyCode;
                        Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW errCode = profile.AddNew(ref MessageError);
                    }
                }

                if (Session["ProfileRegion"] == null || Session["ProfileCode"] == null || Session["ProfileRegionName"] == null)
                {
                    se = new ProfileSE();
                }
                else
                {
                    se.ProfileRegion = Session["ProfileRegion"].ToString();
                    se.ProfileRegionName = int.Parse(Session["ProfileRegionName"].ToString());
                    se.ProfileCode = Session["ProfileCode"].ToString();
                }
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.Search(ref MessageError, User.ProjectID, User.OutletTypeID, User.SaleID, se, ref list);
                return PartialView("_List", list);
            }
        }

        public JsonResult GetRegionName(string _regionType)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Region region = new Business.Region();
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    var List = region.GetRegionNameSaleSup(_regionType, User.ProjectID, User.OutletTypeID, User.ListSaleID);
                    return Json(List, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var List = region.GetRegionName(_regionType, User.ProjectID, User.OutletTypeID);
                    return Json(List, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
