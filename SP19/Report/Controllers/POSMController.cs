﻿using Business;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Report.Controllers
{
    // quản lý báo cáo POSM
    public class POSMController : BaseController
    {
        //
        // GET: /ReportPOSM/
        
        public ActionResult Index()
        {

            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                POSM_Report posm = new POSM_Report();
                List<POSMEXT> list = new List<POSMEXT>();
                POSMSE se = new POSMSE();

                ViewBag.outlets = Business.Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                //get outlet
                Outlet outlet = new Outlet();
                SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;

                //get area 
                Business.Region region = new Region();
                SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listRegion = listRegion;

                //get district
                SelectList listDistrict = new SelectList(region.GetDistrictForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listDistrict = listDistrict;

                ViewBag.posm = Business.POSM.GetForProject(User.ProjectID, User.OutletTypeID);
                ViewBag.posmdelta = Business.POSM.GetForProjectdelta(User.ProjectID, User.OutletTypeID);
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = posm.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                return View(list);
            }
        }

        // lấy tỉnh theo area
        public JsonResult GetDistrictForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Team team = new Business.Team();
                var List = team.GetDistrictForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        //------------------------Get Outlet for district
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetOutletForDistrict(string _DistrictID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForDistrict(int.Parse(_DistrictID), User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult Search(string _DistrictAttendance, string _RegionPOSM, string _OutletPOSM, string _DatePOSM)
        {
            if (User != null)
            {
               
                string _messageError = String.Empty;
                POSM_Report posm = new POSM_Report();
                List<POSMEXT> list = new List<POSMEXT>();
                POSMSE se = new POSMSE();
                ViewBag.posm = Business.POSM.GetForProject(User.ProjectID, User.OutletTypeID);
                ViewBag.posmdelta = Business.POSM.GetForProjectdelta(User.ProjectID, User.OutletTypeID);
                se.DatePOSM = Common.ConvertUtils.ConvertStringToShortDate(_DatePOSM);
                se.OutletPOSM = int.Parse(_OutletPOSM);
                se.DistrictAttendance = int.Parse(_DistrictAttendance);
                se.RegionPOSM = _RegionPOSM;
                Session["DatePOSM"] = _DatePOSM;
                Session["OutletPOSM"] = se.OutletPOSM;
                Session["RegionPOSM"] = _RegionPOSM;
                Session["DistrictAttendancePOSM"] = se.DistrictAttendance;
                ViewBag.outlets = Business.Outlet.SearchOutlet(int.Parse(_DistrictAttendance), User.ProjectID, User.OutletTypeID, int.Parse(_OutletPOSM), _RegionPOSM, "0");
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = posm.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                POSM_Report posm = new POSM_Report();
                List<POSMEXT> list = new List<POSMEXT>();
                POSMSE se = new POSMSE();


                if (Session["DistrictAttendancePOSM"] == null || Session["DatePOSM"] == null || Session["OutletPOSM"] == null || Session["RegionPOSM"] == null)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = posm.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                }
                else
                {
                    se.DatePOSM = Common.ConvertUtils.ConvertStringToShortDate(Session["DatePOSM"].ToString());
                    se.OutletPOSM = long.Parse(Session["OutletPOSM"].ToString());
                    se.DistrictAttendance = int.Parse(Session["DistrictAttendancePOSM"].ToString());
                    se.RegionPOSM = Session["RegionPOSM"].ToString();
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = posm.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                }

                if (Session["DistrictAttendancePOSM"] == null)
                {
                    Session["DistrictAttendancePOSM"] = "0";
                }

                if (Session["RegionPOSM"] == null)
                {
                    Session["RegionPOSM"] = "Tất Cả";
                }

                if (Session["OutletPOSM"] == null)
                {
                    Session["OutletPOSM"] = 0;
                }

                List<Outlet> outletlist = Business.Outlet.SearchOutlet(int.Parse(Session["DistrictAttendancePOSM"].ToString()), User.ProjectID, User.OutletTypeID, int.Parse(Session["OutletPOSM"].ToString()), Session["RegionPOSM"].ToString(), "0");
                List<POSM> listposm = Business.POSM.GetForProject(User.ProjectID, User.OutletTypeID);
                List<POSM> listposmdelta = Business.POSM.GetForProjectdelta(User.ProjectID, User.OutletTypeID);
                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\POSM_Report.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report
                int x = 1;
                for (int i = 0; i < outletlist.Count(); i++)
                {
                    var simple = list.Where(temp => temp.OutletID == outletlist[i].ID).ToList();
                    if (simple.Count() > 0)
                    {
                        IRow row7 = sheet_Report.CreateRow(i + 3);
                        row7.CreateCell(0).SetCellValue(simple.FirstOrDefault().datetime);
                        row7.GetCell(0).CellStyle = cellStyle;
                        row7.CreateCell(1).SetCellValue(outletlist[i].OutletName);
                        int y = 2;
                        for (int item = 0; item < listposm.Count(); item++)
                        {
                            if (simple.FirstOrDefault().list.Where(p => p.POSM.POSMType == listposm[item].POSMType && p.POSM.Brand.BrandName == listposm[item].Brand.BrandName).Count() > 0)
                            {
                                row7.CreateCell(y).SetCellValue(simple.FirstOrDefault().list.Where(p => p.POSM.POSMType == listposm[item].POSMType && p.POSM.Brand.BrandName == listposm[item].Brand.BrandName).FirstOrDefault().Number);
                            }
                            else
                            {
                                if (simple.FirstOrDefault().list.Where(p => p.POSM.POSMType == listposmdelta[item].POSMType && p.POSM.Brand.BrandName == listposmdelta[item].Brand.BrandName).Count() > 0)
                                {
                                    row7.CreateCell(y).SetCellValue(simple.FirstOrDefault().list.Where(p => p.POSM.POSMType == listposmdelta[item].POSMType && p.POSM.Brand.BrandName == listposmdelta[item].Brand.BrandName).FirstOrDefault().Number);
                                }
                                else
                                {
                                    row7.CreateCell(y).SetCellValue("");
                                }

                            }
                            y++;
                        }
                        x++;
                    }
                    else
                    {
                        IRow row7 = sheet_Report.CreateRow(i + 3);
                        row7.CreateCell(0).SetCellValue("");
                        row7.CreateCell(1).SetCellValue(outletlist[i].OutletName);
                        int y = 2;
                        for (int item = 0; item < listposm.Count(); item++)
                        {
                            row7.CreateCell(y).SetCellValue("");
                            y++;
                        }
                        x++;
                    }
                }
                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "Report_POSM" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }
    }
}
