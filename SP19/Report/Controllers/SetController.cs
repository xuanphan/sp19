﻿using Business;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Report.Models;

namespace Report.Controllers
{
    public class SetController : Controller
    {
        //
     // quản lý các đơn yêu cầu giao thêm set quà tại siêu thị :  link này dùng để xem trên app
        public int p = 0;
        public ActionResult Index(int _ProjectID, int _pOutletType)
        {
            string _messageError = String.Empty;
            List<View_Warehouse_Request> list = new List<View_Warehouse_Request>();
            Warehouse_Requirement_Set warehouse_Requirement_Set = new Warehouse_Requirement_Set();
            RequirementSetSE se = new RequirementSetSE();
            //get outlet
            Outlet outlet = new Outlet();
            SelectList listOutlet = new SelectList(outlet.GetAllForCombo(_ProjectID, _pOutletType), "ID", "Text");
            ViewBag.listOutlet = listOutlet;
            //get area
            Business.Region region = new Region();
            SelectList listRegion = new SelectList(region.GetAllForCombo(_ProjectID, _pOutletType), "ID", "Text");
            ViewBag.listRegion = listRegion;

            //lấy brand
            ViewBag.brand = Business.Brand.GetAll(_ProjectID);

            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT errCode = warehouse_Requirement_Set.Search(ref _messageError, _ProjectID, _pOutletType, ref list, se);
            ViewBag.ProjectID = _ProjectID;
            ViewBag.OutletType = _pOutletType;
            return View(list);
        }
        [HttpGet]
        public ActionResult Search(int? page, string _RegionRequirementSetSE, string _OutletRequirementSetSE, string _DateRequirementSetSE, string _ProjectID, string _OutletType)
        {

            string _messageError = String.Empty;
            List<View_Warehouse_Request> list = new List<View_Warehouse_Request>();
            Warehouse_Requirement_Set warehouse_Requirement_Set = new Warehouse_Requirement_Set();
            RequirementSetSE se = new RequirementSetSE();
            ViewBag.brand = Business.Brand.GetAll(int.Parse(_ProjectID));
            se.DateRequirementSetSE = Common.ConvertUtils.ConvertStringToShortDate(_DateRequirementSetSE);
            se.OutletRequirementSetSE = int.Parse(_OutletRequirementSetSE);
            se.RegionRequirementSetSE = _RegionRequirementSetSE;
            Session["OutletRequirementSetSE"] = se.OutletRequirementSetSE;
            Session["RegionRequirementSetSE"] = se.RegionRequirementSetSE;
            Session["DateRequirementSetSE"] = _DateRequirementSetSE;
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = warehouse_Requirement_Set.Search(ref _messageError, int.Parse(_ProjectID), int.Parse(_OutletType), ref list, se);
            return PartialView("_List", list);
        }

        //------------------------Get Outlet for district
         public JsonResult GetOutletForRegion(string _RegionID, string idprojectID, string idOutletType)
        {
            string messageSystemError = String.Empty;
            Outlet outlet = new Business.Outlet();
            var List = outlet.GetOutletForRegion(_RegionID, int.Parse(idprojectID), int.Parse(idOutletType));
            return Json(List, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult _Detail(string _WarehouseRequirementSetID, string _OutletID, string _ProjectID)
        {
                RequirementSetModel model = new RequirementSetModel();
                List<Warehouse_Requirement_Set_Detail> list = new List<Warehouse_Requirement_Set_Detail>();
                List<Outlet_Current_Set> list_outlet_Current_Set = new List<Outlet_Current_Set>();
                Outlet_Current_Set outlet_Current_Set = new Outlet_Current_Set();
                Warehouse_Requirement_Set_Detail warehouse_Requirement_Set_Detail = new Warehouse_Requirement_Set_Detail();
                string _messageError = String.Empty;
                //Lấy Set hiện tại
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = outlet_Current_Set.GetCurrent(ref _messageError, int.Parse(_ProjectID), int.Parse(_OutletID), ref list_outlet_Current_Set);
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error1 = warehouse_Requirement_Set_Detail.GetList(ref _messageError, _WarehouseRequirementSetID, ref list);
                model.outlet_Current_Set = list_outlet_Current_Set;
                model.warehouse_Requirement_Set_Detail = list;
                ViewBag.brand = Business.Brand.GetAll(int.Parse(_ProjectID));
                return PartialView(model);
        }
    }
}
