﻿using Business;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Report.Models;

namespace Report.Controllers
{
    public class RequirementSetController : BaseController
    {
        //
        // GET: /Notification/
        // quản lý, xem tình trạng các yêu cầu giao thêm set quà tại các outlet
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                List<View_Warehouse_Request> list = new List<View_Warehouse_Request>();
                Warehouse_Requirement_Set warehouse_Requirement_Set = new Warehouse_Requirement_Set();
                RequirementSetSE se = new RequirementSetSE();
                //get outlet
                Outlet outlet = new Outlet();
                SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;
                //get area
                Business.Region region = new Region();
                SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listRegion = listRegion;

                //lấy brand
                ViewBag.brand = Business.Brand.GetAll(User.ProjectID);

                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT errCode = warehouse_Requirement_Set.Search(ref _messageError, User.ProjectID,User.OutletTypeID, ref list, se);
                return View(list);
            }
        }
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult Search(int? page, string _RegionRequirementSetSE, string _OutletRequirementSetSE, string _DateRequirementSetSE)
        {
            if (User != null)
            {
                string _messageError = String.Empty;
                List<View_Warehouse_Request> list = new List<View_Warehouse_Request>();
                Warehouse_Requirement_Set warehouse_Requirement_Set = new Warehouse_Requirement_Set();
                RequirementSetSE se = new RequirementSetSE();
                ViewBag.brand = Business.Brand.GetAll(User.ProjectID);
                se.DateRequirementSetSE = Common.ConvertUtils.ConvertStringToShortDate(_DateRequirementSetSE);
                se.OutletRequirementSetSE = int.Parse(_OutletRequirementSetSE);
                se.RegionRequirementSetSE = _RegionRequirementSetSE;
                Session["OutletRequirementSetSE"] = se.OutletRequirementSetSE;
                Session["RegionRequirementSetSE"] = se.RegionRequirementSetSE;
                Session["DateRequirementSetSE"] = _DateRequirementSetSE;
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = warehouse_Requirement_Set.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult _Detail(string _WarehouseRequirementSetID, string _OutletID)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                RequirementSetModel model = new RequirementSetModel();
                List<Warehouse_Requirement_Set_Detail> list = new List<Warehouse_Requirement_Set_Detail>();
                List<Outlet_Current_Set> list_outlet_Current_Set = new List<Outlet_Current_Set>();
                Outlet_Current_Set outlet_Current_Set = new Outlet_Current_Set();
                Warehouse_Requirement_Set_Detail warehouse_Requirement_Set_Detail = new Warehouse_Requirement_Set_Detail();
                string _messageError = String.Empty;
                //Lấy Set hiện tại
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = outlet_Current_Set.GetCurrent(ref _messageError, User.ProjectID, int.Parse(_OutletID), ref list_outlet_Current_Set);
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error1 = warehouse_Requirement_Set_Detail.GetList(ref _messageError, _WarehouseRequirementSetID, ref list);
                model.outlet_Current_Set = list_outlet_Current_Set;
                model.warehouse_Requirement_Set_Detail = list;
                ViewBag.brand = Business.Brand.GetAll(User.ProjectID);
                return PartialView(model);
            }
        }
    }
}
