﻿using Business;
using Report.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPOI.HSSF.Util;
using Common;

namespace Report.Controllers
{
    // Báo cáo số lượng hàng bán : TakeOffVolume 
    public class ReportTakeOffVolumeController : BaseController
    {
        //
        // GET: /StockManager/

        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                Take_Off_Volumn take_Off_Volumn = new Take_Off_Volumn();
                List<Take_Off_VolumnOutletForSum> list = new List<Take_Off_VolumnOutletForSum>();
                Take_Off_VolumnSE se = new Take_Off_VolumnSE();
                //danh sach product
                ViewBag.product = Business.Product.GetProduct(User.ProjectID);
                //ViewBag.outlets = Business.Outlet.SearchOutletTakeOff(User.ProjectID, User.OutletTypeID,0,null,se.DateFromTakeOffSE, se.DateToTakeOffSE);
                //get outlet
                Outlet outlet = new Outlet();
                SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID,User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;

                //get area 
                Business.Region region = new Region();
                SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");

                //get district
                SelectList listDistrict = new SelectList(region.GetDistrictForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listDistrict = listDistrict;

                //get sale 
                Business.User user = new User();
                SelectList listsale = new SelectList(user.GetUserSale(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listsale = listsale;

                ViewBag.listRegion = listRegion;
                ViewBag.outlets = Business.Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = take_Off_Volumn.Search(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                return View(list);
            }
        }
        // lấy tỉnh theo area
        public JsonResult GetDistrictForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Team team = new Business.Team();
                var List = team.GetDistrictForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetOutletForDistrict(string _DistrictID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForDistrict(int.Parse(_DistrictID), User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSaleForRegion(string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                User user = new Business.User();
                var List = user.GetSaleForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        //------------------------Get Outlet for district
        public JsonResult GetOutletForSale(string _SaleID, string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForSale(int.Parse(_SaleID), _RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        //------------------------Get Outlet for district
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {
               
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForRegion(_RegionID, User.ProjectID,User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public ActionResult Search(int? page, string _DistrictAttendance, string _SaleReportSE, string _OutletTakeOffSE, string _RegionTakeOffSE, string _dateformTakeOffSE, string _datetoTakeOffSE)
        {
            if (User != null)
            {
                
                string _messageError = String.Empty;
                Take_Off_Volumn take_Off_Volumn = new Take_Off_Volumn();
                List<Take_Off_VolumnOutletForSum> list = new List<Take_Off_VolumnOutletForSum>();
                Take_Off_VolumnSE se = new Take_Off_VolumnSE();
                se.DateFromTakeOffSE = Common.ConvertUtils.ConvertStringToShortDate(_dateformTakeOffSE);
                se.DateToTakeOffSE = Common.ConvertUtils.ConvertStringToShortDate(_datetoTakeOffSE);
                se.OutletTakeOffSE = int.Parse(_OutletTakeOffSE);
                se.DistrictAttendance = int.Parse(_DistrictAttendance);
                se.SaleReportSE = long.Parse(_SaleReportSE);
                se.RegionTakeOffSE = _RegionTakeOffSE;
                Session["OutletTakeOffSE"] = se.OutletTakeOffSE;
                Session["RegionTakeOffSE"] = se.RegionTakeOffSE;
                Session["DateFromTakeOffSE"] = _dateformTakeOffSE;
                Session["DateToTakeOffSE"] = _datetoTakeOffSE;
                Session["DistrictAttendanceTakeOff"] = se.DistrictAttendance;
                Session["SaleReportSETakeOff"] = se.SaleReportSE;
                ViewBag.product = Business.Product.GetProduct(User.ProjectID);
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                //var _poutlet = Business.Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                ViewBag.outlets = Business.Outlet.SearchOutlet(int.Parse(_DistrictAttendance), User.ProjectID, User.OutletTypeID,int.Parse(_OutletTakeOffSE), _RegionTakeOffSE, _SaleReportSE);
                //ViewBag.outlets = _poutlet.Where(p => (p.Region.Type == se.RegionTakeOffSE || se.RegionTakeOffSE == "Tất Cả") && (p.ID == se.OutletTakeOffSE || se.OutletTakeOffSE == null || se.OutletTakeOffSE == 0)).ToList();
                //ViewBag.outlets = Business.Outlet.SearchOutletTakeOff(User.ProjectID, User.OutletTypeID, int.Parse(_OutletTakeOffSE), _RegionTakeOffSE, se.DateFromTakeOffSE, se.DateToTakeOffSE);

               
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = take_Off_Volumn.Search(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                Take_Off_Volumn take_Off_Volumn = new Take_Off_Volumn();
                List<Take_Off_VolumnOutletForSum> list = new List<Take_Off_VolumnOutletForSum>();
                Take_Off_VolumnSE se = new Take_Off_VolumnSE();
                if (Session["DistrictAttendanceTakeOff"] == null || Session["SaleReportSETakeOff"] == null || Session["OutletTakeOffSE"] == null || Session["DateFromTakeOffSE"] == null || Session["DateToTakeOffSE"] == null || Session["RegionTakeOffSE"] == null)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = take_Off_Volumn.Search(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                }
                else
                {
                    se.OutletTakeOffSE = long.Parse(Session["OutletTakeOffSE"].ToString());
                    se.DistrictAttendance = int.Parse(Session["DistrictAttendanceTakeOff"].ToString());
                    se.SaleReportSE = long.Parse(Session["SaleReportSETakeOff"].ToString());
                    se.RegionTakeOffSE = Session["RegionTakeOffSE"].ToString();
                    se.DateFromTakeOffSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateFromTakeOffSE"].ToString());
                    se.DateToTakeOffSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateToTakeOffSE"].ToString());
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = take_Off_Volumn.Search(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                }

                if (Session["DistrictAttendanceTakeOff"] == null)
                {
                    Session["DistrictAttendanceTakeOff"] = "0";
                }

                if (Session["SaleReportSETakeOff"] == null)
                {
                    Session["SaleReportSETakeOff"] = "0";
                }

                if (Session["RegionTakeOffSE"] == null)
                {
                    Session["RegionTakeOffSE"] = "Tất Cả";
                }

                if (Session["OutletTakeOffSE"] == null)
                {
                    Session["OutletTakeOffSE"] = 0;
                }
                
                var product = Business.Product.GetProduct(User.ProjectID);
                //var _poutlet = Business.Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                //List<Outlet> outletlist = _poutlet.Where(p => (p.Region.Type == se.RegionTakeOffSE || se.RegionTakeOffSE == "Tất Cả" || se.RegionTakeOffSE == null) && (p.ID == se.OutletTakeOffSE || se.OutletTakeOffSE == null || se.OutletTakeOffSE == 0)).ToList();

                List<Outlet> outletlist = Business.Outlet.SearchOutlet(int.Parse(Session["DistrictAttendanceTakeOff"].ToString()),User.ProjectID, User.OutletTypeID,int.Parse(Session["OutletTakeOffSE"].ToString()), Session["RegionTakeOffSE"].ToString(), Session["SaleReportSETakeOff"].ToString());
                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\Report_Take_Off_Volume.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.None;

                HSSFCellStyle hStyle = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle.SetFont(font1);
                hStyle.FillBackgroundColor = HSSFColor.Green.Index;
                hStyle.FillForegroundColor = HSSFColor.Green.Index;
                HSSFCellStyle hStyle1 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle1.FillBackgroundColor = HSSFColor.Yellow.Index;
                hStyle1.FillForegroundColor = HSSFColor.Yellow.Index;
                HSSFCellStyle hStyle2 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle2.FillBackgroundColor = HSSFColor.Red.Index;
                hStyle2.FillForegroundColor = HSSFColor.Red.Index;

                HSSFCellStyle hStyle3 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle3.FillBackgroundColor = HSSFColor.White.Index;
                hStyle3.FillForegroundColor = HSSFColor.White.Index;


                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report

                int x = 1;

                for (int item = 0; item < outletlist.Count(); item++)
                {

                    var simple = list.Where(temp => temp.OutletID == outletlist[item].ID).ToList();
                    if (simple.Count() > 0)
                    {
                        IRow row7 = sheet_Report.CreateRow(item + 5);
                        row7.CreateCell(0).SetCellValue(x);
                        row7.CreateCell(1).SetCellValue(outletlist[item].Region.Type);
                        row7.CreateCell(2).SetCellValue(outletlist[item].City);
                        row7.CreateCell(3).SetCellValue(outletlist[item].OutletName);
                        int z = 4;
                        for (int i = 0; i < product.Count(); i++)
                        {
                            var _detailproduct = simple.Where(p => p.ProductID == product[i].ID);                            
                            if (_detailproduct.Count() > 0)
                            {
                                foreach (var y in _detailproduct)
                                {
                                    if (y.ProductID == product[i].ID)
                                    {
                                        row7.CreateCell(z).SetCellValue(y.TotalNumber);
                                        row7.GetCell(z).CellStyle = hStyle3;
                                    }
                                }
                            }
                            z++;
                        }

                    }
                    else
                    {
                        IRow row7 = sheet_Report.CreateRow(item + 5);
                        row7.CreateCell(0).SetCellValue(x);
                        row7.CreateCell(1).SetCellValue(outletlist[item].Region.Type);
                        row7.CreateCell(2).SetCellValue(outletlist[item].City);
                        row7.CreateCell(3).SetCellValue(outletlist[item].OutletName);
                    }

                    x++;
                }
                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "Report_TakeOffVolume" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }
    }
}


