﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Business;
using Report.Models;
using NPOI.HSSF.Util;

namespace Report.Controllers
{
    // thêm outlet
    public class AddOutletController : BaseController
    {
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                AddOutletModel model = new AddOutletModel();
                model.status = 0;
                model.message = "";
                List<Outlet> listData = Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                model.listData = listData;
                return View(model);
            }
        }

        public ActionResult ExportFormat()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<Chanel> listChanel = Chanel.getByProject(User.ProjectID);
                List<Region> listRegion = Region.getByProject(User.ProjectID);
                List<Group> listGroup = Group.getByProject(User.ProjectID);
                List<Brand> listBrand = Brand.getByProject(User.ProjectID);
                string _messageError = String.Empty;

                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\FormatOutlet.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);

                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");


                HSSFCellStyle styleTitle = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                styleTitle.BorderTop = BorderStyle.Thin;
                styleTitle.BorderBottom = BorderStyle.Thin;
                styleTitle.BorderLeft = BorderStyle.Thin;
                styleTitle.BorderRight = BorderStyle.Thin;
                styleTitle.FillBackgroundColor = HSSFColor.Green.Index;
                styleTitle.FillPattern = FillPattern.SolidForeground;
                styleTitle.FillForegroundColor = HSSFColor.Green.Index;

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //List<View_TeamOutletNotAssign> listNotAssign = new List<View_TeamOutletNotAssign>();
                //List<Outlet> listOutlet = new List<Outlet>();
                //listNotAssign = Business.User.getTeamOutletNotAssign(User.ProjectID, User.OutletTypeID);
                //listOutlet = Outlet.getByProjectAndType(User.ProjectID, User.OutletTypeID);
                HSSFSheet sheetChanel = templateWorkbook.GetSheet("Chanel") as HSSFSheet;
                HSSFSheet sheetRegion = templateWorkbook.GetSheet("Region") as HSSFSheet;
                HSSFSheet sheetGroup = templateWorkbook.GetSheet("Group") as HSSFSheet;
                HSSFSheet sheetPromotion = templateWorkbook.GetSheet("Promotion") as HSSFSheet;
                HSSFSheet sheetBrand = templateWorkbook.GetSheet("Brand") as HSSFSheet;

                //excel data teamoutlet
                int x = 1;
                int u = 2;

                IRow rowTitleChanel = sheetChanel.CreateRow(x);
                rowTitleChanel.Height = 500;
                rowTitleChanel.CreateCell(1).SetCellValue("ID");
                rowTitleChanel.CreateCell(2).SetCellValue("Description");
                rowTitleChanel.GetCell(1).CellStyle = styleTitle;
                rowTitleChanel.GetCell(2).CellStyle = styleTitle;
                for (int i = 0; i < listChanel.Count(); i++)
                {

                    IRow row = sheetChanel.CreateRow(u);
                    row.CreateCell(1).SetCellValue(listChanel[i].ID);
                    row.CreateCell(2).SetCellValue(listChanel[i].ChanelName);
                    u++;
                }
                u = 2;

                IRow rowTitleRegion = sheetRegion.CreateRow(x);
                rowTitleRegion.Height = 500;
                rowTitleRegion.CreateCell(1).SetCellValue("ID");
                rowTitleRegion.CreateCell(2).SetCellValue("Description");
                rowTitleRegion.GetCell(1).CellStyle = styleTitle;
                rowTitleRegion.GetCell(2).CellStyle = styleTitle;
                for (int i = 0; i < listRegion.Count(); i++)
                {

                    IRow row = sheetRegion.CreateRow(u);
                    row.CreateCell(1).SetCellValue(listRegion[i].ID);
                    row.CreateCell(2).SetCellValue(listRegion[i].RegionName);
                    u++;
                }
                u = 2;


                IRow rowTitleGroup = sheetGroup.CreateRow(x);
                rowTitleGroup.Height = 500;
                rowTitleGroup.CreateCell(1).SetCellValue("ID");
                rowTitleGroup.CreateCell(2).SetCellValue("Name");
                rowTitleGroup.CreateCell(3).SetCellValue("Description");
                rowTitleGroup.GetCell(1).CellStyle = styleTitle;
                rowTitleGroup.GetCell(2).CellStyle = styleTitle;
                rowTitleGroup.GetCell(3).CellStyle = styleTitle;
                for (int i = 0; i < listGroup.Count(); i++)
                {

                    IRow row = sheetGroup.CreateRow(u);
                    row.CreateCell(1).SetCellValue(listGroup[i].ID);
                    row.CreateCell(2).SetCellValue(listGroup[i].GroupCode);
                    row.CreateCell(3).SetCellValue(listGroup[i].GroupName);
                    u++;
                }
                u = 2;

                IRow rowTitlePromotion = sheetPromotion.CreateRow(x);
                rowTitlePromotion.Height = 500;
                rowTitlePromotion.CreateCell(1).SetCellValue("Value");
                rowTitlePromotion.CreateCell(2).SetCellValue("Description");
                rowTitlePromotion.GetCell(1).CellStyle = styleTitle;
                rowTitlePromotion.GetCell(2).CellStyle = styleTitle;
                IRow rowKM1 = sheetPromotion.CreateRow(u);
                rowKM1.CreateCell(1).SetCellValue("TRUE");
                rowKM1.CreateCell(2).SetCellValue("Chạy khuyến mãi đổi quà tặng");
                u++;
                IRow rowKM2 = sheetPromotion.CreateRow(u);
                rowKM2.CreateCell(1).SetCellValue("FALSE");
                rowKM2.CreateCell(2).SetCellValue("Không chạy khuyến mãi đổi quà tặng");

                u = 2;


                IRow rowTitleBrand = sheetBrand.CreateRow(x);
                rowTitleBrand.Height = 500;
                rowTitleBrand.CreateCell(1).SetCellValue("ID");
                rowTitleBrand.CreateCell(2).SetCellValue("Description");
                rowTitleBrand.GetCell(1).CellStyle = styleTitle;
                rowTitleBrand.GetCell(2).CellStyle = styleTitle;
                for (int i = 0; i < listBrand.Count(); i++)
                {

                    IRow row = sheetBrand.CreateRow(u);
                    row.CreateCell(1).SetCellValue(listBrand[i].ID);
                    row.CreateCell(2).SetCellValue(listBrand[i].BrandName);
                    u++;
                }
                u = 2;

                sheetChanel.ForceFormulaRecalculation = true;
                sheetRegion.ForceFormulaRecalculation = true;
                sheetGroup.ForceFormulaRecalculation = true;
                sheetPromotion.ForceFormulaRecalculation = true;
                sheetBrand.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "FormartOutlet" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }

        public ActionResult Import()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return RedirectToAction("Login", "Account");
                //AddOutletModel model = new AddOutletModel();
                //try
                //{
                //    string _messagerError = String.Empty;
                //    string _FilePath = String.Empty;
                //    string imageUrl = String.Empty;
                //    string imagePath = String.Empty;
                //    string imageName = String.Empty;
                //    string Link = String.Empty;
                //    ISheet sheet = null;

                //    imageName = Request.Files[0].FileName;
                //    Business.Connection.CreateImagePathWeb(Request.PhysicalApplicationPath, Request.Url.Authority, "FILEOUTLET", _FilePath, out imageUrl);
                //    imagePath = Request.PhysicalApplicationPath + "/FILEOUTLET/" + _FilePath;

                //    Directory.CreateDirectory(imagePath);

                //    if (System.IO.File.Exists(imagePath + "/" + imageName))
                //    {
                //        imageName = "exist" + Common.CodeMD5.MD5Hash(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss ttt")) + imageName;
                //    }

                //    Request.Files[0].SaveAs(imagePath + "/" + imageName);
                //    Link = imageUrl + DateTime.Now.ToString("ddMMyyyy") + "/" + imageName;
                //    imageUrl = imagePath + "/" + imageName;


                //    XSSFWorkbook xssfExcel = new XSSFWorkbook();
                //    FileInfo fi = new FileInfo(Request.Files[0].FileName);
                //    List<Outlet> listRP = new List<Outlet>();
                //    using (FileStream files = new FileStream(imageUrl, FileMode.Open, FileAccess.Read))
                //    {
                //        xssfExcel = new XSSFWorkbook(files);
                //    }

                //    sheet = xssfExcel.GetSheet("Data");

                //    int indexStart = 2;
                //    for (int i = indexStart; i <= sheet.LastRowNum; i++)
                //    {
                //        Outlet entity = new Outlet();
                //        entity.ProjectID = User.ProjectID;
                //        entity.ChanelID = int.Parse(Cell2.GetCellSP(sheet.GetRow(i).GetCell(1)));
                //        entity.OutletCode = Cell2.GetCellSP(sheet.GetRow(i).GetCell(2));
                //        entity.OutletName = Cell2.GetCellSP(sheet.GetRow(i).GetCell(3));
                //        entity.OutletAddress = Cell2.GetCellSP(sheet.GetRow(i).GetCell(4));
                //        entity.City = Cell2.GetCellSP(sheet.GetRow(i).GetCell(5));
                //        entity.District = Cell2.GetCellSP(sheet.GetRow(i).GetCell(6));
                //        entity.OutletTypeID = int.Parse(Cell2.GetCellSP(sheet.GetRow(i).GetCell(7)));
                //        entity.RegionID = int.Parse(Cell2.GetCellSP(sheet.GetRow(i).GetCell(8)));
                //        entity.GroupID = int.Parse(Cell2.GetCellSP(sheet.GetRow(i).GetCell(9)));
                //        entity.LongGPS = float.Parse(Cell2.GetCellSP(sheet.GetRow(i).GetCell(10)));
                //        entity.LatGPS = float.Parse(Cell2.GetCellSP(sheet.GetRow(i).GetCell(11)));
                //        if (int.Parse(Cell2.GetCellSP(sheet.GetRow(i).GetCell(12))) == 0)
                //        {
                //            entity.Promotion = false;
                //        }
                //        else
                //        {
                //            entity.Promotion = true;
                //        }
                //        entity.CreatedBy = User.UserID;
                //        entity.CreatedDateTime = DateTime.Now;
                //        entity.Status = 1;
                //        entity.RowVersion = 1;
                //        listRP.Add(entity);
                //    }
                //    if (listRP.Count() == 0)
                //    {
                //        model.status = 2;
                //        model.message = "Không có dữ liệu cần import.";
                //        model.listData = Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                //        return PartialView("_List", model);
                //    }
                //    else
                //    {
                //        MainResult result = Outlet.addRP(listRP);

                //        model.status = result.Status;
                //        model.message = result.Description;
                //        model.listData = Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                //        return PartialView("_List", model);
                //    }
                //}
                //catch (Exception ex)
                //{
                //    model.status = 2;
                //    model.message = "Lỗi. " + ex.Message;
                //    model.listData = Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                //    return PartialView("_List", model);
                //}
            }
        }
    }
}
