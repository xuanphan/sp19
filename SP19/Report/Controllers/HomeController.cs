﻿using Business;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Report.Helpers;
using Report.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Report.Controllers
{
    // trang chủ
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (User.UserRoles.FirstOrDefault().ToString() == "sale" || User.UserRoles.FirstOrDefault().ToString() == "SaleSup")
                {
                    return RedirectToAction("Index", "StockManagerForSale");
                }
                else
                {
                    string _messageError = String.Empty;
                    ReportHome reportHome = new ReportHome();
                    ReportHomeEXT reportHomeEXT = new ReportHomeEXT();
                    ViewBag.brand = Business.Brand.GetAll(User.ProjectID);
                    ViewBag.region = Business.Region.GetAll(User.ProjectID, User.OutletTypeID);
                    ViewBag.outletinregion = Business.Region.CountOutletInRegion(User.ProjectID, User.OutletTypeID);
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = reportHome.GetAll(ref _messageError, User.ProjectID, User.OutletTypeID, ref reportHomeEXT);
                    User user = new Business.User();
                    ViewBag.UserChangePass = user.GetPass(User.UserID);
                    ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                    ViewBag.outletType = User.OutletTypeID;
                    return View(reportHomeEXT);
                }

            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }


        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {

                string _messageError = String.Empty;
                ReportHome reportHome = new ReportHome();
                AttendanceTracking attendance = new AttendanceTracking();
                List<View_AttendanceTracking> list = new List<View_AttendanceTracking>();
                AttendanceTrackingSE se = new AttendanceTrackingSE();
                ReportHomeEXT reportHomeEXT = new ReportHomeEXT();
                List<Region> region = Business.Region.GetAll(User.ProjectID, User.OutletTypeID);
                List<Business.RegionOutlet> regionOulet = Business.Region.CountOutletInRegion(User.ProjectID, User.OutletTypeID);
                List<Brand> brand = Business.Brand.GetAll(User.ProjectID);
                brand = brand.Where(p => p.BrandCode != "BR05").ToList();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = reportHome.GetAll(ref _messageError, User.ProjectID, User.OutletTypeID, ref reportHomeEXT);
                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\ReportSet.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report
                int x = 1;
                int u = 1;
                for (int z = 0; z < region.Count(); z++)
                {
                    for (int i = 0; i < brand.Count(); i++)
                    {
                        IRow row7 = sheet_Report.CreateRow(x);
                        var simple = reportHomeEXT.list.Where(p => p.BrandName == brand[i].BrandName && p.RegionID == region[z].ID);
                        var queryregion = regionOulet.Where(p => p.RegionID == region[z].ID);
                        row7.CreateCell(0).SetCellValue(x);
                        row7.CreateCell(1).SetCellValue(region[z].RegionName);
                        row7.CreateCell(2).SetCellValue(brand[i].BrandName);
                        row7.CreateCell(3).SetCellValue(simple.FirstOrDefault().NumberSet);
                        row7.CreateCell(4).SetCellValue(queryregion.FirstOrDefault().NunberOutlet);
                        x++;
                    }
                }
                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "Report_Set" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Contact1()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Contact2()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
