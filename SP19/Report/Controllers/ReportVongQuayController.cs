﻿using Business;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Helpers;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Report.Controllers
{
    // THỐNG KÊ SỐ LẦN QUAY CÁC VÒNG QUAY THEO OUTLET 
    public class ReportVongQuayController : BaseController
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                ReportSanLuongSE se = new ReportSanLuongSE();
                List<ReportSoLanQuay> list = new List<ReportSoLanQuay>();
                ReportProduct report = new ReportProduct();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = report.ReportSoVongQuay(ref _messageError, ref list, User.OutletTypeID, User.ProjectID, se);
                //lấy brand
                ViewBag.brand = Business.Brand.GetAll(User.ProjectID);
                return View(list);
            }
        }

        public ActionResult Search(string _dateformreport, string _datetoreport, string _cty)
        {
            //-------------------------------------------Lấy list danh Sách Report ghi ra excel
            string _messageError = String.Empty;
            ReportSanLuongSE se = new ReportSanLuongSE();
            se.DateFromReport = Common.ConvertUtils.ConvertStringToShortDate(_dateformreport);
            se.DateToReport = Common.ConvertUtils.ConvertStringToShortDate(_datetoreport);
            List<ReportSoLanQuay> list = new List<ReportSoLanQuay>();
            ReportProduct report = new ReportProduct();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = report.ReportSoVongQuay(ref _messageError, ref list, int.Parse(_cty), User.ProjectID, se);
            //lấy brand
            ViewBag.brand = Business.Brand.GetAll(User.ProjectID);
            return PartialView("_List", list);
        }        
    }
}
