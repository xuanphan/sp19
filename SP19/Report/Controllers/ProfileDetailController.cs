﻿using Business;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Web.UI;
namespace Report.Controllers
{
    public class ProfileDetailController : Controller
    {
        //
        // GET: /Profile/
        // Chi tiết nhân viên
        public ActionResult ProfileDetail(string _pCompany, string _pCode)
        {
            ViewBag.company = _pCompany;
            ViewBag.code = _pCode;
            ViewBag.MyVariable = 1;
            ViewBag.Theme = "~/Views/Shared/_LayoutLogin.cshtml";
            return View();
        }
        //public ActionResult ProfileDetail(string _pCode)
        //{
        //    Profile profile = new Business.Profile();
        //    string MessageError = String.Empty;
        //    ProfileEXT simple = new ProfileEXT();
        //    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.GetSimple(ref MessageError, _pCode, ref simple);
        //    return View("ProfileDetail", simple);
        //}


        [HttpPost]
        public ActionResult LoginProfile(string _code, string _company, string _password)
        {
            Profile profile = new Business.Profile();
            string MessageError = String.Empty;
            ProfileEXT simple = new ProfileEXT();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = profile.GetSimple(ref MessageError, _code, _company, _password, ref simple);
            if (simple == null || simple.ProfileName == null)
            {
                var json = new { list1 = "Header", list2 = "Footer" , list3 = "2"};
                return CustomJson(json).AddPartialView("_ProfileDetail", simple).AddPartialView("_Result");
            }
            else
            {
                ViewBag.Theme = "~/Views/Shared/_ProfileLayout.cshtml";
                var json = new { list1 = "Header", list2 = "Footer", list3 = "1" };
                return CustomJson(json).AddPartialView("_ProfileDetail", simple).AddPartialView("_Result").AddPartialView("_Layout");
            }
           
        }

        /// <summary> </summary>
        /// <param name="json"></param>
        /// <param name="allowGet"></param>
        /// <returns></returns>
         protected internal virtual CustomJsonResult CustomJson(object json = null, bool allowGet = true)
        {
            return new CustomJsonResult(json)
            {
                JsonRequestBehavior = allowGet ? JsonRequestBehavior.AllowGet : JsonRequestBehavior.DenyGet
            };
        }
        public class CustomJsonResult : JsonResult
        {
            public CustomJsonResult(object json)
            {
                _json = json;
                _partials = new List<KeyValuePair<string, object>>();
                _results = new List<string>();
            }

            private readonly object _json;
            private readonly IList<KeyValuePair<string, object>> _partials;
            private readonly IList<string> _results;

            public CustomJsonResult AddPartialView(string partialViewName = null, object model = null)
            {
                _partials.Add(new KeyValuePair<string, object>(partialViewName, model));
                return this;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                foreach (var partial in _partials)
                {
                    var html = ControllerHelper.RenderPartialViewToString(context, partial.Key, partial.Value);
                    _results.Add(html);
                }
                base.Data = Data;
                base.ExecuteResult(context);
            }

            public new object Data
            {
                get
                {
                    return new
                    {
                        Html = _results,
                        Json = _json
                    };
                }
            }
        }
        public static class ControllerHelper
        {

            public static string RenderPartialViewToString(ControllerContext context, string viewName, object model)
            {
                var controller = context.Controller;

                var partialView = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);

                var stringBuilder = new StringBuilder();
                using (var stringWriter = new StringWriter(stringBuilder))
                {
                    using (var htmlWriter = new HtmlTextWriter(stringWriter))
                    {
                        controller.ViewData.Model = model;
                        partialView.View.Render(
                            new ViewContext(
                                controller.ControllerContext,
                                partialView.View,
                                controller.ViewData,
                                new TempDataDictionary(),
                                htmlWriter),
                            htmlWriter);
                    }
                }
                return stringBuilder.ToString();
            }


        }
        
        
    }
}
