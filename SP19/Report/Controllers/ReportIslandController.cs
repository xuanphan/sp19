﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business;
using Report.Helpers;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace Report.Controllers
{
    // báo cáo Ụ kệ
    public class ReportIslandController : BaseController
    {
        //
        // GET: /ReportIsland/

        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                Brewery_Brand_IsLand brewery_Brand_IsLand = new Brewery_Brand_IsLand();
                List<IslandEXT> list = new List<IslandEXT>();
                IsLandSE se = new IsLandSE();
                //get area
                Business.Region region = new Region();
                if (User.OutletTypeID == -1)
                {
                    if (User.SaleSupID != -1 && User.SaleSupID != 0)
                    {
                        SelectList listRegion = new SelectList(region.GetAllForComboSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID), "ID", "Text");
                        ViewBag.listRegion = listRegion;
                    }
                    else
                    {
                        SelectList listRegion = new SelectList(region.GetAllForComboSale(User.ProjectID, User.SaleID), "ID", "Text");
                        ViewBag.listRegion = listRegion;
                    }
                }
                else
                {
                    SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listRegion = listRegion;
                }
                //get brewery
                Business.Brewery brewery = new Brewery();
                SelectList listBrewery = new SelectList(brewery.GetAllForCombo(User.ProjectID), "ID", "Text");
                ViewBag.listBrewery = listBrewery;
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = brewery_Brand_IsLand.SearchSaleSup(ref _messageError, User.ProjectID, User.SaleSupID, User.ListSaleID, ref list, se);
                }
                else
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = brewery_Brand_IsLand.Search(ref _messageError, User.ProjectID, User.SaleID, User.OutletTypeID, ref list, se);
                    
                }
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                return View(list);
            }
        }


        public ActionResult Search(string _RegionIsLandSE, string _BreweryIsLandSE, string _DateIsLandSE, string _DateIsLandToSE)
        {
            if (User != null)
            {
                string _messageError = String.Empty;
                Brewery_Brand_IsLand brewery_Brand_IsLand = new Brewery_Brand_IsLand();
                List<IslandEXT> list = new List<IslandEXT>();
                IsLandSE se = new IsLandSE();

                se.DateIsLandSE = Common.ConvertUtils.ConvertStringToShortDate(_DateIsLandSE);
                se.DateIsLandToSE = Common.ConvertUtils.ConvertStringToShortDate(_DateIsLandToSE);
                se.RegionIsLandSE = _RegionIsLandSE;// != null ? int.Parse(_OutletFestiveSE) : 0;
                se.BreweryIsLandSE = long.Parse(_BreweryIsLandSE);
                Session["DateIsLandSE"] = _DateIsLandSE;
                Session["DateIsLandToSE"] = _DateIsLandToSE;
                Session["RegionIsLandSE"] = se.RegionIsLandSE;
                Session["BreweryIsLandSE"] = se.BreweryIsLandSE;
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = brewery_Brand_IsLand.SearchSaleSup(ref _messageError, User.ProjectID, User.SaleSupID, User.ListSaleID, ref list, se);
                }
                else
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = brewery_Brand_IsLand.Search(ref _messageError, User.ProjectID, User.SaleID, User.OutletTypeID, ref list, se);

                }
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {

                string _messageError = String.Empty;
                Brewery_Brand_IsLand brewery_Brand_IsLand = new Brewery_Brand_IsLand();
                List<IslandEXT> list = new List<IslandEXT>();
                IsLandSE se = new IsLandSE();
                if (Session["DateIsLandSE"] == null || Session["DateIsLandToSE"] == null || Session["RegionIsLandSE"] == null || Session["BreweryIsLandSE"] == null)
                {
                    if (User.SaleSupID != -1 && User.SaleSupID != 0)
                    {
                        Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = brewery_Brand_IsLand.SearchSaleSup(ref _messageError, User.ProjectID, User.SaleSupID, User.ListSaleID, ref list, se);
                    }
                    else
                    {
                        Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = brewery_Brand_IsLand.Search(ref _messageError, User.ProjectID, User.SaleID, User.OutletTypeID, ref list, se);

                    }
                }
                else
                {
                    se.RegionIsLandSE = Session["RegionIsLandSE"].ToString();
                    se.DateIsLandSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateIsLandSE"].ToString());
                    se.DateIsLandToSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateIsLandToSE"].ToString());
                    se.BreweryIsLandSE = int.Parse(Session["BreweryIsLandSE"].ToString());
                    if (User.SaleSupID != -1 && User.SaleSupID != 0)
                    {
                        Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = brewery_Brand_IsLand.SearchSaleSup(ref _messageError, User.ProjectID, User.SaleSupID, User.ListSaleID, ref list, se);
                    }
                    else
                    {
                        Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = brewery_Brand_IsLand.Search(ref _messageError, User.ProjectID, User.SaleID, User.OutletTypeID, ref list, se);

                    }
                }

                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\Report_Island.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report
                int x = 1;
                int u = 1;
                for (int i = 0; i < list.Count(); i++)
                {
                    IRow row7 = sheet_Report.CreateRow(i + 2);
                    row7.CreateCell(0).SetCellValue(x);
                    row7.CreateCell(1).SetCellValue(list[i].DateTimeDevice.Date);
                    row7.GetCell(1).CellStyle = cellStyle;
                    row7.CreateCell(2).SetCellValue(list[i].RegionName);
                    row7.CreateCell(3).SetCellValue(list[i].OutLetName);
                    row7.CreateCell(4).SetCellValue(list[i].BreweryName);
                    row7.CreateCell(5).SetCellValue(list[i].BrandName);
                    row7.CreateCell(6).SetCellValue(list[i].Outside);
                    row7.CreateCell(7).SetCellValue(list[i].Column);
                    row7.CreateCell(8).SetCellValue(list[i].Pallet);
                    x++;
                }
                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "Report_Island" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }
    }
}
