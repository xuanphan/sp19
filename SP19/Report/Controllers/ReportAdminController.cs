﻿using Business;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Helpers;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Report.Controllers
{
    // xuất dữ liệu data theo tùy option
    public class ReportAdminController : BaseController
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                ReportSanLuongSE se = new ReportSanLuongSE();
                return View();
            }
        }
        public ActionResult SearchGiftBrandUse(string _dateformreport, string _datetoreport)
        {
            //-------------------------------------------Lấy list danh Sách Report ghi ra excel
            string _messageError = String.Empty;
            ReportSanLuongSE se = new ReportSanLuongSE();
            se.DateFromReport = Common.ConvertUtils.ConvertStringToShortDate(_dateformreport);
            se.DateToReport = Common.ConvertUtils.ConvertStringToShortDate(_datetoreport);
            List<ReportGiftOutEXT> list = new List<ReportGiftOutEXT>();
            ReportProduct report = new ReportProduct();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = report.ReportExcelGiftUse(ref _messageError, ref list, User.OutletTypeID, User.ProjectID, se);
            //add data vao excel
            FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\ReportAdminGift.xlsx"), FileMode.Open, FileAccess.Read);
            XSSFWorkbook templateWorkbook = new XSSFWorkbook(fs);
            XSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as XSSFSheet;
            ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
            ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
            IDataFormat format = templateWorkbook.CreateDataFormat();
            cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
            cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

            var font1 = templateWorkbook.CreateFont();
            font1.FontHeightInPoints = 11;
            font1.FontName = "Calibri";
            font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

            ICellStyle style2 = templateWorkbook.CreateCellStyle();
            style2.SetFont(font1);

            //excel sheet report
            int x = 1;
            int u = 1;
            for (int i = 0; i < list.Count(); i++)
            {
                IRow row7 = sheet_Report.CreateRow(i + 1);
                row7.CreateCell(0).SetCellValue(list[i].Number);
                row7.CreateCell(1).SetCellValue(list[i].OutletName);
                row7.CreateCell(2).SetCellValue(list[i].BrandName);
                row7.CreateCell(3).SetCellValue(list[i].Name);
            }
            sheet_Report.ForceFormulaRecalculation = true;
            MemoryStream mstream = new MemoryStream();
            templateWorkbook.Write(mstream);
            string filename = "ReportGiftOut" + DateTime.Now.ToString("_dd_MM_yyyy_HH_mm") + ".xlsx";
            FileStream xfile = new FileStream((Server.MapPath("~/ReportGiftOut/" + filename)), FileMode.Create, System.IO.FileAccess.Write);
            templateWorkbook.Write(xfile);
            xfile.Close();
            mstream.Close();


            // lấy file excel mới ra để tiến hành tạo table pivot
            //đếm số hàng range
            int count = list.Count() + 1;
            // join cột và hàng để range
            string namerange = "A1:" + "D" + count;
            //lấy file
            string filePathDat = Path.GetFullPath(Server.MapPath("~/ReportGiftOut/" + filename));

            //tạo table pivot
            Workbook workbook = new Workbook();
            workbook.LoadFromFile(filePathDat, ExcelVersion.Version2013);
            Worksheet sheet = workbook.Worksheets[0];

            sheet.Name = "Data Source";

            Worksheet sheet2 = workbook.CreateEmptySheet();

            sheet2.Name = "Gift_Out";
            CellRange dataRange = sheet.Range[namerange];

            PivotCache cache = workbook.PivotCaches.Add(dataRange);

            PivotTable pt = sheet2.PivotTables.Add("Pivot Table", sheet.Range["A1:D1888"], cache);
            var r1 = pt.PivotFields["Name"];

            r1.Axis = AxisTypes.Row;

            pt.Options.RowHeaderCaption = "Location";

            var r4 = pt.PivotFields["ProductID"];

            r4.Axis = AxisTypes.Column;

            var r3 = pt.PivotFields["ProductName"];

            r3.Axis = AxisTypes.Column;


            pt.DataFields.Add(pt.PivotFields["number"], " ", SubtotalTypes.None);
            pt.BuiltInStyle = PivotBuiltInStyles.PivotStyleDark7;
            string path = string.Empty;
            path = Server.MapPath("~/ReportGiftOut/");
            path = path + "PivotTable.xlsx";
            workbook.SaveToFile(path, ExcelVersion.Version2013);

            return File(new FileStream(path, FileMode.Open), "application/vnd.ms-excel", "ReportGiftOut.xlsx");
        }
        public ActionResult SearchGift(string _dateformreport, string _datetoreport)
        {
            //-------------------------------------------Lấy list danh Sách Report ghi ra excel
            string _messageError = String.Empty;
            ReportSanLuongSE se = new ReportSanLuongSE();
            se.DateFromReport = Common.ConvertUtils.ConvertStringToShortDate(_dateformreport);
            se.DateToReport = Common.ConvertUtils.ConvertStringToShortDate(_datetoreport);
            List<ReportGiftOutEXT> list = new List<ReportGiftOutEXT>();
            ReportProduct report = new ReportProduct();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = report.ReportExcelGift(ref _messageError, ref list, User.OutletTypeID,User.ProjectID, se);
            //add data vao excel
            FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\ReportAdminGift.xlsx"), FileMode.Open, FileAccess.Read);
            XSSFWorkbook templateWorkbook = new XSSFWorkbook(fs);
            XSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as XSSFSheet;
            ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
            ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
            IDataFormat format = templateWorkbook.CreateDataFormat();
            cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
            cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

            var font1 = templateWorkbook.CreateFont();
            font1.FontHeightInPoints = 11;
            font1.FontName = "Calibri";
            font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

            ICellStyle style2 = templateWorkbook.CreateCellStyle();
            style2.SetFont(font1);

            //excel sheet report
            int x = 1;
            int u = 1;
            for (int i = 0; i < list.Count(); i++)
            {
                IRow row7 = sheet_Report.CreateRow(i + 1);
                row7.CreateCell(0).SetCellValue(list[i].Number);
                row7.CreateCell(1).SetCellValue(list[i].OutletName);
                row7.CreateCell(2).SetCellValue(list[i].BrandName);
                row7.CreateCell(3).SetCellValue(list[i].Name);
            }
            sheet_Report.ForceFormulaRecalculation = true;
            MemoryStream mstream = new MemoryStream();
            templateWorkbook.Write(mstream);
            string filename = "ReportGiftOut" + DateTime.Now.ToString("_dd_MM_yyyy_HH_mm") + ".xlsx";
            FileStream xfile = new FileStream((Server.MapPath("~/ReportGiftOut/" + filename)), FileMode.Create, System.IO.FileAccess.Write);
            templateWorkbook.Write(xfile);
            xfile.Close();
            mstream.Close();


            // lấy file excel mới ra để tiến hành tạo table pivot
            //đếm số hàng range
            int count = list.Count()+1;
            // join cột và hàng để range
            string namerange = "A1:" + "D" + count;
            //lấy file
            string filePathDat = Path.GetFullPath(Server.MapPath("~/ReportGiftOut/" + filename));

            //tạo table pivot
            Workbook workbook = new Workbook();
            workbook.LoadFromFile(filePathDat, ExcelVersion.Version2013);
            Worksheet sheet = workbook.Worksheets[0];

            sheet.Name = "Data Source";

            Worksheet sheet2 = workbook.CreateEmptySheet();

            sheet2.Name = "Gift_Out";
            CellRange dataRange = sheet.Range[namerange];

            PivotCache cache = workbook.PivotCaches.Add(dataRange);

            PivotTable pt = sheet2.PivotTables.Add("Pivot Table", sheet.Range["A1:D1888"], cache);
            var r1 = pt.PivotFields["Name"];

            r1.Axis = AxisTypes.Row;

            pt.Options.RowHeaderCaption = "Location";

            var r4 = pt.PivotFields["ProductID"];

            r4.Axis = AxisTypes.Column; 

            var r3 = pt.PivotFields["ProductName"];

            r3.Axis = AxisTypes.Column;

            
            pt.DataFields.Add(pt.PivotFields["number"], " ", SubtotalTypes.None);
            pt.BuiltInStyle = PivotBuiltInStyles.PivotStyleDark7;
            string path = string.Empty;
            path = Server.MapPath("~/ReportGiftOut/");
            path = path + "PivotTable.xlsx";
            workbook.SaveToFile(path, ExcelVersion.Version2013);

            return File(new FileStream(path, FileMode.Open), "application/vnd.ms-excel", "ReportGiftOut.xlsx");
        }

        public ActionResult SearchGiftKho(string _dateformreport, string _datetoreport)
        {
            //-------------------------------------------Lấy list danh Sách Report ghi ra excel
            string _messageError = String.Empty;
            ReportSanLuongSE se = new ReportSanLuongSE();
            se.DateFromReport = Common.ConvertUtils.ConvertStringToShortDate(_dateformreport);
            se.DateToReport = Common.ConvertUtils.ConvertStringToShortDate(_datetoreport);
            List<ReportGiftOutEXT> list = new List<ReportGiftOutEXT>();
            ReportProduct report = new ReportProduct();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = report.ReportExcelGift(ref _messageError, ref list, User.OutletTypeID, User.ProjectID, se);
            //add data vao excel
            FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\ReportAdminGift.xlsx"), FileMode.Open, FileAccess.Read);
            XSSFWorkbook templateWorkbook = new XSSFWorkbook(fs);
            XSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as XSSFSheet;
            ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
            ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
            IDataFormat format = templateWorkbook.CreateDataFormat();
            cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
            cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

            var font1 = templateWorkbook.CreateFont();
            font1.FontHeightInPoints = 11;
            font1.FontName = "Calibri";
            font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

            ICellStyle style2 = templateWorkbook.CreateCellStyle();
            style2.SetFont(font1);

            //excel sheet report
            int x = 1;
            int u = 1;
            for (int i = 0; i < list.Count(); i++)
            {
                IRow row7 = sheet_Report.CreateRow(i + 1);
                row7.CreateCell(0).SetCellValue(list[i].Number);
                row7.CreateCell(1).SetCellValue(list[i].OutletName);
                row7.CreateCell(2).SetCellValue(list[i].BrandName);
                row7.CreateCell(3).SetCellValue(list[i].Name);
            }
            sheet_Report.ForceFormulaRecalculation = true;
            MemoryStream mstream = new MemoryStream();
            templateWorkbook.Write(mstream);
            string filename = "ReportGiftOut" + DateTime.Now.ToString("_dd_MM_yyyy_HH_mm") + ".xlsx";
            FileStream xfile = new FileStream((Server.MapPath("~/ReportGiftOut/" + filename)), FileMode.Create, System.IO.FileAccess.Write);
            templateWorkbook.Write(xfile);
            xfile.Close();
            mstream.Close();


            // lấy file excel mới ra để tiến hành tạo table pivot
            //đếm số hàng range
            int count = list.Count() + 1;
            // join cột và hàng để range
            string namerange = "A1:" + "D" + count;
            //lấy file
            string filePathDat = Path.GetFullPath(Server.MapPath("~/ReportGiftOut/" + filename));

            //tạo table pivot
            Workbook workbook = new Workbook();
            workbook.LoadFromFile(filePathDat, ExcelVersion.Version2013);
            Worksheet sheet = workbook.Worksheets[0];

            sheet.Name = "Data Source";

            Worksheet sheet2 = workbook.CreateEmptySheet();

            sheet2.Name = "Gift_Out";
            CellRange dataRange = sheet.Range[namerange];

            PivotCache cache = workbook.PivotCaches.Add(dataRange);

            PivotTable pt = sheet2.PivotTables.Add("Pivot Table", sheet.Range["A1:D1888"], cache);
            var r1 = pt.PivotFields["Name"];

            r1.Axis = AxisTypes.Row;

            pt.Options.RowHeaderCaption = "Location";

            var r4 = pt.PivotFields["ProductID"];

            r4.Axis = AxisTypes.Column;

            var r3 = pt.PivotFields["ProductName"];

            r3.Axis = AxisTypes.Column;


            pt.DataFields.Add(pt.PivotFields["number"], " ", SubtotalTypes.None);
            pt.BuiltInStyle = PivotBuiltInStyles.PivotStyleDark7;
            string path = string.Empty;
            path = Server.MapPath("~/ReportGiftOut/");
            path = path + "PivotTable.xlsx";
            workbook.SaveToFile(path, ExcelVersion.Version2013);

            return File(new FileStream(path, FileMode.Open), "application/vnd.ms-excel", "ReportGiftOut.xlsx");
        }
        public ActionResult SearchVolume(string _dateformreport, string _datetoreport)
        {
            //-------------------------------------------Lấy list danh Sách Report ghi ra excel
            string _messageError = String.Empty;
            ReportSanLuongSE se = new ReportSanLuongSE();
            se.DateFromReport = Common.ConvertUtils.ConvertStringToShortDate(_dateformreport);
            se.DateToReport = Common.ConvertUtils.ConvertStringToShortDate(_datetoreport);
            List<ReportGiftOutEXT> list = new List<ReportGiftOutEXT>();
            ReportProduct report = new ReportProduct();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = report.ReportExcelSale(ref _messageError, ref list, User.OutletTypeID, User.ProjectID, se);
            //add data vao excel
            FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\ReportAdmin.xlsx"), FileMode.Open, FileAccess.Read);
            XSSFWorkbook templateWorkbook = new XSSFWorkbook(fs);
            XSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as XSSFSheet;
            ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
            ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
            IDataFormat format = templateWorkbook.CreateDataFormat();
            cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
            cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

            var font1 = templateWorkbook.CreateFont();
            font1.FontHeightInPoints = 11;
            font1.FontName = "Calibri";
            font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

            ICellStyle style2 = templateWorkbook.CreateCellStyle();
            style2.SetFont(font1);

            //excel sheet report
            int x = 1;
            int u = 1;
            for (int i = 0; i < list.Count(); i++)
            {
                IRow row7 = sheet_Report.CreateRow(i + 1);
                row7.CreateCell(0).SetCellValue(list[i].Number);
                row7.CreateCell(1).SetCellValue(list[i].OutletName);
                row7.CreateCell(2).SetCellValue(list[i].Date);
                row7.GetCell(2).CellStyle = cellStyle;
                row7.CreateCell(3).SetCellValue(list[i].BrandName);
                row7.CreateCell(4).SetCellValue(list[i].Name);
            }
            sheet_Report.ForceFormulaRecalculation = true;
            MemoryStream mstream = new MemoryStream();
            templateWorkbook.Write(mstream);
            string filename = "Report_Sale_Volume" + DateTime.Now.ToString("_dd_MM_yyyy_HH_mm") + ".xlsx";
            FileStream xfile = new FileStream((Server.MapPath("~/Report_Sale_Volume/" + filename)), FileMode.Create, System.IO.FileAccess.Write);
            templateWorkbook.Write(xfile);
            xfile.Close();
            mstream.Close();


            // lấy file excel mới ra để tiến hành tạo table pivot
            //đếm số hàng range
            int count = list.Count()+1;
            // join cột và hàng để range
            string namerange = "A1:" + "E" + count;
            //lấy file
            string filePathDat = Path.GetFullPath(Server.MapPath("~/Report_Sale_Volume/" + filename));

            //tạo table pivot
            Workbook workbook = new Workbook();
            workbook.LoadFromFile(filePathDat, ExcelVersion.Version2013);
            Worksheet sheet = workbook.Worksheets[0];

            sheet.Name = "Data Source";

            Worksheet sheet2 = workbook.CreateEmptySheet();

            sheet2.Name = "SaleVolume";
            CellRange dataRange = sheet.Range[namerange];

            PivotCache cache = workbook.PivotCaches.Add(dataRange);

            PivotTable pt = sheet2.PivotTables.Add("Pivot Table", sheet.Range["A1:E1888"], cache);
            var r1 = pt.PivotFields["Name"];

            r1.Axis = AxisTypes.Row;

            pt.Options.RowHeaderCaption = "Location";

            var r2 = pt.PivotFields["ngay"];

            r2.Axis = AxisTypes.Column;
            var r3 = pt.PivotFields["ProductName"];

            r3.Axis = AxisTypes.Column;
            pt.DataFields.Add(pt.PivotFields["number"], " ", SubtotalTypes.None);
            pt.BuiltInStyle = PivotBuiltInStyles.PivotStyleDark7;
            string path = string.Empty;
            path = Server.MapPath("~/Report_Sale_Volume/");
            path = path + "PivotTable.xlsx";
            workbook.SaveToFile(path, ExcelVersion.Version2013);

            return File(new FileStream(path, FileMode.Open), "application/vnd.ms-excel", "ReportSalevolume.xlsx");
        }

        public ActionResult SearchVolumeSum(string _dateformreport, string _datetoreport)
        {
            //-------------------------------------------Lấy list danh Sách Report ghi ra excel
            string _messageError = String.Empty;
            ReportSanLuongSE se = new ReportSanLuongSE();
            se.DateFromReport = Common.ConvertUtils.ConvertStringToShortDate(_dateformreport);
            se.DateToReport = Common.ConvertUtils.ConvertStringToShortDate(_datetoreport);
            List<ReportGiftOutEXT> list = new List<ReportGiftOutEXT>();
            ReportProduct report = new ReportProduct();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = report.ReportExcelSaleSum(ref _messageError, ref list, User.OutletTypeID, User.ProjectID, se);
            //add data vao excel
            FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\ReportAdmin.xlsx"), FileMode.Open, FileAccess.Read);
            XSSFWorkbook templateWorkbook = new XSSFWorkbook(fs);
            XSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as XSSFSheet;
            ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
            ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
            IDataFormat format = templateWorkbook.CreateDataFormat();
            cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
            cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

            var font1 = templateWorkbook.CreateFont();
            font1.FontHeightInPoints = 11;
            font1.FontName = "Calibri";
            font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

            ICellStyle style2 = templateWorkbook.CreateCellStyle();
            style2.SetFont(font1);

            //excel sheet report
            int x = 1;
            int u = 1;
            for (int i = 0; i < list.Count(); i++)
            {
                IRow row7 = sheet_Report.CreateRow(i + 1);
                row7.CreateCell(0).SetCellValue(list[i].Number);
                row7.CreateCell(1).SetCellValue(list[i].OutletName);
                row7.CreateCell(3).SetCellValue(list[i].BrandName);
                row7.CreateCell(4).SetCellValue(list[i].Name);
            }
            sheet_Report.ForceFormulaRecalculation = true;
            MemoryStream mstream = new MemoryStream();
            templateWorkbook.Write(mstream);
            string filename = "Report_Sale_Volume" + DateTime.Now.ToString("_dd_MM_yyyy_HH_mm") + ".xlsx";
            FileStream xfile = new FileStream((Server.MapPath("~/Report_Sale_Volume/" + filename)), FileMode.Create, System.IO.FileAccess.Write);
            templateWorkbook.Write(xfile);
            xfile.Close();
            mstream.Close();


            // lấy file excel mới ra để tiến hành tạo table pivot
            //đếm số hàng range
            int count = list.Count() + 1;
            // join cột và hàng để range
            string namerange = "A1:" + "E" + count;
            //lấy file
            string filePathDat = Path.GetFullPath(Server.MapPath("~/Report_Sale_Volume/" + filename));

            //tạo table pivot
            Workbook workbook = new Workbook();
            workbook.LoadFromFile(filePathDat, ExcelVersion.Version2013);
            Worksheet sheet = workbook.Worksheets[0];

            sheet.Name = "Data Source";

            Worksheet sheet2 = workbook.CreateEmptySheet();

            sheet2.Name = "SaleVolume";
            CellRange dataRange = sheet.Range[namerange];

            PivotCache cache = workbook.PivotCaches.Add(dataRange);

            PivotTable pt = sheet2.PivotTables.Add("Pivot Table", sheet.Range["A1:E1888"], cache);
            var r1 = pt.PivotFields["Name"];

            r1.Axis = AxisTypes.Row;

            pt.Options.RowHeaderCaption = "Location";

            var r3 = pt.PivotFields["ProductName"];

            r3.Axis = AxisTypes.Column;
            pt.DataFields.Add(pt.PivotFields["number"], " ", SubtotalTypes.None);
            pt.BuiltInStyle = PivotBuiltInStyles.PivotStyleDark7;
            string path = string.Empty;
            path = Server.MapPath("~/Report_Sale_Volume/");
            path = path + "PivotTable.xlsx";
            workbook.SaveToFile(path, ExcelVersion.Version2013);

            return File(new FileStream(path, FileMode.Open), "application/vnd.ms-excel", "ReportSalevolume.xlsx");
        }
    }
}
