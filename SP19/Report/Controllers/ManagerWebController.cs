﻿using Business;
using Report.Helpers;
using Report.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Report.Controllers
{
    // quản lý danh sách User
    public class ManagerWebController : BaseController
    {
        //
        // GET: /ManagerWeb/

        public ActionResult Index()
        {

            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string _messageError = String.Empty;
                //get role
                Role role = new Role();
                SelectList listRole = new SelectList(role.GetAllForCombo(), "ID", "Text");
                ViewBag.listRole = listRole;
                //get statusstock
                CodeDetail codeDetail = new CodeDetail();
                SelectList listUserType = new SelectList(codeDetail.GetForComboAdmin("UserType"), "ID", "Text");
                ViewBag.listUserType = listUserType;
                //list user trong hệ thống
                User user = new User();
                List<UserEXT> list = new List<UserEXT>();
                UserSE se = new UserSE();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = user.GetAll(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                return View(list);
            }
        }
        public ActionResult Search(string _TypeUser, string _RoleType)
        {
            if (User != null)
            {
                string _messageError = String.Empty;
                User user = new User();
                List<UserEXT> list = new List<UserEXT>();
                UserSE se = new UserSE();

                se.RoleType = int.Parse(_RoleType);// != null ? int.Parse(_OutletFestiveSE) : 0;
                se.TypeUser = _TypeUser;
                Session["TypeUser"] = se.TypeUser;
                Session["RoleType"] = se.RoleType;
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = user.GetAll(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        //xoa
        [HttpGet]
        public ActionResult Delete(string _listIDuser, string _listtype)
        {
            if (User != null)
            {
                string _messageError = String.Empty;
                User user = new User();
                List<UserEXT> list = new List<UserEXT>();
                UserSE se = new UserSE();
                //xoa user
                Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE errCode1 = user.Delete(ref _messageError, _listIDuser, _listtype, User.UserID);
                if (Session["RoleType"] == null || Session["TypeUser"] == null)
                {
                    se = new UserSE();
                }
                else
                {
                    se.TypeUser = Session["TypeUser"].ToString();
                    se.RoleType = int.Parse(Session["RoleType"].ToString());
                }
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = user.GetAll(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                if (list.Where(p => p.UserID == User.UserID && p.UserType.Trim() == "ADMIN").Count() != 0)
                {
                    return PartialView("_List", list);
                }
                else
                {
                    return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }

        }
        //chinh sua
        public ActionResult Detail(string _UserID, string _UserType)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string _messageError = String.Empty;
                User user = new User();
                UserEXT userext = new UserEXT();
                List<UserEXT> list = new List<UserEXT>();
                UserSE se = new UserSE();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = user.GetSimple(ref _messageError, _UserID, _UserType, ref userext);
                ViewBag.UserID = _UserID;
                ViewBag.UserType = _UserType;
                ViewBag.listteamoutlet = Team_Outlet.GetAll(User.ProjectID);
                return PartialView("Detail", userext);
            }
        }
        [HttpGet]
        public ActionResult UpdateDetail(string _username, string _passcu, string _passmoi, string _idUser, string _typeUser, string _teamoutletID)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string _messageError = String.Empty;
                User user = new User();
                List<UserEXT> list = new List<UserEXT>();
                UserSE se = new UserSE();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE errCode1 = user.Update(ref _messageError, _username, _passcu, _passmoi, _idUser, _typeUser, _teamoutletID, User.UserID);
                if (Session["RoleType"] == null || Session["TypeUser"] == null)
                {
                    se = new UserSE();
                }
                else
                {
                    se.TypeUser = Session["TypeUser"].ToString();
                    se.RoleType = int.Parse(Session["RoleType"].ToString());
                }
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = user.GetAll(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                return PartialView("_List", list);
            }
        }
        //thêm mới
        public ActionResult Add()
        {
            //get role
            Role role = new Role();
            SelectList listRole = new SelectList(role.GetAllForCombo(), "ID", "Text");
            ViewBag.listRole = listRole;
            //get statusstock
            CodeDetail codeDetail = new CodeDetail();
            SelectList listUserType = new SelectList(codeDetail.GetForComboAdminWeb("UserType"), "ID", "Text");
            ViewBag.listUserType = listUserType;
            return PartialView();
        }
        [HttpGet]
        public ActionResult AddOutlet(string _UserType, string _UserID)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                ChangeOutletModel model = new ChangeOutletModel();
                //danh sách outlet của user
                model.listforuser = Outlet.GetForUser(_UserType, _UserID, User.ProjectID);
                //danh sách outlet con lại
                var list = Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                model.listoutlet = list.Where(p => !model.listforuser.Any(p2 => p2.ID == p.ID)).ToList();
                ViewBag.UserID = _UserID;
                ViewBag.UserType = _UserType;
                return PartialView(model);
            }
        }
        [HttpGet]
        public ActionResult AddNew(string _pUsername, string _pUserType, string _pPassWord)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string _messageError = String.Empty;
                User user = new User();
                List<UserEXT> list = new List<UserEXT>();
                UserSE se = new UserSE();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW errCode1 = user.AddNew(ref _messageError, _pUsername, _pUserType, _pPassWord,User.OutletTypeID, User.ProjectID, User.UserID);
                if (Session["RoleType"] == null || Session["TypeUser"] == null)
                {
                    se = new UserSE();
                }
                else
                {
                    se.TypeUser = Session["TypeUser"].ToString();
                    se.RoleType = int.Parse(Session["RoleType"].ToString());
                }

                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = user.GetAll(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                return PartialView("_List", list);
            }
        }
        [HttpGet]
        public ActionResult AddNewOutlet(string _pListUserID, string _pUserID, string _pUserType)
        {
            if (User == null)
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string _messageError = String.Empty;
                User user = new User();
                Outlet outlet = new Outlet();
                List<UserEXT> list = new List<UserEXT>();
                UserSE se = new UserSE();
                Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE errCode1 = outlet.UpdateOutletForUser(ref _messageError, _pListUserID, _pUserID, _pUserType, User.ProjectID, User.UserID);
                if (Session["RoleType"] == null || Session["TypeUser"] == null)
                {
                    se = new UserSE();
                }
                else
                {
                    se.TypeUser = Session["TypeUser"].ToString();
                    se.RoleType = int.Parse(Session["RoleType"].ToString());
                }
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Error = user.GetAll(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                return PartialView("_List", list);
            }
        }
    }
}
