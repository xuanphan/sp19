﻿using Business;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IDS.Controllers
{
    public class AttendanceController : BaseController
    {
        //
        // GET: /Attendance/
        // báo cáo chấm công
        public ActionResult Index(int? page)
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string a = "";
                AttendanceTracking attendance = new AttendanceTracking();
                List<View_AttendanceTracking> list = new List<View_AttendanceTracking>();
                string _messageError = String.Empty;
                AttendanceTrackingSE se = new AttendanceTrackingSE();
                //get team
                Team team = new Team();
                SelectList listTeam = new SelectList(team.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listTeam = listTeam;
                //get area
                Business.Region region = new Region();
                SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listRegion = listRegion;

                SelectList listDistrict = new SelectList(region.GetDistrictForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listDistrict = listDistrict;

                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = attendance.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                return View(list);

            }
        }
        //------------------------Get Outlet for district
        public JsonResult GetTeamForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Team team = new Business.Team();
                var List = team.GetTeamForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTeamForDistrict(string _DistrictID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Team team = new Business.Team();
                var List = team.GetTeamForDistrict(int.Parse(_DistrictID), User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDistrictForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Team team = new Business.Team();
                var List = team.GetDistrictForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpGet]
        public ActionResult Search(int? page,string _districtAttendance,  string _teamAttendance, string _dateformAttendance, string _datetoAttendance, string _regionAttendance)
        {
            if (User != null)
            {

                string _messageError = String.Empty;
                AttendanceTracking attendance = new AttendanceTracking();
                AttendanceTrackingSE se = new AttendanceTrackingSE();
                List<View_AttendanceTracking> list = new List<View_AttendanceTracking>();
                se.DateFromAttendance = Common.ConvertUtils.ConvertStringToShortDate(_dateformAttendance);
                se.DateToAttendance = Common.ConvertUtils.ConvertStringToShortDate(_datetoAttendance);
                se.TeamAttendance = int.Parse(_teamAttendance);
                se.DistrictAttendance = int.Parse(_districtAttendance);
                se.RegionAttendance = _regionAttendance;
                Session["TeamAttendance"] = se.TeamAttendance;
                Session["DistrictAttendance"] = se.DistrictAttendance;
                Session["DateFromAttendance"] = _dateformAttendance;
                Session["DateToAttendance"] = _datetoAttendance;
                Session["RegionAttendance"] = se.RegionAttendance;
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = attendance.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                return PartialView("_List", list);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {

                string _messageError = String.Empty;
                AttendanceTracking attendance = new AttendanceTracking();
                List<View_AttendanceTracking> list = new List<View_AttendanceTracking>();
                AttendanceTrackingSE se = new AttendanceTrackingSE();
                if (Session["TeamAttendance"] == null || Session["DistrictAttendance"] == null || Session["DateFromAttendance"] == null || Session["DateToAttendance"] == null || Session["RegionAttendance"] == null)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = attendance.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                }
                else
                {
                    se.TeamAttendance = long.Parse(Session["TeamAttendance"].ToString());
                    se.DistrictAttendance = int.Parse(Session["DistrictAttendance"].ToString());
                    se.DateFromAttendance = Common.ConvertUtils.ConvertStringToShortDate(Session["DateFromAttendance"].ToString());
                    se.DateToAttendance = Common.ConvertUtils.ConvertStringToShortDate(Session["DateToAttendance"].ToString());
                    se.RegionAttendance = Session["RegionAttendance"].ToString();
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = attendance.Search(ref _messageError, User.ProjectID, User.OutletTypeID, ref list, se);
                }

                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\Attendance.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report
                int x = 1;
                int u = 1;
                for (int i = 0; i < list.Count(); i++)
                {
                    u--;
                    IRow row7 = sheet_Report.CreateRow(i + 3 + u);
                    row7.CreateCell(0).SetCellValue(x);
                    row7.CreateCell(1).SetCellValue(list[i].OutletType);
                    row7.CreateCell(2).SetCellValue(list[i].TeamName);
                    row7.GetCell(2).CellStyle = cellStyle;
                    row7.CreateCell(3).SetCellValue(list[i].NumberPGIn);
                    row7.CreateCell(4).SetCellValue(list[i].NumberPGOut);
                    int countin = list[i].listin.Count();
                    int countout = list[i].listout.Count();
                    List<int> _count = new List<int>();
                    _count.Add(countin);
                    _count.Add(countout);
                    int _max = _count.Max();

                    if (_max > 0)
                    {
                        for (int e = 0; e < _max; e++)
                        {
                            if (countin != 0 && countin > e)
                            {
                                row7.CreateCell(5).SetCellValue(list[i].listin[e].DeviceDateTime);
                                row7.GetCell(5).CellStyle = cellStyle;
                                row7.GetCell(5).CellStyle.Alignment = HorizontalAlignment.Center;
                                row7.CreateCell(7).SetCellValue(list[i].listin[e].FilePath);
                            }
                            else
                            {
                                if(list[i].LOGIN_TIME != null)
                                {
                                    row7.CreateCell(5).SetCellValue(list[i].LOGIN_TIME);
                                    row7.CreateCell(7).SetCellValue("");
                                }
                                else
                                {
                                    row7.CreateCell(5).SetCellValue("");
                                    row7.CreateCell(7).SetCellValue("");
                                }      
                            }
                            if (countout != 0 && countout > e)
                            {
                                row7.CreateCell(6).SetCellValue(list[i].listout[e].DeviceDateTime);
                                row7.GetCell(6).CellStyle = cellStyle;
                                row7.GetCell(6).CellStyle.Alignment = HorizontalAlignment.Center;
                                row7.CreateCell(8).SetCellValue(list[i].listout[e].FilePath);
                            }
                            else
                            {
                                if (list[i].LOGOUT_TIME != null)
                                {
                                    row7.CreateCell(6).SetCellValue(list[i].LOGOUT_TIME);
                                    row7.CreateCell(8).SetCellValue("");
                                }
                                else
                                {
                                    row7.CreateCell(6).SetCellValue("");
                                    row7.CreateCell(8).SetCellValue("");
                                }  
                            }
                            u++;
                            row7 = sheet_Report.CreateRow(i + 3 + u);
                        }
                    }
                    else
                    {
                        row7.CreateCell(7).SetCellValue("");
                        row7.CreateCell(8).SetCellValue("");
                        u++;
                        row7 = sheet_Report.CreateRow(i + 3 + u);
                    }
                    x++;
                }
                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "Report_Attendance" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }

        [HttpGet]
        public ActionResult _ImageAttendance(long _attendanceid)
        {
            List<Image> list = new List<Image>();
            Image image = new Image();
            string _messageError = String.Empty;
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = image.GetList(ref _messageError, _attendanceid, ref list);
            return PartialView(list);
        }

        [HttpGet]
        public ActionResult _Image(string filepath)
        {
            ViewBag.filepath = filepath;
            return PartialView();
        }
    }
}
