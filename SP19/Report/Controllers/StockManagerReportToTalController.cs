﻿using Business;
using Report.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPOI.HSSF.Util;
using Common;

namespace Report.Controllers
{
    public class StockManagerReportTotalController : BaseController
    {
        //
        // GET: /StockManager/
        // báo cáo tồn kho tổng quát
        public static List<StockEXT> list;
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = DateTime.Now.Date.AddDays(-6); date <= DateTime.Now.Date; date = date.AddDays(1))
                    allDates.Add(date);
                ViewBag.countdate = allDates;
                string _messageError = String.Empty;
                Stock stock = new Stock();
                StockReportSE se = new StockReportSE();
                //danh sach product
                ViewBag.product = Business.Product.GetProduct(User.ProjectID);
                ViewBag.productdelta = Business.Product.GetProductdelta(User.ProjectID);
                ViewBag.outlets = Business.Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                ViewBag.havestatus = false;
                //get outlet
                Outlet outlet = new Outlet();
                SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listOutlet = listOutlet;

                //get area 
                Business.Region region = new Region();
                SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listRegion = listRegion;

                //get district
                SelectList listDistrict = new SelectList(region.GetDistrictForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listDistrict = listDistrict;

                //get sale 
                Business.User user = new User();
                SelectList listsale = new SelectList(user.GetUserSale(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listsale = listsale;

                //get statusstock
                CodeDetail codeDetail = new CodeDetail();
                SelectList listStatusStock = new SelectList(codeDetail.GetForCombo("StatusStock"), "ID", "Text");
                ViewBag.listStatusStock = listStatusStock;

                SelectList listoos = new SelectList(codeDetail.GetForComboStatusOOS(), "ID", "Text");
                ViewBag.listoos = listoos;

                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchStockreport7ngayNew(ref _messageError, User.ProjectID, User.OutletTypeID, se,null, ref list);

                ViewBag.list = list;
                return View(list);
            }
        }

        // lấy tỉnh theo area
        public JsonResult GetDistrictForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Team team = new Business.Team();
                var List = team.GetDistrictForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        //------------------------Get Outlet for district
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        } //------------------------Get Outlet for district
        public JsonResult GetSaleForRegion(string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                User user = new Business.User();
                var List = user.GetSaleForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        //------------------------Get Outlet for district
        public JsonResult GetOutletForSale(string _SaleID, string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForSale(int.Parse(_SaleID), _RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetOutletForDistrict(string _DistrictID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForDistrict(int.Parse(_DistrictID), User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        //------------------------Get Outlet for district
        public ActionResult GetDateInWeekInDateF(string _dateF)
        {
            if (User != null)
            {
                List<DateTime> allDates = new List<DateTime>();
                string messageSystemError = String.Empty;
                CodeDetail codeDetail = new CodeDetail();
                DateTime _d = Common.ConvertUtils.ConvertStringToShortDate(_dateF);
                for (DateTime date = _d.Date.AddDays(-6); date <= _d.Date; date = date.AddDays(1))
                    allDates.Add(date);
                ViewBag.countdate = allDates;
                return PartialView("_Date");
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult Search(int? page, string _DistrictAttendance, string _RegionStockReportSE, string _OutletStockReportSE, string _StatusStockReportSE, string _DateStockTSE, string _SaleReportSE, string _OOS, string listdate)
        {
            if (User != null)
            {

                string _messageError = String.Empty;
                var list2 = new List<StockEXT>();
                Stock stock = new Stock();
                StockReportSE se = new StockReportSE();
                ViewBag.product = Business.Product.GetProduct(User.ProjectID);
                var _out = Business.Outlet.SearchOutlet(int.Parse(_DistrictAttendance??"0"), User.ProjectID, User.OutletTypeID, int.Parse(_OutletStockReportSE), _RegionStockReportSE, _SaleReportSE);
                if (_StatusStockReportSE == "0")
                {
                    ViewBag.havestatus = false;
                }
                else
                {
                    ViewBag.havestatus = true;
                }
                se.DateStockTSE = Common.ConvertUtils.ConvertStringToShortDate(_DateStockTSE);
                se.OutletStockReportSE = int.Parse(_OutletStockReportSE);
                se.RegionStockReportSE = _RegionStockReportSE;
                se.StatusStockReportSE = long.Parse(_StatusStockReportSE);
                se.SaleReportSE = long.Parse(_SaleReportSE);
                se.OOS = _OOS;
                se.DistrictAttendance = int.Parse(_DistrictAttendance??"0");
                Session["DateStockTSE"] = _DateStockTSE;
                Session["OutletStockReportSE"] = se.OutletStockReportSE;
                Session["RegionStockReportSE"] = se.RegionStockReportSE;
                Session["StatusStockReportSE"] = se.StatusStockReportSE;
                Session["SaleReportSE"] = se.SaleReportSE;
                Session["OOS"] = se.OOS;
                Session["listdate"] = listdate;
                Session["DistrictStockReportTotalSE"] = se.DistrictAttendance;
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = se.DateStockTSE.Date.AddDays(-6); date <= se.DateStockTSE.Date; date = date.AddDays(1))
                    allDates.Add(date);
                if (listdate != null && listdate != "")
                {
                    string[] _listdate = listdate.Remove(0, 1).Split('|');
                    allDates = allDates.Where(item => _listdate.Any(item2 => Common.ConvertUtils.ConvertStringToShortDate(item2).Date == item.Date)).ToList();
                }
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchStockreport7ngayNew(ref _messageError, User.ProjectID, User.OutletTypeID, se, listdate, ref list2);
                ViewBag.countdate = allDates;
                ViewBag.list = list2;
                ViewBag.outlets = _out.Where(item => list2.Any(item2 => item2.OutletID == item.ID.ToString())).ToList();
                return PartialView("_List", list2);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
       
        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string listdate = String.Empty;
                var culture = new System.Globalization.CultureInfo("vi-VN");
                string _messageError = String.Empty;
                Stock stock = new Stock();
                List<StockEXT> listext = new List<StockEXT>();
                StockReportSE se = new StockReportSE();
                bool havestatus = false;
                if (Session["DistrictStockReportTotalSE"] == null || Session["DateStockTSE"] == null || Session["OutletStockReportSE"] == null || Session["RegionStockReportSE"] == null || Session["StatusStockReportSE"] == null || Session["SaleReportSE"] == null || Session["OOS"] == null)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchStockreport7ngayNew(ref _messageError, User.ProjectID, User.OutletTypeID, se,null , ref listext);
                }
                else
                {
                    se.OutletStockReportSE = int.Parse(Session["OutletStockReportSE"].ToString());
                    se.DistrictAttendance = int.Parse(Session["DistrictStockReportTotalSE"].ToString());
                    se.RegionStockReportSE = Session["RegionStockReportSE"].ToString();
                    se.StatusStockReportSE = long.Parse(Session["StatusStockReportSE"].ToString());
                    se.SaleReportSE = long.Parse(Session["SaleReportSE"].ToString());
                    se.DateStockTSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateStockTSE"].ToString());
                    se.OOS = Session["OOS"].ToString();
                   
                    if(Session["listdate"] != null)
                    {
                        listdate = Session["listdate"].ToString();
                    }
                    
                    if (Session["StatusStockReportSE"].ToString() != "0")
                    {
                        havestatus = true;
                    }
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchStockreport7ngayNew(ref _messageError, User.ProjectID, User.OutletTypeID, se, listdate, ref listext);
                }
                if (Session["RegionStockReportSE"] == null)
                { Session["RegionStockReportSE"] = "0"; }
                if (Session["OutletStockReportSE"] == null)
                { Session["OutletStockReportSE"] = "0"; }
                if (Session["SaleReportSE"] == null)
                { Session["SaleReportSE"] = "0"; }
                if (Session["DistrictStockReportTotalSE"] == null)
                { Session["DistrictStockReportTotalSE"] = "0"; }
                var product = Business.Product.GetProduct(User.ProjectID);
                var oulteList = Business.Outlet.SearchOutletExcel(int.Parse(Session["DistrictStockReportTotalSE"].ToString()), User.ProjectID, User.OutletTypeID, int.Parse(Session["OutletStockReportSE"].ToString()), Session["RegionStockReportSE"].ToString(), Session["SaleReportSE"].ToString());
                if (havestatus == true)
                {
                    if (listext.Count() == 0)
                    {
                        oulteList = new List<Business.Sale_Outlet>();
                    }
                    else
                    {
                        oulteList = oulteList.Where(p => listext.Any(l => p.OutletID == int.Parse(l.OutletID))).ToList().OrderBy(p => p.Outlet.Chanel.ChanelName).ToList();
                    }
                }
                oulteList = oulteList.Where(item => listext.Any(item2 => item2.OutletID == item.OutletID.ToString())).ToList();
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = se.DateStockTSE.Date.AddDays(-6); date <= se.DateStockTSE.Date; date = date.AddDays(1))
                    allDates.Add(date);
                if (listdate != null && listdate != "")
                {
                    string[] _listdate = listdate.Remove(0, 1).Split('|');
                    allDates = allDates.Where(item => _listdate.Any(item2 => Common.ConvertUtils.ConvertStringToShortDate(item2).Date == item.Date)).ToList();
                }
                var kams = Business.View_Kam_Outlet.getList();
                var salesups = Business.View_SaleSup_Outlet.getList();
                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\ReportStock_2.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.None;

                HSSFCellStyle hStyle = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle.SetFont(font1);
                hStyle.FillBackgroundColor = HSSFColor.Green.Index;
                hStyle.FillPattern = FillPattern.SolidForeground;
                hStyle.FillForegroundColor = HSSFColor.Green.Index;
                HSSFCellStyle hStyle1 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle1.FillBackgroundColor = HSSFColor.Yellow.Index;
                hStyle1.FillPattern = FillPattern.SolidForeground;
                hStyle1.FillForegroundColor = HSSFColor.Yellow.Index;
                HSSFCellStyle hStyle2 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle2.FillBackgroundColor = HSSFColor.Red.Index;
                hStyle2.FillPattern = FillPattern.SolidForeground;
                hStyle2.FillForegroundColor = HSSFColor.Red.Index;
                HSSFCellStyle hStyle22 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle22.FillBackgroundColor = HSSFColor.Grey40Percent.Index;
                hStyle22.FillPattern = FillPattern.SolidForeground;
                hStyle22.FillForegroundColor = HSSFColor.Grey40Percent.Index;
                HSSFCellStyle hStyle3 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report

                int v = 1;
                int u = 1;
                for (int item = 0; item < oulteList.Count(); item++)
                {
                    var simple = listext.Where(temp => int.Parse(temp.OutletID) == oulteList[item].Outlet.ID).ToList();
                    var simpleKAM = kams.Where(p => p.OutletID == oulteList[item].Outlet.ID).FirstOrDefault();
                    var simpleSaleSup = salesups.Where(p => p.OutletID == oulteList[item].Outlet.ID).FirstOrDefault();
                    if (simple.Count() > 0)
                    {
                        u--;
                        IRow row7 = sheet_Report.CreateRow(item + 2 + u);
                        row7.CreateCell(0).SetCellValue(v);
                        row7.CreateCell(1).SetCellValue(oulteList[item].Outlet.Region.Type);
                        row7.CreateCell(2).SetCellValue(oulteList[item].Outlet.City);
                        row7.CreateCell(3).SetCellValue(oulteList[item].Outlet.Chanel.ChanelName);
                        row7.CreateCell(4).SetCellValue(oulteList[item].Outlet.OutletName);
                        row7.CreateCell(5).SetCellValue(oulteList[item].User_Web.FullName);
                        row7.CreateCell(6).SetCellValue(simpleSaleSup != null ? simpleSaleSup.NameSaleSup : "");
                        row7.CreateCell(7).SetCellValue(simpleKAM != null ? simpleKAM.FullName : "");

                        foreach (var x in allDates.OrderBy(p => p.Date))
                        {
                            int k = 9;
                            if (simple.Where(p => p.DateStock.Date == x.Date && x.Date == DateTime.Now.Date).Count() > 0)
                            {
                                var temp = simple.Where(p => p.DateStock.Date == x.Date && x.Date == DateTime.Now.Date);
                                if (x.DayOfWeek.ToString() == "Sunday")
                                {
                                    row7.CreateCell(8).SetCellValue(culture.DateTimeFormat.GetDayName(temp.FirstOrDefault().DateStock.DayOfWeek )+ "-" + temp.FirstOrDefault().DateStock.ToString("dd/MM/yyyy"));
                                    row7.GetCell(8).CellStyle = cellStyle;
                                    row7.GetCell(8).CellStyle = hStyle2;
                                }
                                else
                                {
                                    row7.CreateCell(8).SetCellValue(culture.DateTimeFormat.GetDayName(temp.FirstOrDefault().DateStock.DayOfWeek )+ "-" + temp.FirstOrDefault().DateStock.ToString("dd/MM/yyyy"));
                                    row7.GetCell(8).CellStyle = cellStyle;
                                }
                               
                                for (int i = 0; i < product.Count(); i++)
                                {
                                    var _detailproduct = simple.Where(p => p.ProductID == product[i].ID);
                                    if (_detailproduct.Count() > 0)
                                    {
                                        foreach (var y in _detailproduct)
                                        {
                                            if (y.ProductID == product[i].ID)
                                            {
                                                if (y.StatusName == Business.Stock.StatusStock.OOS.ToString())
                                                {
                                                    if (y.NumberSale == null)
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSP);
                                                        row7.GetCell(k).CellStyle = hStyle2;
                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSale.GetValueOrDefault());
                                                        row7.GetCell(k).CellStyle = hStyle2;
                                                    }
                                                }
                                                else if (y.StatusName == Business.Stock.StatusStock.ALERT.ToString())
                                                {
                                                    if (y.NumberSale == null)
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSP);
                                                        row7.GetCell(k).CellStyle = hStyle3;
                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSale.GetValueOrDefault());
                                                        row7.GetCell(k).CellStyle = hStyle1;
                                                    }
                                                }
                                                else if (y.StatusName == Business.Stock.StatusStock.SAFE.ToString())
                                                {
                                                    if (y.NumberSale == null)
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSP);
                                                        row7.GetCell(k).CellStyle = hStyle1;
                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSale.GetValueOrDefault());
                                                        row7.GetCell(k).CellStyle = hStyle;
                                                    }
                                                }
                                                else
                                                {
                                                    if (product[i].GroupID.ToString().Contains(oulteList[item].Outlet.GroupID.ToString()) && oulteList[item].Outlet.GroupID.ToString().Length == 2)
                                                    {
                                                        row7.CreateCell(k).SetCellValue("");
                                                        row7.GetCell(k).CellStyle = hStyle22;

                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue("");
                                                    }
                                                }

                                            }

                                        }
                                    }
                                    else
                                    {

                                        if (product[i].IsNaN != null)
                                        {
                                            if (product[i].IsNaN.Split('|').ToList().Where(p => p == oulteList[item].Outlet.GroupID.ToString()).Count() > 0)
                                            {
                                                row7.CreateCell(k).SetCellValue("");
                                                row7.GetCell(k).CellStyle = hStyle22;
                                            }
                                            else
                                            {
                                                row7.CreateCell(k).SetCellValue("");
                                            }
                                        }
                                        else
                                        {
                                            row7.CreateCell(k).SetCellValue("");
                                        }
                                    }
                                    k++;
                                }
                                row7.CreateCell(26).SetCellValue(simple.FirstOrDefault().Note);
                            }
                            else if (simple.Where(p => p.DateStock.Date == x.Date).Count() > 0)
                            {
                                var temp = simple.Where(p => p.DateStock.Date == x.Date);
                                if (x.DayOfWeek.ToString() == "Sunday")
                                {
                                    row7.CreateCell(8).SetCellValue(culture.DateTimeFormat.GetDayName(temp.FirstOrDefault().DateStock.DayOfWeek) + "-" + temp.FirstOrDefault().DateStock.ToString("dd/MM/yyyy"));
                                    row7.GetCell(8).CellStyle = cellStyle;
                                    row7.GetCell(8).CellStyle = hStyle2;
                                }
                                else
                                {
                                    row7.CreateCell(8).SetCellValue(culture.DateTimeFormat.GetDayName(temp.FirstOrDefault().DateStock.DayOfWeek) + "-" + temp.FirstOrDefault().DateStock.ToString("dd/MM/yyyy"));
                                    row7.GetCell(8).CellStyle = cellStyle;
                                }
                                for (int i = 0; i < product.Count(); i++)
                                {
                                    var _detailproduct = temp.Where(p => p.ProductID == product[i].ID);
                                    if (_detailproduct.Count() > 0)
                                    {
                                        foreach (var z in _detailproduct)
                                        {
                                            if (product[i].ProductName == z.ProductName)
                                            {

                                                if (z.StatusName == Business.Stock.StatusStock.OOS.ToString())
                                                {
                                                    if (z.NumberSale == null)
                                                    {
                                                        row7.CreateCell(k).SetCellValue(z.NumberSP);
                                                        row7.GetCell(k).CellStyle = hStyle2;
                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue(z.NumberSale.GetValueOrDefault());
                                                        row7.GetCell(k).CellStyle = hStyle2;
                                                    }
                                                }
                                                else
                                                {
                                                    if (z.NumberSale == null)
                                                    {
                                                        row7.CreateCell(k).SetCellValue(z.NumberSP);
                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue(z.NumberSale.GetValueOrDefault());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {

                                        if (product[i].IsNaN != null)
                                        {
                                            if (product[i].IsNaN.Split('|').ToList().Where(p => p == oulteList[item].Outlet.GroupID.ToString()).Count() > 0)
                                            {
                                                row7.CreateCell(k).SetCellValue("");
                                                row7.GetCell(k).CellStyle = hStyle22;
                                            }
                                            else
                                            {
                                                row7.CreateCell(k).SetCellValue("");
                                            }
                                        }
                                        else
                                        {
                                            row7.CreateCell(k).SetCellValue("");
                                        }
                                    }
                                    k++;
                                }
                                row7.CreateCell(26).SetCellValue(simple.FirstOrDefault().Note);
                            }
                            else
                            {
                                if (x.DayOfWeek.ToString() == "Sunday")
                                {
                                    row7.CreateCell(8).SetCellValue(culture.DateTimeFormat.GetDayName(x.DayOfWeek) + "-" + x.ToString("dd/MM/yyyy"));
                                    row7.GetCell(8).CellStyle = cellStyle;
                                    row7.GetCell(8).CellStyle = hStyle2;
                                }
                                else
                                {
                                    row7.CreateCell(8).SetCellValue(culture.DateTimeFormat.GetDayName(x.DayOfWeek) + "-" + x.ToString("dd/MM/yyyy"));
                                    row7.GetCell(8).CellStyle = cellStyle;
                                }                              
                                row7.CreateCell(k).SetCellValue("");
                            }
                            u++;
                            row7 = sheet_Report.CreateRow(item + 2 + u);
                        }
                    }
                    else
                    {
                        u--;
                        IRow row7 = sheet_Report.CreateRow(item + 2 + u);
                        row7.CreateCell(0).SetCellValue(v);
                        row7.CreateCell(1).SetCellValue(oulteList[item].Outlet.Region.Type);
                        row7.CreateCell(2).SetCellValue(oulteList[item].Outlet.City);
                        row7.CreateCell(3).SetCellValue(oulteList[item].Outlet.Chanel.ChanelName);
                        row7.CreateCell(4).SetCellValue(oulteList[item].Outlet.OutletName);
                        row7.CreateCell(5).SetCellValue(oulteList[item].User_Web.FullName);
                        row7.CreateCell(6).SetCellValue(simpleSaleSup != null ? simpleSaleSup.NameSaleSup : "");
                        row7.CreateCell(7).SetCellValue(simpleKAM != null ? simpleKAM.FullName : "");
                        row7.CreateCell(8).SetCellValue("");
                        int z = 9;
                        foreach (var x in product)
                        {
                            if (x.IsNaN != null)
                            {
                                if (x.IsNaN.Split('|').ToList().Where(p => p == oulteList[item].Outlet.GroupID.ToString()).Count() > 0)
                                {
                                    row7.CreateCell(z).SetCellValue("");
                                    row7.GetCell(z).CellStyle = hStyle22;
                                }
                                else
                                {
                                    row7.CreateCell(z).SetCellValue("");
                                }
                            }
                            else
                            {
                                row7.CreateCell(z).SetCellValue("");
                            }
                            z++;
                        }
                        u++;
                        row7 = sheet_Report.CreateRow(item + 2 + u);
                    }
                    v++;
                }

                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "Report_Stock_Daily" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }
    }
}


