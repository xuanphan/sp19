﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Business;

namespace Report.Controllers
{
    public class UpdateNameOLController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Import()
        {


            try
            {
                string _messagerError = String.Empty;
                string _FilePath = String.Empty;
                string imageUrl = String.Empty;
                string imagePath = String.Empty;
                string imageName = String.Empty;
                string Link = String.Empty;
                ISheet sheet = null;

                imageName = Request.Files[0].FileName;
                //string loaiuser = Request.Form[0];
                Business.Connection.CreateImagePathWeb(Request.PhysicalApplicationPath, Request.Url.Authority, "FILEOUTLET", _FilePath, out imageUrl);
                imagePath = Request.PhysicalApplicationPath + "/FILEOUTLET/" + _FilePath;

                Directory.CreateDirectory(imagePath);

                if (System.IO.File.Exists(imagePath + "/" + imageName))
                {
                    imageName = "exist" + Common.CodeMD5.MD5Hash(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss ttt")) + imageName;
                }

                Request.Files[0].SaveAs(imagePath + "/" + imageName);
                Link = imageUrl + DateTime.Now.ToString("ddMMyyyy") + "/" + imageName;
                imageUrl = imagePath + "/" + imageName;


                XSSFWorkbook xssfExcel = new XSSFWorkbook();
                FileInfo fi = new FileInfo(Request.Files[0].FileName);

                using (FileStream files = new FileStream(imageUrl, FileMode.Open, FileAccess.Read))
                {
                    xssfExcel = new XSSFWorkbook(files);
                }

                sheet = xssfExcel.GetSheet("Data");
                List<User_App> listUpdate = new List<User_App>();
                int indexStart = 2;
                for (int i = indexStart; i <= sheet.LastRowNum; i++)
                {
                    User_App entity = new User_App();
                    entity.Status = int.Parse(Cell2.GetCellSP(sheet.GetRow(i).GetCell(1)));
                    entity.FullName = Cell2.GetCellSP(sheet.GetRow(i).GetCell(3));
                   
                    listUpdate.Add(entity);
                }
                XuLy.updateOutlet(listUpdate);

                return PartialView("_List");


            }
            catch (Exception ex)
            {

                return PartialView("_List");
            }

        }
    }
}
