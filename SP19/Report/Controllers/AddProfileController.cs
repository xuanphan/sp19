﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Business;
using Report.Models;

namespace Report.Controllers
{
    // thêm nhân viên
    public class AddProfileController : BaseController
    {
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                AddOutletModel model = new AddOutletModel();
                //get area
                Business.Region region = new Region();
                SelectList listRegionName = new SelectList(region.GetAllForComboName(User.ProjectID, User.OutletTypeID), "ID", "Text");
                ViewBag.listRegionName = listRegionName;

                return View();
            }
        }

        public ActionResult Import()
        {
            if (User != null)
            {
                AddOutletModel model = new AddOutletModel();

                string _messagerError = String.Empty;
                string _FilePath = String.Empty;
                string imageUrl = String.Empty;
                string imagePath = String.Empty;
                string imageName = String.Empty;
                string Link = String.Empty;
                ISheet sheet = null;

                imageName = Request.Files[0].FileName;
                string pProfileCode = Request.Form["ProfileCode"];
                string pProfileName = Request.Form["ProfileName"];
                string pProjectCode = Request.Form["ProjectCode"];
                string pProfilePhone = Request.Form["ProfilePhone"];
                string pExperience = Request.Form["Experience"];
                string pPass = Request.Form["Pass"];
                string pRegionProfile = Request.Form["RegionProfile"];
                //lay tên tỉnh ra làm folder
                string _pNameRegion = Region.GetSimple(int.Parse(pRegionProfile));

                Business.Connection.CreateImagePathWeb(Request.PhysicalApplicationPath, Request.Url.Authority, "ProfileWeb", _FilePath, out imageUrl);
                imagePath = Request.PhysicalApplicationPath + "/ProfileWeb" + "/" + _pNameRegion + "/" + _FilePath;

                Directory.CreateDirectory(imagePath);


                imageName = pProfileName + "_" + pProfileCode + ".jpg";

                Request.Files[0].SaveAs(imagePath + "/" + imageName);
                Link = imageUrl + _pNameRegion + "/" + imageName;
                imageUrl = imagePath + "/" + imageName;

                // add New
                Profile profile = new Profile();
                profile.ProfileCode = pProfileCode;
                profile.ProfileName = pProfileName;
                profile.ProfilePhone = pProfilePhone;
                profile.Experience = pExperience;
                profile.ProfileAddress = _pNameRegion;
                profile.ProjectCode =pProjectCode;
                profile.ProjectID = User.ProjectID;
                profile.RegionID = int.Parse(pRegionProfile);
                profile.IsEmergency = false;
                profile.PassWord = pPass;
                profile.OutletTypeID = User.OutletTypeID;
                profile.Avatar = Link;
                profile.CreatedBy = User.UserID;
                profile.CreatedDateTime = DateTime.Now;
                profile.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                profile.RowVersion = 1;
                Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = profile.AddNew(ref  _messagerError, profile);

                return Json(new { result = _messagerError }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
