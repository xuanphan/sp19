﻿using Business;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using Report.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace Report.Controllers
{
    // xuất data quà dup  (bỏ)
    public class ExportDataDupController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult XuatDataDup()
        {
            List<Customer_Gift> listCustomer = Customer_Gift.xuatDup();
            string _messageError = String.Empty;

            FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\DataDUP.xls"), FileMode.Open, FileAccess.Read);
            HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);

            ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
            ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
            IDataFormat format = templateWorkbook.CreateDataFormat();
            cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy HH:mm");
            cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");


            HSSFCellStyle styleTitle = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
            styleTitle.BorderTop = BorderStyle.Thin;
            styleTitle.BorderBottom = BorderStyle.Thin;
            styleTitle.BorderLeft = BorderStyle.Thin;
            styleTitle.BorderRight = BorderStyle.Thin;
            styleTitle.FillBackgroundColor = HSSFColor.Green.Index;
            styleTitle.FillPattern = FillPattern.SolidForeground;
            styleTitle.FillForegroundColor = HSSFColor.Green.Index;

            var font1 = templateWorkbook.CreateFont();
            font1.FontHeightInPoints = 11;
            font1.FontName = "Calibri";
            font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font1.Color = (short)NPOI.SS.UserModel.FontColor.Red;

            ICellStyle style2 = templateWorkbook.CreateCellStyle();
            style2.SetFont(font1);

            //List<View_TeamOutletNotAssign> listNotAssign = new List<View_TeamOutletNotAssign>();
            //List<Outlet> listOutlet = new List<Outlet>();
            //listNotAssign = Business.User.getTeamOutletNotAssign(User.ProjectID, User.OutletTypeID);
            //listOutlet = Outlet.getByProjectAndType(User.ProjectID, User.OutletTypeID);
            HSSFSheet sheet = templateWorkbook.GetSheet("Data") as HSSFSheet;

            //excel data teamoutlet
            int x = 1;
            int u = 2;
            foreach (var item in listCustomer)
            {
                IRow row = sheet.CreateRow(u);
                row.Height = 500;
                row.CreateCell(1).SetCellValue(item.CustomerID + "");
                row.CreateCell(2).SetCellValue(item.CreatedDateTime.ToString("dd/MM/yyyy"));
                u++;
            }


            sheet.ForceFormulaRecalculation = true;
            MemoryStream output = new MemoryStream();
            string downloadFileName = "FormartOutlet" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
            templateWorkbook.Write(output);
            return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
        }
    }
}
