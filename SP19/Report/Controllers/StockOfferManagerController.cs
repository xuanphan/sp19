﻿using Business;
using Report.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPOI.HSSF.Util;
using Common;

namespace Report.Controllers
{
    public class StockOfferManagerController : BaseController
    {
        //
        // GET: /StockManager/
        // quản lý, đề xuất nhập hàng dựa theo báo cáo tồn kho 
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                    string _messageError = String.Empty;
                    Stock stock = new Stock();
                    List<StockEXT> list = new List<StockEXT>();
                    StockSE se = new StockSE();
                    //danh sach product
                    ViewBag.product = Business.Product.GetProduct(User.ProjectID);
                    ViewBag.outlets = Business.Outlet.GetAll(User.ProjectID, User.OutletTypeID);
                    ViewBag.havestatus = false;
                    //get outlet
                    Outlet outlet = new Outlet();
                    SelectList listOutlet = new SelectList(outlet.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listOutlet = listOutlet;
                    //get sale 
                    Business.User user = new User();
                    SelectList listsale = new SelectList(user.GetUserSale(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listsale = listsale;
                    //get area 
                    Business.Region region = new Region();
                    SelectList listRegion = new SelectList(region.GetAllForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listRegion = listRegion;


                    //get district
                    SelectList listDistrict = new SelectList(region.GetDistrictForCombo(User.ProjectID, User.OutletTypeID), "ID", "Text");
                    ViewBag.listDistrict = listDistrict;

                    //get statusstock
                    CodeDetail codeDetail = new CodeDetail();
                    SelectList listStatusStock = new SelectList(codeDetail.GetForCombo("StatusStock"), "ID", "Text");
                    ViewBag.listStatusStock = listStatusStock;
                    ViewBag.UserRole = User.UserRoles.FirstOrDefault();
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchOffer(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
                    return View(list);
            }
        }

        public JsonResult GetDistrictForRegion(string _RegionID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Team team = new Business.Team();
                var List = team.GetDistrictForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        //------------------------Get Outlet for district
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        } //------------------------Get Outlet for district
        public JsonResult GetSaleForRegion(string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                User user = new Business.User();
                var List = user.GetSaleForRegion(_RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        //------------------------Get Outlet for district
        public JsonResult GetOutletForSale(string _SaleID, string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForSale(int.Parse(_SaleID), _RegionID, User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetOutletForDistrict(string _DistrictID)
        {
            if (User != null)
            {
                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                var List = outlet.GetOutletForDistrict(int.Parse(_DistrictID), User.ProjectID, User.OutletTypeID);
                return Json(List, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Search(int? page,string _DistrictAttendance, string _RegionStockSE, string _OutletStockSE, string _StatusStockSE, string _DateStockSE,string _SaleID)
        {

            string _messageError = String.Empty;
            Stock stock = new Stock();
            List<StockEXT> list = new List<StockEXT>();
            StockSE se = new StockSE();
            ViewBag.product = Business.Product.GetProduct(User.ProjectID);
            ViewBag.outlets = Business.Outlet.SearchOutlet(int.Parse(_DistrictAttendance??"0"), User.ProjectID, User.OutletTypeID, int.Parse(_OutletStockSE), _RegionStockSE, _SaleID);
            if (_StatusStockSE == "0")
            {
                ViewBag.havestatus = false;
            }
            else
            {
                ViewBag.havestatus = true;
            }
            se.DateStockSE = Common.ConvertUtils.ConvertStringToShortDate(_DateStockSE);
            se.OutletStockSE = int.Parse(_OutletStockSE);
            se.DistrictAttendance = int.Parse(_DistrictAttendance);
            se.RegionStockSE = _RegionStockSE;
            se.StatusStockSE = long.Parse(_StatusStockSE);
            se.SaleSE = long.Parse(_SaleID);
            Session["DateStockSE"] = _DateStockSE;
            Session["OutletStockSE"] = se.OutletStockSE;
            Session["RegionStockSE"] = se.RegionStockSE;
            Session["StatusStockSE"] = se.StatusStockSE;
            Session["SaleSE"] = se.SaleSE;
            Session["DistrictAttendanceOffer"] = se.DistrictAttendance;
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchOffer(ref _messageError, User.ProjectID, User.OutletTypeID, se, ref list);
            ViewBag.UserRole = User.UserRoles.FirstOrDefault();
            return PartialView("_List", list);
        }
       

       
        public ActionResult TimeOut()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }
    }
}


