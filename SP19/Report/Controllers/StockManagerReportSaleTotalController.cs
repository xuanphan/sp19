﻿using Business;
using Report.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPOI.HSSF.Util;
using Common;

namespace Report.Controllers
{
    public class StockManagerReportSaleTotalController : BaseController
    {
        //
        // GET: /StockManager/
        // báo cáo tồn kho tổng quát : SALE XEM
        public static List<StockEXT> list;
        public ActionResult Index()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = DateTime.Now.Date.AddDays(-6); date <= DateTime.Now.Date; date = date.AddDays(1))
                    allDates.Add(date);
                ViewBag.countdate = allDates;
                string _messageError = String.Empty;
                Stock stock = new Stock();
                StockReportSE se = new StockReportSE();
                //danh sach product
                ViewBag.product = Business.Product.GetProduct(User.ProjectID);
                ViewBag.productdelta = Business.Product.GetProductdelta(User.ProjectID);
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    ViewBag.outlets = Business.Outlet.GetAllForSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID);
                    ViewBag.havestatus = false;
                    //get outlet
                    Outlet outlet = new Outlet();
                    SelectList listOutlet = new SelectList(outlet.GetAllForComboSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID), "ID", "Text");
                    ViewBag.listOutlet = listOutlet;

                    //get area 
                    Business.Region region = new Region();
                    SelectList listRegion = new SelectList(region.GetAllForComboSaleSup(User.ProjectID, User.SaleSupID, User.ListSaleID), "ID", "Text");
                    ViewBag.listRegion = listRegion;
                }
                else
                {
                    ViewBag.outlets = Business.Outlet.GetAllForSale(User.ProjectID, User.SaleID, User.OutletTypeID);
                    ViewBag.havestatus = false;
                    //get outlet
                    Outlet outlet = new Outlet();
                    SelectList listOutlet = new SelectList(outlet.GetAllForComboSale(User.ProjectID, User.SaleID), "ID", "Text");
                    ViewBag.listOutlet = listOutlet;

                    //get area 
                    Business.Region region = new Region();
                    SelectList listRegion = new SelectList(region.GetAllForComboSale(User.ProjectID, User.SaleID), "ID", "Text");
                    ViewBag.listRegion = listRegion;
                    se.SaleReportSE = User.UserID;
                }

                //get statusstock
                CodeDetail codeDetail = new CodeDetail();
                SelectList listStatusStock = new SelectList(codeDetail.GetForCombo("StatusStock"), "ID", "Text");
                ViewBag.listStatusStock = listStatusStock;
                SelectList listdateinweek = new SelectList(codeDetail.GetForComboDateInWeek(se.DateStockTSE.Date), "Value", "Text");
                ViewBag.listdateinweek = listdateinweek;

                SelectList listoos = new SelectList(codeDetail.GetForComboStatusOOS(), "ID", "Text");
                ViewBag.listoos = listoos;


                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchStockreport7ngayNew(ref _messageError, User.ProjectID, User.OutletTypeID, se,null, ref list);

                ViewBag.list = list;
                return View(list);
            }
        }
        //------------------------Get Outlet for district
        public JsonResult GetOutletForRegion(string _RegionID)
        {
            if (User != null)
            {

                string messageSystemError = String.Empty;
                Outlet outlet = new Business.Outlet();
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    var List = outlet.GetOutletForRegionSaleSup(_RegionID, User.ProjectID, User.SaleSupID, User.ListSaleID);
                    return Json(List, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var List = outlet.GetOutletForRegionSale(_RegionID, User.ProjectID, User.OutletTypeID, User.SaleID);
                    return Json(List, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult Search(int? page, string _RegionStockReportSE, string _OutletStockReportSE, string _StatusStockReportSE, string _DateStockTSE, string _OOS, string listdate)
        {
            if (User != null)
            {
                var _out = new List<Outlet>();
                string _messageError = String.Empty;
                Stock stock = new Stock();
                var list2 = new List<StockEXT>();
                StockReportSE se = new StockReportSE();
                ViewBag.product = Business.Product.GetProduct(User.ProjectID);
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    _out = Business.Outlet.SearchOutletForSaleSup(User.ProjectID, User.SaleSupID, int.Parse(_OutletStockReportSE), _RegionStockReportSE, User.ListSaleID);
                }
                else
                {
                    _out = Business.Outlet.SearchOutletForSale(User.ProjectID, User.SaleID, int.Parse(_OutletStockReportSE), _RegionStockReportSE);
                    se.SaleReportSE = User.UserID;
                }
                if (_StatusStockReportSE == "0")
                {
                    ViewBag.havestatus = false;
                }
                else
                {
                    ViewBag.havestatus = true;
                }
                se.DateStockTSE = Common.ConvertUtils.ConvertStringToShortDate(_DateStockTSE);
                se.OutletStockReportSE = int.Parse(_OutletStockReportSE);
                se.RegionStockReportSE = _RegionStockReportSE;
                se.StatusStockReportSE = long.Parse(_StatusStockReportSE);
                se.OOS = _OOS;
                Session["DateStockTSE"] = _DateStockTSE;
                Session["OutletStockReportSE"] = se.OutletStockReportSE;
                Session["RegionStockReportSE"] = se.RegionStockReportSE;
                Session["StatusStockReportSE"] = se.StatusStockReportSE;
                Session["OOS"] = se.OOS;
                Session["listdate"] = listdate;
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = se.DateStockTSE.Date.AddDays(-6); date <= se.DateStockTSE.Date; date = date.AddDays(1))
                    allDates.Add(date);
                if (listdate != null && listdate != "")
                {
                    string[] _listdate = listdate.Remove(0, 1).Split('|');
                    allDates = allDates.Where(item => _listdate.Any(item2 => Common.ConvertUtils.ConvertStringToShortDate(item2).Date == item.Date)).ToList();
                }
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchStockreport7ngayNew(ref _messageError, User.ProjectID, User.OutletTypeID, se, listdate, ref list2);
                ViewBag.countdate = allDates;
                ViewBag.list = list2;
                ViewBag.outlets = _out.Where(item => list2.Any(item2 => item2.OutletID == item.ID.ToString())).ToList();
                return PartialView("_List", list2);
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetDateInWeekInDateF(string _dateF)
        {
            if (User != null)
            {
                List<DateTime> allDates = new List<DateTime>();
                string messageSystemError = String.Empty;
                CodeDetail codeDetail = new CodeDetail();
                DateTime _d = Common.ConvertUtils.ConvertStringToShortDate(_dateF);
                for (DateTime date = _d.Date.AddDays(-6); date <= _d.Date; date = date.AddDays(1))
                    allDates.Add(date);
                ViewBag.countdate = allDates;
                return PartialView("_Date");
            }
            else
            {
                return Json(new { ok = false, newurl = Url.Action("Login", "Account") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ExportExcel()
        {
            if (User == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                string listdate = String.Empty;
                var culture = new System.Globalization.CultureInfo("vi-VN");
                string _messageError = String.Empty;
                Stock stock = new Stock();
                List<StockEXT> listext = new List<StockEXT>();
                StockReportSE se = new StockReportSE();
                bool havestatus = false;
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {

                }
                else
                {
                    se.SaleReportSE = User.UserID;
                }
                if (Session["DateStockTSE"] == null || Session["OutletStockReportSE"] == null || Session["RegionStockReportSE"] == null || Session["StatusStockReportSE"] == null || Session["OOS"] == null)
                {
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchStockreport7ngayNew(ref _messageError, User.ProjectID, User.OutletTypeID, se,null, ref listext);
                }
                else
                {
                    se.OutletStockReportSE = int.Parse(Session["OutletStockReportSE"].ToString());
                    se.RegionStockReportSE = Session["RegionStockReportSE"].ToString();
                    se.StatusStockReportSE = long.Parse(Session["StatusStockReportSE"].ToString());
                    se.DateStockTSE = Common.ConvertUtils.ConvertStringToShortDate(Session["DateStockTSE"].ToString());
                    se.OOS = Session["OOS"].ToString();
                    if (Session["listdate"] != null)
                    {
                        listdate = Session["listdate"].ToString();
                    }
                    if (Session["StatusStockReportSE"].ToString() != "0")
                    {
                        havestatus = true;
                    }
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = stock.SearchStockreport7ngayNew(ref _messageError, User.ProjectID, User.OutletTypeID, se, listdate, ref listext);
                }
                if (Session["RegionStockReportSE"] == null)
                { Session["RegionStockReportSE"] = "0"; }
                if (Session["OutletStockReportSE"] == null)
                { Session["OutletStockReportSE"] = "0"; }
                if (Session["SaleReportSE"] == null)
                { Session["SaleReportSE"] = "0"; }
                var product = Business.Product.GetProduct(User.ProjectID);
                var oulteList = new List<Outlet>();
                if (User.SaleSupID != -1 && User.SaleSupID != 0)
                {
                    oulteList = Business.Outlet.SearchOutletForSaleSup(User.ProjectID, User.SaleSupID, int.Parse(Session["OutletStockReportSE"].ToString()), Session["RegionStockReportSE"].ToString(), User.ListSaleID);
                }
                else
                {
                    oulteList = Business.Outlet.SearchOutletForSale(User.ProjectID, User.SaleID, int.Parse(Session["OutletStockReportSE"].ToString()), Session["RegionStockReportSE"].ToString());
                }

                oulteList = oulteList.Where(item => listext.Any(item2 => item2.OutletID == item.ID.ToString())).ToList();
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = se.DateStockTSE.Date.AddDays(-6); date <= se.DateStockTSE.Date; date = date.AddDays(1))
                    allDates.Add(date);
                if (listdate != null && listdate != "")
                {
                    string[] _listdate = listdate.Remove(0, 1).Split('|');
                    allDates = allDates.Where(item => _listdate.Any(item2 => Common.ConvertUtils.ConvertStringToShortDate(item2).Date == item.Date)).ToList();
                }
                FileStream fs = new FileStream(Server.MapPath(@"~\ReportExcel\ReportStock_2.xls"), FileMode.Open, FileAccess.Read);
                HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);
                HSSFSheet sheet_Report = templateWorkbook.GetSheet("Report") as HSSFSheet;
                ICellStyle cellStyle = templateWorkbook.CreateCellStyle();
                ICellStyle cellStyle2 = templateWorkbook.CreateCellStyle();
                IDataFormat format = templateWorkbook.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("dd/MM/yyyy");
                cellStyle2.DataFormat = format.GetFormat("HH:mm:ss");

                var font1 = templateWorkbook.CreateFont();
                font1.FontHeightInPoints = 11;
                font1.FontName = "Calibri";
                font1.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
                font1.Color = (short)NPOI.SS.UserModel.FontColor.None;

                HSSFCellStyle hStyle = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle.SetFont(font1);
                hStyle.FillBackgroundColor = HSSFColor.Green.Index;
                hStyle.FillPattern = FillPattern.SolidForeground;
                hStyle.FillForegroundColor = HSSFColor.Green.Index;
                HSSFCellStyle hStyle1 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle1.FillBackgroundColor = HSSFColor.Yellow.Index;
                hStyle1.FillPattern = FillPattern.SolidForeground;
                hStyle1.FillForegroundColor = HSSFColor.Yellow.Index;
                HSSFCellStyle hStyle2 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle2.FillBackgroundColor = HSSFColor.Red.Index;
                hStyle2.FillPattern = FillPattern.SolidForeground;
                hStyle2.FillForegroundColor = HSSFColor.Red.Index;
                HSSFCellStyle hStyle22 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();
                hStyle22.FillBackgroundColor = HSSFColor.Grey40Percent.Index;
                hStyle22.FillPattern = FillPattern.SolidForeground;
                hStyle22.FillForegroundColor = HSSFColor.Grey40Percent.Index;
                HSSFCellStyle hStyle3 = (HSSFCellStyle)templateWorkbook.CreateCellStyle();

                ICellStyle style2 = templateWorkbook.CreateCellStyle();
                style2.SetFont(font1);

                //excel sheet report

                int v = 1;
                int u = 1;
                for (int item = 0; item < oulteList.Count(); item++)
                {
                    var simple = listext.Where(temp => int.Parse(temp.OutletID) == oulteList[item].ID).ToList();
                    if (simple.Count() > 0)
                    {
                        u--;
                        IRow row7 = sheet_Report.CreateRow(item + 2 + u);
                        row7.CreateCell(0).SetCellValue(v);
                        row7.CreateCell(1).SetCellValue(oulteList[item].Region.Type);
                        row7.CreateCell(2).SetCellValue(oulteList[item].City);
                        row7.CreateCell(3).SetCellValue(oulteList[item].Chanel.ChanelName);
                        row7.CreateCell(4).SetCellValue(oulteList[item].OutletName);
                        row7.CreateCell(5).SetCellValue(oulteList[item].SaleName);

                        foreach (var x in allDates.OrderBy(p => p.Date))
                        {
                            int k = 7;
                            if (simple.Where(p => p.DateStock.Date == x.Date && x.Date == DateTime.Now.Date).Count() > 0)
                            {
                                var temp = simple.Where(p => p.DateStock.Date == x.Date && x.Date == DateTime.Now.Date);
                                if (x.DayOfWeek.ToString() == "Sunday")
                                {
                                    row7.CreateCell(6).SetCellValue(culture.DateTimeFormat.GetDayName(temp.FirstOrDefault().DateStock.DayOfWeek) + "-" + temp.FirstOrDefault().DateStock.ToString("dd/MM/yyyy"));
                                    row7.GetCell(6).CellStyle = cellStyle;
                                    row7.GetCell(6).CellStyle = hStyle2;
                                }
                                else
                                {
                                    row7.CreateCell(6).SetCellValue(culture.DateTimeFormat.GetDayName(temp.FirstOrDefault().DateStock.DayOfWeek) + "-" + temp.FirstOrDefault().DateStock.ToString("dd/MM/yyyy"));
                                    row7.GetCell(6).CellStyle = cellStyle;
                                }

                                for (int i = 0; i < product.Count(); i++)
                                {
                                    var _detailproduct = simple.Where(p => p.ProductID == product[i].ID);
                                    if (_detailproduct.Count() > 0)
                                    {
                                        foreach (var y in _detailproduct)
                                        {
                                            if (y.ProductID == product[i].ID)
                                            {
                                                if (y.StatusName == Business.Stock.StatusStock.OOS.ToString())
                                                {
                                                    if (y.NumberSale == null)
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSP);
                                                        row7.GetCell(k).CellStyle = hStyle2;
                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSale.GetValueOrDefault());
                                                        row7.GetCell(k).CellStyle = hStyle2;
                                                    }
                                                }
                                                else if (y.StatusName == Business.Stock.StatusStock.ALERT.ToString())
                                                {
                                                    if (y.NumberSale == null)
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSP);
                                                        row7.GetCell(k).CellStyle = hStyle3;
                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSale.GetValueOrDefault());
                                                        row7.GetCell(k).CellStyle = hStyle1;
                                                    }
                                                }
                                                else if (y.StatusName == Business.Stock.StatusStock.SAFE.ToString())
                                                {
                                                    if (y.NumberSale == null)
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSP);
                                                        row7.GetCell(k).CellStyle = hStyle1;
                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue(y.NumberSale.GetValueOrDefault());
                                                        row7.GetCell(k).CellStyle = hStyle;
                                                    }
                                                }
                                                else
                                                {
                                                    if (product[i].GroupID.ToString().Contains(oulteList[item].GroupID.ToString()) && oulteList[item].GroupID.ToString().Length == 2)
                                                    {
                                                        row7.CreateCell(k).SetCellValue("");
                                                        row7.GetCell(k).CellStyle = hStyle22;

                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue("");
                                                    }
                                                }

                                            }

                                        }
                                    }
                                    else
                                    {

                                        if (product[i].IsNaN != null)
                                        {
                                            if (product[i].IsNaN.Split('|').ToList().Where(p => p == oulteList[item].GroupID.ToString()).Count() > 0)
                                            {
                                                row7.CreateCell(k).SetCellValue("");
                                                row7.GetCell(k).CellStyle = hStyle22;
                                            }
                                            else
                                            {
                                                row7.CreateCell(k).SetCellValue("");
                                            }
                                        }
                                        else
                                        {
                                            row7.CreateCell(k).SetCellValue("");
                                        }
                                    }
                                    k++;
                                }
                                row7.CreateCell(24).SetCellValue(simple.FirstOrDefault().Note);
                            }
                            else if (simple.Where(p => p.DateStock.Date == x.Date).Count() > 0)
                            {
                                var temp = simple.Where(p => p.DateStock.Date == x.Date);
                                if (x.DayOfWeek.ToString() == "Sunday")
                                {
                                    row7.CreateCell(6).SetCellValue(culture.DateTimeFormat.GetDayName(temp.FirstOrDefault().DateStock.DayOfWeek) + "-" + temp.FirstOrDefault().DateStock.ToString("dd/MM/yyyy"));
                                    row7.GetCell(6).CellStyle = cellStyle;
                                    row7.GetCell(6).CellStyle = hStyle2;
                                }
                                else
                                {
                                    row7.CreateCell(6).SetCellValue(culture.DateTimeFormat.GetDayName(temp.FirstOrDefault().DateStock.DayOfWeek) + "-" + temp.FirstOrDefault().DateStock.ToString("dd/MM/yyyy"));
                                    row7.GetCell(6).CellStyle = cellStyle;
                                }
                                for (int i = 0; i < product.Count(); i++)
                                {
                                    var _detailproduct = temp.Where(p => p.ProductID == product[i].ID);
                                    if (_detailproduct.Count() > 0)
                                    {
                                        foreach (var z in _detailproduct)
                                        {
                                            if (product[i].ProductName == z.ProductName)
                                            {

                                                if (z.StatusName == Business.Stock.StatusStock.OOS.ToString())
                                                {
                                                    if (z.NumberSale == null)
                                                    {
                                                        row7.CreateCell(k).SetCellValue(z.NumberSP);
                                                        row7.GetCell(k).CellStyle = hStyle2;
                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue(z.NumberSale.GetValueOrDefault());
                                                        row7.GetCell(k).CellStyle = hStyle2;
                                                    }
                                                }
                                                else
                                                {
                                                    if (z.NumberSale == null)
                                                    {
                                                        row7.CreateCell(k).SetCellValue(z.NumberSP);
                                                    }
                                                    else
                                                    {
                                                        row7.CreateCell(k).SetCellValue(z.NumberSale.GetValueOrDefault());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {

                                        if (product[i].IsNaN != null)
                                        {
                                            if (product[i].IsNaN.Split('|').ToList().Where(p => p == oulteList[item].GroupID.ToString()).Count() > 0)
                                            {
                                                row7.CreateCell(k).SetCellValue("");
                                                row7.GetCell(k).CellStyle = hStyle22;
                                            }
                                            else
                                            {
                                                row7.CreateCell(k).SetCellValue("");
                                            }
                                        }
                                        else
                                        {
                                            row7.CreateCell(k).SetCellValue("");
                                        }
                                    }
                                    k++;
                                }
                                row7.CreateCell(24).SetCellValue(simple.FirstOrDefault().Note);
                            }
                            else
                            {
                                if (x.DayOfWeek.ToString() == "Sunday")
                                {
                                    row7.CreateCell(6).SetCellValue(culture.DateTimeFormat.GetDayName(x.DayOfWeek) + "-" + x.ToString("dd/MM/yyyy"));
                                    row7.GetCell(6).CellStyle = cellStyle;
                                    row7.GetCell(6).CellStyle = hStyle2;
                                }
                                else
                                {
                                    row7.CreateCell(6).SetCellValue(culture.DateTimeFormat.GetDayName(x.DayOfWeek) + "-" + x.ToString("dd/MM/yyyy"));
                                    row7.GetCell(6).CellStyle = cellStyle;
                                }
                                row7.CreateCell(k).SetCellValue("");
                            }
                            u++;
                            row7 = sheet_Report.CreateRow(item + 2 + u);
                        }
                     
                    }
                    else
                    {
                        u--;
                        IRow row7 = sheet_Report.CreateRow(item + 2 + u);
                        row7.CreateCell(0).SetCellValue(v);
                        row7.CreateCell(1).SetCellValue(oulteList[item].Region.Type);
                        row7.CreateCell(2).SetCellValue(oulteList[item].City);
                        row7.CreateCell(3).SetCellValue(oulteList[item].Chanel.ChanelName);
                        row7.CreateCell(4).SetCellValue(oulteList[item].OutletName);
                        row7.CreateCell(5).SetCellValue(oulteList[item].SaleName);
                        row7.CreateCell(6).SetCellValue("");
                        int z = 7;
                        foreach (var x in product)
                        {
                            if (x.IsNaN != null)
                            {
                                if (x.IsNaN.Split('|').ToList().Where(p => p == oulteList[item].GroupID.ToString()).Count() > 0)
                                {
                                    row7.CreateCell(z).SetCellValue("");
                                    row7.GetCell(z).CellStyle = hStyle22;
                                }
                                else
                                {
                                    row7.CreateCell(z).SetCellValue("");
                                }
                            }
                            else
                            {
                                row7.CreateCell(z).SetCellValue("");
                            }
                            z++;
                        }
                        u++;
                        row7 = sheet_Report.CreateRow(item + 2 + u);
                    }
                    v++;
                }

                sheet_Report.ForceFormulaRecalculation = true;
                MemoryStream output = new MemoryStream();
                string downloadFileName = "Report_Stock_Daily" + " " + DateTime.Now.ToString("dd/MM/yyyy") + ".xls";
                templateWorkbook.Write(output);
                return File(output.ToArray(), "application/vnd.ms-excel", downloadFileName);
            }
        }
    }
}



