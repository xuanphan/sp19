﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Notification
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string _messageSystemError,string _regionAttendance, string _listoutlet, int _pProjectID, int _pOutletType, int _pUserID)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                List<string> ListToken = new List<string>();
                string[] arayID = _listoutlet.Split('|');

                //có outlet gửi
                if (_listoutlet != "")
                {
                    Notification notification = new Notification();
                    notification.NotificationCode = DateTime.Now.ToString("ddMMyyyyHHmmsstt");
                    notification.NotificationContent = this.NotificationContent; ;
                    notification.NotificationTitle = this.NotificationTitle;
                    notification.IsToMutiOutlet = true;
                    notification.OutletTypeID = _pOutletType;
                    notification.ProjectID = _pProjectID;
                    notification.CreatedBy = _pUserID;
                    notification.CreatedDateTime = DateTime.Now;
                    notification.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    notification.RowVersion = 1;
                    context.Notifications.InsertOnSubmit(notification);
                    context.SubmitChanges();

                    var queryOutletTotal = from item in context.Outlets
                                        select item;
                    var listOutletTotal = queryOutletTotal.ToList();
                    for (int i = 0; i < arayID.Count(); i++)
                    {

                        if (arayID[i].ToString() != String.Empty)
                        {
                            Notification_To_Outlet simple = new Notification_To_Outlet();
                            simple.NotificationID = notification.ID;
                            simple.OutletID = int.Parse(arayID[i]);
                            simple.CreatedBy = _pUserID;
                            simple.CreatedDateTime = DateTime.Now;
                            simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            simple.RowVersion = 1;
                            context.Notification_To_Outlets.InsertOnSubmit(simple);
                            context.SubmitChanges();

                            var tem = listOutletTotal.Where(item=> item.ID == int.Parse(arayID[i])).FirstOrDefault();
                            ListToken.Add(tem.DeviceToken);
                        }
                    }
                    //gui thông báo
                    FCM.SendSMSToList(ListToken, this.NotificationTitle, this.NotificationContent);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                    // gửi outlet theo vùng
                else if (_regionAttendance != null && _regionAttendance != "Tất Cả")
                {
                    Notification notification = new Notification();
                    notification.NotificationCode = DateTime.Now.ToString("ddMMyyyyHHmmsstt");
                    notification.NotificationContent = this.NotificationContent; ;
                    notification.NotificationTitle = this.NotificationTitle;
                    notification.IsToMutiOutlet = true;
                    notification.OutletTypeID = _pOutletType;
                    notification.ProjectID = _pProjectID;
                    notification.CreatedBy = _pUserID;
                    notification.CreatedDateTime = DateTime.Now;
                    notification.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    notification.RowVersion = 1;
                    context.Notifications.InsertOnSubmit(notification);
                    context.SubmitChanges();

                    var queryOutlet = from item in context.Outlets
                                      where item.Region.Type == _regionAttendance
                                      && item.Status != 2
                                      && item.Region.Status != 2
                                      select item;
                    var listOutlet = queryOutlet.ToList();
                    List<Notification_To_Outlet> listIP = new List<Notification_To_Outlet>();
                    foreach (var item in listOutlet)
                    {
                        Notification_To_Outlet simple = new Notification_To_Outlet();
                        simple.NotificationID = notification.ID;
                        simple.OutletID = item.ID;
                        simple.CreatedBy = _pUserID;
                        simple.CreatedDateTime = DateTime.Now;
                        simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                        simple.RowVersion = 1;
                        listIP.Add(simple);
                        ListToken.Add(item.DeviceToken);
                    }
                    context.Notification_To_Outlets.InsertAllOnSubmit(listIP);
                    context.SubmitChanges();
                    FCM.SendSMSToList(ListToken, this.NotificationTitle, this.NotificationContent);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    this.IsToMutiOutlet = false;
                    this.OutletTypeID = _pOutletType;
                    this.ProjectID = _pProjectID;
                    this.CreatedBy = _pUserID;
                    this.CreatedDateTime = DateTime.Now;
                    this.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    this.RowVersion = 1;
                    context.Notifications.InsertOnSubmit(this);
                    context.SubmitChanges();
                    var query = from item in context.Outlets
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.OutletTypeID == this.OutletTypeID
                                && item.ProjectID == _pProjectID
                                select item.DeviceToken;
                    var _listtoken = query.ToList();
                    ListToken = ListToken.Union(_listtoken).ToList();
                    //gui thông báo
                    FCM.SendSMSToList(ListToken, this.NotificationTitle, this.NotificationContent);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(string _pAppCode, int pProjectID, int pOutletTypeID, int pOutletID, ref NotificationResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu thông báo chung  theo loại outlet và project , và biến IsToMutiOutlet = false
                    var query = from item in context.Notifications
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.ProjectID == pProjectID
                                  && item.OutletTypeID.Value == pOutletTypeID
                                  && item.IsToMutiOutlet == false
                                orderby item.CreatedDateTime descending
                                select new SimpleNotification
                                (
                                    item.ID,
                                    item.NotificationCode,
                                    item.NotificationTitle,
                                   item.NotificationContent,
                                   item.CreatedDateTime
                                );

                    // đối với những thông báo IsToMutiOutlet = true thì sẽ là những thông báo gửi cho những outlet được chọn 
                    // lấy dữ liệu thông báo riêng cho outlet
                    var query2 = from item in context.Notification_To_Outlets
                                 where item.OutletID == pOutletID
                                 orderby item.CreatedDateTime descending
                                 select new SimpleNotification
                                 (
                                     item.Notification.ID,
                                     item.Notification.NotificationCode,
                                     item.Notification.NotificationTitle,
                                    item.Notification.NotificationContent,
                                    item.CreatedDateTime
                                 );
                    // gán giá trị cho phần trả về
                    result.Data = new List<SimpleNotification>();
                    result.Data.AddRange(query.ToList());
                    result.Data.AddRange(query2.ToList());
                    // sắp xếp theo thời gian giảm dần và chỉ lấy 10 thông báo gần nhất
                    result.Data = result.Data.OrderByDescending(o => o.CreatedDateTime).Take(10).ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchAll(ref string _messageSystemError, int _pProjectID, int _pOutletType, ref List<Notification> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Notifications
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select item;
                list = query.ToList().OrderByDescending(p => p.CreatedDateTime.Date).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;

            }
            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static List<Outlet> GetOutletSend(int _pNotificationid)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Notification_To_Outlets
                            join outlet in context.Outlets on item.OutletID equals outlet.ID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.NotificationID == _pNotificationid)
                            select outlet;
                var list = query.ToList().OrderByDescending(p => p.CreatedDateTime.Date).ToList();
                return list;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetSimple(ref string _messageSystemError, string _pID, ref Notification simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Notifications
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.ID == int.Parse(_pID)
                            select item;
                if (query.Count() > 0)
                {
                    simple = query.FirstOrDefault();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;

            }
            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public void SendNotice(string deviceId, string message)
        {

        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE Update(ref string _messageError, string _pID, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string[] arayID = _pID.Split('|');

                for (int i = 0; i < arayID.Count(); i++)
                {
                    if (arayID[i].ToString() != String.Empty)
                    {
                        //xem brand set này thuộc hiện tại k?
                        var query = from item in context.Notifications
                                    where item.ID == int.Parse(arayID[i])
                                    select item;
                        // không có ở hiện tại thì tạo và update trạng thái
                        if (query.Count() > 0)
                        {
                            //xác nhận
                            var simple = query.FirstOrDefault();
                            simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE;
                            simple.LastUpdatedBy = _pUserID;
                            simple.LastUpdatedDateTime = DateTime.Now;
                            simple.RowVersion += 1;
                            // update set hiện tại
                            context.SubmitChanges();
                        }
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE Update(ref string _messageSystemError, string _content, string _idnotification, string _title, int _pUserID)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                var query = from item in context.Notifications
                            where item.ID == int.Parse(_idnotification)
                            select item;
                if (query.Count() > 0)
                {
                    var simple = query.FirstOrDefault();
                    simple.NotificationTitle = _title;
                    simple.NotificationContent = _content;
                    simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    simple.LastUpdatedBy = _pUserID;
                    simple.LastUpdatedDateTime = DateTime.Now;
                    simple.RowVersion += 1;
                    context.SubmitChanges();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class SimpleNotification
    {
        private long _ID;
        public long ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _NotificationCode;
        public string NotificationCode
        {
            get { return _NotificationCode; }
            set { _NotificationCode = value; }
        }

        private string _NotificationTitle;
        public string NotificationTitle
        {
            get { return _NotificationTitle; }
            set { _NotificationTitle = value; }
        }
        private string _NotificationContent;
        public string NotificationContent
        {
            get { return _NotificationContent; }
            set { _NotificationContent = value; }
        }


        private string _CreatedDateTime = String.Empty;
        public string CreatedDateTime
        {
            get { return _CreatedDateTime; }
            set
            {
                _CreatedDateTime = Common.ConvertUtils.ToString(value);
            }
        }

        public SimpleNotification() { }

        public SimpleNotification(long ID, string NotificationCode, string NotificationTitle, string NotificationContent, DateTime CreatedDateTime)
        {
            this.ID = ID;
            this.NotificationCode = NotificationCode;
            this.NotificationTitle = NotificationTitle;
            this.NotificationContent = NotificationContent;
            this.CreatedDateTime = CreatedDateTime.ToString("dd/MM/yyyy HH:mm:ss");
        }
    }
}
