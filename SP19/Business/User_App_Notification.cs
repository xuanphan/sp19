﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class User_App_Notification
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW add(User_App_Notification tempNotifi)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                context.User_App_Notifications.InsertOnSubmit(tempNotifi); // thêm
                context.SubmitChanges();// thực thi câu lệnh
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }



        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT getByID(string pAppCode, int userID, ref GetNotificationForUserResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() != Common.Global.AppCode.ToLower().Trim())
                {
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }

                // lấy dữ liệu
                var query = from item in context.User_App_Notifications
                            where item.UserID == userID
                            orderby item.ID descending
                            select new SimpleUserAppNotificationWS
                            {
                                ID = item.ID,
                                Title = item.Title,
                                Message = item.Message,
                                CreatedDateTime = Common.ConvertUtils.ConvertDatetimeToStringReport(item.CreatedDateTime)
                            };
                // gán giá trị cho phần trả về
                result.Data = query.ToList();
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }

    public class SimpleUserAppNotificationWS
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string CreatedDateTime { get; set; }
    }
}
