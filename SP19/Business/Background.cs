﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Background
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(string _pAppCode, string _pProjectID, ref BackgroundResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu theo _pProjectID 
                    var query = from item in context.Backgrounds
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.ProjectID == int.Parse(_pProjectID)
                                select new SimpleBackground
                                {
                                    ID = item.ID,
                                    ProjectID = item.ProjectID,
                                    BrandID = item.BrandID,
                                    BGLayout = item.BGLayout,
                                    BGButton = item.BGButton,
                                    BGRing = item.BGRing,
                                    BGArrow = item.BGArrow,
                                    ColorBorder = item.ColorBorder
                                };

                    // gán giá trị cho phần trả về
                    result.Data =  query.ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }
    public class SimpleBackground
    {
        private int _ID;
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private int _ProjectID;
        public int ProjectID
        {
            get { return _ProjectID; }
            set { _ProjectID = value; }
        }

        private int _BrandID;
        public int BrandID
        {
            get { return _BrandID; }
            set { _BrandID = value; }
        }
        private string _BGLayout;
        public string BGLayout
        {
            get { return _BGLayout; }
            set { _BGLayout = value; }
        }
       
        private string _BGButton;
        public string BGButton
        {
            get { return _BGButton; }
            set { _BGButton = value; }
        }
        private string _BGRing;
        public string BGRing
        {
            get { return _BGRing; }
            set { _BGRing = value; }
        }
        private string _BGArrow;
        public string BGArrow
        {
            get { return _BGArrow; }
            set { _BGArrow = value; }
        }
        private string _ColorBorder;
        public string ColorBorder
        {
            get { return _ColorBorder; }
            set { _ColorBorder = value; }
        }

        public SimpleBackground() { }

        public SimpleBackground(int ID, int ProjectID, int BrandID,string BGLayout, string BGButton, string BGRing, string BGArrow, string ColorBorder)
        {
            
            this.ID = ID;
            this.ProjectID = ProjectID;
            this.BrandID = BrandID;
            this.BGLayout = BGLayout;
            this.BGButton = BGButton;
            this.BGRing = BGRing;
            this.BGArrow = BGArrow;
            this.ColorBorder = ColorBorder;
        }
    }
}
