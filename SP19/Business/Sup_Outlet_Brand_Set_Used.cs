﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Sup_Outlet_Brand_Set_Used
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW add(string pAppCode, List<Sup_Outlet_Brand_Set_Used> list, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() != Common.Global.AppCode.ToLower().Trim())
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
                try
                {
                    // khởi tạo transaction
                    context.Connection.Open();
                    context.Transaction = context.Connection.BeginTransaction();

                    context.Sup_Outlet_Brand_Set_Useds.InsertAllOnSubmit(list);// thêm list vào db
                    context.SubmitChanges(); // thực thi câu lệnh

                    context.Transaction.Commit(); // hoàn thành transaction
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                catch (Exception ex)
                {
                    context.Transaction.Rollback();// trả lại ban đầu nếu trong quá trình xử lý có lỗi
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + " " + ex.Message;
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                }
                finally
                {
                    context.Connection.Close();
                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT update()
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Apps
                            where item.UserType == "SUP"
                            select item;
                var listTemp = query.ToList();
                foreach (var item in listTemp)
                {
                    var listOutlet = item.Sup_Outlets.Where(p => p.Status != 2).ToList();
                    if (listOutlet.Count() > 0)
                    {
                        foreach (var outlet in listOutlet)
                        {
                            var listBrand = outlet.Outlet.Outlet_Brands.Where(p => p.Status != 2).ToList();
                            foreach (var brand in listBrand)
                            {
                                if (brand.Brand.Brand_Sets.Count() > 0)
                                {
                                    // khởi tạo object thêm
                                    Sup_Outlet_Current_Set entitySup = new Sup_Outlet_Current_Set();
                                    // gán dữ liệu
                                    entitySup.SupID = item.ID;
                                    entitySup.OutletID = outlet.OutletID;
                                    entitySup.BrandSetID = brand.Brand.Brand_Sets.Where(p => p.IsDefault == true && p.OutletTypeID == outlet.Outlet.OutletTypeID).FirstOrDefault().ID;
                                    entitySup.CreatedBy = 1;
                                    entitySup.CreatedDateTime = DateTime.Now.Date;
                                    entitySup.Status = 1;
                                    entitySup.RowVersion = 1;
                                    // kiểm tra xem Sup_Outlet_Current_Set có tồn tại dữ liệu thêm này chưa -- nếu chưa thì add vào db -- có rồi thì thôi
                                    if (context.Sup_Outlet_Current_Sets.Where(p => p.SupID == entitySup.SupID && p.OutletID == entitySup.OutletID && p.BrandSetID == entitySup.BrandSetID).Count() == 0)
                                    {
                                        context.Sup_Outlet_Current_Sets.InsertOnSubmit(entitySup); // thêm 
                                        context.SubmitChanges(); // thực thi câu lệnh
                                    }

                                }
                            }
                        }
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }
}
