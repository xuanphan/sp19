﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.ERROR_CODE;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Business
{
    public partial class User
    {
        public class UserTypes
        {
            public static string Admin = "ADMIN";
            public static string Sale = "SALE";
            public static string SaleSup = "SALESUP";
            public static string SP = "SP";
            public static string SUP = "SUP";
            public static string KHO = "KHO";
        }


        public IEnumerable<ComboItem> GetSaleForRegion(string _pRegionID, int _pProjectID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Sale_Outlets
                            where (item.Outlet.Region.Type == _pRegionID || _pRegionID == "Tất Cả")
                            && item.Outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                           && (item.Outlet.ProjectID == _pProjectID)
                           && (item.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select new ComboItem
                            {
                                ID = item.SaleID,
                                Value = item.SaleID.ToString(),
                                Text = item.User_Web.FullName
                            };
                List<ComboItem> list = new List<ComboItem>();
                var _temp = query.ToList().Distinct(new Business.Region.ComparerRegionData()).OrderBy(p => p.Text).ToList();
                list.AddRange(_temp.ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }


        public Common.ERROR_CODE.ERROR_CODE_LOGIN LoginSP(string _pUserName, string _pPassword, string _pUserType, string _pAppCode, string token, string deviceID, ref LoginResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    //Lấy user theo UserName
                    var query = from item in context.User_Apps
                                where item.UserName == _pUserName
                                && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                && item.UserType == _pUserType
                                select item;
                    var queryBrandSetDetail = from item in context.Brand_Set_Details
                                              select item;
                    var listBrandSetDetail = queryBrandSetDetail.ToList();
                    //Lấy user
                    if (query.Count() == 0)
                    {
                        //------------------Kết quả trả về -- sai user

                        result.Data = null;

                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_USERNAME;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_USERNAME);
                        return ERROR_CODE_LOGIN.WRONG_USERNAME;
                    }
                    // gán dữ liệu user
                    var firstUser = query.FirstOrDefault();

                    //Kiểm tra mật khẩu
                    if (firstUser.PassWord != _pPassword)
                    {
                        //------------------Kết quả trả về -- sai mật khẩu
                        result.Data = null;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_PASS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_PASS);
                        return ERROR_CODE_LOGIN.WRONG_PASS;
                    }

                    //get thông tin team login
                    // kiểm tra có phân công cho team chưa
                    if (firstUser.TeamOutletID != 0) // nếu có tiếp tục
                    {
                        // kiểm tra DeviceID của dt có trùng với DeviceID truyền lên
                        Outlet tempoutlet = firstUser.Team_Outlet.Outlet;
                        if (deviceID != "")
                        {
                            if (tempoutlet.DeviceID != null) // nếu DeviceID tồn tại trước đó thì so sánh
                            {
                                // nếu DeviceID có sẵn == DeviceID truyền lên thì ok đăng nhập
                                if (tempoutlet.DeviceID == deviceID) 
                                {
                                    // update các trạng thái cho outlet
                                    tempoutlet.IsOnline = true;
                                    tempoutlet.DeviceToken = token;
                                    context.SubmitChanges();
                                }
                                else // nếu không
                                {
                                    // kiểm tra em có đang online không
                                    if (tempoutlet.IsOnline == true)  // nếu có thì báo lỗi . đang đăng nhập vào máy khác
                                    {
                                        // có máy khác đnag đăng nhập
                                        result.Data = null;
                                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.IS_LOGIN;
                                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.IS_LOGIN);
                                        return ERROR_CODE_LOGIN.IS_LOGIN;
                                    }
                                    else // nếu không thì ok
                                    {
                                        // update các trạng thái cho outlet
                                        tempoutlet.DeviceID = deviceID;
                                        tempoutlet.IsOnline = true;
                                        tempoutlet.DeviceToken = token;
                                        context.SubmitChanges();
                                    }
                                }
                            }
                            else // nếu không có nghĩa là đăng nhập lần đầu => ok
                            {
                                // update các trạng thái cho outlet
                                tempoutlet.DeviceID = deviceID;
                                tempoutlet.IsOnline = true;
                                tempoutlet.DeviceToken = token;
                                context.SubmitChanges();
                            }
                        }

                        // update token cho user
                        firstUser.DeviceToken = token;
                        context.SubmitChanges();

                        // thêm lịch sử đăng nhập
                        try
                        {
                            Log_LoginSP tempHis = new Log_LoginSP();
                            tempHis.OutletID = firstUser.Team_Outlet.Outlet.ID;
                            tempHis.Token = token;
                            tempHis.DeviceID = deviceID;
                            tempHis.TimeLogin = DateTime.Now;
                            context.Log_LoginSPs.InsertOnSubmit(tempHis);
                            context.SubmitChanges();
                        }
                        catch (Exception ex)
                        {

                        }

                        //var currentBrand = context.Outlet_Current_Sets.Where(p => p.OutletID == firstUser.Team_Outlet.Outlet.ID).Select(o => new SimpleCurrentBrandSet { BrandID = o.Brand_Set.BrandID, BrandSetID = o.BrandSetID, ListCurrentGift = new List<SimpleCurrentGift>() }).ToList();
                        //var currentGift = context.Outlet_Current_Gifts.Where(p => p.OutletID == firstUser.Team_Outlet.Outlet.ID).ToList();

                        //------------------Kết quả trả về - thành công -- 
                        result.Data = new SimpleUser();
                        result.Data.ProjectID = firstUser.Team_Outlet.Outlet.Project.ID;
                        result.Data.ProjectName = firstUser.Team_Outlet.Outlet.Project.ProjectName;
                        result.Data.ProjectBackground = firstUser.Team_Outlet.Outlet.Project.ProjectBackground;
                        result.Data.Name = firstUser.FullName;
                        result.Data.UserID = firstUser.ID;
                        result.Data.TeamID = firstUser.Team_Outlet.TeamID;
                        result.Data.TeamOutletID = firstUser.Team_Outlet.ID;
                        result.Data.EntityOutlet = new SimpleOutlet();

                        result.Data.EntityOutlet.OutletID = firstUser.Team_Outlet.Outlet.ID;
                        result.Data.EntityOutlet.OutletName = firstUser.Team_Outlet.Outlet.OutletName;
                        result.Data.EntityOutlet.City = firstUser.Team_Outlet.Outlet.City;
                        result.Data.EntityOutlet.OutletAddress = firstUser.Team_Outlet.Outlet.OutletAddress;
                        result.Data.EntityOutlet.District = firstUser.Team_Outlet.Outlet.District;
                        result.Data.EntityOutlet.OutletTypeID = firstUser.Team_Outlet.Outlet.OutletTypeID;
                        result.Data.EntityOutlet.Promotion = firstUser.Team_Outlet.Outlet.Promotion;
                        result.Data.EntityOutlet.ChanelID = firstUser.Team_Outlet.Outlet.ChanelID == 36 ? 22 : firstUser.Team_Outlet.Outlet.ChanelID;
                        //result.Data.EntityOutlet.IsLuckyMega = firstUser.Team_Outlet.Outlet.Chanel.ChanelName == "Mega Market" ? true : false;
                        result.Data.EntityOutlet.IsLuckyMega = false; // KHÔNG CHẠY QUAY SỐ MEGA NỮA
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS);
                        return ERROR_CODE_LOGIN.SUCCESS;
                    }
                    else // nếu chưa - báo lỗi chưa phân công team
                    {
                        // gán giá trị cho phần trả về
                        result.Data = null;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.NOT_ASSIGN_TEAM;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.NOT_ASSIGN_TEAM);
                        return ERROR_CODE_LOGIN.NOT_ASSIGN_TEAM;
                    }
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.NOT_EXIST_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.NOT_EXIST_APP);
                    return ERROR_CODE_LOGIN.NOT_EXIST_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR;
                result.Description = "Thất bại: " + ex.Message;
                result.Data = null;
                return ERROR_CODE_LOGIN.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public ERROR_CODE_LOGIN LoginSUP(string username, string pass, string usertype, string appcode, string token, ref LoginSUPResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (appcode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // kiểm tra username và pass có đúng
                    var query = from item in context.User_Apps
                                where item.UserName == username
                                && item.PassWord == pass
                                && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                && item.UserType == usertype
                                select item;
                    if (query.Count() == 0) // nếu không
                    {
                        //------------------Kết quả trả về -- sai user
                        // gán giá trị cho phần trả về
                        result.Data = null;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_USERNAME;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_USERNAME);
                        return ERROR_CODE_LOGIN.WRONG_USERNAME;
                    } 

                    // gán dữ liệu user
                    var firstUser = query.FirstOrDefault();
                    // cập nhật token
                    firstUser.DeviceToken = token;
                    context.SubmitChanges(); // thực thi câu lệnh

                    // lấy danh sách outlet thuộc quyền quản lý của SUP
                    var querySupOutlet = from item in context.Sup_Outlets
                                         where item.SupID == firstUser.ID
                                         && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                         select new SimpleOutlet
                                         {
                                             OutletID = item.OutletID,
                                             OutletName = item.Outlet.OutletName,
                                             City = item.Outlet.City,
                                             OutletAddress = item.Outlet.OutletAddress,
                                             District = item.Outlet.District,
                                             OutletTypeID = item.Outlet.OutletTypeID,
                                             Promotion = item.Outlet.Promotion,
                                             ChanelID = item.Outlet.ChanelID,
                                             //IsLuckyMega = item.Outlet.Chanel.ChanelName == "Mega Market" ? true : false
                                             IsLuckyMega = false
                                         };
                    // gán giá trị cho phần trả về
                    result.Data = new SimpleUserSUP();
                    result.Data.ProjectID = firstUser.Sup_Outlets.FirstOrDefault() != null ? firstUser.Sup_Outlets.FirstOrDefault().Outlet.ProjectID : 0;
                    result.Data.ProjectName = firstUser.Sup_Outlets.FirstOrDefault() != null ? firstUser.Sup_Outlets.FirstOrDefault().Outlet.Project.ProjectName : "";
                    result.Data.ProjectBackground = firstUser.Sup_Outlets.FirstOrDefault() != null ? firstUser.Sup_Outlets.FirstOrDefault().Outlet.Project.ProjectBackground : "";
                    result.Data.Name = firstUser.FullName;
                    result.Data.UserID = firstUser.ID;

                    result.Data.ListOutlet = new List<SimpleOutlet>();
                    result.Data.ListOutlet = querySupOutlet.ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS);
                    return ERROR_CODE_LOGIN.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.NOT_EXIST_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.NOT_EXIST_APP);
                    return ERROR_CODE_LOGIN.NOT_EXIST_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR)+ex.Message;
                result.Data = null;
                return ERROR_CODE_LOGIN.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }

        }


        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetSimple(ref string MessageError, long _id, ref ProfileEXT _simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Profiles
                            where item.ID == _id
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select new ProfileEXT
                            {
                                ProfileName = item.ProfileName,
                                ProfileCode = item.ProfileCode,
                                Avatar = item.Avatar,
                                ID = item.ID,
                                Phone = item.ProfilePhone,
                                Address = item.ProfileAddress,
                                FilePath = item.Avatar,
                                Experience = item.Experience,
                                Discipline = string.Join(",", context.Profile_Disciplines.Where(p => p.ProfileID == item.ID).Select(p => p.Description).ToList())
                            };

                _simple = query.FirstOrDefault();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetSimple(ref string MessageError, long _id, ref User_Web _simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Webs
                            where item.ID == _id
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select item;

                _simple = query.FirstOrDefault();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }


        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetSimpleEmergency(ref string MessageError, long _id, ref Profile_Emergency _simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Profiles
                            join profile_Emergencies in context.Profile_Emergencies on item.ID equals profile_Emergencies.ProfileID
                            where item.ID == _id
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select profile_Emergencies;

                _simple = query.ToList().OrderByDescending(p => p.CreatedDateTime.Date).FirstOrDefault();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Profile_Emergency GetSimpleEmergency(long _id, ref Profile_Emergency _simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Profiles
                            join profile_Emergencies in context.Profile_Emergencies on item.ID equals profile_Emergencies.ProfileID
                            where item.ID == _id
                            && item.CreatedDateTime.Date == DateTime.Now.Date
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select profile_Emergencies;

                _simple = query.ToList().OrderByDescending(p => p.CreatedDateTime.Date).FirstOrDefault();
                return _simple;
            }
            catch (Exception ex)
            {
                string MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public LoginInfo Login(string _pUserName, string _pPassword)
        {
            DatabaseDataContext context = null;
            try
            {
                Common.LoginInfo loginInfo = new Common.LoginInfo();
                loginInfo.UserName = _pUserName.ToLower().Trim();
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Webs
                            where item.UserName.Replace("\r\n", string.Empty) == _pUserName
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select item;
                var fuser = query.FirstOrDefault();
                if (fuser == null)
                {
                    loginInfo.LoginResult = Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_USERNAME;
                    throw new Common.ExceptionUtils.CustomException(AUTH_MESSAGE_STRING.UNVALID_LOGIN_USER);
                }

                // Kiểm tra mật khẩu

                if (fuser.PassWord != _pPassword)
                {
                    loginInfo.LoginResult = Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_PASS;
                    throw new Common.ExceptionUtils.CustomException(AUTH_MESSAGE_STRING.UNVALID_LOGIN_USER);
                }
                else
                {
                    // cập nhật thông tin login
                    loginInfo.LoginResult = Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS;
                    Common.UserInfo userInfo = new Common.UserInfo();
                    userInfo.DisplayName = fuser.UserName;
                    loginInfo.UserInfo = userInfo;
                    loginInfo.UserID = int.Parse(fuser.ID.ToString());
                    loginInfo.UserInfo.DisplayName = fuser.FullName;
                    // nếu là admin thì lấy projectID
                    //Admin theo OutletType
                    if (fuser.UserType.Trim() == UserTypes.Admin)
                    {
                        loginInfo.ProjectID = int.Parse(fuser.ProjectID.ToString());
                        loginInfo.OutletTypeID = fuser.OutletTypeID.GetValueOrDefault();
                        loginInfo.SaleID = -1;
                    }
                    //Sale không có outlettype vì nối bảng userSale
                    else if (fuser.UserType.Trim() == UserTypes.Sale)
                    {
                        loginInfo.ProjectID = int.Parse(fuser.ProjectID.ToString());
                        loginInfo.SaleID = fuser.ID;
                        loginInfo.OutletTypeID = -1;
                    }
                    //Danh sách siêu thị theo sup
                    else if (fuser.UserType.Trim() == UserTypes.SUP)
                    {
                        loginInfo.ProjectID = int.Parse(fuser.ProjectID.ToString());
                        loginInfo.SaleID = fuser.ID;
                        loginInfo.OutletTypeID = fuser.OutletTypeID.GetValueOrDefault();
                    }
                    //Danh sách siêu thị theo kho
                    else if (fuser.UserType.Trim() == UserTypes.KHO)
                    {
                        loginInfo.ProjectID = int.Parse(fuser.ProjectID.ToString());
                        loginInfo.SaleID = fuser.ID;
                        loginInfo.OutletTypeID = fuser.OutletTypeID.GetValueOrDefault();
                    }
                    //Danh sách siêu thị theo kho
                    else if (fuser.UserType.Trim() == UserTypes.SaleSup)
                    {
                        //lấy danh sách List SaleID
                        var querysale = from item in context.Sale_Sups
                                        where item.SaleSupID == fuser.ID
                                        select item.SaleID;
                        loginInfo.ProjectID = int.Parse(fuser.ProjectID.ToString());
                        loginInfo.SaleID = 0;
                        loginInfo.ListSaleID = string.Join("|", querysale.ToList());
                        loginInfo.SaleSupID = fuser.ID;
                        loginInfo.OutletTypeID = -1;
                    }
                    //Lấy hết Outlettype
                    else
                    {
                        // admin full quyền
                        loginInfo.ProjectID = int.Parse(fuser.ProjectID.ToString());
                        loginInfo.SaleID = 0;
                        loginInfo.OutletTypeID = 0;
                        loginInfo.SaleSupID = 0;
                    }
                    //Cập nhật thông tin Login
                    Business.User_Web userTemp = context.User_Webs.Single(p => p.UserName.Replace("\r\n", string.Empty) == _pUserName);
                    context.SubmitChanges();
                }
                return loginInfo;

            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
        }
        public LoginInfo LoginAppWeb(string _pUserName, string _pPassword)
        {
            DatabaseDataContext context = null;
            try
            {
                Common.LoginInfo loginInfo = new Common.LoginInfo();
                loginInfo.UserName = _pUserName.ToLower().Trim();
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Apps
                            where item.UserName.Replace("\r\n", string.Empty) == _pUserName
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select item;
                var fuser = query.FirstOrDefault();
                if (fuser == null)
                {
                    loginInfo.LoginResult = Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_USERNAME;
                    throw new Common.ExceptionUtils.CustomException(AUTH_MESSAGE_STRING.UNVALID_LOGIN_USER);
                }

                // Kiểm tra mật khẩu

                if (fuser.PassWord != _pPassword)
                {
                    loginInfo.LoginResult = Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_PASS;
                    throw new Common.ExceptionUtils.CustomException(AUTH_MESSAGE_STRING.UNVALID_LOGIN_USER);
                }
                else
                {
                    // cập nhật thông tin login
                    loginInfo.LoginResult = Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS;
                    Common.UserInfo userInfo = new Common.UserInfo();
                    userInfo.DisplayName = fuser.UserName;
                    loginInfo.UserInfo = userInfo;
                    loginInfo.UserID = int.Parse(fuser.ID.ToString());
                    loginInfo.UserInfo.DisplayName = fuser.FullName;
                    // nếu là admin thì lấy projectID
                    //Admin theo OutletType

                    //Danh sách siêu thị theo sup
                    if (fuser.UserType.Trim() == UserTypes.SUP)
                    {
                        loginInfo.ProjectID = fuser.ProjectID.GetValueOrDefault();
                        loginInfo.SaleID = fuser.ID;
                    }
                    //Danh sách siêu thị theo kho
                    else if (fuser.UserType.Trim() == UserTypes.KHO)
                    {
                        loginInfo.ProjectID = fuser.ProjectID.GetValueOrDefault();
                        loginInfo.SaleID = fuser.ID;

                    }
                }
                return loginInfo;

            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
        }

        public static IEnumerable<string> GetRoleCodes(long userID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.ConnectionInfo.ConnectionStringSQL);
                var result = from user in context.User_Webs
                             join userrole in context.User_Roles on user.ID equals userrole.UserID
                             join role in context.Roles on userrole.RoleID equals role.ID
                             where user.ID == userID
                             select role.Name;
                return result.ToList();
            }
            catch (Exception ex)
            {
                throw new Common.ExceptionUtils.CustomException(ex.Message);
            }
        }

        public static IEnumerable<string> GetMenuCodes(long userID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.ConnectionInfo.ConnectionStringSQL);
                var userRoles = from user in context.User_Webs
                                join userrole in context.User_Roles on user.ID equals userrole.UserID
                                join role in context.Roles on userrole.RoleID equals role.ID
                                where user.ID == userID
                                select role.ID;

                var userMenus = from menu in context.Menus
                                join rolemenu in context.Role_Menus on menu.ID equals rolemenu.MenuID
                                join role in context.Roles on rolemenu.RoleID equals role.ID
                                where role.ID == userRoles.FirstOrDefault()
                                select menu.Name;

                return userMenus.ToList();
            }
            catch (Exception ex)
            {
                throw new Common.ExceptionUtils.CustomException(ex.Message);
            }
        }

        public static IEnumerable<string> GetRoleCodesAppWeb(long userID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.ConnectionInfo.ConnectionStringSQL);
                var result = from user in context.User_Apps
                             join userrole in context.User_Role_AppWebs on user.ID equals userrole.UserID
                             join role in context.Roles on userrole.RoleID equals role.ID
                             where user.ID == userID
                             select role.Name;
                return result.ToList();
            }
            catch (Exception ex)
            {
                throw new Common.ExceptionUtils.CustomException(ex.Message);
            }
        }

        public static IEnumerable<string> GetMenuCodesAppWeb(long userID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.ConnectionInfo.ConnectionStringSQL);
                var userRoles = from user in context.User_Apps
                                join userrole in context.User_Role_AppWebs on user.ID equals userrole.UserID
                                join role in context.Roles on userrole.RoleID equals role.ID
                                where user.ID == userID
                                select role.ID;

                var userMenus = from menu in context.Menus
                                join rolemenu in context.Role_Menus on menu.ID equals rolemenu.MenuID
                                join role in context.Roles on rolemenu.RoleID equals role.ID
                                where role.ID == userRoles.FirstOrDefault()
                                select menu.Name;

                return userMenus.ToList();
            }
            catch (Exception ex)
            {
                throw new Common.ExceptionUtils.CustomException(ex.Message);
            }
        }
        public ERROR_CODE_CHANGE_PASS_WORD ChangePassWord(string pUserID, string pAppCode, string pOldPass, string pNewPass, ref ChangePassResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu user
                    var query = from item in context.User_Apps
                                where item.ID == int.Parse(pUserID)
                                 && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                select item;
                    if (query.Count() > 0) // nếu có
                    {
                        var temp = query.FirstOrDefault();
                        // kiểm tra mật khẩu cũ đug không
                        if (temp.PassWord == pOldPass) // nếu đúng cập nhật trả về kết quả
                        {
                            temp.PassWord = pNewPass;
                            context.SubmitChanges(); // thực thi câu lệnh

                            // gán giá trị cho phần trả về
                            result.Data = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.SUCCESS);
                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.SUCCESS;
                            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.SUCCESS);
                            return ERROR_CODE_CHANGE_PASS_WORD.SUCCESS;
                        }
                        else // sai thì thông báo sai mật khẩu cũ
                        {
                            // gán giá trị cho phần trả về
                            result.Data = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.WRONG_PASS);
                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.WRONG_PASS;
                            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.WRONG_PASS);
                            return ERROR_CODE_CHANGE_PASS_WORD.WRONG_PASS;
                        }
                    } // không có thì thông báo không có data 
                    else
                    {
                        // gán giá trị cho phần trả về
                        result.Data = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.NOT_EXIST_DATA);
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.NOT_EXIST_DATA;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.NOT_EXIST_DATA);
                        return ERROR_CODE_CHANGE_PASS_WORD.SYSTEM_ERROR;
                    }
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.NOT_EXIST_APP);
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.NOT_EXIST_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.NOT_EXIST_APP);
                    return ERROR_CODE_CHANGE_PASS_WORD.NOT_EXIST_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.SYSTEM_ERROR);
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD.SYSTEM_ERROR)+ex.Message;
                return ERROR_CODE_CHANGE_PASS_WORD.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close() ;
                context = null;
                GC.Collect();
            }
        }



        public ERROR_CODE_LOGIN LoginKho(string _pUserName, string _pPassword, string _pUserType, string _pAppCode, string token, ref LoginKhoResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    //Lấy user theo UserName
                    var query = from item in context.User_Apps
                                where item.UserName == _pUserName
                                && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                && item.UserType == _pUserType
                                select item;
                    var queryProject = from item in context.Projects
                                       select item;

                    //Lấy user
                    if (query.Count() == 0)
                    {
                        //------------------Kết quả trả về -- sai user

                        result.Data = null;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_USERNAME;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_USERNAME);
                        return ERROR_CODE_LOGIN.WRONG_USERNAME;
                    }
                    var firstUser = query.FirstOrDefault();

                    //Kiểm tra mật khẩu
                    if (firstUser.PassWord != _pPassword)
                    {
                        //------------------Kết quả trả về -- sai mật khẩu
                        result.Data = null;

                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_PASS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_PASS);
                        return ERROR_CODE_LOGIN.WRONG_PASS;
                    }
                    firstUser.DeviceToken = token;
                    var project = queryProject.Where(p => p.ID == firstUser.ProjectID).FirstOrDefault();
                    context.SubmitChanges();
                    //------------------Kết quả trả về - thành công -- sup
                    // gán giá trị cho phần trả về
                    result.Data = new SimpleUserKHO();
                    result.Data.ProjectID = project.ID;
                    result.Data.OutletTypeID = 0;
                    result.Data.ProjectName = project.ProjectName;
                    result.Data.ProjectBackground = project.ProjectBackground;
                    result.Data.Name = firstUser.FullName;
                    if (firstUser.Kho_Outlets.Count() > 0)
                    {
                        var temp = firstUser.Kho_Outlets.FirstOrDefault();
                        result.Data.OutletTypeID = temp.Outlet.OutletTypeID;
                    }

                    result.Data.UserID = firstUser.ID;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS);
                    return ERROR_CODE_LOGIN.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.NOT_EXIST_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.NOT_EXIST_APP);
                    return ERROR_CODE_LOGIN.NOT_EXIST_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR);
                result.Data = null;
                return ERROR_CODE_LOGIN.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }


        /// <summary>
        /// Cập Nhập User
        /// </summary>
        /// <param name="MessageError"></param>
        /// <param name="_pProjectid"></param>
        /// <param name="se"></param>
        /// <param name="_list"></param>
        /// <returns></returns>
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(ref string MessageError, int _pProjectid, int _pOutletType, UserSE se, ref List<UserEXT> _list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var queryapp = from item in context.User_Apps
                               where (item.Team_Outlet.Outlet.ProjectID == _pProjectid)
                                
                               && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                               && (item.UserType == se.TypeUser || se.TypeUser == "Tất Cả" || se.TypeUser == null)
                               && (se.RoleType == 0 || se.RoleType == null)
                               && (item.Team_Outlet.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                               select new UserEXT
                               {
                                   FullName = item.FullName,
                                   UserName = item.UserName,
                                   UserType = item.UserType,
                                   CreatedDateTime = item.CreatedDateTime.Date,
                                   UserID = item.ID,
                                   PassWord = item.PassWord,
                                   ProjectID = _pProjectid,
                                   TeamOutletID = item.TeamOutletID,
                                   //OutletID = item.Team_Outlet.OutletID
                               };
                var queryappsup = from item in context.User_Apps
                                  join supoutlet in context.Sup_Outlets on item.ID equals supoutlet.SupID
                                  where (supoutlet.Outlet.ProjectID == _pProjectid)
                                  && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                  && (item.UserType == se.TypeUser || se.TypeUser == "Tất Cả" || se.TypeUser == null)
                                  && (se.RoleType == 0 || se.RoleType == null)
                                  && (supoutlet.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                                  select new UserEXT
                                  {
                                      FullName = item.FullName,
                                      UserName = item.UserName,
                                      UserType = item.UserType,
                                      CreatedDateTime = item.CreatedDateTime.Date,
                                      UserID = item.ID,
                                      PassWord = item.PassWord,
                                      ProjectID = _pProjectid,
                                      TeamOutletID = item.TeamOutletID,
                                      //OutletID = item.Team_Outlet.OutletID
                                  };
                var queryappkho = from item in context.User_Apps
                                  join khooutlet in context.Kho_Outlets on item.ID equals khooutlet.KhoID
                                  where (khooutlet.Outlet.ProjectID == _pProjectid)
                                  && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                  && (item.UserType == se.TypeUser || se.TypeUser == "Tất Cả" || se.TypeUser == null)
                                  && (se.RoleType == 0 || se.RoleType == null)
                                  && (khooutlet.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                                  select new UserEXT
                                  {
                                      FullName = item.FullName,
                                      UserName = item.UserName,
                                      UserType = item.UserType,
                                      CreatedDateTime = item.CreatedDateTime.Date,
                                      UserID = item.ID,
                                      PassWord = item.PassWord,
                                      ProjectID = _pProjectid,
                                      TeamOutletID = item.TeamOutletID,
                                      //OutletID = item.Team_Outlet.OutletID
                                  };
                var queryweb = from item in context.User_Webs
                               join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.SaleID
                               join role in context.User_Roles on item.ID equals role.UserID
                               where (item.ProjectID == _pProjectid)
                               && item.UserType != "ADMIN"
                                 && item.UserType != "MASTER"
                                && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                && (item.UserType.Contains(se.TypeUser) || se.TypeUser == "Tất Cả" || se.TypeUser == null)
                                && (role.RoleID == se.RoleType || se.RoleType == 0 || se.RoleType == null)
                                && (saleoutlet.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                               select new UserEXT
                               {
                                   FullName = item.FullName,
                                   UserName = item.UserName,
                                   UserType = item.UserType,
                                   CreatedDateTime = item.CreatedDateTime.Date,
                                   UserID = item.ID,
                                   PassWord = item.PassWord,
                                   ProjectID = _pProjectid,

                               };
                var listapp = queryapp.ToList().Distinct(new ComparerUser()).Where(p => p.UserType == "SP").ToList();
                var listweb = queryweb.ToList().Distinct(new ComparerUser()).ToList();
                var listappsup = queryappsup.ToList().Distinct(new ComparerUser()).ToList();
                var listappkho = queryappkho.ToList().Distinct(new ComparerUser()).ToList();
                _list = listapp.Union(listweb).ToList();
                _list = listappsup.Union(_list).ToList();
                _list = listappkho.Union(_list).ToList().Distinct().ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE Delete(ref string _messageError, string _pID, string _pType, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string[] arayID = _pID.Split('|');
                string[] arayType = _pType.Split('|');

                for (int i = 0; i < arayID.Count(); i++)
                {
                    if (arayID[i].ToString() != String.Empty)
                    {
                        //xem brand set này thuộc hiện tại k?
                        var query = from item in context.User_Apps
                                    where item.ID == int.Parse(arayID[i])
                                    && item.UserType == arayType[i]
                                     && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                    select item;
                        // không có ở hiện tại thì tạo và update trạng thái
                        if (query.Count() > 0)
                        {
                            //xác nhận
                            var simple = query.FirstOrDefault();
                            simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE;
                            simple.LastUpdatedBy = _pUserID;
                            simple.LastUpdatedDateTime = DateTime.Now;
                            simple.RowVersion += 1;
                            // update set hiện tại
                            context.SubmitChanges();
                        }
                        else
                        {
                            var queryWeb = from item in context.User_Webs
                                           where item.ID == int.Parse(arayID[i])
                                            && item.UserType == arayType[i]
                                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                           select item;
                            if (queryWeb.Count() > 0)
                            {
                                //xác nhận
                                var simple = queryWeb.FirstOrDefault();
                                simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE;
                                simple.LastUpdatedBy = _pUserID;
                                simple.LastUpdatedDateTime = DateTime.Now;
                                simple.RowVersion += 1;
                                // update set hiện tại
                                context.SubmitChanges();
                            }
                        }
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE Update(ref string _messageError, string _pusername, string _passcu, string _passmoi, string _idUser, string _pType, string _pTeamOutletID, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                //xem brand set này thuộc hiện tại k?
                var query = from item in context.User_Apps
                            where item.ID == int.Parse(_idUser)
                            && item.UserType == _pType
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select item;
                // không có ở hiện tại thì tạo và update trạng thái
                if (query.Count() > 0)
                {
                    var simple = query.FirstOrDefault();
                    if (_passmoi != "")
                    {
                        simple.PassWord = _passmoi;
                    }
                    //xác nhận 
                    simple.TeamOutletID = int.Parse(_pTeamOutletID);
                    simple.UserName = _pusername;
                    simple.LastUpdatedBy = _pUserID;
                    simple.LastUpdatedDateTime = DateTime.Now;
                    simple.RowVersion += 1;
                    // update set hiện tại
                    context.SubmitChanges();
                }
                else
                {
                    var queryWeb = from item in context.User_Webs
                                   where item.ID == int.Parse(_idUser)
                                    && item.UserType == _pType
                                     && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                   select item;
                    if (queryWeb.Count() > 0)
                    {
                        var simple = queryWeb.FirstOrDefault();
                        if (_passmoi != "")
                        {
                            simple.PassWord = _passmoi;
                        }
                        //xác nhận
                        simple.UserName = _pusername;
                        simple.LastUpdatedBy = _pUserID;
                        simple.LastUpdatedDateTime = DateTime.Now;
                        simple.RowVersion += 1;
                        // update set hiện tại
                        context.SubmitChanges();
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetSimple(ref string MessageError, string _pUserID, string _UserType, ref UserEXT _pUser)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

                var query = from item in context.User_Apps
                            where item.ID == int.Parse(_pUserID)
                            && item.UserType == _UserType
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select new UserEXT
                            {
                                FullName = item.FullName,
                                UserName = item.UserName,
                                UserType = item.UserType,
                                CreatedDateTime = item.CreatedDateTime.Date,
                                UserID = item.ID,
                                PassWord = item.PassWord,
                                //ProjectID = item.Team_Outlet.Outlet.ProjectID,
                                TeamOutletID = item.TeamOutletID
                                // OutletID = item.Team_Outlet.OutletID
                            };
                if (query.Count() == 0)
                {
                    var queryWeb = from item in context.User_Webs
                                   where item.ID == int.Parse(_pUserID)
                                   && item.UserType == _UserType
                                    && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                   select new UserEXT
                                   {
                                       FullName = item.FullName,
                                       UserName = item.UserName,
                                       UserType = item.UserType,
                                       CreatedDateTime = item.CreatedDateTime.Date,
                                       UserID = item.ID,
                                       PassWord = item.PassWord,
                                       ProjectID = item.ProjectID
                                   };
                    _pUser = queryWeb.FirstOrDefault();
                }
                else
                {
                    _pUser = query.FirstOrDefault();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string MessageError, string _pUserName, string _pUserType, string _pPassWord, int _pOutletType, int _pProjectID, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var queryWeb = from item in context.User_Webs
                               where item.UserName == _pUserName
                                && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                               select item;
                if (queryWeb.Count() == 0)
                {
                    //add user
                    User_Web userweb = new User_Web();
                    userweb.ProjectID = _pProjectID;
                    userweb.FullName = _pUserName;
                    userweb.UserName = _pUserName;
                    userweb.Code = DateTime.Now.ToString("HHmmss");
                    userweb.UserType = _pUserType;
                    userweb.PassWord = _pPassWord;
                    userweb.OutletTypeID = _pOutletType;
                    userweb.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    userweb.CreatedBy = _pUserID;
                    userweb.CreatedDateTime = DateTime.Now;
                    userweb.RowVersion = 1;
                    context.User_Webs.InsertOnSubmit(userweb);
                    context.SubmitChanges();
                    //lay role id
                    var queryrole = from item in context.Roles
                                    where item.Name.ToLower().Trim() == _pUserType.ToLower().Trim()
                                    select item;
                    //add role
                    User_Role userrole = new User_Role();
                    userrole.UserID = userweb.ID;
                    userrole.RoleID = queryrole.FirstOrDefault().ID;
                    // update set hiện tại
                    context.User_Roles.InsertOnSubmit(userrole);
                    context.SubmitChanges();
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                }


            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static List<User_App> getApp(int _pProjectID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            List<User_App> listRP = new List<User_App>();
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Apps
                            where item.Status != 2
                            && item.Team_Outlet.Outlet.ProjectID == _pProjectID
                            && (item.Team_Outlet.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select item;
                var query2 = from item in context.User_Apps
                             where item.TeamOutletID == null
                             select item;
                var queryVKho = from item in context.View_UserKHOs
                                select item;
                var queryVSup = from item in context.View_UserSUPs
                                select item;

                listRP = query.ToList();
                var listAllKho = queryVKho.ToList();
                var listAllSup = queryVSup.ToList();
                var listKHO = query2.ToList().Where(p => p.UserType == "KHO").ToList();
                var listSUP = query2.ToList().Where(p => p.UserType == "SUP").ToList();
                foreach (var item in listKHO)
                {
                    if (listAllKho.Where(o => o.ID == item.ID && o.ProjectID == _pProjectID && (o.OutletTypeID == _pOutletType || _pOutletType == 0)).Count() > 0)
                    {
                        listRP.Add(item);
                    }
                }

                foreach (var item in listSUP)
                {
                    if (listAllSup.Where(o => o.ID == item.ID && o.ProjectID == _pProjectID && (o.OutletTypeID == _pOutletType || _pOutletType == 0)).Count() > 0)
                    {
                        listRP.Add(item);
                    }
                }


                return listRP;
            }
            catch (Exception)
            {
                return listRP;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static List<User_Web> getWeb(int _pProjectID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            List<User_Web> listRP = new List<User_Web>();
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Webs
                            where item.Status != 2
                            && item.UserType != "ADMIN"
                            && item.UserType != "MASTER"
                            && item.ProjectID == _pProjectID
                            //&& (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select item;
                return query.ToList();
            }
            catch (Exception)
            {
                return listRP;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static MainResult addRPAPP(List<User_App> listRPAPP)
        {
            MainResult result = new MainResult();
            DatabaseDataContext context = null;
            User_App currentitem;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();

                context.User_Apps.InsertAllOnSubmit(listRPAPP);
                context.SubmitChanges();

                foreach (var item in listRPAPP.Where(o => o.UserType == "KHO"))
                {
                    currentitem = item;
                    foreach (var entity in item.listOutlet)
                    {
                        if (context.Kho_Outlets.Where(o => o.KhoID == item.ID && o.OutletID == entity).Count() == 0)
                        {
                            Kho_Outlet entityKHO = new Kho_Outlet();
                            entityKHO.KhoID = item.ID;
                            entityKHO.OutletID = entity;
                            entityKHO.CreatedBy = item.CreatedBy;
                            entityKHO.CreatedDateTime = DateTime.Now;
                            entityKHO.Status = 1;
                            entityKHO.RowVersion = 1;
                            context.Kho_Outlets.InsertOnSubmit(entityKHO);
                            context.SubmitChanges();
                        }
                    }
                }


                foreach (var item in listRPAPP.Where(o => o.UserType == "SUP"))
                {
                    foreach (var entity in item.listOutlet)
                    {
                        if (context.Sup_Outlets.Where(o => o.SupID == item.ID && o.OutletID == entity).Count() == 0)
                        {
                            Sup_Outlet entitySUP = new Sup_Outlet();
                            entitySUP.SupID = item.ID;
                            entitySUP.OutletID = entity;
                            entitySUP.CreatedBy = item.CreatedBy;
                            entitySUP.CreatedDateTime = DateTime.Now;
                            entitySUP.Status = 1;
                            entitySUP.RowVersion = 1;
                            context.Sup_Outlets.InsertOnSubmit(entitySUP);
                            context.SubmitChanges();
                        }
                    }
                }


                context.Transaction.Commit();
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS);
                return result;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                result.Status = 2;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR) + " " + ex.Message;
                return result;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static MainResult addRPWEB(List<User_Web> listRPWEB)
        {
            MainResult result = new MainResult();
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();
                var queryRole = from item in context.Roles
                                select item;
                var listRole = queryRole.ToList();
                context.User_Webs.InsertAllOnSubmit(listRPWEB);

                context.SubmitChanges();

                foreach (var item in listRPWEB)
                {
                    foreach (var entity in item.listOutlet)
                    {
                        if (context.Sale_Outlets.Where(o => o.SaleID == item.ID && o.OutletID == entity).Count() == 0)
                        {
                            Sale_Outlet entitySALE = new Sale_Outlet();
                            entitySALE.SaleID = item.ID;
                            entitySALE.OutletID = entity;
                            entitySALE.CreatedBy = item.CreatedBy;
                            entitySALE.CreatedDateTime = DateTime.Now;
                            entitySALE.Status = 1;
                            entitySALE.RowVersion = 1;
                            context.Sale_Outlets.InsertOnSubmit(entitySALE);
                            context.SubmitChanges();
                        }
                    }
                    var existRole = listRole.Where(o => o.Name.Trim().ToUpper() == item.UserType.Trim().ToUpper()).FirstOrDefault();
                    if (existRole != null)
                    {
                        User_Role entityRoleMenu = new User_Role();
                        entityRoleMenu.UserID = item.ID;
                        entityRoleMenu.RoleID = existRole.ID;
                        context.User_Roles.InsertOnSubmit(entityRoleMenu);
                        context.SubmitChanges();
                    }
                    else
                    {
                        context.Transaction.Rollback();
                        result.Status = 2;
                        result.Description = "User "+item.FullName + " được phân quyền không tồn tại trên hệ thống. Vui long kiểm tra lại";
                        return result;
                    }
                }

                context.Transaction.Commit();
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS);
                return result;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                result.Status = 2;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR) + " " + ex.Message;
                return result;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public ERROR_CODE_ITEMS_UPDATE ChangPass(ref string _messageError, string _username, string _passcu, string _passmoi, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Webs
                            where item.ID == _pUserID
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select item;
                if (query.Count() > 0)
                {
                    var temp = query.FirstOrDefault();
                    if (temp.PassWord == _passcu)
                    {
                        temp.PassWord = _passmoi;
                        context.SubmitChanges();

                        return ERROR_CODE_ITEMS_UPDATE.SUCCESS;
                    }
                    else
                    {
                        return ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_DATA;
                    }
                }
                else
                {
                    return ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
                }
            }
            catch (Exception ex)
            {
                return ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
        public string GetPass(string _pUserName)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Webs
                            where item.UserName.Trim().ToLower() == _pUserName.Trim().ToLower()
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select item.PassWord;
                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
        public string GetPass(int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Webs
                            where item.ID == _pUserID
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select item.PassWord;
                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
        class ComparerUser : IEqualityComparer<UserEXT>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(UserEXT x, UserEXT y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.UserID == y.UserID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(UserEXT item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashtuserID = item.UserID == null ? 0 : item.UserID.GetHashCode();
                //Calculate the hash code for the product.
                return hashtuserID;
            }
        }

        public System.Collections.IEnumerable GetUserSale(int _pProjectid, int _pOutletType, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Webs
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.SaleID
                            where (item.ProjectID == _pProjectid)
                             && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                             && (saleoutlet.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select new Common.ComboItem
                         {
                             ID = item.ID,
                             Value = item.ID.ToString(),
                             Text = item.FullName
                         };
                List<ComboItem> items = query.ToList().Distinct(new Business.Region.ComparerRegionData()).OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public static List<View_TeamOutletNotAssign> getTeamOutletNotAssign(int ProjectID, int OutletTypeID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_TeamOutletNotAssigns
                            where item.ProjectID == ProjectID
                           && (item.OutletTypeID == OutletTypeID || OutletTypeID == 0)
                           && item.TeamOutletID == null
                            select item;
                return query.ToList();
            }
            catch (Exception ex)
            {
                return new List<View_TeamOutletNotAssign>();
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }

        public ERROR_CODE_LOGIN LoginWarehouse(string username, string pass, string pAppCode, string pDeviceToken, string pDevideID, ref LoginWarehouseResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // kiểm tra username và pass có đúng
                    var query = from item in context.User_Apps
                                where item.UserName == username
                                && item.PassWord == pass
                                && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                && (item.UserType == "PM" || item.UserType == "KHO")
                                && item.ProjectID == 1
                                select item;
                    if (query.Count() == 0) // nếu không
                    {
                        //------------------Kết quả trả về -- sai user
                        // gán giá trị cho phần trả về
                        result.Data = null;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_USERNAME;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.WRONG_USERNAME);
                        return ERROR_CODE_LOGIN.WRONG_USERNAME;
                    }

                    // gán dữ liệu user
                    var firstUser = query.FirstOrDefault();
                    // cập nhật token
                    firstUser.DeviceToken = pDeviceToken;
                    context.SubmitChanges(); // thực thi câu lệnh
                    result.Data =new SimpleUserWarehouse();
                    result.Data.ID = firstUser.ID;
                    result.Data.Type = firstUser.UserType == "KHO" ? 2 : firstUser.UserType == "PM" ? 1 : 2;
                    result.Data.IsCenter = firstUser.isCenter.HasValue?firstUser.isCenter.Value:false;
                    result.Data.Name = firstUser.FullName;
                    result.Data.Phone = firstUser.Phone;
                    result.Data.Address = "";
                    result.Data.OwnerName = "";
                    result.Data.OwnerPhone = "";
                    result.Data.Size = "";
                    result.Data.Lat = 0;
                    result.Data.Lon = 0;
                    result.Data.Fields = new List<SimpleField>();
                    if (result.Data.Type != 1)
                    {
                        var queryFields = from item in context.Kho_Outlets
                                          where item.KhoID == firstUser.ID
                                          && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                          && item.Outlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                          select new SimpleField
                                          {
                                              ID = item.OutletID,
                                              FieldName = item.Outlet.OutletName,
                                              RegionID = item.Outlet.RegionID,
                                              RegionName = item.Outlet.Region.RegionName,
                                              OutletTypeID = item.Outlet.OutletTypeID,
                                              SPID = item.Outlet.Team_Outlets.Where(p => p.Status != 2).FirstOrDefault()!= null? item.Outlet.Team_Outlets.Where(p => p.Status != 2).FirstOrDefault().User_Apps.FirstOrDefault().ID:0,
                                              BrandSets = new List<SimpleBrandSet>()
                                          };
                        var queryOutletBrand = from item in context.Outlet_Brands
                                              where item.Status != 2
                                              select item;
                        var listOutletBrand = queryOutletBrand.ToList();

                        var listTemp = queryFields.ToList();
                        foreach (var item in listTemp)
                        {
                            var outletbrands = listOutletBrand.Where(o => o.OutletID == item.ID).ToList();
                            List<SimpleBrandSet> listresult = new List<SimpleBrandSet>();
                            foreach (var brand in outletbrands)
                            {
                                var brandsets = brand.Brand.Brand_Sets.Where(p=> p.Status != 2 && p.OutletTypeID == item.OutletTypeID).Select(o => new SimpleBrandSet
                                {
                                    BrandID = o.BrandID,
                                    BrandSetID = o.ID,
                                    IsDefault = o.IsDefault,
                                    SetName = o.SetName,
                                    OutletTypeID = o.OutletTypeID
                                }).ToList();
                                listresult.AddRange(brandsets);
                            }
                            item.BrandSets = listresult;
                        }
                        result.Data.Fields = listTemp.ToList();

                    }
                    var queryUserKDT = from item in context.kdt_warehouses
                                       where item.warehouse_id == firstUser.ID
                                       select item;
                    if (queryUserKDT.Count() > 0)
                    {
                        var infoKDT = queryUserKDT.FirstOrDefault();
                        result.Data.Name = infoKDT.name;
                        result.Data.Phone = infoKDT.phone;
                        result.Data.Address = infoKDT.address;
                        result.Data.OwnerName = infoKDT.owner_name;
                        result.Data.OwnerPhone = infoKDT.owner_phone;
                        result.Data.Size = infoKDT.area;
                        result.Data.Lat = infoKDT.lat??0;
                        result.Data.Lon = infoKDT.lon??0;
                        result.Data.Image1 = infoKDT.image1 != null ? "https://apikdt.imark.com.vn/" + context.kdt_images.Where(p => p.id == infoKDT.image1).FirstOrDefault().url : null;
                        result.Data.Image2 = infoKDT.image2 != null ? "https://apikdt.imark.com.vn/" + context.kdt_images.Where(p => p.id == infoKDT.image2).FirstOrDefault().url : null;
                        result.Data.Image3 = infoKDT.image3 != null ? "https://apikdt.imark.com.vn/" + context.kdt_images.Where(p => p.id == infoKDT.image3).FirstOrDefault().url : null;
                    }
                   
                    
                    
                    
                    //result.Data.Image1 = "https://cdn.oneesports.gg/wp-content/uploads/sites/4/2019/09/K7.jpg";
                    //result.Data.Image2 = "https://cdn.oneesports.gg/wp-content/uploads/sites/4/2019/09/K7.jpg";
                    //result.Data.Image3 = "https://cdn.oneesports.gg/wp-content/uploads/sites/4/2019/09/K7.jpg";
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SUCCESS);


                    var queryToken = from item in context.AccessTokens
                                     where item.UserName == firstUser.UserName
                                     && item.Expires > DateTime.Now
                                     && item.LoginType == "APP"
                                     select item;
                    if (queryToken.Count() >0)
                    {
                        // update
                        var existToken = queryToken.FirstOrDefault();
                        existToken.Expires = DateTime.Now.AddDays(7);
                        context.SubmitChanges();
                        result.Data.AccessToken = existToken.Token;
                        result.Data.Expires = (int)DateTime.Now.AddDays(7).Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                    }
                    else
                    {
                        // new 
                        AccessToken newToken = new AccessToken();
                        newToken.UserName = firstUser.UserName;
                        newToken.Expires = DateTime.Now.AddDays(7);
                        newToken.LoginType = "APP";
                        newToken.UserType = firstUser.UserType;
                        newToken.ProjectID = 1;
                        var key = new  Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(Global.Serverkeytoken));
                        var signInCred = new Microsoft.IdentityModel.Tokens.SigningCredentials(key, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);
                        var claimsdata = new[] {new Claim(ClaimTypes.Name,firstUser.UserName)};
                        var token = new JwtSecurityToken(
                            issuer: "https://sp20.imark.com.vn/WS",
                            audience: "https://sp20.imark.com.vn/WS",
                            expires: DateTime.Now.AddDays(7),
                            claims: claimsdata,
                            signingCredentials: signInCred
                            );
                        var tokeString = new JwtSecurityTokenHandler().WriteToken(token);
                        newToken.Token = tokeString.ToString();
                        context.AccessTokens.InsertOnSubmit(newToken);
                        context.SubmitChanges();
                        result.Data.AccessToken = newToken.Token;
                        result.Data.Expires = (int)DateTime.Now.AddDays(7).Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                    }

                    return ERROR_CODE_LOGIN.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.NOT_EXIST_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.NOT_EXIST_APP);
                    return ERROR_CODE_LOGIN.NOT_EXIST_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR) + ex.Message;
                result.Data = null;
                return ERROR_CODE_LOGIN.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public ERROR_CODE_LOGIN checkAccessToken(string token, ref CheckAccessTokenResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var queryToken = from item in context.AccessTokens
                                 where item.Token == token
                                 select item;
                if (queryToken.Count() > 0)
                {
                    if (queryToken.FirstOrDefault().Expires > DateTime.Now)
                    {
                        var entityFirse = queryToken.FirstOrDefault();
                        result.Data = new SimpleAccessToken();
                        result.Data.ProjectID = entityFirse.ProjectID;
                        result.Data.LoginType = entityFirse.LoginType;
                        result.Data.UserType = entityFirse.UserType;
                        result.Data.UserName = entityFirse.UserName;
                        result.Data.Expires = (int)entityFirse.Expires.Subtract(new DateTime(1970,1,1)).TotalSeconds;
                        result.Status = 1;
                        result.Description = "Success";
                    }
                    else
                    {
                        result.Data = null;
                        result.Status = 2;
                        result.Description = "Expired";
                    }
                    
                }
                else
                {
                    result.Data = null;
                    result.Status = 3;
                    result.Description = "Not exist";
                }
                return ERROR_CODE_LOGIN.SUCCESS;
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_LOGIN.SYSTEM_ERROR) + ex.Message;
                return ERROR_CODE_LOGIN.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }


    }

    public class SimpleAccessToken
    {
        public int ProjectID { get; set; }
        public string LoginType { get; set; }
        public string UserType { get; set; }
        public string UserName { get; set; }
        public long Expires { get; set; }
    }

    public class SimpleUser
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string ProjectBackground { get; set; }
        public string Name { get; set; }
        public int UserID { get; set; }
        public int TeamID { get; set; }
        public int TeamOutletID { get; set; }
        public SimpleOutlet EntityOutlet { get; set; }

    }

    public class SimpleField
    {
        public int ID { get; set; }
        public string FieldName { get; set; }
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int OutletTypeID { get; set; }
        public int SPID { get; set; }
        public List<SimpleBrandSet> BrandSets { get; set; }
    }
    public class SimpleUserWarehouse
    {
        public int ID { get; set; }
        public int Type { get; set; }
        public bool IsCenter { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string OwnerName { get; set; }
        public string OwnerPhone { get; set; }
        public string Size { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string AccessToken { get; set; }
        public long Expires { get; set; }
        public List<SimpleField> Fields { get; set; }
             
    }


    public class SimpleUserSUP
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string ProjectBackground { get; set; }
        public string Name { get; set; }
        public int UserID { get; set; }
        public List<SimpleOutlet> ListOutlet { get; set; }

    }

    public class SimpleUserKHO
    {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string ProjectBackground { get; set; }
        public string Name { get; set; }
        public int UserID { get; set; }
        public int OutletTypeID { get; set; }


    }

    public class SimpleOutlet
    {
        public int OutletID { get; set; }
        public string OutletName { get; set; }
        public string City { get; set; }
        public string OutletAddress { get; set; }
        public string District { get; set; }
        public int OutletTypeID { get; set; }
        public bool Promotion { get; set; }
        public int ChanelID { get; set; }
        public bool IsLuckyMega { get; set; }
    }
    public class UserEXT
    {
        public int ProjectID { get; set; }
        public int? TeamOutletID { get; set; }
        public int OutletID { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string PassWord { get; set; }
        public string UserCreated { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public long UserID { get; set; }


    }
    public class UserSE
    {
        [UIHint("_UserType")]
        [Display(Name = "Loại User")]
        public string TypeUser { get; set; }
        [UIHint("_RoleType")]
        [Display(Name = "Quyền")]
        public int RoleType { get; set; }
    }
}
