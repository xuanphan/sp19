﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class AttendanceTracking_Sup
    {

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew( string pAppCode, ref AddAttendanceTrackingSUPResult result)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    //lấy chấm công
                    var queryattendance = from attendance in context.AttendanceTracking_Sups
                                          where attendance.SupID == this.SupID
                                          && attendance.OutletID == this.OutletID
                                          && attendance.DeviceDateTime.Date == this.DeviceDateTime.Date
                                          && attendance.TimePointType == this.TimePointType
                                          select attendance;
                    //Nếu chưa tồn tại chấm công
                    if (queryattendance.Count() == 0)
                    {

                        this.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                        this.CreatedDateTime = DateTime.Now;
                        this.ServerDateTime = DateTime.Now;
                        this.CreatedBy = this.SupID;
                        this.RowVersion = 1;
                        context.AttendanceTracking_Sups.InsertOnSubmit(this); // thêm
                        context.SubmitChanges(); // thực thi câu lệnh
                        result.Data = this.ID;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    }
                    else //Nếu đã tồn tại thì cập nhật
                    {
                        var simple = queryattendance.FirstOrDefault();
                        simple.LastUpdatedDateTime = DateTime.Now;
                        simple.LastUpdatedBy = this.SupID;
                        simple.RowVersion += 1;
                        context.SubmitChanges(); // thực thi câu lệnh

                        // gán giá trị cho phần trả về
                        result.Data = simple.ID;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    }

                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = 0;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = 0;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW addNew(ref string _messagererror, AttendanceTracking_Sup temp)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                try
                {
                    context.Connection.Open();
                    context.Transaction = context.Connection.BeginTransaction();
                    var query = from item in context.AttendanceTracking_Sups
                                where item.OutletID == temp.OutletID
                                && item.SupID == temp.SupID
                                && item.TimePointType == temp.TimePointType
                                && item.DeviceDateTime.Date == temp.DeviceDateTime.Date
                                select item;
                    // tồn tại update
                    if (query.Count() > 0)
                    {
                        var simple = query.FirstOrDefault();
                        simple.LastUpdatedDateTime = DateTime.Now;
                        simple.LastUpdatedBy = temp.CreatedBy;
                        simple.RowVersion += 1;
                        context.SubmitChanges();
                    }
                    //-- tạo mới
                    else
                    {
                        context.AttendanceTracking_Sups.InsertOnSubmit(temp);
                        context.SubmitChanges();
                    }
                    context.Transaction.Commit();
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                catch (Exception ex)
                {
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                }
                finally
                {
                    context.Connection.Close();
                    GC.Collect();
                }
            }
            catch (Exception ex)
            {

                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }

        public static List<AttendanceTracking_Sup> GetAttendance(int _pSupID, int _outletID)
        {

            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.AttendanceTracking_Sups
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.DeviceDateTime.Date == DateTime.Now.Date)
                            && (item.SupID == _pSupID)
                            && (item.OutletID == _outletID)
                            select item;
                var list = query.ToList().OrderByDescending(p => p.CreatedDateTime.Date).ToList();
                return list;

            }
            catch (Exception ex)
            {
                string _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
}
