﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Outlet_Current_Set
    {
        public class SETNAME
        {
            public static string SET35 = "35";
            public static string SET65 = "65";
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageError, int _pProjectID, int _pOutletType, ref List<OutletCurrentSetEXT> list, OutletCurrentSetSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_Outlet_Current_Sets
                            where (item.Type == se.RegionCurrentSet || se.RegionCurrentSet == "Tất Cả" || se.RegionCurrentSet == null)
                            && (item.OutletID == se.OutletCurrentSet || se.OutletCurrentSet == 0 || se.OutletCurrentSet == null)
                            && (item.ProjectID == _pProjectID)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select new OutletCurrentSetEXT
                            {
                                OutletID = item.OutletID,
                                OutletName = item.OutletName,
                                RegionName = item.RegionName,
                                ChanelName = item.ChanelName
                                //StatusRequest = context.Outlet_Requirement_Change_Sets.Where(p => p.CreatedDateTime.Date == DateTime.Now.Date && p.OutletID == item.OutletID && p.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE).Count(),

                            };

                list = query.ToList().Distinct(new ComparerOutletCurrentSet()).ToList();
                foreach (var i in list)
                {
                    i.list = context.View_Outlet_Current_Sets.Where(p => p.OutletID == i.OutletID).ToList();
                    var status = context.Outlet_Requirement_Change_Sets.Where(p => p.OutletID == i.OutletID && p.State == false);
                    if (status.Count() > 0)
                    {
                        i.StatusRequest = status.Count();
                    }
                    else
                    {
                        i.StatusRequest = 0;
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchSup(ref string _messageError, int _pProjectID, int _pOutletType, int _pSupID, int _outletID, ref List<OutletCurrentSetEXT> list, OutletCurrentSetSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_Outlet_Current_Set_Sups
                            where (item.OutletID == _outletID)
                            && (item.ProjectID == _pProjectID)
                             && (item.SupID == _pSupID)
                            select new OutletCurrentSetEXT
                            {
                                OutletID = item.OutletID,
                                OutletName = item.OutletName,
                                RegionName = item.RegionName,
                                ChanelName = item.ChanelName,
                                StatusRequest = context.Outlet_Requirement_Change_Sets.Where(p => p.CreatedDateTime.Date == DateTime.Now.Date && p.OutletID == item.OutletID && p.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE).Count(),
                                list = context.View_Outlet_Current_Sets.Where(p => p.OutletID == item.OutletID).ToList()
                            };

                list = query.ToList().Distinct(new ComparerOutletCurrentSet()).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchKho(ref string _messageError, int _pProjectID, int _pOutletType, int _pKhoID, int _outletID, ref List<OutletCurrentSetEXT> list, OutletCurrentSetSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_Outlet_Current_Set_Khos
                            where (item.OutletID == _outletID)
                            && (item.ProjectID == _pProjectID)
                            && (item.KhoID == _pKhoID)
                            select new OutletCurrentSetEXT
                            {
                                OutletID = item.OutletID,
                                OutletName = item.OutletName,
                                RegionName = item.RegionName,
                                ChanelName = item.ChanelName,
                                StatusRequest = context.Outlet_Requirement_Change_Sets.Where(p => p.CreatedDateTime.Date == DateTime.Now.Date && p.OutletID == item.OutletID && p.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE).Count(),
                                list = context.View_Outlet_Current_Sets.Where(p => p.OutletID == item.OutletID).ToList()
                            };

                list = query.ToList().Distinct(new ComparerOutletCurrentSet()).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        //Ham Group
        class ComparerOutletCurrentSet : IEqualityComparer<OutletCurrentSetEXT>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(OutletCurrentSetEXT x, OutletCurrentSetEXT y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.OutletID == y.OutletID;
                // return x.AssignID == y.AssignID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(OutletCurrentSetEXT item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashproductid = item.OutletID == null ? 0 : item.OutletID.GetHashCode();
                //Calculate the hash code for the product.
                return hashproductid;
                // return hashAssignID;
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetCurrent(ref string _messageError, int _pProjectID, int _pOutletID, ref List<Outlet_Current_Set> outlet_Current_Set)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlet_Current_Sets
                            where item.OutletID == _pOutletID
                            && (item.Outlet.ProjectID == _pProjectID || _pProjectID == 0)
                            select item;
                if (query.Count() > 0)
                {
                    outlet_Current_Set = query.ToList();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE Update(ref string _messageError, int _pProjectID, int _pOutletID, string _pBrandSetID, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string[] arayBrandSetID = _pBrandSetID.Split('|');
                var queryBrandSet = from item in context.Brand_Sets
                                    select item;
                var listBrandSet = queryBrandSet.ToList();

                List<SimpleChangeSetResult> listRP = new List<SimpleChangeSetResult>();

                for (int i = 0; i < arayBrandSetID.Count(); i++)
                {
                    if (arayBrandSetID[i].ToString() != String.Empty)
                    {
                        int iDBrand = listBrandSet.Where(p => p.ID == int.Parse(arayBrandSetID[i])).FirstOrDefault().BrandID;
                        //xem brand set này thuộc hiện tại k?
                        var query = from item in context.Outlet_Current_Sets
                                    where item.OutletID == _pOutletID
                                    && item.BrandSetID == int.Parse(arayBrandSetID[i])
                                    && item.Brand_Set.Brand.ID == iDBrand
                                    select item;
                        // không có ở hiện tại thì tạo và update trạng thái
                        if (query.Count() == 0)
                        {
                            var queryChangeSet = from item in context.Outlet_Requirement_Change_Sets
                                                 where item.Brand_Set.BrandID == iDBrand
                                                 && item.OutletID == _pOutletID
                                                 && item.State == false
                                                 select item;
                            if (queryChangeSet.Count() > 0)
                            {
                                var temp = queryChangeSet.FirstOrDefault();
                                temp.BrandSetID = int.Parse(arayBrandSetID[i]);
                                context.SubmitChanges();
                                SimpleChangeSetResult entity = new SimpleChangeSetResult();
                                entity.BrandID = iDBrand;
                                entity.BrandSetID = temp.BrandSetID;
                                entity.RequimentChangeSetID = temp.ID;
                                entity.NumberUsed = 0;
                                var read = context.Brand_Set_Useds.Where(o => o.BrandSetID == entity.BrandSetID && o.OutletID == _pOutletID).OrderByDescending(o => o.ID).FirstOrDefault();
                                if (read != null)
                                {
                                    entity.NumberUsed = read.Number;
                                }
                                listRP.Add(entity);
                            }
                            else
                            {
                                Outlet_Requirement_Change_Set temp = new Outlet_Requirement_Change_Set();
                                temp.OutletID = _pOutletID;
                                temp.BrandSetID = int.Parse(arayBrandSetID[i]);
                                temp.State = false;
                                temp.CreatedBy = _pUserID;
                                temp.CreatedDateTime = DateTime.Now;
                                temp.Status = 1;
                                temp.RowVersion = 1;
                                context.Outlet_Requirement_Change_Sets.InsertOnSubmit(temp);
                                context.SubmitChanges();

                                SimpleChangeSetResult entity = new SimpleChangeSetResult();
                                entity.BrandID = iDBrand;
                                entity.BrandSetID = temp.BrandSetID;
                                entity.RequimentChangeSetID = temp.ID;
                                entity.NumberUsed = 0;
                                var read = context.Brand_Set_Useds.Where(o => o.BrandSetID == entity.BrandSetID && o.OutletID == _pOutletID).OrderByDescending(o => o.ID).FirstOrDefault();
                                if (read != null)
                                {
                                    entity.NumberUsed = read.Number;
                                }
                                listRP.Add(entity);
                            }

                        }
                    }
                    else
                    {

                    }

                }

                if (listRP.Count() > 0)
                {
                    var query = from item in context.User_Apps
                                where item.Team_Outlet.Outlet.ID == _pOutletID
                                && item.UserType == "SP"
                                select item;
                    foreach (var item in query)
                    {
                        if (item.DeviceToken != null)
                        {
                            FCM.SendSMSChangeSet(item.DeviceToken, listRP);
                            string message = "Có thông báo khần thay đổi set quà.";
                            User_App_Notification tempNotifi = new User_App_Notification();
                            tempNotifi.Title = "Thông báo đổi set quà";
                            tempNotifi.Message = message;
                            tempNotifi.UserID = item.ID;
                            tempNotifi.DeviceToken = item.DeviceToken;
                            tempNotifi.CreatedDateTime = DateTime.Now;
                            tempNotifi.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = User_App_Notification.add(tempNotifi);
                        }
                    }
                }


                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT getByOutlet(string pAppCode, int pOutletID, ref GetCurrentSetResult result)
        {

            DatabaseDataContext context = null;
            // khởi tạo list data trả về
            List<SimpleCurrentBrandSet> listRP = new List<SimpleCurrentBrandSet>();
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.Trim().ToLower() == Common.Global.AppCode.Trim().ToLower())
                {

                    // lấy sách sét qua hiện tại đang chạy
                    var currentBrand = context.Outlet_Current_Sets
                        .Where(p => p.OutletID == pOutletID)
                        .Select(o => new SimpleCurrentBrandSet
                        {
                            BrandID = o.Brand_Set.BrandID,
                            BrandSetID = o.BrandSetID,
                            ListCurrentGift = new List<SimpleCurrentGift>()
                        }).ToList();

                    // lấy danh sách quà đang còn lại trong set
                    var currentGift = context.Outlet_Current_Gifts.Where(p => p.OutletID == pOutletID).ToList();

                    // lấy danh sách các chi tiết quà theo set
                    var queryBrandSetDetail = from item in context.Brand_Set_Details
                                              select item;
                    var listBrandSetDetail = queryBrandSetDetail.ToList();

                    // gán dữ liệu vào list data trả về
                    listRP.AddRange(currentBrand);

                    // chạy vòng lặp for để gán dữ liệu cho ListCurrentGift
                    foreach (var item in listRP)
                    {
                        item.NumberUsed = 0; // truyền  mặc định là 0 để khi app nhận . quay hết set thì sẽ +1 thì khi uplen thì nó sẽ là 1 log mới khi tính tổng sẽ summ các log này lại

                        // lấy danh sách chi tiết quà trong set
                        var listGift = listBrandSetDetail.Where(p => p.BrandSetID == item.BrandSetID).ToList();

                        // tạo 1 list tạm để lưu thông tin quà hiện tại
                        List<SimpleCurrentGift> listTempGift = new List<SimpleCurrentGift>();

                        // chạy vòng lặp kiểm tra quà hiện tại
                        foreach (var entity in listGift)
                        {
                            // nếu trong list số lượng quà hiện tại có quà này thì lấy theo số lượng hiện tại
                            if (currentGift.Where(p => p.GiftID == entity.GiftID).FirstOrDefault() != null)
                            {
                                // add dữ liệu vào list tạm
                                listTempGift.Add(new SimpleCurrentGift { GiftID = entity.GiftID, Number = currentGift.Where(p => p.GiftID == entity.GiftID).FirstOrDefault().Number });
                            }
                            // nếu không có thì nghĩa là quà này trong set chưa quay trúng lần nào nên lấy theo SL mặc định của sét
                            else
                            {
                                // add dữ liệu vào list tạm
                                listTempGift.Add(new SimpleCurrentGift { GiftID = entity.GiftID, Number = entity.Number });
                            }
                        }

                        // xong vòng lặp thì gán dữ liệu cho ListCurrentGift
                        item.ListCurrentGift = listTempGift.OrderBy(o => o.GiftID).ToList();
                    }
                    // lấy dữ liệu xong đầy dủ

                    // gán giá trị cho phần trả về
                    result.Data = listRP;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }

            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }


        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT getByOutletSUP(string pAppCode, int pOutletID, int pSupID, ref GetCurrentSetResult result)
        {
            DatabaseDataContext context = null;
            // khởi tạo list data trả về
            List<SimpleCurrentBrandSet> listRP = new List<SimpleCurrentBrandSet>();
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.Trim().ToLower() == Common.Global.AppCode.Trim().ToLower())
                {
                    // lấy sách sét qua hiện tại đang chạy của app sup
                    var currentBrand = context.Sup_Outlet_Current_Sets
                        .Where(p => p.OutletID == pOutletID && p.SupID == pSupID)
                        .Select(o => new SimpleCurrentBrandSet
                        {
                            BrandID = o.Brand_Set.BrandID,
                            BrandSetID = o.BrandSetID,
                            ListCurrentGift = new List<SimpleCurrentGift>()
                        }).ToList();

                    // lấy danh sách quà đang còn lại trong set
                    var currentGift = context.Sup_Outlet_Current_Gifts.Where(p => p.OutletID == pOutletID && p.SupID == pSupID).ToList();

                    // lấy danh sách các chi tiết quà theo set
                    var queryBrandSetDetail = from item in context.Brand_Set_Details
                                              select item;
                    var listBrandSetDetail = queryBrandSetDetail.ToList();

                    // gán dữ liệu vào list data trả về
                    listRP.AddRange(currentBrand);

                    // chạy vòng lặp for để gán dữ liệu cho ListCurrentGift
                    foreach (var item in listRP)
                    {
                        item.NumberUsed = 0; // truyền  mặc định là 0 để khi app nhận . quay hết set thì sẽ +1 thì khi uplen thì nó sẽ là 1 log mới khi tính tổng sẽ summ các log này lại

                        // lấy danh sách chi tiết quà trong set
                        var listGift = listBrandSetDetail.Where(p => p.BrandSetID == item.BrandSetID).ToList();

                        // tạo 1 list tạm để lưu thông tin quà hiện tại
                        List<SimpleCurrentGift> listTempGift = new List<SimpleCurrentGift>();

                        // chạy vòng lặp kiểm tra quà hiện tại
                        foreach (var entity in listGift)
                        {
                            // nếu trong list số lượng quà hiện tại có quà này thì lấy theo số lượng hiện tại
                            if (currentGift.Where(p => p.GiftID == entity.GiftID).FirstOrDefault() != null)
                            {
                                // add dữ liệu vào list tạm
                                listTempGift.Add(new SimpleCurrentGift { GiftID = entity.GiftID, Number = currentGift.Where(p => p.GiftID == entity.GiftID).FirstOrDefault().Number });
                            }
                            // nếu không có thì nghĩa là quà này trong set chưa quay trúng lần nào nên lấy theo SL mặc định của sét
                            else
                            {
                                // add dữ liệu vào list tạm
                                listTempGift.Add(new SimpleCurrentGift { GiftID = entity.GiftID, Number = entity.Number });
                            }
                        }

                        // xong vòng lặp thì gán dữ liệu cho ListCurrentGift
                        item.ListCurrentGift = listTempGift.OrderBy(o => o.GiftID).ToList();
                    }
                    // lấy dữ liệu xong đầy dủ

                    // gán giá trị cho phần trả về
                    result.Data = listRP;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }

                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }

            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }



    public class SimpleCurrentBrandSet
    {
        public int BrandID { get; set; }
        public int BrandSetID { get; set; }
        public int NumberUsed { get; set; }
        public List<SimpleCurrentGift> ListCurrentGift { get; set; }
    }

    public class SimpleCurrentGift
    {
        public int GiftID { get; set; }
        public int Number { get; set; }
    }
    public class OutletCurrentSetSE
    {
        [UIHint("_ListOutlet")]
        [Display(Name = "Siêu Thị")]
        public long OutletCurrentSet { get; set; }

        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionCurrentSet { get; set; }
    }
    public class OutletCurrentSetEXT
    {
        public long OutletID { get; set; }
        public string OutletName { get; set; }
        public string RegionName { get; set; }
        public string ChanelName { get; set; }
        public int StatusRequest { get; set; }
        public List<View_Outlet_Current_Set> list { get; set; }
    }
}
