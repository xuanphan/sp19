﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Sup_Outlet_Current_Gift
    {

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE update(string pAppCode, IList<OutletCurrenrGiftSUPDetail> listEntity, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.Trim().ToLower() == Common.Global.AppCode.Trim().ToLower())
                {
                    // kiểm tra list tải lên có dữ liệu không
                    if (listEntity.Count() > 0) // nếu có
                    {
                        // khởi tạo transaction
                        context.Connection.Open();
                        context.Transaction = context.Connection.BeginTransaction();

                        //  lấy dữ liệu Sup_Outlet_Current_Gift
                        var query = from item in context.Sup_Outlet_Current_Gifts
                                    select item;
                        // gán dữ liệu lấy được
                        var listTemp = query.ToList();

                        // tạo list object để thêm vào db
                        List<Sup_Outlet_Current_Gift> listRP = new List<Sup_Outlet_Current_Gift>();
                        
                        // chạy vòng lặp list tải lên để xử lý dữ liệu
                        foreach (var item in listEntity)
                        {
                            // lấy dữ liệu quà hiện tại của outlet theo ID quà và supID
                            var read = listTemp.Where(p => p.OutletID == item.pOutletID && p.GiftID == item.pGiftID && p.SupID == item.pSupID).FirstOrDefault();
                            if (read != null) // nếu có thì update số lượng quà = số lượng quà của item
                            {
                                read.Number = item.pNumber;
                                read.LastUpdatedBy = item.pSupID;
                                read.LastUpdatedDateTime = DateTime.Now;
                                read.RowVersion++;
                                context.SubmitChanges(); // thực thi câu lệnh
                            }
                            else // nếu không có thì tạo mới
                            {
                                // tạo object
                                Sup_Outlet_Current_Gift tempEntity = new Sup_Outlet_Current_Gift();
                                // gán dữ liệu
                                tempEntity.OutletID = item.pOutletID;
                                tempEntity.SupID = item.pSupID;
                                tempEntity.GiftID = item.pGiftID;
                                tempEntity.Number = item.pNumber;
                                tempEntity.CreatedBy = item.pSupID;
                                tempEntity.CreatedDateTime = DateTime.Now;
                                tempEntity.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                                tempEntity.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                                // thêm vào list object khởi tạo phía trên
                                listRP.Add(tempEntity);
                            }
                        }
                        // nếu list obj có dữ liệu thì add vào db
                        if (listRP.Count() > 0)
                        {
                            context.Sup_Outlet_Current_Gifts.InsertAllOnSubmit(listRP); // thêm list object
                            context.SubmitChanges(); // thực thi câu lệnh
                        }
                        
                        context.Transaction.Commit(); // hoàn thành transaction
                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;


                    }
                    else // nếu không trả về không có dữ liệu
                    {
                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_DATA_OR_WRONG_FORMAT;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_DATA_OR_WRONG_FORMAT);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_DATA_OR_WRONG_FORMAT;
                    }
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_APP;
                }
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback(); // trả lại ban đầu nếu trong quá trình xử lý có lỗi
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT thongkequa(string pAppCode, string pOutletID, ref GetGiftUsedByOutletResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                // tạo list tạm data trả về
                List<SUPOutletGift> listRP = new List<SUPOutletGift>();
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.Trim().ToLower() == Common.Global.AppCode.Trim().ToLower())
                {
                    // lấy thông tin outlet
                    var queryOutlet = from item in context.Outlets
                                      where item.ID == int.Parse(pOutletID)
                                      select item;
                    var entityOutlet = queryOutlet.FirstOrDefault();

                    // lấy ds nhãn hàng outlet đang chạy
                    var readBrand = entityOutlet.Outlet_Brands.Where(p => p.Status != 2).ToList();

                    // lấy danh sách quà quay trong ngày
                    var queryGift = from item in context.Customer_Gifts
                                    where item.Customer.OutletID == int.Parse(pOutletID)
                                    && item.Customer.DeviceDateTime.Date == DateTime.Now.Date
                                    && item.CreatedDateTime.Date == DateTime.Now.Date
                                    select item;
                    var listCustomerGift = queryGift.ToList();

                    // chạy danh sách nhãn hàng
                    foreach (var item in readBrand)
                    {
                        // lấy danh sách các set quà đang chạy theo loại outlet
                        var readBrandSet = item.Brand.Brand_Sets.Where(p => p.OutletTypeID == entityOutlet.OutletTypeID).FirstOrDefault();
                        if (readBrandSet != null)   // nếu có thì lấy theo brand set detail vì brand này đổi quà = vòng quay may mắn
                        {
                            // chạy vòng lặp để lấy dữ liệu quà đổi
                            foreach (var entityGift in readBrandSet.Brand_Set_Details)
                            {
                                SUPOutletGift tempGift = new SUPOutletGift();
                                tempGift.GiftID = entityGift.GiftID;
                                tempGift.Number = listCustomerGift.Where(p => p.GiftID == entityGift.GiftID).Sum(o => o.NumberGift);
                                listRP.Add(tempGift);
                            }
                        }
                        else // nếu không thì brand này chạy đổi quà trực tiếp
                        {
                            // lấy danh sách quà đổi trực tiếp theo sản phẩm
                            var gift = from e in context.Product_Gifts
                                       where e.Product.BrandID == item.BrandID
                                       select e;
                            if (gift.Count() > 0)
                            {
                                // chạy vòng lặp để lấy dữ liệu quà đổi
                                foreach (var entityGift in gift)
                                {
                                    SUPOutletGift tempGift = new SUPOutletGift();
                                    tempGift.GiftID = entityGift.GiftID;
                                    tempGift.Number = listCustomerGift.Where(p => p.GiftID == entityGift.GiftID).Sum(o => o.NumberGift);
                                    listRP.Add(tempGift);
                                }
                            }
                        }
                    }
                    // gán giá trị cho phần trả về
                    result.Data = listRP;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;

                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }

    class ComparerCustomerGift : IEqualityComparer<Customer_Gift>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(Customer_Gift x, Customer_Gift y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.GiftID == y.GiftID;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(Customer_Gift item)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(item, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashgiftid = item.GiftID == null ? 0 : item.GiftID.GetHashCode();
            //Calculate the hash code for the product.
            return hashgiftid;
        }
    }

    public class SUPOutletBrand
    {
        public int BrandID { get; set; }
        public List<SUPOutletGift> ListGiftUsed { get; set; }
    }
    public class SUPOutletGift
    {
        public int GiftID { get; set; }
        public int Number { get; set; }

    }


    public class OutletCurrenrGiftSUPDetail
    {
        public int pOutletID { get; set; }
        public int pGiftID { get; set; }
        public int pNumber { get; set; }
        public int pSupID { get; set; }
    }
}
