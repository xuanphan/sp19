﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
   public partial class Role
    {
       public IEnumerable<ComboItem> GetAllForCombo(bool defaultOption = false, bool allOption = true)
       {
           DatabaseDataContext context = null;
           try
           {
               context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
               var query = from item in context.Roles
                           where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                           select new Common.ComboItem
                           {
                               ID = item.ID,
                               Value = item.Name,
                               Text = item.Name
                           };
               List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
               if (allOption)
                   items.Insert(0, new ComboItem { ID = 0, Text = "Chọn Quyền" });
               if (defaultOption)
                   items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
               return items;
           }
           catch (Exception ex)
           {
               throw ExceptionUtils.ThrowCustomException(ex);
           }
           finally
           {
               context = null;
           }
       }
    }
}
