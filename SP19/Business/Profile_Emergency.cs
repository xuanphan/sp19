﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.ERROR_CODE;

namespace Business
{
    public partial class Profile_Emergency
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(string _pAppCode, string pProfileCode, int pProjectID, ref AddProfileEmergencyResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {

                    var query = from item in context.Profiles
                                where item.ProfileCode.ToUpper().Replace("HVBL", "").Replace("_", "-").Replace(" ", "") == pProfileCode.ToUpper().Replace("HVBL", "").Replace("_", "-").Replace(" ", "")
                                && item.ProjectID == pProjectID
                                select item;

                    if (query.Count() > 0) // nếu có =>
                    {
                        var entity = query.FirstOrDefault();
                        if (entity.IsEmergency) // nếu nhân viên đang trong trạng thái khẩn cấp thì return
                        {
                            result.Data = 0;
                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                            result.Description = "Nhân viên đang trong trạng thái khẩn cấp.";
                            return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                        }
                        else // chưa có thì thêm mới và chuyển trạng thái khẩn cấp nhân viên sang true
                        {
                            this.ProfileID = entity.ID; // gán dữ liệu
                            entity.IsEmergency = true; // chuyển trạng trái
                            context.Profile_Emergencies.InsertOnSubmit(this); // thêm
                            context.SubmitChanges(); // thực thi câu lệnh

                            // gán giá trị cho phần trả về
                            result.Data = this.ID;
                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                            return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                        }
                    }
                    else // không có nhân viên thì trả về
                    {
                        // gán giá trị cho phần trả về
                        result.Data = 0;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA;
                        result.Description = "Mã nhân viên không tồn tại trong hệ thống";
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA;
                    }
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = 0;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = 0;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }



        public static ERROR_CODE_ITEMS_SELECT complete(string pAppCode, long pProfileEmergencyID, int pTeamOutletID, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu khẩn cấp theo ID truyền lên
                    var query = from item in context.Profile_Emergencies
                                where item.ID == pProfileEmergencyID
                                select item;

                    if (query.Count() == 0) // không có báo lỗi không có data
                    {
                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                    }
                    else // nếu có 
                    {
                        var temp = query.FirstOrDefault();
                        if (temp.EndDateTime == null)
                        {
                            // cập nhật thời gian hoàn thành cho khẩn cấp
                            temp.EndDateTime = DateTime.Now;
                            temp.LastUpdatedBy = pTeamOutletID;
                            temp.LastUpdatedDateTime = DateTime.Now;

                            // cập nhật trạng thái về bình thường cho nhân viên
                            var tempProfile = temp.Profile;
                            tempProfile.IsEmergency = false;

                            context.SubmitChanges(); // thực thi câu lệnh
                        }

                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    }
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static ERROR_CODE_ITEMS_SELECT clear(ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // lấy danh sách nhân viên đang trong trạng thái khẩn cấp
                var query = from item in context.Profiles
                            where item.IsEmergency == true
                            select item;
                var listTemp = query.ToList();
                // chạy vòng lặp từng nhân viên
                foreach (var item in listTemp)
                {
                    item.IsEmergency = false;
                    // lấy ds khẩn cấp chưa có thời gian hoàn thành khẩn cấp
                    var listEmer = item.Profile_Emergencies.Where(o => o.EndDateTime == null).ToList();
                    // cập nhật thời gian hoàn thành cho danh sách lấy dc
                    listEmer.All(o => { o.EndDateTime = DateTime.Now; return true; });
                    context.SubmitChanges(); // thực thi câu lệnh
                }

                // gán giá trị cho phần trả về
                result.Status = 1;
                result.Description = "Thành Công";
                return ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {

                // gán giá trị cho phần trả vềs
                result.Status = -1;
                result.Description = "Lỗi: " + ex.Message;
                return ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }
}
