﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class ReportHome
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(ref string _messageSystemError, int _pProjectID, int _pOutletTypeID, ref ReportHomeEXT simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                simple.TotalOutlet = context.Outlets.Where(p => p.ProjectID == _pProjectID && (p.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && p.Status!=2).Select(p => p.ID).Count();
                var attendance = context.AttendanceTrackings.Where(p => p.ServerDateTime.Date == DateTime.Now.Date && (p.Team_Outlet.Outlet.ProjectID == _pProjectID && (p.Team_Outlet.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)));
                if (attendance.Count() != 0)
                {
                    simple.ToTalAttendance = context.AttendanceTrackings.Where(p => p.ServerDateTime.Date == DateTime.Now.Date && (p.Team_Outlet.Outlet.ProjectID == _pProjectID && (p.Team_Outlet.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0))).Select(p => p.Team_Outlet.OutletID).Distinct().Count();
                }
                else
                {
                    simple.ToTalAttendance = 0;
                }

                simple.TotalProfile = context.Profiles.Where(p => p.ProjectID == _pProjectID && (p.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)).Select(p => p.ID).Count();

                var ProfileDiscipline = context.Profile_Disciplines.Where(p => p.Profile.ProjectID == _pProjectID && (p.Profile.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0));
                if (ProfileDiscipline.Count() != 0)
                {
                    simple.ToTalProfileDiscipline = context.Profile_Disciplines.Where(p => p.Profile.ProjectID == _pProjectID && (p.Profile.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)).Select(p => p.ProfileID).Distinct().Count();
                }
                else
                {
                    simple.ToTalProfileDiscipline = 0;
                }
                var set = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && p.DeviceDateTime.Date == DateTime.Now.Date);
                if (set.Count() != 0)
                {
                    simple.ToTalSet = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && p.DeviceDateTime.Date == DateTime.Now.Date).Select(p => p.Number).Sum();
                }
                else
                {
                    simple.ToTalSet = 0;
                }
                var sethnk = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && p.DeviceDateTime.Date == DateTime.Now.Date && p.Brand_Set.Brand.BrandCode == "BR01");
                if (sethnk.Count() != 0)
                {
                    simple.ToTalSetHNK = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && p.DeviceDateTime.Date == DateTime.Now.Date && p.Brand_Set.Brand.BrandCode == "BR01").Select(p => p.Number).Sum();
                }
                else
                {
                    simple.ToTalSetHNK = 0;
                }
                var settiger = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && p.DeviceDateTime.Date == DateTime.Now.Date && p.Brand_Set.Brand.BrandCode == "BRTIGER");
                if (settiger.Count() != 0)
                {
                    simple.ToTalSetTIGER = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && p.DeviceDateTime.Date == DateTime.Now.Date && p.Brand_Set.Brand.BrandCode == "BRTIGER").Select(p => p.Number).Sum();
                }
                else
                {
                    simple.ToTalSetTIGER = 0;
                }
                simple.ToTalSetTigerNorth = 0;
                simple.ToTalSetHNKNorth = 0;
                if (_pOutletTypeID == 0)
                {
                    var settigerNorth = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == 3) && p.DeviceDateTime.Date == DateTime.Now.Date && p.Brand_Set.Brand.BrandCode == "BRTIGER").ToList();
                    if (settigerNorth.Count() != 0)
                    {
                        simple.ToTalSetTigerNorth = settigerNorth.Select(p => p.Number).Sum();
                    }

                    var setHeinekenNorth = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == 3) && p.DeviceDateTime.Date == DateTime.Now.Date && p.Brand_Set.Brand.BrandCode == "BR01").ToList();
                    if (setHeinekenNorth.Count() != 0)
                    {
                        simple.ToTalSetHNKNorth = setHeinekenNorth.Select(p => p.Number).Sum();
                    }
                }


                var setsb = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && p.DeviceDateTime.Date == DateTime.Now.Date && p.Brand_Set.Brand.BrandCode == "BR03");
                if (setsb.Count() != 0)
                {
                    simple.ToTalSetSB = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && p.DeviceDateTime.Date == DateTime.Now.Date && p.Brand_Set.Brand.BrandCode == "BR03").Select(p => p.Number).Sum();
                }
                else
                {
                    simple.ToTalSetSB = 0;
                }
                var takeoff = context.Take_Off_Volumns.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0));
                if (takeoff.Count() != 0)
                {
                    simple.ToTalTakeOff = context.Take_Off_Volumns.Where(p => p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)).Select(p => p.Number).Sum();
                }
                else
                {
                    simple.ToTalTakeOff = 0;
                }


                // Thống kê set theo region
                simple.list = new List<ReportSet>();
                var query = from item in context.Brand_Set_Useds
                            where (item.Outlet.ProjectID == _pProjectID && (item.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0))
                           && item.DeviceDateTime.Date <= DateTime.Now.Date
                           && item.Outlet.Status != 2
                            select new ReportSet
                            {
                                BrandID = item.Brand_Set.BrandID,
                                RegionID = item.Outlet.RegionID,
                                RegionName = item.Outlet.Region.RegionName,
                                BrandName = item.Brand_Set.Brand.BrandName,
                                NumberSet = context.Brand_Set_Useds.Where(p => p.Outlet.ProjectID == _pProjectID && item.Outlet.Status != 2 && (p.Outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && p.Outlet.RegionID == item.Outlet.RegionID && p.Brand_Set.BrandID == item.Brand_Set.BrandID && p.DeviceDateTime.Date <= DateTime.Now.Date).Select(p => p.Number).Sum()
                            };
                var listset = query.ToList().Distinct(new ComparerReportSetView()).ToList();
                simple.list = listset.ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        class ComparerReportSetView : IEqualityComparer<ReportSet>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(ReportSet x, ReportSet y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.RegionID == y.RegionID && x.BrandID == y.BrandID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(ReportSet item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashRegionID = item.RegionID == null ? 0 : item.RegionID.GetHashCode();
                int hashBrandID = item.BrandID == null ? 0 : item.BrandID.GetHashCode();
                //Calculate the hash code for the product.
                return hashRegionID ^ hashBrandID;
            }
        }

        //Bổ Sung ngày 21/12/2018
        //Báo Cáo Sản Lương
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchSaleVolume(ref string _messageError, int _pProjectID, int _pOutletType, StockSE se, ref List<SaleVolumeEXT> list, ref List<SaleVolumeEXT> simple)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.SoLuongHangBan2(se.DateStockSE.Date, _pProjectID, _pOutletType, se.RegionStockSE, int.Parse(se.SaleSE.ToString()), int.Parse(se.OutletStockSE.ToString()))
                            select new SaleVolumeEXT
                            {
                                Number = item.number.GetValueOrDefault(),
                                ProductCode = item.ProductCode,
                                ProductID = item.ProductID,
                                TeamOutletID = item.CreatedBy
                            };
                List<int> _productID = new List<int>();
                var productdisplay = context.Products.Where(p => p.Brand.ProjectID == _pProjectID && p.IsReportDisplay != null);
                if (productdisplay.Count() > 0)
                {
                    _productID = productdisplay.Select(p => p.ID).ToList();
                }
                list = query.ToList().Where(p => _productID.Any(p2 => p2 == p.ProductID)).ToList(); 
                List<SaleVolumeEXT> result = list
                          .GroupBy(l => new { l.ProductCode })
                          .Select(cl => new SaleVolumeEXT
                          {
                              ProductCode = cl.First().ProductCode,
                              ProductID = cl.First().ProductID,
                              Number = cl.Sum(c => c.Number),
                          }).ToList();
                if (result.Count() > 0)
                {
                    simple = result.ToList();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchSaleVolumeSale(ref string _messageError, int _pProjectID, int _pOutletType, StockSE se, string _SaleID, ref List<SaleVolumeEXT> list, ref List<SaleVolumeEXT> simple)
        {
            DatabaseDataContext context = null;

            try
            {
                List<string> listsale = new List<string>();
                listsale = _SaleID.Split('|').ToList();
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.SoLuongHangBan2(se.DateStockSE.Date, _pProjectID, _pOutletType, se.RegionStockSE, int.Parse(se.SaleSE.ToString()), int.Parse(se.OutletStockSE.ToString()))
                            where listsale.Any(p=>p == item.SaleID.ToString())
                            select new SaleVolumeEXT
                            {
                                Number = item.number.GetValueOrDefault(),
                                ProductCode = item.ProductCode,
                                ProductID = item.ProductID,
                                TeamOutletID = item.CreatedBy
                            };
                List<int> _productID = new List<int>();
                var productdisplay = context.Products.Where(p => p.Brand.ProjectID == _pProjectID && p.IsReportDisplay != null);
                if(productdisplay.Count()>0)
                {
                    _productID = productdisplay.Select(p => p.ID).ToList();
                }
                list = query.ToList().Where(p => _productID.Any(p2 => p2 == p.ProductID)).ToList(); 
                List<SaleVolumeEXT> result = list
                          .GroupBy(l => new { l.ProductCode })
                          .Select(cl => new SaleVolumeEXT
                          {
                              ProductCode = cl.First().ProductCode,
                              ProductID = cl.First().ProductID,
                              Number = cl.Sum(c => c.Number),
                          }).ToList();
                if (result.Count() > 0)
                {
                    simple = result.ToList();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        //Báo cáo quà
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchGift(ref string _messageError, int _pProjectID, int _pOutletType, StockSE se, ref List<GiftChangeEXT> list, ref List<GiftChangeEXT> simple)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.SoLuongQua2(se.DateStockSE.Date, _pProjectID, _pOutletType, se.RegionStockSE, int.Parse(se.SaleSE.ToString()), int.Parse(se.OutletStockSE.ToString()))
                            select new GiftChangeEXT
                            {
                                Number = item.Column1.GetValueOrDefault(),
                                GiftID = item.GiftID,
                                TeamOutletID = item.CreatedBy
                            };
                list = query.ToList();

                List<GiftChangeEXT> result = list
                            .GroupBy(l => new { l.GiftID})
                            .Select(cl => new GiftChangeEXT
                            {
                                GiftID = cl.First().GiftID,
                                Number = cl.Sum(c => c.Number),
                            }).ToList();
                if (result.Count()>0)
                {
                    simple = result.ToList();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchGiftSale(ref string _messageError, int _pProjectID, int _pOutletType, StockSE se,string _saleID, ref List<GiftChangeEXT> list, ref List<GiftChangeEXT> simple)
        {
            DatabaseDataContext context = null;

            try
            {
                List<string> listsale = new List<string>();
                listsale = _saleID.Split('|').ToList();
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.SoLuongQua2(se.DateStockSE.Date, _pProjectID, _pOutletType, se.RegionStockSE, int.Parse(se.SaleSE.ToString()), int.Parse(se.OutletStockSE.ToString()))
                            where listsale.Any(p => p == item.SaleID.ToString())
                            select new GiftChangeEXT
                            {
                                Number = item.Column1.GetValueOrDefault(),
                                GiftID = item.GiftID,
                                TeamOutletID = item.CreatedBy
                            };
                list = query.ToList();

                List<GiftChangeEXT> result = list
                            .GroupBy(l => new { l.GiftID })
                            .Select(cl => new GiftChangeEXT
                            {
                                GiftID = cl.First().GiftID,
                                Number = cl.Sum(c => c.Number),
                            }).ToList();
                if (result.Count() > 0)
                {
                    simple = result.ToList();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class ReportHomeEXT
    {
        public int TotalOutlet { get; set; }
        public int ToTalAttendance { get; set; }
        public int TotalProfile { get; set; }
        public int ToTalProfileDiscipline { get; set; }
        public int ToTalSet { get; set; }
        public int ToTalSetHNK { get; set; }
        public int ToTalSetTIGER { get; set; }
        public int ToTalSetSB { get; set; }
        public int ToTalTakeOff { get; set; }
        public int ToTalSetHNKNorth { get; set; }
        public int ToTalSetTigerNorth { get; set; }
        public List<ReportSet> list { get; set; }
    }
    public class SaleVolumeEXT
    {
        public int Number { get; set; }
        public int ProductID { get; set; }
        public string ProductCode { get; set; }
        public int TeamOutletID { get; set; }
    }
    public class GiftChangeEXT
    {
        public int Number { get; set; }
        public int GiftID { get; set; }
        public int TeamOutletID { get; set; }
    }
    public class ReportSet
    {
        public int RegionID { get; set; }
        public int NumberOutletInRegion { get; set; }
        public int BrandID { get; set; }
        public string RegionName { get; set; }
        public string BrandName { get; set; }
        public int NumberSet { get; set; }
    }
}
