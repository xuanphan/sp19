﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Outlet_Requirement_Change_Set
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string _messageSystemError, string _pAppCode, ref AddNewResult result)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {

                    var query = from item in context.Outlet_Requirement_Change_Sets
                                where item.OutletID == this.OutletID
                                    //&& item.DeviceDateTime.GetValueOrDefault().Date == this.DeviceDateTime.GetValueOrDefault().Date
                                            && item.BrandSetID == this.BrandSetID
                                select item;
                    //Nếu chưa tồn tại chấm công
                    if (query.Count() == 0)
                    {
                        this.State = false;
                        this.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                        this.CreatedDateTime = DateTime.Now;
                        this.CreatedBy = this.CreatedBy;
                        this.RowVersion = 1;
                        context.Outlet_Requirement_Change_Sets.InsertOnSubmit(this);
                        context.SubmitChanges();

                        result.Data = this.ID.ToString();
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    }
                    else //Nếu đã tồn tại thì cập nhật
                    {
                        var simple = query.FirstOrDefault();
                        simple.LastUpdatedDateTime = DateTime.Now;
                        simple.LastUpdatedBy = this.CreatedBy;
                        simple.RowVersion += 1;
                        context.SubmitChanges();
                        result.Data = simple.ID.ToString();
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    }

                }
                else
                {
                    result.Data = "-1";
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                result.Data = "-1";
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetRequest(ref string _messageError, int _pProjectID, int _pOutletID, ref List<Outlet_Requirement_Change_Set> outlet_Requirement_Change_Set)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlet_Requirement_Change_Sets
                            where item.OutletID == _pOutletID
                            && (item.Brand_Set.Brand.ProjectID == _pProjectID || _pProjectID ==0)
                            select item;
                if (query.Count() > 0)
                {
                    outlet_Requirement_Change_Set = query.ToList();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT complete( string pAppCode, long pRequimentChangeSetID, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() != Common.Global.AppCode.ToLower().Trim())
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }

                // lấy dữ liệu đổi set 
                var query = from item in context.Outlet_Requirement_Change_Sets
                            where item.ID == pRequimentChangeSetID
                            select item;
                // kiểm tra xem đã update chưa // phòng ngừa dup
                if (query.Count() > 0 && query.FirstOrDefault().Status != 3) // chưa thì update
                {
                    // gán dữ liệu vừa lấy được và cập nhật hoàn thành
                    var temp = query.FirstOrDefault();
                    temp.State = true;
                    temp.Status = 3;
                    temp.RowVersion++;
                    context.SubmitChanges(); // thực thi câu lệnh

                    // từ set đổi sẽ biết set đổi thuộc brand nào . thì sẽ thay đổi set đang chạy của brand đó đổi sang set đổi
                    // lấy set hiện tại đang chạy theo brand 
                    var query2 = from item in context.Outlet_Current_Sets
                                 where item.Brand_Set.Brand.ID == temp.Brand_Set.Brand.ID
                                 && item.OutletID == temp.OutletID
                                 select item;

                    // kiểm tra có lấy được dữ liệu không
                    if (query2.Count() > 0)  // nếu có
                    {
                        // lấy dữ liệu set hiện tại
                        var tempSet = query2.FirstOrDefault();

                        // kiểm tra xem set hiện tại vs set đổi có giống nhau
                        if (tempSet.BrandSetID != temp.BrandSetID) // không giống
                        {
                            // kiểm tra xem set đổi có phải set mặc định hay không.
                            if (temp.Brand_Set.IsDefault)
                            { 
                                // nếu là mặc định thì update quà hiện tại trong set tại outlet là số mặc định của brand_set_detail 
                                // vì đổi về set mặc định thì bắt buộc phải quay hết quà trong set hiện tại mới được đổi về 
                           
                                // lấy danh detail quà trong set đổi
                                var listBrandsetDetail = context.Brand_Set_Details.Where(p => p.BrandSetID == temp.BrandSetID && p.Status != 2).ToList();
                                foreach (var item in listBrandsetDetail)
                                {
                                    // lấy dữ liệu quà hiện tại theo outlet và id quà
                                    var currentgift = context.Outlet_Current_Gifts.Where(p => p.OutletID == temp.OutletID && p.GiftID == item.GiftID).FirstOrDefault();
                                    // nếu có thì update số lượng = số  lượng mặc định trong detail quà
                                    if (currentgift != null)
                                    {
                                        currentgift.Number = item.Number;
                                        item.RowVersion++;
                                        context.SubmitChanges(); // thực thi câu lệnh
                                    }
                                    else // nếu không có thì thêm mới
                                    {
                                        Outlet_Current_Gift tempgift = new Outlet_Current_Gift();
                                        tempgift.OutletID = temp.OutletID;
                                        tempgift.GiftID = item.GiftID;
                                        tempgift.Number = item.Number;
                                        tempgift.CreatedBy = 1;
                                        tempgift.CreatedDateTime = DateTime.Now;
                                        tempgift.Status = 1;
                                        tempgift.RowVersion = 1;
                                        context.Outlet_Current_Gifts.InsertOnSubmit(tempgift);
                                        context.SubmitChanges();
                                    }
                                }
                            }
                            else // nếu không phải set mặc định => đổi từ set 35 sang set 70.
                            {
                                // vì thay đổi set quà 35 => 70 nên quà hiện tại sẽ thay đổi = cách những quà nào là thuộc tính IsGiftBackup = true thì sẽ thay đổi sl của quà này

                                // lấy ds quà backup để update số lượng
                                var listBrandsetDetail = context.Brand_Set_Details.Where(p => p.BrandSetID == temp.BrandSetID && p.IsGiftBackup != null).ToList();
                                
                                // lấy ds quà của set mặc định
                                var listBrandSetDefault = context.Brand_Sets.Where(o => o.BrandID == temp.Brand_Set.Brand.ID && o.IsDefault == true && o.OutletTypeID== temp.Outlet.OutletTypeID).FirstOrDefault();
                                
                                // chạy vòng lặp quà backup để update
                                foreach (var item in listBrandsetDetail)
                                {
                                    // ds dữ liệu quà hiện tại
                                    var currentgift = context.Outlet_Current_Gifts.Where(p => p.OutletID == temp.OutletID && p.GiftID == item.GiftID).FirstOrDefault();
                                    if (currentgift != null) // nếu có
                                    {
                                        // số lượng += số lượng quà (set thay đổi) - số lượng quà(set mặc định)
                                        currentgift.Number += item.Number - listBrandSetDefault.Brand_Set_Details.Where(p => p.GiftID == item.GiftID).FirstOrDefault().Number;
                                        item.RowVersion++;
                                        context.SubmitChanges(); // thực thi câu lệnh
                                    }
                                    else // nếu không có thì thêm mới số lượng = số lượng mặc định của item vòng lặp
                                    {
                                        Outlet_Current_Gift tempgift = new Outlet_Current_Gift();
                                        tempgift.OutletID = temp.OutletID;
                                        tempgift.GiftID = item.GiftID;
                                        //tempgift.Number = item.Number - listBrandSetDefault.Brand_Set_Details.Where(p => p.GiftID == item.GiftID).FirstOrDefault().Number;
                                        tempgift.Number = item.Number;
                                        tempgift.CreatedBy = 1;
                                        tempgift.CreatedDateTime = DateTime.Now;
                                        tempgift.Status = 1;
                                        tempgift.RowVersion = 1;
                                        context.Outlet_Current_Gifts.InsertOnSubmit(tempgift); // thêm
                                        context.SubmitChanges(); // thực thi câu lệnh
                                    }
                                }
                            }
                        }
                        else
                        {
                            // nếu không thì thôi
                        }
                        
                        // update set hiện tại
                        tempSet.BrandSetID = temp.BrandSetID;
                        context.SubmitChanges();
                    } 
                    else // nếu không thì tạo set hiện tại cho outlet này luôn
                    {
                        Outlet_Current_Set entity = new Outlet_Current_Set();
                        entity.OutletID = temp.OutletID;
                        entity.BrandSetID = temp.BrandSetID;
                        entity.CreatedBy = 1;
                        entity.CreatedDateTime = DateTime.Now;
                        entity.Status = 1;
                        entity.RowVersion = 1;
                        context.Outlet_Current_Sets.InsertOnSubmit(entity); // thêm
                        context.SubmitChanges(); // thực thi câu lệnh
                    }

                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else // rồi thì trả về ok
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                
            }
            catch (Exception ex)
            {

                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR)+ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }
}
