﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Outlet_Product
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(ref string _messageSystemError, string _pAppCode, string _pOutletID, ref OutletProductResult result)
      {
          DatabaseDataContext context = null;
          try
          {
              context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
              if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
              {

                  var query = from item in context.Outlet_Products
                              where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                              && item.OutletID == long.Parse(_pOutletID)
                              select new SimpleOutletProduct
                              {
                                  ID = int.Parse( item.ID.ToString()),
                                  OutletID = item.OutletID,
                                  ProductID = item.ProductID
                              };

                  var list = query.ToList();
                  result.Data = list;
                  result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                  result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                  return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
              }
              else
              {
                  result.Data = null;
                  result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                  result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                  return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
              }
          }


          catch (Exception ex)
          {
              _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
              result.Data = null;
              result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
              result.Description = ex.Message;
              return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
          }
          finally
          {
              context = null;
              GC.Collect();
          }
      }
    }
    public class SimpleOutletProduct
  {
      private int? _ID;
      public int? ID
      {
          get { return _ID; }
          set { _ID = value; }
      }
      private int? _OutletID;
      public int? OutletID
      {
          get { return _OutletID; }
          set { _OutletID = value; }
      }
      private int? _ProductID;
      public int? ProductID
      {
          get { return _ProductID; }
          set { _ProductID = value; }
      }

     
      public SimpleOutletProduct() { }

      public SimpleOutletProduct(int ID, int OutletID, int ProductID)
      {
          this.OutletID = OutletID;
          this.ID = ID;
          this.ProductID = ProductID;
      }
  }
}
