﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Brewery_Brand
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll( string _pAppCode, int pProjectID, ref Brewery_BrandResult result)
      {
          DatabaseDataContext context = null;
          try
          {
              context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
              // kiểm tra xem appcode truyền lên có đúng không
              if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
              {

                  var query = from item in context.Brewery_Brands
                              where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                              && item.Brewery.ProjectID == pProjectID
                              select new SimpleBreweryBrand
                              {
                                  ID = item.ID,
                                  BreweryID = item.BreweryID,
                                  BrandName = item.BrandName
                              };
                  
                  // gán giá trị cho phần trả về
                  result.Data =  query.ToList();
                  result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                  result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                  return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
              }
              else
              {
                  // gán giá trị cho phần trả về
                  result.Data = null;
                  result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                  result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                  return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
              }
          }
          catch (Exception ex)
          {
              // gán giá trị cho phần trả về
              result.Data = null;
              result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
              result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
              return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
          }
          finally
          {
              context.Connection.Close();
              context = null;
              GC.Collect();
          }
      }
        public List<Brewery_Brand> GetAll(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Brewery_Brands
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.Brewery.ProjectID == _pProjectID)
                            select item;
                return query.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
            }
        }
    }
  public class SimpleBreweryBrand
  {
      private int? _ID;
      public int? ID
      {
          get { return _ID; }
          set { _ID = value; }
      }

      private int? _BreweryID;
      public int? BreweryID
      {
          get { return _BreweryID; }
          set { _BreweryID = value; }
      }

      private string _BrandName;
      public string BrandName
      {
          get { return _BrandName; }
          set { _BrandName = value; }
      }


      public SimpleBreweryBrand() { }

      public SimpleBreweryBrand(int ID, int BreweryID, string BrandName)
      {
          this.BreweryID = BreweryID;
          this.ID = ID;
          this.BrandName = BrandName;
      }
  }
}
