﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class WSResult
    {

    }
    public class MainResult
    {
        public int Status { get; set; }
        public string Description { get; set; }
    }

    public class GetGiftUsedByOutletResult:MainResult{
        public List<SUPOutletGift> Data { get; set; }
    }
    

    public class GetProductSUPResult : MainResult
    {
        private List<Business.SimpleProduct> _Data;
        public List<Business.SimpleProduct> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleProduct>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
    }
    public class GetDataMegaResult : MainResult
    {
        public List<WSSimpleDataMega> Data { get; set; }
    }

    public class GetNotificationForUserResult :MainResult
    {
        public List<SimpleUserAppNotificationWS> Data { get; set; }
    }
    public class GetAllOutletResult : MainResult
    {
        public List<SimpleOutletWS> Data { get; set; }
    }
    public class GetProfileByCodeResult : MainResult
    {
        public SimpleProfile Data;
    }
    public class GetWarehouseRequirementResult : MainResult
    {
        public List<SimpleWarehouseRequirementSet> Data { get; set; }
    }

    public class GetCurrentSetResult : MainResult
    {
        public List<SimpleCurrentBrandSet> Data { get; set; }
    }

    public class GetProductGiftResult : MainResult
    {
        public List<SimpleProductGift> Data { get; set; }
    }

    public class AddCustomerResult : MainResult
    {
        public long Data { get; set; }
    }

    public class GetOutletBrandResult : MainResult
    {
        public List<int> Data { get; set; }
    }

    public class LoginKhoResult : MainResult
    {
        public SimpleUserKHO Data { get; set; }
    }
    public class LoginResult : MainResult
    {
        public SimpleUser Data { get; set; }
    }

    public class CheckAccessTokenResult : MainResult
    {
        public SimpleAccessToken Data { get; set; }
    }

    public class LoginWarehouseResult : MainResult
    {
        public SimpleUserWarehouse Data { get; set; }
    }
    public class LoginSUPResult : MainResult
    {
        public SimpleUserSUP Data { get; set; }
    }
    public class AddProfileEmergencyResult : MainResult
    {
        public long Data { get; set; }
    }
    public class ChangePassResult : MainResult
    {
        public string Data { get; set; }
    }
    //----------------------------------------Add 

    /// <summary>
    /// Kết quả trả về ws thêm 
    /// </summary>
    public class AddNewResult
    {
        public string Data { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }

    public class AddAttendanceTrackingSUPResult : MainResult
    {
        public long Data { get; set; }
    }

    public class WarehouseRequirementSetResult : MainResult
    {
        public long Data { get; set; }
    }

    public class AddImageResult : MainResult
    {
        public long Data { get; set; }
    }





    /// <summary>
    /// Kết quả trả về ws thêm product
    /// </summary>
    public class AddProductResult
    {
        public string ProductID { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }


    /// <summary>
    /// Kết quả trả về ws thêm product in
    /// </summary>
    public class AddPresentINResult
    {
        public string ID { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }

    /// <summary>
    /// Kết quả trả về ws thêm product out
    /// </summary>
    public class AddPresentOutResult
    {
        public string ID { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }

    /// <summary>
    /// Kết quả trả về ws updateversion
    /// </summary>
    public class UpdateVersionResult
    {
        public int Status { get; set; }
        public string Description { get; set; }
        public string Data { get; set; }
    }


    /// <summary>
    /// Kết quả trả về ws thêm product
    /// </summary>
    public class AddProductShownResult
    {
        public string ProductShownID { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }

    /// <summary>
    /// Kết quả trả về ws thêm product competitor
    /// </summary>
    public class AddProductShownCompetitorResult
    {
        public string ProductShownCompetitorID { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }


    /// <summary>
    /// Kết quả trả về ws thêm image outlet vào phân công
    /// </summary>
    public class AddImageForAssignResult
    {
        public string Assign_ImageID { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    public class AddAttendanceTrackingImageResult : MainResult
    {
        public long Data { get; set; }
    }

    public class AddAttendanceTrackingSupImageResult : MainResult
    {
        public long Data { get; set; }
    }


    /// <summary>
    /// Kết quả trả về ws thêm image outlet vào chấm công
    /// </summary>
    public class AddImageForAttendanceResult
    {
        public string Attendance_ImageID { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }



    /// <summary>
    /// Kết quả trả về ws thêm image outlet vào trưng bày
    /// </summary>
    public class AddImageForProductShownResult
    {
        public string ProductShown_ImageID { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }

    /// <summary>
    /// Kết quả trả về ws thêm image outlet vào trưng bày competitor
    /// </summary>
    public class AddImageForProductShownCompetitorResult
    {
        public string ProductShownCompetitor_ImageID { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    /// <summary>
    /// Kết quả trả về ws Codedetail
    /// </summary>
    public class CodeDetailResult
    {
        private List<Business.SimpleCodeDetail> _Data;
        public List<Business.SimpleCodeDetail> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleCodeDetail>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Product
    //</summary>
    public class ProductResult
    {
        private List<Business.SimpleProduct> _Data;
        public List<Business.SimpleProduct> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleProduct>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Brand
    //</summary>
    public class POSMResult
    {
        private List<Business.SimplePOSM> _Data;
        public List<Business.SimplePOSM> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimplePOSM>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Emergency
    //</summary>
    public class EmergencyResult
    {
        private List<Business.SimpleEmergency> _Data;
        public List<Business.SimpleEmergency> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleEmergency>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }

    public class GetProfileResult : MainResult
    {
        private List<Business.SimpleProfile> _Data;
        public List<Business.SimpleProfile> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleProfile>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }

    }
    //<summary>
    //Kết quả trả về ws Brand
    //</summary>
    public class BackgroundResult
    {
        private List<Business.SimpleBackground> _Data;
        public List<Business.SimpleBackground> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleBackground>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Brand
    //</summary>
    public class BrandResult
    {
        private List<Business.SimpleBrand> _Data;
        public List<Business.SimpleBrand> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleBrand>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Outlet_POSM
    //</summary>
    public class OutletPOSMResult
    {
        private List<Business.SimpleOutletPOSM> _Data;
        public List<Business.SimpleOutletPOSM> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleOutletPOSM>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Group
    //</summary>
    public class GroupResult
    {
        private List<Business.SimpleGroup> _Data;
        public List<Business.SimpleGroup> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleGroup>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Gift
    //</summary>
    public class GiftResult
    {
        private List<Business.SimpleGift> _Data;
        public List<Business.SimpleGift> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleGift>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Brand_Set
    //</summary>
    public class Brand_SetResult
    {
        private List<Business.SimpleBrandSet> _Data;
        public List<Business.SimpleBrandSet> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleBrandSet>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Brand_Set_Detail
    //</summary>
    public class Brand_Set_DetailResult
    {
        private List<Business.SimpleBrandSetDetail> _Data;
        public List<Business.SimpleBrandSetDetail> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleBrandSetDetail>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Brewery
    //</summary>
    public class BreweryResult
    {
        private List<Business.SimpleBrewery> _Data;
        public List<Business.SimpleBrewery> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleBrewery>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }
    //<summary>
    //Kết quả trả về ws Brewery_Brand
    //</summary>
    public class Brewery_BrandResult
    {
        private List<Business.SimpleBreweryBrand> _Data;
        public List<Business.SimpleBreweryBrand> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<SimpleBreweryBrand>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }


    //<summary>
    //Kết quả trả về ws simple  Notification
    //</summary>
    public class NotificationResult
    {
        private List<Business.SimpleNotification> _Data;
        public List<Business.SimpleNotification> Data
        {
            get
            {
                if (_Data == null)
                    _Data = new List<Business.SimpleNotification>();
                return _Data;
            }
            set
            {
                _Data = value;
            }
        }
        public int Status { get; set; }
        public string Description { get; set; }
    }

}
