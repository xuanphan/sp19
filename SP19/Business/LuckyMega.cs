﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class LuckyMega
    {

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT getData(string pAppCode, ref GetDataMegaResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu vòng quay
                    var query = from item in context.Lucky_Mega_Turns
                                where item.Status != 2
                                select new WSSimpleDataMega
                                {
                                    TurnID = item.ID,
                                    DateStart = item.DateStart,
                                    DateEnd = item.DateEnd,
                                    ListGift = item.Lucky_Mega_Gifts.Select(o => new WSSimpleDataGift
                                    {
                                        GiftID = o.ID,
                                        Name = o.Name,
                                        FilePath = o.FilePath,
                                        IsGift = o.IsGift
                                    }).ToList()

                                };
                    // gán giá trị cho phần trả về
                    result.Data = query.ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;

                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW addGiftMega(string pAppCode, Customer_Gift_Mega entity, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.Trim().ToLower() != Common.Global.AppCode.Trim().ToLower())
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
                // khởi tạo transaction
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();

                context.Customer_Gift_Megas.InsertOnSubmit(entity); // thêm
                context.SubmitChanges(); // thực thi câu lệnh

                context.Transaction.Commit(); // hoàn thành transaction
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();// trả lại ban đầu nếu trong quá trình xử lý có lỗi
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = "Lỗi: " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT setReaded(string pAppCode, string pOutletID, string pGiftID, string pTeamOutletID, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu thông báo gửi xuống
                    var query = from item in context.Lucky_Mega_Set_Gift_To_Outlets
                                where item.OutletID == int.Parse(pOutletID)
                                && item.GiftID == int.Parse(pGiftID)
                                && item.CreatedDateTime.Date >= DateTime.Now.Date
                                select item;

                    if (query.Count() > 0) // nếu có
                    {
                        // gán dữ liệu Lucky_Mega_Set_Gift_To_Outlet
                        var temp = query.FirstOrDefault();

                        // kiểm tra IsNotificationToDevice 
                        if (temp.IsNotificationToDevice) // nếu = true thì đã có xác nhận => trả về
                        {
                            // gán giá trị cho phần trả về
                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                            result.Description = "Đã có device khác xác nhận!";
                            return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.EXIT_DATA;
                        }
                        else // nếu = false thì cập nhật thành công
                        {
                            temp.IsNotificationToDevice = true;
                            temp.LastUpdatedBy = int.Parse(pTeamOutletID);
                            temp.LastUpdatedDateTime = DateTime.Now;
                            temp.RowVersion++;
                            context.SubmitChanges(); // thực thi câu lệnh

                            // gán giá trị cho phần trả về
                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                            return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                        }
                    }
                    else // nếu không báo lỗi không có dữ liệu
                    {
                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                    }
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;

                }



            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = "Lỗi: " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW addNumberDialMega(string pAppCode, List<Customer_Total_Dial_Mega> list, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // khởi tạo transaction
                    context.Connection.Open();
                    context.Transaction = context.Connection.BeginTransaction();

                    context.Customer_Total_Dial_Megas.InsertAllOnSubmit(list); // thêm
                    context.SubmitChanges(); // thực thi câu lệnh

                    context.Transaction.Commit(); // hoàn thành transaction
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = "Lỗi: " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }

    public class WSSimpleDataMega
    {
        public int TurnID { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public List<WSSimpleDataGift> ListGift { get; set; }
    }

    public class WSSimpleDataGift
    {
        public int GiftID { get; set; }
        public string Name { get; set; }
        public string FilePath { get; set; }
        public bool IsGift { get; set; }
    }
}
