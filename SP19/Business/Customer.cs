﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Customer
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(string _pAppCode, ref AddCustomerResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    context.Customers.InsertOnSubmit(this); // thêm
                    context.SubmitChanges(); // thực thi câu lệnh

                    // gán giá trị cho phần trả về
                    result.Data = this.ID;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = 0;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }

            }

            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = 0;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageSystemError, int _pProjectID, int _pOutletType, int pageNum, CustomerSE se, ref List<CustomerEXT> list)
        {
            DatabaseDataContext context = null;
            try
            {
                int pagesize = 20;
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.BaoCaoKhachHang(se.OutletCustomerSE, se.DateFromCustomerSE.Date, se.DateToCustomerSE.Date,_pProjectID,_pOutletType,se.RegionCustomerSE,pageNum,pagesize).ToList()
                            select new CustomerEXT
                            {
                                Total = item.TotalCount.GetValueOrDefault(),
                                CustomerID = item.ID,
                                RegionName = item.RegionName,
                                City = item.City,
                                OutletName = item.OutletName,
                                CreateDate = item.DeviceDateTime,
                                CustomerName = item.CustomerName,
                                CustomerPhone = item.CustomerPhone,
                                CustomerAddress = item.Address,
                                NumberBill = item.BillNumber
                            };
                list = query.ToList();
                foreach (var i in list)
                {
                    i.listproducts = context.KhachHangSanPham(i.CustomerID).ToList();
                    i.listimages = context.KhachHangImage(i.CustomerID).ToList();
                    i.listgifts = context.KhachHangGift(i.CustomerID).ToList();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                list = null;
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchExcel(ref string _messageSystemError, int _pProjectID, int _pOutletType,  CustomerSE se, ref List<CustomerEXT> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.BaoCaoKhachHangEXT(se.OutletCustomerSE, se.DateFromCustomerSE.Date, se.DateToCustomerSE.Date, _pProjectID, _pOutletType, se.RegionCustomerSE).ToList()
                            select new CustomerEXT
                            {
                                CustomerID = item.ID,
                                RegionName = item.RegionName,
                                City = item.City,
                                OutletName = item.OutletName,
                                CreateDate = item.DeviceDateTime,
                                CustomerName = item.CustomerName,
                                CustomerPhone = item.CustomerPhone,
                                CustomerAddress = item.Address,
                                NumberBill = item.BillNumber

                            };
                list = query.ToList();
                var sanpham = context.View_Customer_Products.ToList();
                var qua = context.View_Customer_Gifts.ToList();
                var hinh = context.View_Customer_Images.ToList();
                foreach (var i in list)
                {
                    i.listproduct = sanpham.Where(p => p.CustomerID == i.CustomerID).ToList();
                    i.listimage = hinh.Where(p => p.CustomerID == i.CustomerID).ToList();
                    i.listgift = qua.Where(p => p.CustomerID == i.CustomerID).ToList();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                list = null;
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchMega(ref string _messageSystemError, int _pProjectID, int _pOutletType, CustomerSE se, ref List<CustomerEXT> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_Customers
                            where (item.DeviceDateTime.Date >= se.DateFromCustomerSE.Date && item.DeviceDateTime.Date <= se.DateToCustomerSE.Date)
                            && (item.ProjectID == _pProjectID)
                            && item.ChanelID == 22
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            && (item.Type == se.RegionCustomerSE || se.RegionCustomerSE == null || se.RegionCustomerSE == "Tất Cả")
                            && (item.OutletID == se.OutletCustomerSE || se.OutletCustomerSE == null || se.OutletCustomerSE == 0)
                            select new CustomerEXT
                            {
                                RegionName = item.RegionName,
                                City = item.City,
                                OutletName = item.OutletName,
                                CreateDate = item.DeviceDateTime,
                                CustomerName = item.CustomerName,
                                CustomerPhone = item.CustomerPhone,
                                CustomerAddress = item.Address,
                                NumberBill = item.BillNumber,
                                listproduct = context.View_Customer_Products.Where(p => p.CustomerID == item.ID).ToList(),
                                listimage = context.View_Customer_Images.Where(p => p.CustomerID == item.ID).ToList(),
                                listgiftmega = context.View_Customer_Gift_Megas.Where(p => p.CustomerID == item.ID).ToList()
                            };
                list = query.ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                list = null;
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }

    public class CustomerSE
    {
        [UIHint("_listOutlet")]
        [Display(Name = "Siêu Thị")]
        public long OutletCustomerSE { get; set; }

        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionCustomerSE { get; set; }

        private DateTime _DateFromCustomerSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Ngày Bắt Đầu")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateFromCustomerSE
        {
            get { return _DateFromCustomerSE; }
            set
            {
                if (value != null)
                    _DateFromCustomerSE = value.Date;
            }
        }

        private DateTime _DateToCustomerSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Ngày Kết Thúc")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateToCustomerSE
        {
            get { return _DateToCustomerSE; }
            set
            {
                if (value != null)
                    _DateToCustomerSE = value.Date;
            }
        }
    }
    public class CustomerEXT
    {
        public int Total { get; set; }
        public long CustomerID { get; set; }
        public string RegionName { get; set; }
        public string City { get; set; }
        public string OutletName { get; set; }
        public DateTime CreateDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerAddress { get; set; }
        public string NumberBill { get; set; }
        public List<KhachHangSanPhamResult> listproducts { get; set; }
        public List<KhachHangImageResult> listimages { get; set; }
        public List<KhachHangGiftResult> listgifts { get; set; }

        public List<View_Customer_Product> listproduct { get; set; }
        public List<View_Customer_Image> listimage { get; set; }
        public List<View_Customer_Gift> listgift { get; set; }
        public List<Customer_Product> listproduct1 { get; set; }
        public List<Customer_Image> listimage1 { get; set; }
        public List<Customer_Gift> listgift1 { get; set; }
        public List<View_Customer_Gift_Mega> listgiftmega { get; set; }
    }
}

