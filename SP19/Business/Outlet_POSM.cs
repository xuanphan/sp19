﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Outlet_POSM
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(ref string _messageSystemError, string _pAppCode, ref List<SimpleOutletPOSM> list, ref OutletPOSMResult result)
      {
          DatabaseDataContext context = null;
          try
          {
              context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
              if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
              {

                  var query = from item in context.Outlet_POSMs
                              where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                              select new SimpleOutletPOSM
                              {
                                  ID = int.Parse( item.ID.ToString()),
                                  POSMID = item.POSMID,
                                  OutletID = item.OutletID
                              };

                  list = query.ToList();
                  result.Data = list;
                  result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                  result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                  return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
              }
              else
              {
                  result.Data = null;
                  result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                  result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                  return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
              }
          }


          catch (Exception ex)
          {
              _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
              result.Data = null;
              result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
              result.Description = ex.Message;
              return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
          }
          finally
          {
              context = null;
              GC.Collect();
          }
      }
    }
  public class SimpleOutletPOSM
  {
      private int? _ID;
      public int? ID
      {
          get { return _ID; }
          set { _ID = value; }
      }
      private int? _OutletID;
      public int? OutletID
      {
          get { return _OutletID; }
          set { _OutletID = value; }
      }
      private int? _POSMID;
      public int? POSMID
      {
          get { return _POSMID; }
          set { _POSMID = value; }
      }      
      public SimpleOutletPOSM() { }

      public SimpleOutletPOSM(int ID, int OutletID, int POSMID)
      {
          this.OutletID = OutletID;
          this.ID = ID;
          this.POSMID = POSMID;
      }
  }
}
