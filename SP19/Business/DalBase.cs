﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    /// <summary>
    /// ParamProcInfo: tham số Procedure
    /// </summary>
    public class ParamProcInfo
    {
        private string _ParamName = String.Empty;
        public string ParamName
        {
            get { return _ParamName; }
            set { _ParamName = value; }
        }

        private SqlDbType _ParamType;
        public SqlDbType ParamType
        {
            get { return _ParamType; }
            set { _ParamType = value; }
        }

        private int _Size = 0;
        public int Size
        {
            get { return _Size; }
            set { _Size = value; }
        }

        private ParameterDirection _ParamDirection;
        public ParameterDirection ParamDirection
        {
            get { return _ParamDirection; }
            set { _ParamDirection = value; }
        }

        private Object _ParamValue;
        public Object ParamValue
        {
            get { return _ParamValue; }
            set { _ParamValue = value; }
        }

        public ParamProcInfo() { }
        public ParamProcInfo(string ParamName, SqlDbType ParamType, ParameterDirection ParamDirection, Object ParamValue)
        {
            _ParamName = ParamName;
            _ParamType = ParamType;
            _ParamDirection = ParamDirection;
            _ParamValue = ParamValue;
        }

        public ParamProcInfo(string ParamName, SqlDbType ParamType, int Size, ParameterDirection ParamDirection, Object ParamValue)
        {
            _ParamName = ParamName;
            _ParamType = ParamType;
            _Size = Size;
            _ParamDirection = ParamDirection;
            _ParamValue = ParamValue;
        }
    }

    /// <summary>
    /// FieldInfo class: lưu thông tin một cell trong table
    /// </summary>
    public class FieldInfo
    {
        private string _ColumnName = string.Empty;
        public string ColumnName
        {
            get { return _ColumnName; }
            set { _ColumnName = value; }
        }

        private object _Value = string.Empty;
        public object Value
        {
            get { return Value; }
            set { _Value = value; }
        }
    }

    public class DalBase
    {
        private SqlConnection Conn;

        public DalBase()
        {

        }

        private void OpenConn()
        {
            if (Conn == null)
                Conn = new SqlConnection(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            Conn.Open();
        }

        private void CloseConn()
        {
            if (Conn != null)
                Conn.Close();
        }

        /// <summary>
        /// Lấy next identity của table
        /// </summary>
        public int GetNextIdentityOfTable(string TableName, int Seek)
        {
            string commandText = String.Format("SELECT IDENT_CURRENT ('{0}') AS NextIdentity", TableName);
            object temp = ExecuteScalarCommand(commandText);
            int nextID = Convert.ToInt32(temp) + Seek;
            return nextID;
        }

        public DataTable GetDataFromQuery(string pCommandText)
        {
            try
            {
                DataTable result = new DataTable();
                OpenConn();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(pCommandText, Conn);
                dataAdapter.Fill(result);
                return result;
            }
            catch (Exception ex)
            {
                CloseConn();
                throw ex;
            }
            finally
            {
                CloseConn();
            }
        }

        public DataTable GetDataFromProc(string ProcName, params ParamProcInfo[] ListParam)
        {
            try
            {
                OpenConn();
                DataTable result = new DataTable();
                SqlCommand command = new SqlCommand(ProcName, Conn);
                command.CommandType = CommandType.StoredProcedure;
                foreach (ParamProcInfo item in ListParam)
                {
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = item.ParamName;
                    param.SqlDbType = item.ParamType;
                    if (item.Size != 0)
                        param.Size = item.Size;
                    param.Direction = item.ParamDirection;
                    param.Value = item.ParamValue;
                    command.Parameters.Add(param);
                }

                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                dataAdapter.Fill(result);
                return result;
            }
            catch (Exception ex)
            {
                CloseConn();
                throw ex;
            }
            finally
            {
                CloseConn();
            }
        }

        public string ExecuteProc(string ProcName, params ParamProcInfo[] ListParam)
        {
            try
            {
                OpenConn();
                DataTable result = new DataTable();
                SqlCommand command = new SqlCommand(ProcName, Conn);
                command.CommandType = CommandType.StoredProcedure;
                foreach (ParamProcInfo item in ListParam)
                {
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = item.ParamName;
                    param.SqlDbType = item.ParamType;
                    if (item.Size != 0)
                        param.Size = item.Size;
                    param.Direction = item.ParamDirection;
                    param.Value = item.ParamValue;
                    command.Parameters.Add(param);
                }
                command.ExecuteNonQuery();

                //Lấy parameter output
                int totalParam = command.Parameters.Count - 1;
                string msg = String.Empty;
                while (command.Parameters[totalParam].Direction == ParameterDirection.Output)
                {
                    msg += command.Parameters[totalParam].Value.ToString();
                    totalParam--;
                }
                return msg;
            }
            catch (Exception ex)
            {
                CloseConn();
                throw ex;
            }
            finally
            {
                CloseConn();
            }
        }

        public string ExecuteProc(SqlCommand SQLCommand)
        {
            try
            {
                OpenConn();
                SQLCommand.CommandType = CommandType.StoredProcedure;
                SQLCommand.Connection = Conn;
                SQLCommand.ExecuteNonQuery();
                //Lấy parameter output
                int totalParam = SQLCommand.Parameters.Count - 1;
                string msg = String.Empty;
                while (SQLCommand.Parameters[totalParam].Direction == ParameterDirection.Output)
                {
                    msg += SQLCommand.Parameters[totalParam].Value.ToString();
                    totalParam--;
                }
                return msg;
            }
            catch (Exception ex)
            {
                CloseConn();
                throw ex;
            }
            finally
            {
                CloseConn();
            }
        }

        public int ExecuteNonqueryCommand(string pCommandText)
        {
            try
            {
                OpenConn();
                SqlCommand command = new SqlCommand(pCommandText, Conn);
                int result = command.ExecuteNonQuery();
                return result;
            }
            catch (Exception ex)
            {
                CloseConn();
                throw ex;
            }
            finally
            {
                CloseConn();
            }
        }

        public object ExecuteScalarCommand(string pCommandText)
        {
            try
            {
                OpenConn();
                SqlCommand command = new SqlCommand(pCommandText, Conn);
                object result = command.ExecuteScalar();
                return result;
            }
            catch (Exception ex)
            {
                CloseConn();
                throw ex;
            }
            finally
            {
                CloseConn();
            }
        }
    }
}
