﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Outlet
    {
        //all 
        public IEnumerable<ComboItem> GetAllForCombo(bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForCombo(int _pProjectID, int _pOutletType, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                              && (item.ProjectID == _pProjectID)
                              && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForComboNotifi(int _pProjectID, int _pOutletType, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                              && (item.ProjectID == _pProjectID)
                              && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }

        public IEnumerable<ComboItem> GetAllForComboMega(int _pProjectID, int _pOutletType, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                              && (item.ProjectID == _pProjectID)
                              && (item.Chanel.ChanelName == "Mega Market")
                              && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForComboSale(int _pProjectID, int _pSaleID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && saleoutlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && saleoutlet.SaleID == _pSaleID
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForComboSaleSup(int _pProjectID, int _pSaleSupID, string _pListSaleID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                List<string> result = _pListSaleID.Split('|').ToList();
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            join salesup in context.Sale_Sups on saleoutlet.SaleID equals salesup.SaleID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && saleoutlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && salesup.SaleSupID == _pSaleSupID
                            && result.Contains(saleoutlet.SaleID.ToString())
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForComboSup(int _pProjectID, int _pSupID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join supoutlet in context.Sup_Outlet_Webs on item.ID equals supoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                             && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && supoutlet.SupID == _pSupID
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetOutletForRegionSale(string _pRegionID, int _pProjectID, int _pOutletType, int _pSaleID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            where (item.Region.Type == _pRegionID || _pRegionID == "Tất Cả")
                            && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                           && (item.ProjectID == _pProjectID)
                           && (item.OutletTypeID == _pOutletType || _pOutletType == 0 || _pOutletType == -1)
                           && (saleoutlet.SaleID == _pSaleID)
                            select new ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetOutletForRegionSaleSup(string _pRegionID, int _pProjectID, int _pSaleSupID, string _pListSaleID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                List<string> result = _pListSaleID.Split('|').ToList();
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            join salesup in context.Sale_Sups on saleoutlet.SaleID equals salesup.SaleID
                            where (item.Region.Type == _pRegionID || _pRegionID == "Tất Cả")
                           && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                           && (item.ProjectID == _pProjectID)
                           && (salesup.SaleSupID == _pSaleSupID)
                           && result.Contains(saleoutlet.SaleID.ToString())
                            select new ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetOutletForRegion(string _pRegionID, int _pProjectID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            where (item.Region.Type == _pRegionID || _pRegionID == "Tất Cả")
                            && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                           && (item.ProjectID == _pProjectID)
                           && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select new ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetOutletForRegion(string _pRegionID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            where (item.Region.Type == _pRegionID || _pRegionID == "Tất Cả")
                            && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            select new ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetOutletForRegionSup(string _pRegionID, int _pProjectID, int _pSupID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join supoutlet in context.Sup_Outlet_Webs on item.ID equals supoutlet.OutletID
                            where (item.Region.Type == _pRegionID || _pRegionID == "Tất Cả")
                            && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                             && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && supoutlet.SupID == _pSupID
                            select new ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }
        public static List<Outlet> GetAll(int _pProjectID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select item;
                var list = query.ToList().OrderBy(p => p.OutletName).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Outlet> GetAllSup(int _pUserID, int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Sup_Outlets
                            where item.Outlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.Outlet.ProjectID == _pProjectID)
                            && item.SupID == _pUserID
                            && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select item.Outlet;
                var list = query.ToList().OrderBy(p => p.OutletName).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Outlet> GetAllKho(int _pUserID, int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Kho_Outlets
                            where item.Outlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.Outlet.ProjectID == _pProjectID)
                            && item.KhoID == _pUserID
                            && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select item.Outlet;
                var list = query.ToList().OrderBy(p => p.OutletName).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Outlet> GetForUser(string _pUserType, string _pUserID, int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                if (_pUserType.Trim() == "SALE")
                {
                    context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                    var query = from item in context.Sale_Outlets
                                where item.Outlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && (item.Outlet.ProjectID == _pProjectID || _pProjectID == 0)
                                && item.SaleID == int.Parse(_pUserID)
                                && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                select item.Outlet;
                    var list = query.ToList();
                    return list;
                }
                else if (_pUserType.Trim() == "SUP")
                {
                    context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                    var query = from item in context.Sup_Outlets
                                where item.Outlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && (item.Outlet.ProjectID == _pProjectID || _pProjectID == 0)
                                && item.SupID == int.Parse(_pUserID)
                                && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                select item.Outlet;
                    var list = query.ToList();
                    return list;
                }
                else
                {
                    context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                    var query = from item in context.Kho_Outlets
                                where item.Outlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && (item.Outlet.ProjectID == _pProjectID || _pProjectID == 0)
                                && item.KhoID == int.Parse(_pUserID)
                                && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                select item.Outlet;
                    var list = query.ToList();
                    return list;
                }


            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Take_Off_VolumnOutletDate> SearchOutletTakeOff(int _pProjectID, int _pOutletType, int _pOutletID, string _pRegionID, DateTime _datef, DateTime _datet)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Take_Off_Volumns
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.Outlet.ProjectID == _pProjectID || _pProjectID == 0)
                            && (item.OutletID == _pOutletID || _pOutletID == 0)
                            && (item.Outlet.Region.Type == _pRegionID || _pRegionID == "Tất Cả" || _pRegionID == null || _pRegionID == "")
                            && (item.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                            && (item.DeviceDateTime.Date >= _datef.Date && item.DeviceDateTime.Date <= _datet.Date)
                            select new Take_Off_VolumnOutletDate
                            {
                                OutletID = item.OutletID,
                                Dates = item.DeviceDateTime.Date,
                                OutletName = item.Outlet.OutletName,
                                City = item.Outlet.City,
                                RegionType = item.Outlet.Region.Type,
                                GroupID = item.Outlet.GroupID
                            };
                var list = query.ToList().Distinct(new ComparerOutletTakeOff()).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Outlet> SearchOutlet(int _DistrictID, int _pProjectID, int _pOutletType, int _pOutletID, string _pRegionID, string _SaleID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && saleoutlet.Status != 2
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && (item.ID == _pOutletID || _pOutletID == 0)
                            && (item.Region.Type == _pRegionID || _pRegionID == "Tất Cả" || _pRegionID == null || _pRegionID == "0")
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            && (item.RegionID == _DistrictID || _DistrictID == 0)
                            && (saleoutlet.SaleID == int.Parse(_SaleID) || _SaleID == "0")
                            select item;
                var list = query.ToList();


                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Sale_Outlet> SearchOutletExcel(int _districtID,int _pProjectID, int _pOutletType, int _pOutletID, string _pRegionID, string _SaleID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && saleoutlet.Status!= 2
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && (item.ID == _pOutletID || _pOutletID == 0)
                            && (item.Region.Type == _pRegionID || _pRegionID == "Tất Cả" || _pRegionID == null || _pRegionID == "0")
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            && (item.RegionID == _districtID || _districtID == 0)
                            && (saleoutlet.SaleID == int.Parse(_SaleID) || _SaleID == "0")
                            select saleoutlet;
                var list = query.ToList().OrderBy(p => p.Outlet.Chanel.ChanelName).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        //Ham Group
        class ComparerOutletTakeOff : IEqualityComparer<Take_Off_VolumnOutletDate>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(Take_Off_VolumnOutletDate x, Take_Off_VolumnOutletDate y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.OutletID == y.OutletID && x.Dates.Date == y.Dates.Date;
                // return x.AssignID == y.AssignID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(Take_Off_VolumnOutletDate item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashOutletID = item.OutletID == null ? 0 : item.OutletID.GetHashCode();
                int hashdate = item.Dates.Date == null ? 0 : item.Dates.Date.GetHashCode();
                //Calculate the hash code for the product.
                return hashOutletID ^ hashdate;
                // return hashAssignID;
            }
        }
        public static List<Outlet> GetAllForSale(int _pProjectID, int _pSaleID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && saleoutlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == -1)
                            && saleoutlet.SaleID == _pSaleID
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Outlet> GetAllForSaleSup(int _pProjectID, int _pSaleSupID, string _pListSaleID)
        {
            DatabaseDataContext context = null;
            try
            {
                List<string> result = _pListSaleID.Split('|').ToList();
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            join salesup in context.Sale_Sups on saleoutlet.SaleID equals salesup.SaleID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && saleoutlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            && salesup.SaleSupID == _pSaleSupID
                            && result.Contains(saleoutlet.SaleID.ToString())
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Outlet> SearchOutletForSale(int _pProjectID, int _pSaleID, int _pOutletID, string _pRegionID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                             && (item.ProjectID == _pProjectID)
                            && (item.ID == _pOutletID || _pOutletID == 0)
                            && (item.Region.Type == _pRegionID || _pRegionID == null || _pRegionID == "0" || _pRegionID == "Tất Cả")
                            && saleoutlet.SaleID == _pSaleID
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Outlet> SearchOutletForSaleSup(int _pProjectID, int _pSaleSupID, int _pOutletID, string _pRegionID, string _pListSaleID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                List<string> result = _pListSaleID.Split('|').ToList();
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            join salesup in context.Sale_Sups on saleoutlet.SaleID equals salesup.SaleID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            && (item.ID == _pOutletID || _pOutletID == 0)
                            && (item.Region.Type == _pRegionID || _pRegionID == null || _pRegionID == "0" || _pRegionID == "Tất Cả")
                            && salesup.SaleSupID == _pSaleSupID
                            && result.Contains(saleoutlet.SaleID.ToString())
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static Outlet GetSimple(int _OutletID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            where item.ID == _OutletID
                            select item;
                if (query.Count() > 0)
                {
                    return query.FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT getAllWS(string pAppCode, int pProjectID, int pUserID, ref GetAllOutletResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {

                    // lấy danh sách thông tin outlet tham chiếu từ lớp cha Kho_Outlet
                    var query = from item in context.Kho_Outlets
                                where (item.KhoID == pUserID || pUserID == 0)
                                && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.Outlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                select new SimpleOutletWS
                                {
                                    ID = item.Outlet.ID,
                                    OutletName = item.Outlet.OutletName,
                                    OutletAddress = item.Outlet.OutletAddress,
                                    City = item.Outlet.City,
                                    District = item.Outlet.District

                                };

                    // gán giá trị cho phần trả về
                    result.Data = query.ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = new List<SimpleOutletWS>();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = new List<SimpleOutletWS>();
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static MainResult addRP(List<EntityOutletForIP> listRP)
        {
            DatabaseDataContext context = null;
            MainResult result = new MainResult();
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();
                foreach (var item in listRP)
                {
                    context.Outlets.InsertOnSubmit(item.mOutlet);
                    context.SubmitChanges(); // có outletID

                    item.mListOutletBrand.All(o => { o.OutletID = item.mOutlet.ID; return true; });
                    item.mListOutletCurrentSet.All(o => { o.OutletID = item.mOutlet.ID; return true; });
                    item.mListOutletPOSM.All(o => { o.OutletID = item.mOutlet.ID; return true; });

                    context.Outlet_Brands.InsertAllOnSubmit(item.mListOutletBrand);
                    context.Outlet_Current_Sets.InsertAllOnSubmit(item.mListOutletCurrentSet);
                    context.Outlet_POSMs.InsertAllOnSubmit(item.mListOutletPOSM);
                    context.Teams.InsertAllOnSubmit(item.mListTeam);
                    context.SubmitChanges(); // có thêm teamID

                    //update teamoutlet
                    for (int i = 0; i < item.mListTeam.Count(); i++)
                    {
                        item.mListTeamOutlet[i].TeamID = item.mListTeam[i].ID;
                        item.mListTeamOutlet[i].OutletID = item.mOutlet.ID;
                    }

                    context.Team_Outlets.InsertAllOnSubmit(item.mListTeamOutlet);
                    context.SubmitChanges(); // teamoutletid

                    //update teamoutlet
                    for (int i = 0; i < item.mListTeamOutlet.Count(); i++)
                    {
                        item.mListTeamPoint[i].TeamOutletID = item.mListTeamOutlet[i].ID;
                    }

                    context.Team_TimePoints.InsertAllOnSubmit(item.mListTeamPoint);
                    context.SubmitChanges(); // done
                }




                context.Transaction.Commit();

                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                result.Description = "Import thành công.";
                return result;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + " " + ex.Message;
                return result;
            }
            finally
            {
                context.Connection.Close();
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
        public static MainResult AddTeamOutlet(Outlet outlet, Team team)
        {
            DatabaseDataContext context = null;
            MainResult result = new MainResult();
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();

                context.Outlets.InsertOnSubmit(outlet);
                context.Teams.InsertOnSubmit(team);
                context.SubmitChanges();
                int idoutlet = outlet.ID;
                int idteam = team.ID;
                Team_Outlet teamoutlet = new Team_Outlet();
                teamoutlet.TeamID = idteam;
                teamoutlet.OutletID = idoutlet;
                teamoutlet.CreatedBy = idteam;
                teamoutlet.CreatedDateTime = DateTime.Now;
                teamoutlet.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                teamoutlet.RowVersion = 1;
                context.Team_Outlets.InsertOnSubmit(teamoutlet);
                context.SubmitChanges();
                context.Transaction.Commit();

                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                result.Description = "Import thành công.";
                return result;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + " " + ex.Message;
                return result;
            }
            finally
            {
                context.Connection.Close();
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE UpdateOutletForUser(ref string _messageError, string _pListUserID, string _pUser, string _pUserType, int _pProjectID, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (_pUserType.Trim() == "SALE")
                {
                    //disbale
                    var query = from item in context.Sale_Outlets
                                where item.SaleID == int.Parse(_pUser)
                                 && item.Outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                 && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                select item;
                    // không có ở hiện tại thì tạo và update trạng thái
                    if (query.Count() > 0)
                    {
                        // disable hết
                        var list = query.ToList();
                        foreach (var y in list)
                        {
                            y.Status = (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE;
                            y.LastUpdatedBy = _pUserID;
                            y.LastUpdatedDateTime = DateTime.Now;
                            y.RowVersion += 1;
                            context.SubmitChanges();
                        }

                    }
                    //add lại ID
                    string[] _pID = _pListUserID.Split('|');
                    for (int i = 0; i < _pID.Count(); i++)
                    {
                        var query2 = from item in context.Sale_Outlets
                                     where item.SaleID == int.Parse(_pUser)
                                      && item.Outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                      && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                      && item.OutletID == int.Parse(_pID[i])
                                     select item;
                        // không có ở hiện tại thì tạo và update trạng thái
                        if (query2.Count() > 0)
                        {
                            // disable hết
                            var simple = query2.FirstOrDefault();
                            simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            simple.LastUpdatedBy = _pUserID;
                            simple.LastUpdatedDateTime = DateTime.Now;
                            simple.RowVersion += 1;
                            context.SubmitChanges();
                        }
                        else
                        {
                            //tạo mới
                            Sale_Outlet sale_Outlets = new Sale_Outlet();
                            sale_Outlets.OutletID = int.Parse(_pID[i]);
                            sale_Outlets.SaleID = int.Parse(_pUser);
                            sale_Outlets.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            sale_Outlets.CreatedBy = _pUserID;
                            sale_Outlets.CreatedDateTime = DateTime.Now;
                            sale_Outlets.RowVersion = 1;
                            context.Sale_Outlets.InsertOnSubmit(sale_Outlets);
                            context.SubmitChanges();
                        }
                    }
                }
                else if (_pUserType.Trim() == "SUP")
                {
                    //disbale
                    var query = from item in context.Sup_Outlets
                                where item.SupID == int.Parse(_pUser)
                                 && item.Outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                 && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                select item;
                    // không có ở hiện tại thì tạo và update trạng thái
                    if (query.Count() > 0)
                    {
                        // disable hết
                        var list = query.ToList();
                        foreach (var y in list)
                        {
                            y.Status = (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE;
                            y.LastUpdatedBy = _pUserID;
                            y.LastUpdatedDateTime = DateTime.Now;
                            y.RowVersion += 1;
                            context.SubmitChanges();
                        }

                    }
                    //add lại ID
                    string[] _pID = _pListUserID.Split('|');
                    for (int i = 0; i < _pID.Count(); i++)
                    {
                        var query2 = from item in context.Sup_Outlets
                                     where item.SupID == int.Parse(_pUser)
                                      && item.Outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                      && item.OutletID == int.Parse(_pID[i])
                                     select item;
                        // không có ở hiện tại thì tạo và update trạng thái
                        if (query2.Count() > 0)
                        {
                            // disable hết
                            var simple = query2.FirstOrDefault();
                            simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            simple.LastUpdatedBy = _pUserID;
                            simple.LastUpdatedDateTime = DateTime.Now;
                            simple.RowVersion += 1;
                            context.SubmitChanges();
                        }
                        else
                        {
                            //tạo mới
                            Sup_Outlet sup_Outlet = new Sup_Outlet();
                            sup_Outlet.OutletID = int.Parse(_pID[i]);
                            sup_Outlet.SupID = int.Parse(_pUser);
                            sup_Outlet.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            sup_Outlet.CreatedBy = _pUserID;
                            sup_Outlet.CreatedDateTime = DateTime.Now;
                            sup_Outlet.RowVersion = 1;
                            context.Sup_Outlets.InsertOnSubmit(sup_Outlet);
                            context.SubmitChanges();
                        }
                    }
                }
                else
                {
                    //disbale
                    var query = from item in context.Kho_Outlets
                                where item.KhoID == int.Parse(_pUser)
                                 && item.Outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                 && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                select item;
                    // không có ở hiện tại thì tạo và update trạng thái
                    if (query.Count() > 0)
                    {
                        // disable hết
                        var list = query.ToList();
                        foreach (var y in list)
                        {
                            y.Status = (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE;
                            y.LastUpdatedBy = _pUserID;
                            y.LastUpdatedDateTime = DateTime.Now;
                            y.RowVersion += 1;
                            context.SubmitChanges();
                        }

                    }
                    //add lại ID
                    string[] _pID = _pListUserID.Split('|');
                    for (int i = 0; i < _pID.Count(); i++)
                    {
                        var query2 = from item in context.Kho_Outlets
                                     where item.KhoID == int.Parse(_pUser)
                                      && item.Outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                                      && item.OutletID == int.Parse(_pID[i])
                                     select item;
                        // không có ở hiện tại thì tạo và update trạng thái
                        if (query2.Count() > 0)
                        {
                            // disable hết
                            var simple = query2.FirstOrDefault();
                            simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            simple.LastUpdatedBy = _pUserID;
                            simple.LastUpdatedDateTime = DateTime.Now;
                            simple.RowVersion += 1;
                            context.SubmitChanges();
                        }
                        else
                        {
                            //tạo mới
                            Kho_Outlet kho_Outlet = new Kho_Outlet();
                            kho_Outlet.OutletID = int.Parse(_pID[i]);
                            kho_Outlet.KhoID = int.Parse(_pUser);
                            kho_Outlet.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            kho_Outlet.CreatedBy = _pUserID;
                            kho_Outlet.CreatedDateTime = DateTime.Now;
                            kho_Outlet.RowVersion = 1;
                            context.Kho_Outlets.InsertOnSubmit(kho_Outlet);
                            context.SubmitChanges();
                        }
                    }
                }

                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static int GetGroup(int _outletID)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                //--------------Co stock trong Ngay
                var query = from item in context.Outlets
                            where item.ID == _outletID
                                  && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select item.GroupID;
                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public object GetOutletForSale(int _SaleID, string _pRegionID, int _pProjectID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Sale_Outlets
                            where (item.Outlet.Region.Type == _pRegionID || _pRegionID == "Tất Cả")
                            && item.Outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                           && (item.Outlet.ProjectID == _pProjectID)
                           && (item.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                           && (item.SaleID == _SaleID || _SaleID == 0)
                            select new ComboItem
                            {
                                ID = item.Outlet.ID,
                                Value = item.Outlet.OutletName,
                                Text = item.Outlet.OutletName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }

        public object GetOutletForDistrict(int _RegionID,int _pProjectID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            where (item.RegionID == _RegionID || _RegionID == 0)
                            && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                           && (item.ProjectID == _pProjectID)
                           && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select new ComboItem
                            {
                                ID = item.ID,
                                Value = item.OutletName,
                                Text = item.OutletName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }

        public static List<Outlet> getByProjectAndType(int ProjectID, int OutletTypeID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            where item.ProjectID == ProjectID
                           && (item.OutletTypeID == OutletTypeID || OutletTypeID == 0)
                            select item;
                return query.ToList();
            }
            catch (Exception ex)
            {
                return new List<Outlet>();
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT logOut(string pAppCode, string pOutletID, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu outlet
                    var query = from item in context.Outlets
                                where item.ID == int.Parse(pOutletID)
                                select item;
                    if (query.Count() > 0)  // nếu có update trạng thái
                    {
                        var temp = query.FirstOrDefault();
                        temp.IsOnline = false;
                        context.SubmitChanges();

                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    }
                    else // nếu không báo lỗi không có dữ liệu
                    {
                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                    }
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static string clearRef(string pOutletID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // lấy dữ liệu
                var query = from item in context.Outlets
                            where item.ID == int.Parse(pOutletID)
                            select item;

                if (query.Count() > 0) // nếu có thì send firebase data xuống cho device 
                {
                    FCM.SendClearRef(query.FirstOrDefault().DeviceToken);
                    // trả về thành công
                    return "Thành công.";
                }
                else // nếu không trả về chuỗi lỗi
                {
                    return "Outlet không tồn tại";
                }

            }
            catch (Exception ex)
            {
                // catch thì báo lỗi
                return "Lỗi"+ex.Message;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }


        public static string resetData(string pOutletID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // lấy dữ liệu
                var query = from item in context.Outlets
                            where (item.ID == int.Parse(pOutletID) || int.Parse(pOutletID) == -1)
                            select item;

                if (query.Count() > 0) // nếu có thì send firebase data xuống cho device 
                {
                    var listData = query.ToList();
                    foreach (var item in listData)
                    {
                        if (item.DeviceToken != null)
                        {
                            FCM.SendResetData(item.DeviceToken);
                        }
                    }
                   
                    // trả về thành công
                    return "Thành công.";
                }
                else // nếu không trả về chuỗi lỗi
                {
                    return "Outlet không tồn tại";
                }

            }
            catch (Exception ex)
            {
                // catch thì báo lỗi
                return "Lỗi" + ex.Message;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public static string sendWinMegaNow(string pOutletID, string pGiftID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // lấy dữ liệu
                var query = from item in context.Outlets
                            where item.ID == int.Parse(pOutletID)
                            select item;

                List<int> listGiftID = new List<int>();
                listGiftID.Add(int.Parse(pGiftID));
                if (query.Count() > 0) // nếu có thì send firebase data xuống device
                {
                    FCM.SendSetWinMegaNow(query.FirstOrDefault().DeviceToken, listGiftID);
                    // trả về thành công
                    return "Thành công.";
                }
                else // nếu không trả về chuỗi lỗi
                {
                    return "Outlet không tồn tại";
                }

            }
            catch (Exception ex)
            {
                // catch trả về lỗi
                return "Lỗi"+ex.Message;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }

    public class SimpleOutletWS
    {
        public int ID { get; set; }
        public string OutletName { get; set; }
        public string OutletAddress { get; set; }
        public string City { get; set; }
        public string District { get; set; }
    }

    public class OutletSE
    {
        [UIHint("_listOutlet")]
        [Display(Name = "Siêu Thị")]
        public long OutletIDSE { get; set; }
    }
    public partial class Outlet
    {
        public string SaleName { get; set; }
    }

    public class EntityOutletForIP
    {
        public Outlet mOutlet { get; set; }
        public List<Outlet_Brand> mListOutletBrand { get; set; }
        public List<Outlet_Current_Set> mListOutletCurrentSet { get; set; }
        public List<Outlet_POSM> mListOutletPOSM { get; set; }

        public List<Team> mListTeam { get; set; }
        public List<Team_Outlet> mListTeamOutlet { get; set; }
        public List<Team_TimePoint> mListTeamPoint { get; set; }
    }
}
