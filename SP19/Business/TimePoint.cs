﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class TimePoint
    {
        public const string TIMEPOINT_DEFAULT = "TP001";
        public const string TIMEPOINT_IN_TIME_TYPE = "IN_TIME";
        public const string TIMEPOINT_OUT_TIME_TYPE = "OUT_TIME";
        public const int ODD_MINUTE_DEFAULT = 15;
        public const string IN_TIME_VALID_TEXT = "Đúng";
        public const string IN_TIME_NOT_VALID_TEXT = "Trễ";
        public const string IN_TIME_SOON_TEXT = "Sớm";
        public const string OUT_TIME_VALID_TEXT = "Đúng giờ";
        public const string OUT_TIME_NOT_VALID_TEXT = "Sớm";
        public const string TIME_POINT_NOT_VALID_ERROR = "TimePoint type is not valid.";

        /// <summary>
        /// Sai số phút
        /// </summary>
        private int _OddMinute;
        public int OddMinute
        {
            get { return _OddMinute; }
            set { _OddMinute = value; }
        }

        public static bool ValidateType(string Type)
        {
            return (Type == TIMEPOINT_IN_TIME_TYPE || Type == TIMEPOINT_OUT_TIME_TYPE);
        }

        public TimeSpan IN_TIME_DEFAULT
        {
            get
            {
                return new TimeSpan(16, 00, 0);
            }
        }

        public TimeSpan OUT_TIME_DEFAULT
        {
            get
            {
                return new TimeSpan(22, 00, 0);
            }
        }


        public TimePoint GetByCode(string Code)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                TimePoint item = context.TimePoints.Single(p => p.TimePointCode == Code);
                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Kiểm tra hợp lệ giờ vào
        /// </summary>
        /// <param name="InTime">Giờ vào quy định</param>
        /// <param name="TrackingTime">Giờ vào</param>
        /// <param name="OddNumber">Số phút sai số</param>
        /// <returns></returns>
        public bool? ValidateInTime(TimeSpan? InTime, DateTime? TrackingTime, int? OddMinute)
        {
            if (TrackingTime == null)
                return null;
            DateTime date = Convert.ToDateTime(TrackingTime);
            if (InTime == null)
                InTime = IN_TIME_DEFAULT;
            if (OddMinute == null)
                OddMinute = ODD_MINUTE_DEFAULT;
            double value = (date.TimeOfDay).Subtract((TimeSpan)InTime).TotalMinutes;
            if (value <= -1)
            {
                return null;
            }
            else if (value >= 0 && value <= 15)
            {
                return true;
            }
            return (false);
        }

        /// <summary>
        /// Kiểm tra hợp lệ giờ ra
        /// </summary>
        /// <param name="OutTime">Giờ ra quy định</param>
        /// <param name="TrackingTime">Giờ ra</param>
        /// <param name="OddNumber">Số phút sai số</param>
        /// <returns></returns>
        public bool? ValidateOutTime(TimeSpan? OutTime, DateTime? TrackingTime, int? OddMinute)
        {
            if (TrackingTime == null)
                return null;
            DateTime date = Convert.ToDateTime(TrackingTime);
            if (OutTime == null)
                OutTime = OUT_TIME_DEFAULT;
            if (OddMinute == null)
                OddMinute = ODD_MINUTE_DEFAULT;
            double value = (date.TimeOfDay).Subtract((TimeSpan)OutTime).TotalMinutes;
            return (value >= OddMinute);
        }
    }
}