﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class XuLy
    {
        public static void updateOutlet(List<User_App> listUpdate)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.User_Apps
                            select item;
                var listOutlet = query.ToList();
                foreach (var item in listUpdate)
                {
                    var entity = listOutlet.Where(o => o.ID == item.Status).FirstOrDefault();
                    entity.FullName = item.FullName;
                    context.SubmitChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
