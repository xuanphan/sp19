﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Outlet_Current_Gift
    {


        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE update(string pAppCode, IList<OutletCurrenrGiftDetail> listEntity, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.Trim().ToLower() == Common.Global.AppCode.Trim().ToLower())
                {
                    // kiểm tra dữ liệu tải lên có không
                    if (listEntity.Count() > 0) // nếu có
                    {
                        // khởi tạo transaction
                        context.Connection.Open();
                        context.Transaction = context.Connection.BeginTransaction();

                        // lấy dữ liệu quà hiện tại trong set 
                        var query = from item in context.Outlet_Current_Gifts
                                    select item;
                        // gán dữ liệu vừa lấy được
                        var listTemp = query.ToList();
                        // tạo list object thêm vào db
                        List<Outlet_Current_Gift> listRP = new List<Outlet_Current_Gift>();

                        // chạy vòng lặp list dữ liệu tải lên để xử lý
                        foreach (var item in listEntity)
                        {
                            // lấy dữ liệu quà hiện tại theo id quà và  id outlet
                            var read = listTemp.Where(p => p.OutletID == item.pOutletID && p.GiftID == item.pGiftID).FirstOrDefault();
                            if (read != null) // nếu có thì cập nhật số lượng = số lượng của item
                            {
                                read.Number = item.pNumber;
                                read.LastUpdatedBy = item.pTeamOutletID;
                                read.LastUpdatedDateTime = DateTime.Now;
                                read.RowVersion++;
                                context.SubmitChanges(); // thực thi câu lệnh
                            }
                            else // nếu chưa có thì thêm mới
                            {
                                // tạo object
                                Outlet_Current_Gift tempEntity = new Outlet_Current_Gift();
                                // gán dữ liệu
                                tempEntity.OutletID = item.pOutletID;
                                tempEntity.GiftID = item.pGiftID;
                                tempEntity.Number = item.pNumber;
                                tempEntity.CreatedBy = item.pTeamOutletID;
                                tempEntity.CreatedDateTime = DateTime.Now;
                                tempEntity.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                                tempEntity.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                                // add vào list object khởi tạo ở trên
                                listRP.Add(tempEntity);
                            }
                        }
                        // kiểm tra list object thêm vào db có không  -- có thì thêm vào db
                        if (listRP.Count() > 0) 
                        {
                            context.Outlet_Current_Gifts.InsertAllOnSubmit(listRP); // thêm list object
                            context.SubmitChanges(); // thực thi câu lệnh
                        }
                        context.Transaction.Commit(); // hoàn thành transaction
                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;


                    }
                    else // nếu không -- trả về lỗi không có dữ liệu
                    {
                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_DATA_OR_WRONG_FORMAT;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_DATA_OR_WRONG_FORMAT);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_DATA_OR_WRONG_FORMAT;
                    }
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIST_APP;
                }

            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();// trả lại ban đầu nếu trong quá trình xử lý có lỗi
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }

    public class OutletCurrenrGiftDetail
    {
        public int pOutletID { get; set; }
        public int pGiftID { get; set; }
        public int pNumber { get; set; }
        public int pTeamOutletID { get; set; }
    }
}
