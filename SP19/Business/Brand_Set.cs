﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Brand_Set
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAllWS(string _pAppCode, int pProjectID, ref Brand_SetResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu set quà
                    var query = from item in context.Brand_Sets
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.Brand.ProjectID == pProjectID
                                select new SimpleBrandSet
                                {
                                    BrandSetID = item.ID,
                                    OutletTypeID = item.OutletTypeID,
                                    BrandID = item.BrandID,
                                    SetName = item.SetName,
                                    NumberTotal = item.Brand_Set_Details.Sum(o => o.Number),
                                    IsDefault = item.IsDefault
                                };

                    // gán giá trị cho phần trả về
                    result.Data = query.ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public IEnumerable<ComboItem> GetAllForCombo(string outletID,int _pProjectID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var queryOutlet = from item in context.Outlets
                                  where item.ID == int.Parse(outletID)
                                  select item;
                var outlet = queryOutlet.FirstOrDefault();
                var query = from item in context.Brand_Sets
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.Brand.ProjectID == _pProjectID
                            && item.OutletTypeID == outlet.OutletTypeID
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.BrandID.ToString(),
                                Text = item.SetName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public List<Brand_Set> GetAll(int _pProjectID, int _pTypeOutletID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Brand_Sets
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.Brand.ProjectID == _pProjectID
                            && item.OutletTypeID == _pTypeOutletID
                            select item;
                var list = query.ToList().Distinct(new ComparerBrandSet()).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public List<Brand_Set> GetAllAppWeb(int _pProjectID, int _pTypeOutletID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Brand_Sets
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.Brand.ProjectID == _pProjectID
                            select item;
                var list = query.ToList().Distinct(new ComparerBrandSet()).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        class ComparerBrandSet : IEqualityComparer<Brand_Set>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(Brand_Set x, Brand_Set y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.SetName == y.SetName && x.BrandID == y.BrandID;
                // return x.AssignID == y.AssignID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(Brand_Set item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashproductid = item.SetName == null ? 0 : item.SetName.GetHashCode();
                int hash2 = item.BrandID == null ? 0 : item.BrandID.GetHashCode();
                //Calculate the hash code for the product.
                return hashproductid ^ hash2;
                // return hashAssignID;
            }
        }

        public static List<Brand_Set> getByProject(int pProjectID, int OutletTypeID)
        {
            List<Brand_Set> listRP = new List<Brand_Set>();
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Brand_Sets
                            where item.Brand.ProjectID == pProjectID
                            && item.OutletTypeID == OutletTypeID
                            select item;
                listRP = query.ToList();

                return listRP;

            }
            catch (Exception ex)
            {
                return listRP;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class SimpleBrandSet
    {
        private int _BrandSetID;
        public int BrandSetID
        {
            get { return _BrandSetID; }
            set { _BrandSetID = value; }
        }
        private int _OutletTypeID;
        public int OutletTypeID
        {
            get { return _OutletTypeID; }
            set { _OutletTypeID = value; }
        }
        private int _BrandID;
        public int BrandID
        {
            get { return _BrandID; }
            set { _BrandID = value; }
        }

        private string _SetName;
        public string SetName
        {
            get { return _SetName; }
            set { _SetName = value; }
        }
        private int _NumberTotal;
        public int NumberTotal
        {
            get { return _NumberTotal; }
            set { _NumberTotal = value; }
        }
        private bool _IsDefault;
        public bool IsDefault
        {
            get { return _IsDefault; }
            set { _IsDefault = value; }
        }

        public SimpleBrandSet() { }

        public SimpleBrandSet(int BrandSetID, int OutletTypeID, int BrandID, string SetName, int NumberTotal, bool IsDefault)
        {
            this.BrandSetID = BrandSetID;
            this.OutletTypeID = OutletTypeID;
            this.BrandID = BrandID;
            this.SetName = SetName;
            this.IsDefault = IsDefault;
            this.NumberTotal = NumberTotal;
        }
    }
}
