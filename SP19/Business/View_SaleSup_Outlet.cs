﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class View_SaleSup_Outlet
    {
        public static List<View_SaleSup_Outlet> getList()
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_SaleSup_Outlets
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return new List<View_SaleSup_Outlet>();
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
    }
}
