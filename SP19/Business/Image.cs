﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public partial class Image
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string _messageSystemError, string _pTeamOutletID, string _pAppCode, ref AddImageResult result)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    //var query = from item in context.Images
                    //            where item.ImageCode == this.ImageCode
                    //            select item;
                    //if (query.Count() > 0)
                    //{
                    //    var simple = query.FirstOrDefault();
                    //    simple.FileName = this.FileName;
                    //    simple.FilePath = this.FilePath;
                    //    simple.LastUpdatedDateTime = DateTime.Now;
                    //    simple.LastUpdatedBy = int.Parse(_pTeamOutletID);
                    //    simple.RowVersion += 1;
                    //    context.SubmitChanges();
                    //    result.Data = simple.ID;
                    //    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    //    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    //    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    //}
                    //else
                    //{
                    this.CreatedBy = int.Parse(_pTeamOutletID);
                    this.CreatedDateTime = DateTime.Now;
                    this.RowVersion = 1;
                    this.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    context.Images.InsertOnSubmit(this);
                    context.SubmitChanges();
                    result.Data = this.ID;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    //}
                }
                else
                {
                    result.Data = 0;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }

            }
            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                result.Data = 0;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string _messageSystemError)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {

                context.Images.InsertOnSubmit(this);
                context.SubmitChanges();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;

            }
            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetList(ref string _messageError, long id, ref List<Image> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.AttendanceTracking_Images
                            join image in context.Images on item.ImageID equals image.ID
                            where item.AttendanceTrackingID == id
                            select image;
                if (query.Count() != 0)
                {
                    list = query.ToList();
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                }
            }
            catch
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetListSup(ref string _messageError, long id, ref List<Image> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.AttendanceTracking_Sup_Images
                            join image in context.Images on item.ImageID equals image.ID
                            where item.AttendanceTrackingSupID == id
                            select image;
                if (query.Count() != 0)
                {
                    list = query.ToList();
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                }
            }
            catch
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetSimple(ref string _messageError, string idimage, ref  List<Image> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.AttendanceTracking_Sup_Images
                            join image in context.Images on item.ImageID equals image.ID
                            where item.AttendanceTrackingSupID == long.Parse(idimage)
                            select item.Image;
                if (query.Count() != 0)
                {

                    list = query.ToList();
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    list = null;
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
            }
            catch
            {
                list = null;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT AddNewDriver(Image fieldImage, string pAppCode, ref AddImageResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {

                    context.Images.InsertOnSubmit(fieldImage); // thêm
                    context.SubmitChanges(); // thực thi câu lệnh

                    // gán giá trị cho phần trả về
                    result.Data = fieldImage.ID;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = -1;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = -1;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static void UpdateImage(Image image, string uri)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            context.CommandTimeout = 3 * 60;
            try
            {
                var query = (from a in context.Images
                             where a.ID == image.ID
                             select a).SingleOrDefault();
                query.FilePath = uri;
                query.LastUpdatedBy = 1;
                query.LastUpdatedDateTime = DateTime.Now;
                query.RowVersion++;
                context.SubmitChanges();
            }
            catch
            {

            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
    }
}
