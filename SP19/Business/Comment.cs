﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Comment
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string MessageSystemError, string _pAppCode, string _pTeamOutletID, ref AddNewResult result)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                if (_AppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                
                    var querycomment = from comment in context.Comments
                                       where comment.SessionCode == this.SessionCode
                                       select comment;
                    if (querycomment.Count() > 0)
                    {
                        var simple = querycomment.FirstOrDefault();
                        simple.Datetime = this.Datetime;
                        simple.Suggestion = this.Suggestion;
                        simple.Type = this.Type;
                        simple.FilePath = this.FilePath;
                        simple.LastUpdatedBy = int.Parse(_pTeamOutletID);
                        simple.LastUpdatedDatetime = DateTime.Now;
                        simple.RowVersion += 1;
                        context.SubmitChanges();

                        result.Data = simple.ID.ToString();
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                    }
                    else
                    {
                        this.CreatedBy = int.Parse(_pTeamOutletID);
                        this.CreatedDatetime = DateTime.Now;
                        this.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                        this.RowVersion = 1;
                        context.Comments.InsertOnSubmit(this);
                        context.SubmitChanges();

                        result.Data = this.ID.ToString();
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    }
                }
                else
                {
                    result.Data = "0";
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                result.Data = "0";
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }     
    }
    public class CommentSE
    {
        [UIHint("_listOutlet")]
        [Display(Name = "Outlet")]
        public long OutletcommentSE { get; set; }

        private DateTime _DateFromcommentSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Date From")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateFromcommentSE
        {
            get { return _DateFromcommentSE; }
            set
            {
                if (value != null)
                    _DateFromcommentSE = value.Date;
            }
        }

        private DateTime _DateTocommentSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Date To")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateTocommentSE
        {
            get { return _DateTocommentSE; }
            set
            {
                if (value != null)
                    _DateTocommentSE = value.Date;
            }
        }
    }

    public class CommentEXT
    {
        public long OutletID { get; set; }
        public string OutletName { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public List<CommentDetailEXT> list { get; set; }
    }

    public class CommentDetailEXT
    {
        public long OutletID { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
    }
}
