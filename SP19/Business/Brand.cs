﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Brand
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAllWS(string _pAppCode, int pProjectID, ref BrandResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu brand theo project ID
                    var query = from item in context.Brands
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.ProjectID == pProjectID
                                select new SimpleBrand
                                {
                                    ID = item.ID,
                                    BrandCode = item.BrandCode,
                                    BrandName = item.BrandName,
                                    IsDialLucky = item.IsDialLucky,
                                    MaximumChangeGift = item.MaximumChangeGift.HasValue ? item.MaximumChangeGift.Value : -1, // không giới hạn (null) thì mặc định là -1
                                    NumberOfEnough = item.NumberOfEnough.HasValue ? item.NumberOfEnough.Value : 0, // không cần (null) thì mặc định là 0
                                    NumberGiftOfDay = item.NumberGiftOfDay.HasValue ? item.NumberGiftOfDay.Value : -1, // không giới hạn (null) thì mặc định là -1
                                    IsTopUpCard = item.IsTopUpCard.HasValue ? item.IsTopUpCard.Value :false,
                                    IsRequest = item.IsRequest.HasValue ? item.IsRequest.Value : true
                                };

                    // gán giá trị cho phần trả về
                    result.Data = query.ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static List<Brand> GetAll(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Brands
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            && item.IsDialLucky == true
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Brand> GetAllDelta(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                if (_pProjectID == 0)
                {
                    _pProjectID = 1;
                }
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Brands
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                             && item.IsDialLucky == true
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Brand> GetAllTakeOff(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                if (_pProjectID == 0)
                {
                    _pProjectID = 1;
                }
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Brands
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.ProjectID == _pProjectID
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static List<Brand> getByProject(int pProjectID)
        {
            List<Brand> listRP = new List<Brand>();
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Brands
                            where item.ProjectID == pProjectID
                            select item;
                listRP = query.ToList();

                return listRP;

            }
            catch (Exception ex)
            {
                return listRP;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class SimpleBrand
    {
        private int? _ID;
        public int? ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _BrandCode;
        public string BrandCode
        {
            get { return _BrandCode; }
            set { _BrandCode = value; }
        }
        private string _BrandName;
        public string BrandName
        {
            get { return _BrandName; }
            set { _BrandName = value; }
        }
        private bool _IsDialLucky;
        public bool IsDialLucky
        {
            get { return _IsDialLucky; }
            set { _IsDialLucky = value; }
        }

        private int? _MaximumChangeGift;
        public int? MaximumChangeGift
        {
            get { return _MaximumChangeGift; }
            set { _MaximumChangeGift = value; }
        }

        private int _NumberOfEnough;
        public int NumberOfEnough
        {
            get { return _NumberOfEnough; }
            set { _NumberOfEnough = value; }
        }
        private int _NumberGiftOfDay;
        public int NumberGiftOfDay
        {
            get { return _NumberGiftOfDay; }
            set { _NumberGiftOfDay = value; }
        }
        private bool _IsTopUpCard;
        public bool IsTopUpCard
        {
            get { return _IsTopUpCard; }
            set { _IsTopUpCard = value; }
        }

        private bool _IsRequest;
        public bool IsRequest
        {
            get { return _IsRequest; }
            set { _IsRequest = value; }
        }

        public SimpleBrand() { }

        public SimpleBrand(int ID, string BrandCode, string BrandName, bool IsDialLucky, int MaximumChangeGift, int NumberOfEnough, int NumberGiftOfDay, bool IsTopUpCard, bool IsRequest)
        {
            this.BrandCode = BrandCode;
            this.ID = ID;
            this.BrandName = BrandName;
            this.IsDialLucky = IsDialLucky;
            this.MaximumChangeGift = MaximumChangeGift;
            this.NumberOfEnough = NumberOfEnough;
            this.NumberGiftOfDay = NumberGiftOfDay;
            this.IsTopUpCard = IsTopUpCard;
            this.IsRequest = IsRequest;
        }
    }
}
