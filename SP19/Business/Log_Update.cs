﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Log_Update
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(string _pAppCode, ref MainResult result)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    //xem đã tồn tại dữ liệu không
                    var query = from item in context.Log_Updates
                                where item.OutletID == this.OutletID
                                      && item.Version == this.Version
                                select item;

                    if (query.Count() > 0) // nếu có thì update lại
                    {
                        var simple = query.FirstOrDefault();
                        simple.LastUpdatedBy = this.CreatedBy;
                        simple.LastUpdatedDateTime = DateTime.Now;
                        simple.RowVersion += 1;
                        context.SubmitChanges(); // thực thi câu lệnh

                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                    }
                    else // chưa có thì thêm mới
                    {
                        this.CreatedDateTime = DateTime.Now;
                        this.RowVersion = 1;
                        context.Log_Updates.InsertOnSubmit(this);// thêm
                        context.SubmitChanges();// thực thi câu lệnh

                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    }
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }
}
