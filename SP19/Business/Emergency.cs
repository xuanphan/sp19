﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Emergency
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll( string _pAppCode,int pProjectID, ref EmergencyResult result)
      {
          DatabaseDataContext context = null;
          try
          {
              context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
              // kiểm tra xem appcode truyền lên có đúng không
              if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
              {
                  // lấy dữ liệu
                  var query = from item in context.Emergencies
                              where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                              && item.ProjectID == pProjectID
                              select new SimpleEmergency
                              {
                                  ID = item.ID,
                                  EmergencyCode = item.EmergencyCode,
                                  EmergencyName = item.EmergencyName,
                                  EmergencyDescription = item.EmergencyDescription
                              };

                  // gán giá trị cho phần trả về
                  result.Data = query.ToList();
                  result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                  result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                  return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
              }
              else
              {
                  // gán giá trị cho phần trả về
                  result.Data = null;
                  result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                  result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                  return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
              }
          }
          catch (Exception ex)
          {
              // gán giá trị cho phần trả về
              result.Data = null;
              result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
              result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
              return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
          }
          finally
          {
              context.Connection.Close();
              context = null;
              GC.Collect();
          }
      }
    }
  public class SimpleEmergency
  {
      private int? _ID;
      public int? ID
      {
          get { return _ID; }
          set { _ID = value; }
      }

      private string _EmergencyCode;
      public string EmergencyCode
      {
          get { return _EmergencyCode; }
          set { _EmergencyCode = value; }
      }
      private string _EmergencyName;
      public string EmergencyName
      {
          get { return _EmergencyName; }
          set { _EmergencyName = value; }
      }
      private string _EmergencyDescription;
      public string EmergencyDescription
      {
          get { return _EmergencyDescription; }
          set { _EmergencyDescription = value; }
      }

      public SimpleEmergency() { }

      public SimpleEmergency(int ID, string EmergencyCode, string EmergencyName, string EmergencyDescription)
      {
          this.EmergencyCode = EmergencyCode;
          this.ID = ID;
          this.EmergencyName = EmergencyName;
          this.EmergencyDescription = EmergencyDescription;
      }
  }
}
