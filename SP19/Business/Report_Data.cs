﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace Business
{
    public partial class Report_Data
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchSup(ref string _messageError, ref List<ReportDataEXT> list, int _pProjectID, int _SupID, ReportDataSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.AttendanceTracking_Sups
                            where item.DeviceDateTime.Date == se.Date.Date
                            && item.Outlet.ProjectID == _pProjectID
                            && item.SupID == _SupID
                            select new ReportDataEXT
                            {
                                OutletID = item.OutletID,
                                OutletName = item.Outlet.OutletName,
                                datetime = item.DeviceDateTime.Date
                            };
                list = query.ToList().Distinct(new ComparerData()).ToList();
                foreach (var reportDataExt in list)
                {
                    var temp1 = context.AttendanceTracking_Sups.ToList().Where(p => p.OutletID == reportDataExt.OutletID
                                                                              &&
                                                                              p.DeviceDateTime.Date ==
                                                                              reportDataExt.datetime.Date
                                                                              && p.TimePointType == "IN_TIME" && p.Outlet.ProjectID == _pProjectID
                          && p.SupID == _SupID);
                    if (temp1.Count() != 0)
                    {
                        reportDataExt.timein = temp1.FirstOrDefault().DeviceDateTime.ToString("dd-MM-yyyy HH:mm");
                        reportDataExt.latin = temp1.FirstOrDefault().LatGPS;
                        reportDataExt.longin = temp1.FirstOrDefault().LongGPS;
                    }
                    else
                    {
                        reportDataExt.timein = "";
                        reportDataExt.latin = 0;
                        reportDataExt.longin = 0;
                    }
                    var temp2 =
                        context.AttendanceTracking_Sups.ToList().Where(p => p.OutletID == reportDataExt.OutletID
                                                                              &&
                                                                              p.DeviceDateTime.Date ==
                                                                              reportDataExt.datetime.Date
                                                                              && p.TimePointType == "OUT_TIME" && p.Outlet.ProjectID == _pProjectID
                          && p.SupID == _SupID);
                    if (temp2.Count() != 0)
                    {
                        reportDataExt.timeout = temp2.FirstOrDefault().DeviceDateTime.ToString("dd-MM-yyyy HH:mm");
                        reportDataExt.latout = temp2.FirstOrDefault().LatGPS;
                        reportDataExt.longout = temp2.FirstOrDefault().LongGPS;
                    }
                    else
                    {
                        reportDataExt.timeout = "";
                        reportDataExt.latout = 0;
                        reportDataExt.longout = 0;
                    }



                    reportDataExt.list = new List<Outlet_Image_Of_Sup>();
                    var image = context.Outlet_Image_Of_Sups.Where(p => p.OutletID == reportDataExt.OutletID
                                                                                      &&
                                                                                      p.DeviceDateTime.Date ==
                                                                                      reportDataExt.datetime.Date && p.Outlet.ProjectID == _pProjectID);
                    if (image.Count() != 0)
                    {
                        reportDataExt.list = image.ToList();
                    }
                    else
                    {
                        reportDataExt.list = new List<Outlet_Image_Of_Sup>();

                    }
                }


                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageError, ref List<ReportDataEXT> list, int _pProjectID, int _pOutletType, ReportDataSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.AttendanceTracking_Sups
                            where item.DeviceDateTime.Date == se.Date.Date
                            && (item.Outlet.Region.Type == se.RegionReportDataSE || se.RegionReportDataSE == "Tất Cả" || se.RegionReportDataSE == null)
                            && (item.Outlet.ID == se.OutletReportDataSE || se.OutletReportDataSE == 0 || se.OutletReportDataSE == null)
                            && (item.Outlet.ProjectID == _pProjectID)
                            && (item.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select new ReportDataEXT
                            {
                                OutletID = item.OutletID,
                                OutletName = item.Outlet.OutletName,
                                datetime = item.DeviceDateTime.Date
                            };
                list = query.ToList().Distinct(new ComparerData()).ToList();
                foreach (var reportDataExt in list)
                {
                    var temp1 = context.AttendanceTracking_Sups.ToList().Where(p => p.OutletID == reportDataExt.OutletID
                                                                              &&
                                                                              p.DeviceDateTime.Date ==
                                                                              reportDataExt.datetime.Date
                                                                              && p.TimePointType == "IN_TIME" && (p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)));
                    if (temp1.Count() != 0)
                    {
                        reportDataExt.timein = temp1.FirstOrDefault().DeviceDateTime.ToString("dd-MM-yyyy HH:mm");
                        reportDataExt.latin = temp1.FirstOrDefault().LatGPS;
                        reportDataExt.longin = temp1.FirstOrDefault().LongGPS;
                        reportDataExt.idin = temp1.FirstOrDefault().ID.ToString();
                    }
                    else
                    {
                        reportDataExt.timein = "";
                        reportDataExt.latin = 0;
                        reportDataExt.longin = 0;
                        reportDataExt.idin = "0";
                    }
                    var temp2 =
                        context.AttendanceTracking_Sups.ToList().Where(p => p.OutletID == reportDataExt.OutletID
                                                                              &&
                                                                              p.DeviceDateTime.Date ==
                                                                              reportDataExt.datetime.Date
                                                                              && p.TimePointType == "OUT_TIME" && (p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)));
                    if (temp2.Count() != 0)
                    {
                        reportDataExt.timeout = temp2.FirstOrDefault().DeviceDateTime.ToString("dd-MM-yyyy HH:mm");
                        reportDataExt.latout = temp2.FirstOrDefault().LatGPS;
                        reportDataExt.longout = temp2.FirstOrDefault().LongGPS;
                        reportDataExt.idout = temp2.FirstOrDefault().ID.ToString();
                    }
                    else
                    {
                        reportDataExt.timeout = "";
                        reportDataExt.latout = 0;
                        reportDataExt.longout = 0;
                        reportDataExt.idout = "0";
                    }



                    reportDataExt.list = new List<Outlet_Image_Of_Sup>();
                    var image = context.Outlet_Image_Of_Sups.Where(p => p.OutletID == reportDataExt.OutletID
                                                                                      &&
                                                                                      p.DeviceDateTime.Date ==
                                                                                      reportDataExt.datetime.Date && (p.Outlet.ProjectID == _pProjectID && (p.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)));
                    if (image.Count() != 0)
                    {
                        reportDataExt.list = image.ToList();
                    }
                    else
                    {
                        reportDataExt.list = new List<Outlet_Image_Of_Sup>();

                    }
                }


                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        private string GetType(int p)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.CodeDetails
                            where item.Value == p.ToString()
                            && item.GroupCode == "ImageType"
                            select item;


                return query.FirstOrDefault().Name;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }


        class ComparerData : IEqualityComparer<ReportDataEXT>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(ReportDataEXT x, ReportDataEXT y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.OutletID == y.OutletID;
                // return x.AssignID == y.AssignID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(ReportDataEXT item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashproductid = item.OutletID == null ? 0 : item.OutletID.GetHashCode();
                //Calculate the hash code for the product.
                return hashproductid;
                // return hashAssignID;
            }
        }
    }
    public class ReportDataSE
    {

        private DateTime _Date = DateTime.Now;
        [Required(ErrorMessage = "{0} Not Empty.")]
        [Display(Name = "Ngày")]
        [DataType(DataType.Date)]
        public DateTime Date
        {
            get { return _Date; }
            set
            {
                if (value != null)
                    _Date = value.Date;
            }
        }
        [UIHint("_ListOutlet")]
        [Display(Name = "Siêu Thị")]
        public long OutletReportDataSE { get; set; }

        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionReportDataSE { get; set; }


    }
    public class ReportDataEXT
    {
        public string idin { get; set; }
        public string idout { get; set; }
        public long OutletID { get; set; }
        public DateTime datetime { get; set; }
        public string OutletName { get; set; }
        public string timein { get; set; }
        public string timeout { get; set; }
        public double latin { get; set; }
        public double longin { get; set; }
        public double latout { get; set; }
        public double longout { get; set; }
        public List<Outlet_Image_Of_Sup> list { get; set; }
    }



    public class ImageEXT
    {
        public long id { get; set; }
        public string Link { get; set; }
        public double lat { get; set; }
        public double loggps { get; set; }
        public int type { get; set; }
    }
}
