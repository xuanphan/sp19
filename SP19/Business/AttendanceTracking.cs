﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlTypes;
using System.Transactions;

using Common;
using System.ComponentModel.DataAnnotations;

namespace Business
{
    /// Description
    /// ========================
    /// [Ordinal] Property: số thứ tự nhóm thông tin chấm công FieldStaff theo ngày



    public partial class AttendanceTracking
    {
        public class TimePointTypeCode
        {
            public static string In_Time = "IN_TIME";
            public static string Out_Time = "OUT_TIME";
        }
        private bool IsExist(ref long ReturnID, ref int ReturnOrdinal, DatabaseDataContext context)
        {
            if (context == null)
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            var query = from item in context.AttendanceTrackings
                        where item.DeviceDateTime.Date == this.DeviceDateTime.Date
                        && item.TimePointType == this.TimePointType
                        select new { ID = item.ID };
            if (query.Count() > 0)
            {
                ReturnID = query.FirstOrDefault().ID;
                return true;
            }
            return false;
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(string _pAppCode, ref AddNewResult result)
        {
            DatabaseDataContext context = null;
           
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    //Lấy ca theo teamoutlet
                    var query = from item in context.Team_Outlets
                                join teamtimepoint in context.Team_TimePoints on item.ID equals teamtimepoint.TeamOutletID
                                join timepoint in context.TimePoints on teamtimepoint.TimePointID equals timepoint.ID
                                where item.ID == this.TeamOutletID
                                select timepoint.TimePointCode;

                    //lấy chấm công
                    var queryattendance = from attendance in context.AttendanceTrackings
                                          where attendance.TeamOutletID == this.TeamOutletID
                                            && attendance.DeviceDateTime.Date == this.DeviceDateTime.Date
                                            && attendance.TimePointType == this.TimePointType
                                          select attendance;
                    //Nếu chưa tồn tại chấm công thì add new
                    if (queryattendance.Count() == 0)
                    {
                        this.TimePointCode = query.FirstOrDefault();
                        this.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                        this.CreatedDateTime = DateTime.Now;
                        this.ServerDateTime = DateTime.Now;
                        this.CreatedBy = this.TeamOutletID;
                        this.RowVersion = 1;
                        context.AttendanceTrackings.InsertOnSubmit(this); // thêm vào db
                        context.SubmitChanges(); //  thực thi câu lệnh

                        // gán giá trị cho phần trả về
                        result.Data = this.ID.ToString(); // add thành công thì trả về ID
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    }
                    else //Nếu đã tồn tại thì cập nhật
                    {
                        var simple = queryattendance.FirstOrDefault();
                        simple.ServerDateTime = DateTime.Now;
                        simple.DeviceDateTime = this.DeviceDateTime;
                        simple.LastUpdatedDateTime = DateTime.Now;
                        simple.LastUpdatedBy = this.TeamOutletID;
                        simple.NumberPG = this.NumberPG;
                        simple.RowVersion += 1;
                        context.SubmitChanges(); //  thực thi câu lệnh

                        // gán giá trị cho phần trả về
                        result.Data = simple.ID.ToString();
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    }

                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = "-1";
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = "-1";
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description =Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR)+ ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string MessageSystemError, int _pProjectID, int _pOutletTypeID, ref List<View_AttendanceTracking> list, AttendanceTrackingSE se)
        {
            DatabaseDataContext context = null;
            try
            {

                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

                var query = from item in context.View_AttendanceTrackings
                            where (item.TeamID == se.TeamAttendance || se.TeamAttendance == 0 || se.TeamAttendance == null)
                            && (item.DeviceDateTime.Date >= se.DateFromAttendance.Date && item.DeviceDateTime.Date <= se.DateToAttendance.Date)
                            && (item.Type == se.RegionAttendance || se.RegionAttendance == "Tất Cả" || se.RegionAttendance == null)
                            && (item.RegionID == se.DistrictAttendance || se.DistrictAttendance == 0 || se.DistrictAttendance == null)
                            && (item.ProjectID == _pProjectID)
                            && (item.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)
                            select item;
                var listTemp = query.ToList();
                foreach (var item in listTemp)
                {
                    item.TeamID = item.TeamID;
                    item.TeamName = item.TeamName;
                    item.OutletType = item.ChanelName;
                    item.OutletName = item.OutletName;
                    item.NumberPGTeam = item.NumberPGDefault.ToString();
                    var tempintime = listTemp.Where(p => p.TeamID == item.TeamID && (p.DeviceDateTime.Date == item.DeviceDateTime.Date) && p.TimePointType == TimePointTypeCode.In_Time);
                    if (tempintime.ToList().Count != 0)
                    {
                        var simpletempintime = tempintime.FirstOrDefault();
                        item.LatGPS = simpletempintime.LatGPS;
                        item.LongGPS = simpletempintime.LongGPS;
                        item.NumberPGIn = simpletempintime.NumberPG.ToString();
                        item.LOGIN_TIME = simpletempintime.DeviceDateTime.ToString("dd/MM/yyyy HH:mm");
                        item.listin = new List<View_AttendanceTracking_Detail>();
                        item.listin = context.View_AttendanceTracking_Details.Where(p => p.AttendanceTrackingID == simpletempintime.AttendanceID).ToList();
                    }
                    else
                    {
                        item.NumberPGIn = "0";
                        item.listin = new List<View_AttendanceTracking_Detail>();
                    }

                    var tempouttime = listTemp.Where(p => p.TeamID == item.TeamID && (p.DeviceDateTime.Date == item.DeviceDateTime.Date) && p.TimePointType == TimePointTypeCode.Out_Time);
                    if (tempouttime.ToList().Count != 0)
                    {
                        var simpletempouttime = tempouttime.FirstOrDefault();
                        item.LatGPS = simpletempouttime.LatGPS;
                        item.LongGPS = simpletempouttime.LongGPS;
                        item.NumberPGOut = simpletempouttime.NumberPG.ToString();
                        item.LOGOUT_TIME = simpletempouttime.DeviceDateTime.ToString("dd/MM/yyyy HH:mm");
                        item.listout = new List<View_AttendanceTracking_Detail>();
                        item.listout = context.View_AttendanceTracking_Details.Where(p => p.AttendanceTrackingID == simpletempouttime.AttendanceID).ToList();
                    }
                    else
                    {
                        item.NumberPGOut = "0";
                        item.listout = new List<View_AttendanceTracking_Detail>();
                    }
                }
                list = listTemp.ToList().Distinct(new ComparerAttendanceView()).OrderByDescending(p => p.DeviceDateTime).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        class ComparerAttendanceView : IEqualityComparer<View_AttendanceTracking>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(View_AttendanceTracking x, View_AttendanceTracking y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.TeamID == y.TeamID && x.DeviceDateTime.Date == y.DeviceDateTime.Date;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(View_AttendanceTracking item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashtteamid = item.TeamID == null ? 0 : item.TeamID.GetHashCode();
                int hashdatetime = item.DeviceDateTime.Date == null ? 0 : item.DeviceDateTime.Date.GetHashCode();
                //Calculate the hash code for the product.
                return hashtteamid ^ hashdatetime;
            }
        }

    }

    public class AttendanceTrackingSE
    {
        [UIHint("_Team")]
        [Display(Name = "Team")]
        public long TeamAttendance { get; set; }

        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionAttendance { get; set; }

        [UIHint("_District")]
        [Display(Name = "Tỉnh")]
        public int DistrictAttendance { get; set; }


        private DateTime _DateFromAttendance = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Ngày Bắt Đầu")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateFromAttendance
        {
            get { return _DateFromAttendance; }
            set
            {
                if (value != null)
                    _DateFromAttendance = value.Date;
            }
        }

        private DateTime _DateToAttendance = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Ngày Kết Thúc")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateToAttendance
        {
            get { return _DateToAttendance; }
            set
            {
                if (value != null)
                    _DateToAttendance = value.Date;
            }
        }


    }
    public partial class View_AttendanceTracking
    {
        public string OutletType { get; set; }
        public string LOGIN_TIME { get; set; }
        public string LOGOUT_TIME { get; set; }
        public string NumberPGTeam { get; set; }
        public string NumberPGIn { get; set; }
        public string NumberPGOut { get; set; }
        public List<View_AttendanceTracking_Detail> listin { get; set; }
        public List<View_AttendanceTracking_Detail> listout { get; set; }
    }
}
