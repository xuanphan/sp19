﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Outlet_Image_Of_Sup
    {

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW add(string pAppCode, List<Outlet_Image_Of_Sup> list, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // khởi tạo transaction
                    context.Connection.Open();
                    context.Transaction = context.Connection.BeginTransaction();

                    context.Outlet_Image_Of_Sups.InsertAllOnSubmit(list); // thêm
                    context.SubmitChanges(); // thực thi câu lệnh

                    context.Transaction.Commit(); // hoàn thành transaction
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();// trả lại ban đầu nếu trong quá trình xử lý có lỗi
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW addNew(ref string _messagererror, Outlet_Image_Of_Sup simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                try
                {
                    context.Connection.Open();
                    context.Transaction = context.Connection.BeginTransaction();

                    context.Outlet_Image_Of_Sups.InsertOnSubmit(simple);
                    context.SubmitChanges();

                    context.Transaction.Commit();
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                catch (Exception ex)
                {
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                }
                finally
                {
                    context.Connection.Close();
                    GC.Collect();
                }
            }
            catch (Exception ex)
            {

                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageSystemError, int _pOutletID, int _pType, ref List<Outlet_Image_Of_Sup> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlet_Image_Of_Sups
                            where (item.OutletID == _pOutletID)
                            && (item.ImageType == _pType || _pType == 0)
                            select item;
                list = query.ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                list = null;
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

    }

    public class OutletImageForSupDetail
    {
        public int pOutletID { get; set; }
        public int pImageID { get; set; }
        public int pImageType { get; set; }
        public string pDeviceDateTime { get; set; }
        public int pSupID { get; set; }
    }
}
