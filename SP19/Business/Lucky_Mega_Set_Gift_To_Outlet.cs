﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Lucky_Mega_Set_Gift_To_Outlet
    {
        public static List<LuckyAssignEXT> GetAllMega(DateTime _date, int _ProjectID, int _OutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (_date == null)
                {
                    _date = DateTime.Now;
                }
                var datetrue = Convert.ToDateTime(_date);
                var query = from item in context.Lucky_Mega_Set_Gift_To_Outlets
                            join outlet in context.Outlets on item.OutletID equals outlet.ID
                            join gift in context.Lucky_Mega_Gifts on item.GiftID equals gift.ID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.DateTrue.Date <= datetrue.Date
                            && (outlet.OutletTypeID == _OutletType || _OutletType == 0)
                            && (outlet.ProjectID == _ProjectID)
                            select new LuckyAssignEXT
                            {
                                OutletID = outlet.ID,
                                status = item.IsNotificationToDevice,
                                ID = item.ID,
                                Date = item.DateTrue,
                                OutletName = outlet.OutletName,
                                GiftName = gift.Name,
                                GiftID = gift.ID

                            };
                var list = query.ToList();

                foreach (var i in list)
                {
                    var gift = context.View_Customer_Gift_Megas.Where(p => p.OutletID == i.OutletID && p.GiftID == i.GiftID);
                    if (gift.Count() > 0)
                    {
                        i.listgift = gift.ToList();
                        i.listimage = context.View_Customer_Images.Where(p => p.CustomerID == gift.FirstOrDefault().CustomerID).ToList();
                    }
                    else
                    {
                        i.listgift = new List<View_Customer_Gift_Mega>();
                        i.listimage = new List<View_Customer_Image>();
                    }


                }
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<LuckyAssignEXT> Search(LuckyAssignSE se, int _ProjectID, int _OutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Lucky_Mega_Set_Gift_To_Outlets
                            join outlet in context.Outlets on item.OutletID equals outlet.ID
                            join gift in context.Lucky_Mega_Gifts on item.GiftID equals gift.ID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.DateTrue.Date <= se.DateAssignSE.GetValueOrDefault().Date
                            && (item.OutletID == se.OutletAssignSE || se.OutletAssignSE == 0 || se.OutletAssignSE == null)
                            && (item.GiftID == se.GiftSE || se.GiftSE == 0 || se.GiftSE == null)
                            && (outlet.OutletTypeID == _OutletType || _OutletType == 0)
                            && (outlet.ProjectID == _ProjectID)
                            select new LuckyAssignEXT
                            {
                                OutletID = outlet.ID,
                                status = item.IsNotificationToDevice,
                                ID = item.ID,
                                Date = item.DateTrue,
                                OutletName = outlet.OutletName,
                                GiftName = gift.Name,
                                GiftID = gift.ID

                            };
                var list = query.ToList();

                foreach (var i in list)
                {
                    var gift = context.View_Customer_Gift_Megas.Where(p => p.OutletID == i.OutletID && p.GiftID == i.GiftID);
                    if (gift.Count() > 0)
                    {
                        i.listgift = gift.ToList();
                        i.listimage = context.View_Customer_Images.Where(p => p.CustomerID == gift.FirstOrDefault().CustomerID).ToList();
                    }
                    else
                    {
                        i.listgift = new List<View_Customer_Gift_Mega>();
                        i.listimage = new List<View_Customer_Image>();
                    }


                }
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string _messageSystemError, string _date, string _outletassign, string _gift, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                DateTime datetrue = Common.ConvertUtils.ConvertStringToShortDate(_date);
                var query = from item in context.Lucky_Mega_Set_Gift_To_Outlets
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.OutletID == int.Parse(_outletassign)
                            && item.GiftID == int.Parse(_gift)
                            && item.DateTrue.Date == datetrue.Date
                            select item;
                if (query.Count() == 0)
                {
                    Lucky_Mega_Set_Gift_To_Outlet simple = new Lucky_Mega_Set_Gift_To_Outlet();
                    simple.DateTrue = datetrue;
                    simple.OutletID = int.Parse(_outletassign);
                    simple.GiftID = int.Parse(_gift);
                    simple.IsNotificationToDevice = false;
                    simple.IsHaveWin = false;
                    simple.CreatedBy = _pUserID;
                    simple.CreatedDateTime = DateTime.Now;
                    simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    simple.RowVersion = 1;
                    context.Lucky_Mega_Set_Gift_To_Outlets.InsertOnSubmit(simple);
                    context.SubmitChanges();
                    // Gửi Thông báo xuống device
                    var temp = context.Outlets.Where(p => p.ID == int.Parse(_outletassign)).FirstOrDefault();
                    SimpleGiftMegaResult _sim = new SimpleGiftMegaResult();
                    _sim.GiftID = int.Parse(_gift);
                    _sim.OutletID = int.Parse(_outletassign);
                    _sim.DateTrue = datetrue.ToString("dd/MM/yyyy");

                    FCM.SendSMSSetGiftMega(temp.DeviceToken, _sim);
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE Update(ref string _messageError, string _pID, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string[] arayID = _pID.Split('|');

                for (int i = 0; i < arayID.Count(); i++)
                {
                    if (arayID[i].ToString() != String.Empty)
                    {
                        //xem brand set này thuộc hiện tại k?
                        var query = from item in context.Lucky_Mega_Set_Gift_To_Outlets
                                    where item.ID == int.Parse(arayID[i])
                                    select item;
                        // không có ở hiện tại thì tạo và update trạng thái
                        if (query.Count() > 0)
                        {
                            //xác nhận
                            var simple = query.FirstOrDefault();
                            simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE;
                            simple.LastUpdatedBy = _pUserID;
                            simple.LastUpdatedDateTime = DateTime.Now;
                            simple.RowVersion += 1;
                            // update set hiện tại
                            context.SubmitChanges();
                        }
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static string SendOTP(ref string _messageSystemError, ref int status, string phone, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
                string codeOPT = FCM.GenerateRandomOTP(4, saAllowedCharacters);
                //xét sđt dc gửi
                var queryphone = from item in context.Lucky_Mega_Phone_Assigns
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.Phone == phone.Trim()
                            select item;
                if (queryphone.Count() > 0)
                {
                    var query = from item in context.SendOTPs
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.NumberPhone == phone.Trim()
                                select item;
                    if (query.Count() > 0)
                    {
                        var list = query.ToList();
                        foreach (var i in list)
                        {
                            i.Status = (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE;
                            context.SubmitChanges();
                        }
                    }

                    //xác nhận
                    SendOTP simple = new SendOTP();
                    simple.NumberPhone = phone;
                    simple.CodeOTP = codeOPT;
                    simple.Status = 1;
                    // update set hiện tại
                    context.SendOTPs.InsertOnSubmit(simple);
                    context.SubmitChanges();

                    string message = codeOPT + " la ma xac nhan cua ban. Vui long quay lai de tiep tuc. Cam on";

                    FCM.GetPOSTResponse(message, phone);

                    _messageSystemError = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    return _messageSystemError;
                }
                else
                {
                    _messageSystemError = "Số điện thoại không có trong hệ thống phân quyền này. Vui lòng liên hệ kỹ thuật.";
                    status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA;
                    return _messageSystemError;
                }
            }
            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return _messageSystemError;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetOPT(ref string MessageError, string _phone, string _password, ref int status)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

                //xem brand set này thuộc hiện tại k?
                var query = from item in context.SendOTPs
                            where item.NumberPhone == _phone.Trim()
                            && item.CodeOTP == _password.Trim()
                            && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select item;
                // không có ở hiện tại thì tạo và update trạng thái
                if (query.Count() > 0)
                {
                    status = 1;
                }
                else
                {
                    status = 2;
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE SendMessage(ref string _messageSystemError, string ID, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Lucky_Mega_Set_Gift_To_Outlets
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.ID == int.Parse(ID)
                            select item;
                if (query.Count() > 0)
                {
                    var simple = query.FirstOrDefault();

                    // Gửi Thông báo xuống device
                    var temp = context.Outlets.Where(p => p.ID == simple.OutletID).FirstOrDefault();
                    SimpleGiftMegaResult _sim = new SimpleGiftMegaResult();
                    _sim.GiftID = simple.GiftID;
                    _sim.OutletID = simple.OutletID;
                    _sim.DateTrue = simple.DateTrue.ToString("dd/MM/yyyy");

                    FCM.SendSMSSetGiftMega(temp.DeviceToken, _sim);
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static List<View_Customer_Gift_Mega> GetCustomerSimple(string _OutletID, string _GiftID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_Customer_Gift_Megas
                            where item.OutletID == int.Parse(_OutletID)
                            && item.GiftID == int.Parse(_GiftID)
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class LuckyAssignEXT
    {
        public bool status { get; set; }
        public int OutletID { get; set; }
        public int GiftID { get; set; }
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public string OutletName { get; set; }
        public string GiftName { get; set; }
        public List<View_Customer_Product> listproduct { get; set; }
        public List<View_Customer_Image> listimage { get; set; }
        public List<View_Customer_Gift_Mega> listgift { get; set; }
    }
    public class LuckyAssignSE
    {
        [UIHint("_listOutlet")]
        [Display(Name = "Siêu Thị")]
        public int OutletAssignSE { get; set; }

        [UIHint("_listGiftMega")]
        [Display(Name = "Quà")]
        public int GiftSE { get; set; }

        private DateTime _DateAssignSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Ngày Trúng")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DateAssignSE
        {
            get { return _DateAssignSE; }
            set
            {
                if (value != null)
                    _DateAssignSE = value.GetValueOrDefault().Date;
            }
        }

        [UIHint("_listInturn")]
        [Display(Name = "Đợt")]
        public int TurnSE { get; set; }

    }
}
