﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class AttendanceTracking_Sup_Image
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW addNewSimple(string pAppCode, long pAttendanceTrackingSupID, long pImageID, ref AddAttendanceTrackingSupImageResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.Trim().ToLower() != Common.Global.AppCode.Trim().ToLower())
                {
                    // gán giá trị cho phần trả về
                    result.Data = 0;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
                // kiểm tra chấm công sup + hình ảnh này có chưa
                var query = from item in context.AttendanceTracking_Sup_Images
                            where item.AttendanceTrackingSupID == pAttendanceTrackingSupID
                            && item.ImageID == pImageID
                            select item;

                // nếu có rồi thì trả về kết quả có sẵn
                if (query.Count() > 0) 
                {
                    // gán giá trị cho phần trả về
                    result.Data = query.FirstOrDefault().ID;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else  // chưa thì tạo object và thêm vào db
                {
                    AttendanceTracking_Sup_Image temp = new AttendanceTracking_Sup_Image();
                    temp.AttendanceTrackingSupID = pAttendanceTrackingSupID;
                    temp.ImageID = pImageID;
                    context.AttendanceTracking_Sup_Images.InsertOnSubmit(temp); // thêm
                    context.SubmitChanges(); // thực thi câu lệnh
                    // gán giá trị cho phần trả về
                    result.Data = temp.ID;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = 0;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW addNew(ref string _messagererror, AttendanceTracking_Sup_Image temp)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.AttendanceTracking_Sup_Images
                            where item.AttendanceTrackingSupID == temp.AttendanceTrackingSupID
                            && item.ImageID == temp.ImageID
                            select item;
                if (query.Count() > 0)
                {
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    context.AttendanceTracking_Sup_Images.InsertOnSubmit(temp);
                    context.SubmitChanges();
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
}
