﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class CodeDetail
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(ref string _messageSystemError, string _pAppCode, string _pGroupCode, ref List<Business.SimpleCodeDetail> ListSimpleCodeDetail, ref CodeDetailResult codedetailresult)
        {
            DatabaseDataContext context = null;
            try
            {
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                    var query = from item in context.CodeDetails
                                where item.GroupCode.ToLower().Trim() == _GroupCode.ToLower().Trim()
                                select new SimpleCodeDetail
                                (
                                    item.GroupCode,
                                    item.Name,
                                    item.Value,
                                    item.Ordinal

                                );
                    if (query.Count() > 0)
                    {
                        ListSimpleCodeDetail = query.ToList();
                        codedetailresult.Data = ListSimpleCodeDetail;
                        codedetailresult.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                        codedetailresult.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    }
                    else
                    {
                        ListSimpleCodeDetail = null;
                        codedetailresult.Data = null;
                        codedetailresult.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                        codedetailresult.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                    }
                }
                else
                {
                    codedetailresult.Data = null;
                    codedetailresult.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    codedetailresult.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                codedetailresult.Data = null;
                codedetailresult.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                codedetailresult.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
            }
        }

        public IEnumerable<ComboItem> GetForCombo(string _pGroupCode, bool defaultOption = false, bool allOption = false)
        {
            try
            {
                DatabaseDataContext context = null;
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var Region = context.CodeDetails
                    .Where(p => p.GroupCode.ToLower().Trim() == _pGroupCode.ToLower().Trim())
            .Select(p => new ComboItem { ID = Convert.ToInt32(p.Value), Text = p.Name.Trim() })
            .OrderBy(p => p.Text);

                List<ComboItem> items = Region.ToList();
                if (!allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 1, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IEnumerable<ComboItem> GetForComboDateInWeek(DateTime datef, bool defaultOption = false, bool allOption = false)
        {
            try
            {
                DatabaseDataContext context = null;
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = datef.Date.AddDays(-6); date <= datef.Date; date = date.AddDays(1))
                    allDates.Add(date);
                var Region = allDates.Select(p => new ComboItem { Value = p.Date.ToString("dd/MM/yyyy"), Text = p.DayOfWeek.ToString() });
                List<ComboItem> items = Region.ToList();
                if (!allOption)
                    items.Insert(0, new ComboItem { Value = "0", Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { Value = "1", Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public object GetDateInWeekForDate(DateTime datef, bool defaultOption = false, bool allOption = false)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                List<DateTime> allDates = new List<DateTime>();
                for (DateTime date = datef.Date.AddDays(-6); date <= datef.Date; date = date.AddDays(1))
                    allDates.Add(date);
                var Region = allDates.Select(p => new ComboItem { Value = p.Date.ToString("dd/MM/yyyy"), Text = p.DayOfWeek.ToString() });
                List<ComboItem> items = Region.ToList();
                if (!allOption)
                    items.Insert(0, new ComboItem { Value = "0", Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { Value = "1", Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }

        public IEnumerable<ComboItem> GetForComboAdmin(string _pGroupCode, bool defaultOption = false, bool allOption = false)
        {
            try
            {
                DatabaseDataContext context = null;
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var Region = context.CodeDetails
                    .Where(p => p.GroupCode.ToLower().Trim() == _pGroupCode.ToLower().Trim())
            .Select(p => new ComboItem { ID = Convert.ToInt32(p.Ordinal), Text = p.Name.Trim() })
            .OrderBy(p => p.Text);

                List<ComboItem> items = Region.ToList();
                if (!allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 1, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public IEnumerable<ComboItem> GetForComboAdminWeb(string _pGroupCode, bool defaultOption = false, bool allOption = false)
        {
            try
            {
                DatabaseDataContext context = null;
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var Region = context.CodeDetails
                    .Where(p => p.GroupCode.ToLower().Trim() == _pGroupCode.ToLower().Trim() && p.Ordinal == 2)
            .Select(p => new ComboItem { ID = Convert.ToInt32(p.Ordinal), Text = p.Name.Trim() })
            .OrderBy(p => p.Text);

                List<ComboItem> items = Region.ToList();
                if (!allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 1, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public static List<CodeDetail> GetAll(string _pCodeName)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.CodeDetails
                            where item.GroupCode == _pCodeName
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public IEnumerable<ComboItem> GetForComboStatusOOS(bool defaultOption = false, bool allOption = false)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

                List<ComboItem> items = new List<ComboItem>();
                items.Add(new ComboItem { ID = 1, Text = "OOS (Urgent) >=2 ngày và Ngày hiện tại OOS" });
                items.Add(new ComboItem { ID = 2, Text = "OOS (Urgent) >=2 ngày" });
                if (!allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 1, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }      
    }
    public class SimpleCodeDetail
    {
        private string _GroupCode;
        public string GroupCode
        {
            get { return _GroupCode; }
            set { _GroupCode = value; }
        }
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private string _Value;
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        private int? _Ordinal;
        public int? Ordinal
        {
            get { return _Ordinal; }
            set { _Ordinal = value; }
        }
        public SimpleCodeDetail() { }

        public SimpleCodeDetail(string GroupCode, string Name, string Value, int? Ordinal)
        {
            this.GroupCode = GroupCode;
            this.Name = Name;
            this.Value = Value;
            this.Ordinal = Ordinal;
        }
    }

}
