﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Product
    {
        public class _ProductTypes
        {
            public static int Stock = 1;
            public static int POSM = 2;
        }
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(string _pAppCode, int pProjectID, string pOutletID, ref ProductResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu product
                    var query = from item in context.Products
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.Brand.ProjectID == pProjectID
                                orderby item.Ordinal, item.ID ascending
                                select new SimpleProduct
                                {
                                    ID = item.ID,
                                    ProductCode = item.ProductCode,
                                    ProductName = item.ProductName,
                                    FilePath = item.FilePath,
                                    BrandID = item.BrandID,
                                    IsPrice = item.IsPrice,
                                    IsStock = item.IsStock,
                                    IsTakeOffVolume = item.IsTakeOffVolume,
                                    IsChangeGift = item.IsChangeGift,
                                    IsGetInfo = item.IsGetInfo,
                                    NumberOfEnough = item.NumberOfEnough.HasValue ? item.NumberOfEnough.Value : 0,
                                    Ordinal = item.Ordinal.HasValue ? item.Ordinal.Value : 0
                                };
                    // gán dữ liệu product vào list tamh
                    var list = query.ToList();

                    // lấy thông tin outlet
                    var outlet = context.Outlets.Where(p => p.ID == int.Parse(pOutletID)).FirstOrDefault();

                    // lấy thông tin đánh giá tồn kho theo nhóm outlet
                    var listOOS = outlet.Group.Stock_Status.ToList();

                    // chạy vòng lặp từng sản phẩm kiểm tra
                    foreach (var item in list)
                    {
                        var read = listOOS.Where(p => p.ProductID == item.ID).FirstOrDefault();
                        // nếu không có trong đánh giá tồn kho . nghĩa là sản phẩm này không dùng để báo cáo, tồn kho, bán hàng, giá
                        if (read == null)
                        {
                            // chuyển trạng thái về false để app nhận biết mà không show lên  trên các báo cáo
                            item.IsPrice = false;
                            item.IsStock = false;
                            item.IsTakeOffVolume = false;
                        }
                    }

                    // gán giá trị cho phần trả về
                    result.Data = list;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public static List<Product> GetProduct(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Products
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.IsStock == true
                            && item.Brand.ProjectID == _pProjectID
                            select item;

                var list = query.ToList().OrderBy(p => p.Ordinal).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Product> GetProductReport(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Products
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.IsStock == true
                            && item.Brand.ProjectID == _pProjectID
                            && item.IsReportDisplay != null
                            select item;

                var list = query.ToList().OrderBy(p => p.IsReportDisplay).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Product> GetProductgift(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Products
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.IsChangeGift == true
                            && item.Brand.ProjectID == _pProjectID
                            select item;

                var list = query.ToList().OrderBy(p => p.Ordinal).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Product> GetProductgiftexcel(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Products
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.IsChangeGift == true
                            && item.Brand.ProjectID == _pProjectID
                            select item;
                var query2 = from item in context.Products
                             where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                             && item.Brand.ProjectID == _pProjectID
                             && item.BrandID == 20
                             select item;
                var list = query.ToList().Union(query2.ToList()).OrderBy(p => p.ID).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Product> GetProductForGroup(int _pProjectID, int _GroupID,string outletid)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var queryOutlet = from item in context.Outlets
                                  where item.ID == int.Parse(outletid)
                                  select item;
                var outlet = queryOutlet.FirstOrDefault();
                var query = from item in context.Products
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.IsStock == true
                            && item.Brand.ProjectID == _pProjectID
                            //&& ((!item.IsNaN.Contains(outlet.GroupID.ToString())) ||
                            // item.IsNaN == null)
                            select item;
                var queryGroup = from item in context.Stock_Status
                                 where item.GroupID == outlet.GroupID
                                 && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                 select item;
                var listProductByGroup = queryGroup.ToList();
                var list = query.ToList().OrderBy(p => p.Ordinal).ToList();
                List<Product> listRP = new List<Product>(); 
                foreach (var item in list)
                {
                    var readExist = listProductByGroup.Where(o => o.ProductID == item.ID).FirstOrDefault();
                    if (readExist != null)
                    {
                        listRP.Add(item);
                    }
                }
                return listRP;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Product> GetProductdelta(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Products
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.IsStock == true
                            && item.Brand.ProjectID == _pProjectID
                            select item;

                var list = query.ToList().OrderBy(p => p.Ordinal).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT getAllForSup(string pAppCode, string pProjectID, ref GetProductSUPResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu
                    var query = from item in context.Products
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.Brand.ProjectID == int.Parse(pProjectID)
                                && item.IsChangeGift == true
                                orderby item.Ordinal, item.ID ascending
                                select new SimpleProduct
                                {
                                    ID = item.ID,
                                    ProductCode = item.ProductCode,
                                    ProductName = item.ProductName,
                                    FilePath = item.FilePath,
                                    BrandID = item.BrandID,
                                    IsPrice = item.IsPrice,
                                    IsStock = item.IsStock,
                                    IsTakeOffVolume = item.IsTakeOffVolume,
                                    IsChangeGift = item.IsChangeGift,
                                    IsGetInfo = item.IsGetInfo,
                                    NumberOfEnough = item.NumberOfEnough.HasValue ? item.NumberOfEnough.Value : 0,
                                    Ordinal = item.Ordinal.HasValue ? item.Ordinal.Value : 0
                                };
                    // gán giá trị cho phần trả về
                    var list = query.ToList();
                    result.Data = list;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }
    public class SimpleProduct
    {
        private long? _ID;
        public long? ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _ProductCode;
        public string ProductCode
        {
            get { return _ProductCode; }
            set { _ProductCode = value; }
        }
        private string _ProductName;
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }
        private string _FilePath;
        public string FilePath
        {
            get { return _FilePath; }
            set { _FilePath = value; }
        }
        private int _BrandID;
        public int BrandID
        {
            get { return _BrandID; }
            set { _BrandID = value; }
        }
        private bool _IsPrice;
        public bool IsPrice
        {
            get { return _IsPrice; }
            set { _IsPrice = value; }
        }

        private bool _IsStock;
        public bool IsStock
        {
            get { return _IsStock; }
            set { _IsStock = value; }
        }

        private bool _IsTakeOffVolume;
        public bool IsTakeOffVolume
        {
            get { return _IsTakeOffVolume; }
            set { _IsTakeOffVolume = value; }
        }


        private bool _IsChangeGift;
        public bool IsChangeGift
        {
            get { return _IsChangeGift; }
            set { _IsChangeGift = value; }
        }

        private bool _IsGetInfo;
        public bool IsGetInfo
        {
            get { return _IsGetInfo; }
            set { _IsGetInfo = value; }
        }

        private int _NumberOfEnough;
        public int NumberOfEnough
        {
            get { return _NumberOfEnough; }
            set { _NumberOfEnough = value; }
        }

        private int _Ordinal;
        public int Ordinal
        {
            get { return _Ordinal; }
            set { _Ordinal = value; }
        }


        public SimpleProduct() { }

        public SimpleProduct(long ID, string ProductCode, string ProductName, string FilePath, int BrandID, bool IsPrice, bool IsStock, bool IsTakeOffVolume, bool IsChangeGift, bool IsGetInfo, int NumberOfEnough, int Ordinal)
        {
            this.ID = ID;
            this.ProductCode = ProductCode;
            this.ProductName = ProductName;
            this.FilePath = FilePath;
            this.BrandID = BrandID;
            this.IsPrice = IsPrice;
            this.IsStock = IsStock;
            this.IsTakeOffVolume = IsTakeOffVolume;
            this.IsChangeGift = IsChangeGift;
            this.IsGetInfo = IsGetInfo;
            this.NumberOfEnough = NumberOfEnough;
            this.Ordinal = Ordinal;

        }
    }
}

