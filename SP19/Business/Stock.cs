﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.EnterpriseServices;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Stock
    {
        public class StatusStock
        {
            public const int OOS = 1;
            public const int ALERT = 2;
            public const int SAFE = 3;

        }

        public class RemarkStock
        {
            public const string LINE = "Ụ Kệ";// "Chỉ đếm số lượng hàng hóa trong line, Ụ, kệ trưng này bia tết";
            public const string STOCK = "Kho & Kệ";//"Số lượng đã bao gồm hàng hóa trong kho";
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(string _pAppCode, List<Stock> list, ref MainResult result)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy danh sách outlet
                    var listOutlet = context.Outlets.ToList();
                    // chạy vòng lặp
                    foreach (var item in list)
                    {
                        // lấy thông tin outlet
                        var outletEntity = listOutlet.Where(p => p.ID == item.OutletID).FirstOrDefault();

                        // lấy thông tin stock theo outlet và ngày nhập của item
                        var stockEntity = outletEntity.Stocks.Where(p => p.ProductID == item.ProductID && p.DeviceDateTime.Date == item.DeviceDateTime.Date).FirstOrDefault();

                        if (stockEntity != null)  // nếu có nghĩa là đã có báo tồn kho hôm nay -- update lại số lượng tải lên lại
                        {
                            stockEntity.NumberSP = item.NumberSP;
                            stockEntity.StatusOOS = item.StatusOOS;
                            stockEntity.LastUpdatedBy = item.CreatedBy;
                            stockEntity.LastUpdatedDateTime = DateTime.Now;
                            stockEntity.ServerDateTime = DateTime.Now;
                            stockEntity.RowVersion += 1;
                            context.SubmitChanges(); // thực thi câu lệnh
                        }
                        else  // không có . nghĩa là báo cáo tồn kho lần đầu -- thêm mới vào db
                        {
                            context.Stocks.InsertOnSubmit(item); // thêm
                            context.SubmitChanges(); // thực thi câu lệnh
                        }


                    }
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchOffer(ref string _messageError, int _pProjectID, int _pOutletType, StockSE se, ref List<StockEXT> list)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

                var _temp = from item in context.spStockRequest(se.DateStockSE.Date)
                            where (item.ID == se.OutletStockSE || se.OutletStockSE == null || se.OutletStockSE == 0)
                            && (item.RegionID == se.DistrictAttendance || se.DistrictAttendance == null || se.DistrictAttendance == 0)
                            && (item.Type == se.RegionStockSE || se.RegionStockSE == null || se.RegionStockSE == "" || se.RegionStockSE == "Tất Cả")
                            && (item.ProjectID == _pProjectID)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0 || _pOutletType == -1)
                            select item;
                var _listtemp = _temp.ToList();

                var query = from item in context.spStockRequest(se.DateStockSE.Date)
                            where (item.ID == se.OutletStockSE || se.OutletStockSE == null || se.OutletStockSE == 0)
                              && (item.RegionID == se.DistrictAttendance || se.DistrictAttendance == null || se.DistrictAttendance == 0)
                            && (item.Type == se.RegionStockSE || se.RegionStockSE == null || se.RegionStockSE == "" || se.RegionStockSE == "Tất Cả")
                            && (item.ProjectID == _pProjectID)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0 || _pOutletType == -1)
                            select new StockEXT
                            {
                                GroupID = item.GroupID.GetValueOrDefault(),
                                OutletName = item.OutletName,
                                OutletID = item.ID.ToString(),
                                ProductID = item.ProductID,
                                ProductName = item.ProductName,
                                NumberOffer = item.NumberSum.GetValueOrDefault(),
                                NumberSP = item.NumberSP,
                                NumberSale = item.NumberSale,
                                NumberType = item.NumberType.ToString(),
                                DateStock = item.Date.GetValueOrDefault(),
                                Note = item.NumberType != 0 ? RemarkStock.LINE : RemarkStock.STOCK,
                                RegionName = item.RegionID.ToString()
                            };
                list = query.ToList();
                foreach (var item in list)
                {
                    item.StatusNameOutlet = _listtemp.Where(p => p.ID.GetValueOrDefault() == int.Parse(item.OutletID)).Select(p => p.StatusName).Min().ToString();
                }
               
                list = list.Where(p => p.StatusNameOutlet == se.StatusStockSE.ToString() || se.StatusStockSE == null || se.StatusStockSE == 0).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageError, int _pProjectID, int _pOutletType, StockSE se, ref List<StockEXT> list)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var _temp = from item in context.spStock(se.DateStockSE.Date)
                            where (item.ID == se.OutletStockSE || se.OutletStockSE == null || se.OutletStockSE == 0)
                            && (item.RegionID == se.DistrictAttendance || se.DistrictAttendance == null || se.DistrictAttendance == 0)
                            && (item.Type == se.RegionStockSE || se.RegionStockSE == null || se.RegionStockSE == "" || se.RegionStockSE == "Tất Cả")
                            && (item.ProjectID == _pProjectID)
                            && (item.SaleID == se.SaleSE || se.SaleSE == null || se.SaleSE == 0)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0 || _pOutletType == -1)
                            select item;
                var _listtemp = _temp.ToList();
                var query = from item in context.spStock(se.DateStockSE.Date)
                            where (item.ID == se.OutletStockSE || se.OutletStockSE == null || se.OutletStockSE == 0)
                            && (item.RegionID == se.DistrictAttendance || se.DistrictAttendance == null || se.DistrictAttendance == 0)
                            && (item.Type == se.RegionStockSE || se.RegionStockSE == null || se.RegionStockSE == "" || se.RegionStockSE == "Tất Cả")
                            &&  (item.ProjectID == _pProjectID)
                            && (item.SaleID == se.SaleSE || se.SaleSE == null || se.SaleSE == 0)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0 || _pOutletType == -1)
                            select new StockEXT
                            {
                                GroupID= item.GroupID.GetValueOrDefault(),
                                OutletName = item.OutletName,
                                OutletID = item.ID.ToString(),
                                ProductID = item.ProductID,
                                ProductName = item.ProductName,                                
                                StatusName = item.StatusName.ToString(),
                                NumberSP = item.NumberSP,
                                NumberSale = item.NumberSale,
                                NumberType = item.NumberType.ToString(),
                                DateStock = item.Date.GetValueOrDefault(),
                                Note = item.NumberType != 0 ? RemarkStock.LINE : RemarkStock.STOCK,
                                RegionName = item.RegionID.ToString()
                            };
                list = query.ToList();
                foreach(var item in list)
                {
                    item.StatusNameOutlet = _listtemp.Where(p => p.ID.GetValueOrDefault() == int.Parse(item.OutletID)).Select(p => p.StatusName).Min().ToString();
                    item.HaveSaleUpdate = _listtemp.Where(p => p.ID.GetValueOrDefault() == int.Parse(item.OutletID)).Select(p => p.NumberSale != null).Count();
                }
                
                list = list.Where(p => p.StatusNameOutlet == se.StatusStockSE.ToString() || se.StatusStockSE == null || se.StatusStockSE == 0).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE UpdateStock(ref string _messageError, int _pOutletID, DateTime _pDateStock, string _pStockID, string _pNumberSale, string _note, int _pUserID)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                //Lấy userID       
                string[] arayid = _pStockID.Remove(0, 1).Split('|');
                string[] arayvalue = _pNumberSale.Remove(0, 1).Split('|');

                for (int i = 0; i < arayid.Count(); i++)
                {
                    var query = from stock in context.Stocks
                                where stock.OutletID == _pOutletID
                                && stock.ProductID == long.Parse(arayid[i])
                                && stock.DeviceDateTime.Date == _pDateStock.Date
                                select stock;
                    if (query.Count() > 0) //nếu như nó update stock
                    {
                        if (arayvalue[i].ToString() != String.Empty)
                        {
                            var simplestock = query.FirstOrDefault();
                            simplestock.NumberType = int.Parse(_note);
                            simplestock.NumberSale = int.Parse(arayvalue[i]);
                            simplestock.LastUpdatedBy = _pUserID;
                            simplestock.LastUpdatedDateTime = DateTime.Now;
                            simplestock.RowVersion += 1;
                            context.SubmitChanges();
                        }
                    }
                    else //tạo mới
                    {
                        if (arayvalue[i].ToString() != String.Empty)
                        {
                            Stock stock = new Stock();
                            stock.OutletID = _pOutletID;
                            stock.DeviceDateTime = _pDateStock.Date;
                            stock.ServerDateTime = _pDateStock.Date;
                            stock.StockCode = DateTime.Now.ToString("ddMMyyyyHHmmss");
                            stock.ProductID = int.Parse(arayid[i]);
                            stock.NumberSale = int.Parse(arayvalue[i]);
                            stock.NumberType = int.Parse(_note);
                            stock.CreatedBy = _pUserID;
                            stock.CreatedDateTime = DateTime.Now;
                            stock.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            stock.RowVersion = 1;
                            context.Stocks.InsertOnSubmit(stock);
                            context.SubmitChanges();
                        }
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }


            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetStockForOutletForsale(ref string _messageError, int _pOutletID, DateTime _pDateStock, ref List<Stock> list)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                //--------------Co stock trong Ngay
                var query = from stock in context.Stocks
                            where stock.OutletID == _pOutletID
                                  && stock.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                  && stock.DeviceDateTime.Date == _pDateStock.Date
                            select stock;
                list = query.ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static int GetMaxOOS(int _pGroupID, int _pProductID)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                //--------------Co stock trong Ngay
                var query = from item in context.Stock_Status
                            where item.GroupID == _pGroupID
                                  && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                  && item.ProductID == _pProductID
                            select item.Max;
                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static string GetNameSale(int _OutletID)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                //--------------Co stock trong Ngay
                var query = from item in context.Sale_Outlets
                            where item.OutletID == _OutletID
                                  && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select item.User_Web.FullName;
                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                return "";
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT updateType(string pAppCode, string pDeviceDateTime, int pOutletID, int pNumberType, ref MainResult result)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                   // parse  string ra datetime ngày  tải lên
                    DateTime deviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(pDeviceDateTime);
                    // lấy danh sách báo cáo tồn kho trong ngày
                    var queryStock = from item in context.Stocks
                                     where item.DeviceDateTime.Date == deviceDateTime.Date
                                     && item.OutletID == pOutletID
                                     select item;
                    if (queryStock.Count() > 0)  // nếu có 
                    {
                        // cập nhật NumberType của list báo cáo tồn kho theo pNumberType tải lên
                        var listRP = queryStock.ToList();
                        listRP.All(c => { c.NumberType = pNumberType; return true; });
                        context.SubmitChanges(); // thực thi câu lệnh
                    }
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchStockreport(ref string _messageError, int _pProjectID, int _pOutletType, StockReportSE se, ref List<StockEXT> list)
        {
            DatabaseDataContext context = null;

            try
            {
                DateTime dt= Common.ConvertUtils.ConvertStringToShortDate(se.DateInWeek);
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.spStockReport(se.DateStockFSE.Date, se.DateStockTSE.Date, se.RegionStockReportSE, _pProjectID, _pOutletType).ToList()
                             where (item.ID == se.OutletStockReportSE || se.OutletStockReportSE == null || se.OutletStockReportSE == 0)
                             && (item.SaleID == se.SaleReportSE || se.SaleReportSE == null || se.SaleReportSE == 0)
                             && (item.Date.GetValueOrDefault().Date == dt.Date || se.DateInWeek == null || se.DateInWeek == "0")
                            select new StockEXT
                            {
                                GroupID = item.GroupID.GetValueOrDefault(),
                                OutletName = item.OutletName,
                                OutletID = item.ID.ToString(),
                                ProductID = item.ProductID,
                                ProductName = item.ProductName,
                                StatusName = item.StatusName.ToString(),
                                NumberSP = item.NumberSP,
                                NumberSale = item.NumberSale,
                                NumberType = item.NumberType.ToString(),
                                DateStock = item.Date.GetValueOrDefault(),
                                Note = item.NumberType != 0 ? RemarkStock.LINE : RemarkStock.STOCK,
                                RegionName = item.RegionID.ToString()
                            };
                list = query.ToList();
                list = list.Where(p => p.StatusName == se.StatusStockReportSE.ToString() || se.StatusStockReportSE == null || se.StatusStockReportSE == 0).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchStockreport7ngay(ref string _messageError, int _pProjectID, int _pOutletType, StockReportSE se, string listdate, ref List<StockEXT> list)
        {
            DatabaseDataContext context = null;

            try
            {
                if (se.OutletStockReportSE == null )
                {
                    se.OutletStockReportSE = 0;
                }
                if (se.SaleReportSE == null)
                {
                    se.SaleReportSE = 0;
                }
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.spStockReport7ngay(se.DateStockTSE.Date.AddDays(-6).Date, se.DateStockTSE.Date, se.RegionStockReportSE, _pProjectID, _pOutletType, int.Parse(se.OutletStockReportSE.ToString()), int.Parse(se.SaleReportSE.ToString())).ToList()
                            where (item.RegionID == se.DistrictAttendance || se.DistrictAttendance == null || se.DistrictAttendance == 0)
                            select new StockEXT
                            {
                                GroupID = item.GroupID.GetValueOrDefault(),
                                OutletName = item.OutletName,
                                OutletID = item.ID.ToString(),
                                ProductID = item.ProductID,
                                ProductName = item.ProductName,
                                StatusName = item.StatusName.ToString(),
                                NumberSP = item.NumberSP,
                                NumberSale = item.NumberSale,
                                NumberType = item.NumberType.ToString(),
                                DateStock = item.Date.GetValueOrDefault(),
                                Note = item.NumberType != 0 ? RemarkStock.LINE : RemarkStock.STOCK,
                                RegionName = item.RegionID.ToString()
                            };
                list = query.ToList();
                list = list.Where(p => p.StatusName == se.StatusStockReportSE.ToString() || se.StatusStockReportSE == null || se.StatusStockReportSE == 0).ToList();
                if ((listdate != null) && (listdate != ""))
                {
                    string[] _listdate = listdate.ToString().Remove(0, 1).Split('|');
                    list = list.Where(item => _listdate.Any(item2 => Common.ConvertUtils.ConvertStringToShortDate(item2).Date == item.DateStock.Date)).ToList();
                }
                if (se.OOS == "1")//lấy OOS hết
                {
                    //lấy danh sách outlet
                    var outlet = list.Select(p => p.OutletID).Distinct().ToList();
                    foreach (var i in outlet)
                    {
                        var xetOOS = list.Where(p => p.OutletID == i && p.StatusName == "1");
                        if (xetOOS.Count() > 0)
                        {
                            List<StockForDate> result = xetOOS
                                .GroupBy(l => new { l.DateStock.Date, l.OutletID })
                                .Select(cl => new StockForDate
                                {
                                    OutletID = cl.First().OutletID.ToString(),
                                    Dates = cl.First().DateStock.Date,
                                    TotalNumber = cl.Count(),
                                }).ToList();
                            if (result.Count() < 2)
                            {
                                list = list.Where(p => p.OutletID != i).ToList();
                            }
                            if (result.Where(p => p.Dates.Date == se.DateStockTSE.Date).Count() == 0)
                            {
                                list = list.Where(p => p.OutletID != i).ToList();
                            }
                        }
                        else
                        {
                            list = list.Where(p => p.OutletID != i).ToList();
                        }
                    }
                }
                else if (se.OOS == "2")//lấy OOS hết
                {
                    //lấy danh sách outlet
                    var outlet = list.Select(p => p.OutletID).Distinct().ToList();
                    foreach (var i in outlet)
                    {
                        var xetOOS = list.Where(p => p.OutletID == i && p.StatusName == "1");
                        if (xetOOS.Count() > 0)
                        {
                            List<StockForDate> result = xetOOS
                                .GroupBy(l => new { l.DateStock.Date, l.OutletID })
                                .Select(cl => new StockForDate
                                {
                                    OutletID = cl.First().OutletID.ToString(),
                                    Dates = cl.First().DateStock.Date,
                                    TotalNumber = cl.Count(),
                                }).ToList();
                            if (result.Count() < 2)
                            {
                                list = list.Where(p => p.OutletID != i).ToList();
                            }
                        }
                        else
                        {
                            list = list.Where(p => p.OutletID != i).ToList();
                        }
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchStockreport7ngayNew(ref string _messageError, int _pProjectID, int _pOutletType, StockReportSE se, string listdate, ref List<StockEXT> list)
        {
            DatabaseDataContext context = null;

            try
            {
                DateTime dt = DateTime.Now;
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.spStockReport7ngay(se.DateStockTSE.Date.AddDays(-6).Date, se.DateStockTSE.Date, se.RegionStockReportSE, _pProjectID, _pOutletType, int.Parse(se.OutletStockReportSE.ToString()), int.Parse(se.SaleReportSE.ToString())).ToList()
                            where (item.RegionID == se.DistrictAttendance || se.DistrictAttendance == null || se.DistrictAttendance == 0)
                            select new StockEXT
                            {
                                GroupID = item.GroupID.GetValueOrDefault(),
                                OutletName = item.OutletName,
                                OutletID = item.ID.ToString(),
                                ProductID = item.ProductID,
                                ProductName = item.ProductName,
                                StatusName = item.StatusName.ToString(),
                                NumberSP = item.NumberSP,
                                NumberSale = item.NumberSale,
                                NumberType = item.NumberType.ToString(),
                                DateStock = item.Date.GetValueOrDefault(),
                                Note = item.NumberType != 0 ? RemarkStock.LINE : RemarkStock.STOCK,
                                RegionName = item.RegionID.ToString()
                            }; list = query.ToList();
                list = list.Where(p => p.StatusName == se.StatusStockReportSE.ToString() || se.StatusStockReportSE == null || se.StatusStockReportSE == 0).ToList();
                if ((listdate != null) && (listdate != ""))
                {
                    string[] _listdate = listdate.Remove(0, 1).Split('|');
                    list = list.Where(item => _listdate.Any(item2 => Common.ConvertUtils.ConvertStringToShortDate(item2).Date == item.DateStock.Date)).ToList();
                }
                var _outlet = list.Select(p => p.OutletID).Distinct().ToList();
                foreach (var i in _outlet)
                {
                    var xetOOS = list.Where(p => p.OutletID == i && p.StatusName == "1" );
                    if (xetOOS.Count() > 0)
                    {
                        List<StockForDate> result = xetOOS
                            .GroupBy(l => new { l.DateStock.Date, l.OutletID })
                            .Select(cl => new StockForDate
                            {
                                OutletID = cl.First().OutletID.ToString(),
                                Dates = cl.First().DateStock.Date,
                                TotalNumber = cl.Count(),
                            }).ToList();
                        if (result.Count() < 2)
                        {
                            list = list.Where(p => p.OutletID != i).ToList();
                        }
                        if (result.Where(p => p.Dates.Date ==se.DateStockTSE.Date).Count() == 0)
                        {
                            list = list.Where(p => p.OutletID != i).ToList();
                        }
                    }
                    else
                    {
                        list = list.Where(p => p.OutletID != i).ToList();
                    }
                }
                if (se.OOS == "1")//lấy OOS hết
                {
                    //lấy danh sách outlet
                    var outlet = list.Select(p => p.OutletID).Distinct().ToList();
                    foreach (var i in outlet)
                    {
                        var xetOOS = list.Where(p => p.OutletID == i && p.StatusName == "1");
                        if (xetOOS.Count() > 0)
                        {
                            List<StockForDate> result = xetOOS
                                .GroupBy(l => new { l.DateStock.Date, l.OutletID })
                                .Select(cl => new StockForDate
                                {
                                    OutletID = cl.First().OutletID.ToString(),
                                    Dates = cl.First().DateStock.Date,
                                    TotalNumber = cl.Count(),
                                }).ToList();
                            if (result.Count() < 2)
                            {
                                list = list.Where(p => p.OutletID != i).ToList();
                            }
                            if (result.Where(p => p.Dates.Date == DateTime.Now.Date).Count() == 0)
                            {
                                list = list.Where(p => p.OutletID != i).ToList();
                            }
                        }
                        else
                        {
                            list = list.Where(p => p.OutletID != i).ToList();
                        }
                    }
                }
                else if (se.OOS == "2")//lấy OOS hết
                {
                    //lấy danh sách outlet
                    var outlet = list.Select(p => p.OutletID).Distinct().ToList();
                    foreach (var i in outlet)
                    {
                        var xetSKUOOS = list.Where(p => p.OutletID == i && p.StatusName == "1");
                        if (xetSKUOOS.Count() > 0)
                        {
                            List<Take_Off_VolumnEXT> result = xetSKUOOS
                                .GroupBy(l => new { l.ProductID, l.OutletID })
                                .Select(cl => new Take_Off_VolumnEXT
                                {
                                    OutletID = cl.First().OutletID.ToString(),
                                    Brand = cl.First().ProductID.ToString(),
                                    TotalNumber = cl.Count(),
                                }).ToList();
                            if (result.Where(p => p.TotalNumber < 2 && p.OutletID == i).Count() > 0)
                            {
                                list = list.Where(p => p.OutletID != i).ToList();
                            }
                        }
                        else
                        {
                            list = list.Where(p => p.OutletID != i).ToList();
                        }
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchStockreportSale(ref string _messageError, int _pProjectID, int _pOutletType, int _userID, StockReportSE se, ref List<StockEXT> list)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.spStockReport(se.DateStockFSE.Date, se.DateStockTSE.Date, se.RegionStockReportSE, _pProjectID, _pOutletType).ToList()
                            where (item.ID == se.OutletStockReportSE || se.OutletStockReportSE == null || se.OutletStockReportSE == 0)
                            select new StockEXT
                            {
                                GroupID = item.GroupID.GetValueOrDefault(),
                                OutletName = item.OutletName,
                                OutletID = item.ID.ToString(),
                                ProductID = item.ProductID,
                                ProductName = item.ProductName,
                                StatusName = item.StatusName.ToString(),
                                NumberSP = item.NumberSP,
                                NumberSale = item.NumberSale,
                                NumberType = item.NumberType.ToString(),
                                DateStock = item.Date.GetValueOrDefault(),
                                Note = item.NumberType != 0 ? RemarkStock.LINE : RemarkStock.STOCK,
                                RegionName = item.RegionID.ToString()
                            };
                list = query.ToList();
                list = list.Where(p => p.StatusNameOutlet == se.StatusStockReportSE.ToString() || se.StatusStockReportSE == null || se.StatusStockReportSE == 0).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class StockDetail : Stock
    {
        public long ProductID { get; set; }
        public long AssignID { get; set; }
        public string ProductName { get; set; }
        public int Number { get; set; }
        public int City { get; set; }
        public int type { get; set; }
        public int StatusName { get; set; }
        public int StatusStock { get; set; }
        public DateTime? date { get; set; }
    }

    public class StockSE
    {

        private DateTime _DateStockSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not Empty.")]
        [Display(Name = "Ngày")]
        [DataType(DataType.Date)]
        public DateTime DateStockSE
        {
            get { return _DateStockSE; }
            set
            {
                if (value != null)
                    _DateStockSE = value.Date;
            }
        }

        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionStockSE { get; set; }

        [UIHint("_District")]
        [Display(Name = "Tỉnh")]
        public int DistrictAttendance { get; set; }

        [UIHint("_ListOutlet")]
        [Display(Name = "Siêu Thị")]
        public long OutletStockSE { get; set; }


        [UIHint("_StatusStock")]
        [Display(Name = "Trạng Thái")]
        public long StatusStockSE { get; set; }

        [UIHint("_Sale")]
        [Display(Name = "Sale")]
        public long SaleSE { get; set; }
    }

    public class StockReportSE
    {

        private DateTime _DateStockFSE = DateTime.Now.Date.AddDays(-6);
        [Required(ErrorMessage = "{0} Not Empty.")]
        [Display(Name = "Từ Ngày")]
        [DataType(DataType.Date)]
        public DateTime DateStockFSE
        {
            get { return _DateStockFSE; }
            set
            {
                if (value != null)
                    _DateStockFSE = value.Date;
            }
        }
        private DateTime _DateStockTSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not Empty.")]
        [Display(Name = "Đến Ngày")]
        [DataType(DataType.Date)]
        public DateTime DateStockTSE
        {
            get { return _DateStockTSE; }
            set
            {
                if (value != null)
                    _DateStockTSE = value.Date;
            }
        }

        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionStockReportSE { get; set; }


        [UIHint("_District")]
        [Display(Name = "Tỉnh")]
        public int DistrictAttendance { get; set; }

        [UIHint("_ListOutlet")]
        [Display(Name = "Siêu Thị")]
        public long OutletStockReportSE { get; set; }


        [UIHint("_StatusStock")]
        [Display(Name = "Trạng Thái")]
        public long StatusStockReportSE { get; set; }

        [UIHint("_Sale")]
        [Display(Name = "Sale")]
        public long SaleReportSE { get; set; }

        [UIHint("_DateInWeek")]
        [Display(Name = "Ngày Trong Tuần")]
        public string DateInWeek { get; set; }

        [UIHint("_OOS")]
        [Display(Name = "Trạng Thái 1")]
        public string OOS { get; set; }
    }

    public class Stock7Ngay
    {
        public int OutletID { get; set; }
        public int CountStatusStock { get; set; }
        public List<StockEXT> list { get; set; }
    }
    public class StockEXT
    {
        public int GroupID { get; set; }
        public int ID { get; set; }
        public string RegionName { get; set; }
        public string OutletID { get; set; }
        public long ProductID { get; set; }
        public string OutletName { get; set; }
        public string ProductName { get; set; }
        public int NumberOffer { get; set; }
        public int NumberSP { get; set; }
        public int? NumberSale { get; set; }
        public string StockName { get; set; }
        public DateTime DateStock { get; set; }
        public string Note { get; set; }
        public string NumberType { get; set; }
        public string StatusName { get; set; }
        public string StatusNameOutlet { get; set; }
        public int HaveSaleUpdate { get; set; }

        public string ChanelName { get; set; }

        public string SaleName { get; set; }
    }

    public class StockEXTDertail
    {
        public string Area { get; set; }
        public string OutletName { get; set; }
        public string ProductName { get; set; }
        public int Number { get; set; }
        public int NumberSale { get; set; }
        public string OutletID { get; set; }
        public string StockName { get; set; }
        public long ProductID { get; set; }
        public string Note { get; set; }
        public string OutletType { get; set; }
    }
}


