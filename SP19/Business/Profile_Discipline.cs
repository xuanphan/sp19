﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Profile_Discipline
    {
        //public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string MessageSystemError, string _pAppCode, string _pUserID, ref AddNewResult result)
        //{
        //    DatabaseDataContext context = null;
        //    context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //    try
        //    {
        //        if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
        //        {

        //            var querycomment = from item in context.Profile_Disciplines
        //                               where comment.ProfileID == this.ProfileID
        //                               select comment;
        //            if (querycomment.Count() > 0)
        //            {
        //                var simple = querycomment.FirstOrDefault();
        //                simple.ProfileID = this.ProfileID;
        //                simple.Description = this.Description;
        //                simple.LastUpdatedBy = int.Parse(_pUserID);
        //                simple.LastUpdatedDateTime = DateTime.Now;
        //                simple.RowVersion += 1;
        //                context.SubmitChanges();

        //                result.Data = simple.ID.ToString();
        //                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
        //                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA);
        //                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
        //            }
        //            else
        //            {
        //                this.CreatedBy = int.Parse(_pUserID);
        //                this.CreatedDateTime = DateTime.Now;
        //                this.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
        //                this.RowVersion = 1;
        //                context.Profile_Disciplines.InsertOnSubmit(this);
        //                context.SubmitChanges();

        //                result.Data = this.ID.ToString();
        //                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
        //                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
        //                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
        //            }
        //        }
        //        else
        //        {
        //            result.Data = "0";
        //            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
        //            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
        //            return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
        //        }
        //    }


        //    catch (Exception ex)
        //    {
        //        MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
        //        result.Data = "0";
        //        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
        //        result.Description = ex.Message;
        //        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //} 
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE Update(ref string _messageError, string _pID, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string[] arayID = _pID.Split('|');

                for (int i = 0; i < arayID.Count(); i++)
                {
                    if (arayID[i].ToString() != String.Empty)
                    {
                        //xem brand set này thuộc hiện tại k?
                        var query = from item in context.Profile_Disciplines
                                    where item.ID == int.Parse(arayID[i])
                                    select item;
                        // không có ở hiện tại thì tạo và update trạng thái
                        if (query.Count() > 0)
                        {
                            //xác nhận
                            var simple = query.FirstOrDefault();
                            simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE;
                            simple.LastUpdatedBy = _pUserID;
                            simple.LastUpdatedDateTime = DateTime.Now;
                            simple.RowVersion += 1;
                            // update set hiện tại
                            context.SubmitChanges();
                        }
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE AddNew(ref string _messageError, string _pProfileID, string _pDescription, string _pTitle, string _pLink, int _pUserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

                //xem brand set này thuộc hiện tại k?
                var query = from item in context.Profile_Disciplines
                            where item.ID == int.Parse(_pProfileID)
                            && item.Description == _pDescription
                            && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select item;
                // không có ở hiện tại thì tạo và update trạng thái
                if (query.Count() == 0)
                {
                    //xác nhận
                    Profile_Discipline simple = new Profile_Discipline();
                    simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    simple.ProfileID = int.Parse(_pProfileID);
                    simple.Title = _pTitle;
                    simple.FilePath = _pLink;
                    simple.Description = _pDescription;
                    simple.CreatedBy = _pUserID;
                    simple.CreatedDateTime = DateTime.Now;
                    simple.RowVersion = 1;
                    context.Profile_Disciplines.InsertOnSubmit(simple);
                    context.SubmitChanges();
                }

                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(ref string MessageError, long _profileID, ref List<Profile_Discipline> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Profile_Disciplines
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.ProfileID == _profileID
                            select item;
                list = query.ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                list = null;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
}
