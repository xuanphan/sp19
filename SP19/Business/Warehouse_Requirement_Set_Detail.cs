﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.EnterpriseServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Warehouse_Requirement_Set_Detail
    {
        public enum StatusStockE
        {
            OOS = 1,
            ALERT = 2,
            ORDER = 3,
            SAFE = 4,
            AWAY = 0
        }

        public class RemarkStock
        {
            public const string LINE = "Ụ Kệ";// "Chỉ đếm số lượng hàng hóa trong line, Ụ, kệ trưng này bia tết";
            public const string STOCK = "Kho & Kệ";//"Số lượng đã bao gồm hàng hóa trong kho";
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string _messageSystemError, string _pAppCode, List<Warehouse_Requirement_Set_Detail> _pList, ref AddNewResult result)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    string _id = String.Empty;
                    foreach (var i in _pList)
                    {
                    var query = from item in context.Warehouse_Requirement_Set_Details
                                where item.WarehouseRequirementSetID == this.WarehouseRequirementSetID
                                && item.BrandSetID == this.BrandSetID
                                select item;
                    if (query.Count() > 0) //nếu như nó update stock
                    {
                        var simple = query.FirstOrDefault();
                        simple.Number = i.Number;
                        simple.LastUpdatedBy =i.CreatedBy;
                        simple.LastUpdatedDateTime = DateTime.Now;
                        simple.RowVersion += 1;
                        context.SubmitChanges();
                        _id += simple.ID.ToString() + "|";
                    }
                    else //tạo mới
                    {
                        Warehouse_Requirement_Set_Detail warehouse_Requirement_Set_Detail = new Warehouse_Requirement_Set_Detail();
                        warehouse_Requirement_Set_Detail.WarehouseRequirementSetID = i.WarehouseRequirementSetID;
                        warehouse_Requirement_Set_Detail.BrandSetID = i.BrandSetID;
                        warehouse_Requirement_Set_Detail.Number = i.Number;
                        warehouse_Requirement_Set_Detail.CreatedBy = i.CreatedBy;
                        warehouse_Requirement_Set_Detail.CreatedDateTime = DateTime.Now;
                        warehouse_Requirement_Set_Detail.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                        warehouse_Requirement_Set_Detail.RowVersion = 1;
                        context.Warehouse_Requirement_Set_Details.InsertOnSubmit(warehouse_Requirement_Set_Detail);
                        context.SubmitChanges();
                        _id += warehouse_Requirement_Set_Detail.ID.ToString() + "|";
                    }
                    }
                    result.Data = _id.Remove(_id.Length - 1);
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    result.Data = "-1";
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                result.Data = "-1";
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }


        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetList(ref string _messageError, string _WarehouseRequirementSetID, ref List<Warehouse_Requirement_Set_Detail> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Warehouse_Requirement_Set_Details
                            where item.WarehouseRequirementSetID == long.Parse(_WarehouseRequirementSetID)
                            select item;
                if (query.Count() > 0)
                {
                    list = query.ToList();
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    list = null;
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                }
            }
            catch (Exception ex)
            {
                list = null;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
            }
        }
    }     

}
