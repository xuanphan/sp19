﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Chanel
    {
        public static List<Chanel> getByProject(int pProjectID)
        {
            List<Chanel> listRP = new List<Chanel>();
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Chanels
                            where item.ProjectID == pProjectID
                            select item;
                listRP = query.ToList();

                return listRP;

            }
            catch (Exception ex)
            {
                return listRP;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
}
