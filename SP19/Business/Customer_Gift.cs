﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Customer_Gift
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew( string _pAppCode, List<Customer_Gift> list, ref MainResult result)
        {
            DatabaseDataContext context = null;
            string customerID = "0";
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // khởi tạo transaction
                    context.Connection.Open();
                    context.Transaction = context.Connection.BeginTransaction();

                    //context.Customer_Gifts.InsertAllOnSubmit(list);// thêm list vào db
                    //context.SubmitChanges(); // thực thi câu lệnh
                    foreach (var item in list)
                    {
                        customerID = item.CustomerID.ToString();
                        context.Customer_Gifts.InsertOnSubmit(item);// thêm list vào db
                        context.SubmitChanges(); 
                    }

                    context.Transaction.Commit(); // hoàn thành transaction
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();// trả lại ban đầu nếu trong quá trình xử lý có lỗi
                recordLogCatch(customerID);
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR)+ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static void recordLogCatch(string customerID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                Log_Catch log = new Log_Catch();
                log.CustomerID = customerID.ToString();
                context.Log_Catches.InsertOnSubmit(log);
                context.SubmitChanges();
                
            }
            catch (Exception ex)
            {

            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static List<Customer_Gift> xuatDup()
        {
            List<Customer_Gift> listRP = new List<Customer_Gift>();
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

                var queryStock = from item in context.Customer_Gifts
                                 select item;
                var listStock = queryStock.ToList();


                var read = listStock.Distinct(new ComparerCustomerGiftDUP()).ToList();
                foreach (var entity in read)
                {
                    if (listStock.Where(o => o.CustomerID == entity.CustomerID && o.GiftID == entity.GiftID).Count() > 1)
                    {

                        listRP.Add(entity);
                        context.Customer_Gifts.DeleteOnSubmit(entity);
                        context.SubmitChanges();
                    }
                }


                return listRP;
            }
            catch (Exception ex)
            {
                string a = ex.Message;
                return new List<Customer_Gift>();
            }
            finally
            {
                context = null;
                GC.Collect();
            }

        }
    }

    class ComparerCustomerGiftDUP : IEqualityComparer<Customer_Gift>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(Customer_Gift x, Customer_Gift y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.CustomerID == y.CustomerID && x.GiftID == y.GiftID;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(Customer_Gift item)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(item, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashtteam = item.CustomerID == null ? 0 : item.CustomerID.GetHashCode();
            int hashproduct = item.GiftID == null ? 0 : item.GiftID.GetHashCode();
            //Calculate the hash code for the product.
            return hashtteam ^ hashproduct;
        }
    }
}
