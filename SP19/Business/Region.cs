﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace Business
{
    public partial class Region
    {
        //all
        public IEnumerable<ComboItem> GetAllForCombo(bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.Type,
                                Text = item.Type
                            };
                List<ComboItem> items = query.ToList().Distinct(new ComparerRegionData()).OrderBy(p => p.Text).ToList();
                if (items.Count() != 1)
                {
                    if (allOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                    if (defaultOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                }
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForCombo(int _pProjectID, int _pOutletTypeID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            join outlet in context.Outlets on item.ID equals outlet.RegionID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && (outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.Type,
                                Text = item.Type
                            };
                List<ComboItem> items = query.ToList().Distinct(new ComparerRegionData()).OrderBy(p => p.Text).ToList();
                if (items.Count() != 1)
                {
                    if (allOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                    if (defaultOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                }
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }

        public IEnumerable<ComboItem> GetDistrictForCombo(int _pProjectID, int _pOutletTypeID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            join outlet in context.Outlets on item.ID equals outlet.RegionID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && (outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.ID.ToString(),
                                Text = item.RegionName
                            };
                List<ComboItem> items = query.ToList().Distinct(new ComparerRegionData()).OrderBy(p => p.Text).ToList();
                if (items.Count() != 1)
                {
                    if (allOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                    if (defaultOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                }
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForComboNameSale(int _pProjectID, int _pSaleID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            join outlet in context.Sale_Outlets on item.ID equals outlet.Outlet.RegionID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && (outlet.SaleID == _pSaleID)
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.RegionName,
                                Text = item.RegionName
                            };
                List<ComboItem> items = query.ToList().Distinct(new ComparerRegionData()).OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForComboNameSaleSup(int _pProjectID, int _pSaleID, string _pListSaleID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                List<string> result = _pListSaleID.Split('|').ToList();
                var query = from item in context.Regions
                            join outlet in context.Sale_Outlets on item.ID equals outlet.Outlet.RegionID
                            join salesup in context.Sale_Sups on outlet.SaleID equals salesup.SaleID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && (outlet.SaleID == _pSaleID)
                            && result.Contains(outlet.SaleID.ToString())
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.RegionName,
                                Text = item.RegionName
                            };
                List<ComboItem> items = query.ToList().Distinct(new ComparerRegionData()).OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForComboName(int _pProjectID, int _pOutletTypeID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            join outlet in context.Outlets on item.ID equals outlet.RegionID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && (outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.RegionName,
                                Text = item.RegionName
                            };
                List<ComboItem> items = query.ToList().Distinct(new ComparerRegionData()).OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public class ComparerRegionData : IEqualityComparer<ComboItem>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(ComboItem x, ComboItem y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.Value == y.Value;
                // return x.AssignID == y.AssignID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(ComboItem item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashproductid = item.Value == null ? 0 : item.Value.GetHashCode();
                //Calculate the hash code for the product.
                return hashproductid;
                // return hashAssignID;
            }
        }
        public IEnumerable<ComboItem> GetAllForComboSup(int _pProjectID, int _pSupID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join region in context.Regions on item.RegionID equals region.ID
                            join supoutlet in context.Sup_Outlets on item.ID equals supoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && supoutlet.SupID == _pSupID
                            select new Common.ComboItem
                            {
                                ID = region.ID,
                                Value = region.RegionName,
                                Text = region.RegionName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForComboSale(int _pProjectID, int _pSaleID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join region in context.Regions on item.RegionID equals region.ID
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && saleoutlet.SaleID == _pSaleID
                            select new Common.ComboItem
                            {
                                ID = region.ID,
                                Value = region.Type,
                                Text = region.Type
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).Distinct(new ComparerRegionData()).OrderBy(p => p.Text).ToList();
                if (items.Count() != 1)
                {
                    if (allOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                    if (defaultOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                }
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllForComboSaleSup(int _pProjectID, int _pSaleSupID, string _pListSaleID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                List<string> result = _pListSaleID.Split('|').ToList();
                var query = from item in context.Outlets
                            join region in context.Regions on item.RegionID equals region.ID
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            join salesup in context.Sale_Sups on saleoutlet.SaleID equals salesup.SaleID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && salesup.SaleSupID == _pSaleSupID
                            && result.Contains(saleoutlet.SaleID.ToString())
                            select new Common.ComboItem
                            {
                                ID = region.ID,
                                Value = region.Type,
                                Text = region.Type
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).Distinct(new ComparerRegionData()).OrderBy(p => p.Text).ToList();
                if (items.Count() != 1)
                {
                    if (allOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                    if (defaultOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                }
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        //public static List<RegionForCombo> GetRegionByParentCodeHasAll(string parentcode)
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        var query = from dt in context.Regions
        //                    where dt.ParentCode.Trim().Equals(parentcode.Trim())
        //                    orderby dt.Ordinal ascending
        //                    select new RegionForCombo { RegionCode = dt.RegionCode, RegionName = dt.RegionName, ParentCode = dt.ParentCode }
        //                    ;
        //        var l = query.ToList();
        //        l.Insert(0, new RegionForCombo { RegionCode = "", RegionName = "Tất cả", ParentCode = parentcode });
        //        return l;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}
        //public static List<ExtRegion> GetRegion(string districtCode, string wardCode)
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        List<ExtRegion> l = new List<ExtRegion>(); 
        //        if (wardCode.Length > 0)
        //        {
        //            var query = from dt in context.Regions
        //                        where dt.RegionCode.Trim().Equals(wardCode.Trim())
        //                        select new ExtRegion(dt, context);
        //            l = query.ToList();
        //        }
        //        else
        //        {
        //            var query = from dt in context.Regions
        //                        where dt.ParentCode.Trim().Equals(districtCode.Trim())
        //                        select new ExtRegion(dt, context);
        //            l = query.ToList();
        //        }
        //        return l;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}
        //public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW Add(ref Region newOne, int userID)
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        Region searchOne = newOne;
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        var query = from dt in context.Regions
        //                    where dt.RegionLevel == searchOne.RegionLevel && dt.RegionName == searchOne.RegionName && dt.ParentCode == searchOne.ParentCode
        //                    orderby dt.RegionCode
        //                    select dt
        //                    ;
        //        if (query.Count() > 0) return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.DUPPLICATE;

        //        string q = context.Regions.OrderByDescending(p => p.RegionCode).Select(p => p.RegionCode).FirstOrDefault();
        //        int ordinal = context.Regions.Where(p => p.ParentCode == searchOne.ParentCode).OrderByDescending(p => p.RegionCode).Select(p => p.Ordinal).FirstOrDefault();
        //        long newCode = long.Parse(q);
        //        newCode++;
        //        string newCodeS = newCode.ToString("00000");
        //        newOne.RegionCode = newCodeS;
        //        newOne.CreatedBy = userID;
        //        newOne.CreatedDateTime = DateTime.Now;
        //        newOne.Ordinal = ordinal + 1;
        //        newOne.RowVersion = 1;
        //        newOne.Status = 1;
        //        context.Regions.InsertOnSubmit(newOne);
        //        context.SubmitChanges();
        //        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
        //    }
        //    catch
        //    {
        //        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}
        //public static Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE Edit(Region editOne, int userID)
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        Region edit = context.Regions.Single(p => p.RegionCode == editOne.RegionCode);
        //        edit.RegionName = editOne.RegionName;
        //        edit.LastUpdatedBy = userID;
        //        edit.LastUpdatedDateTime = DateTime.Now;
        //        edit.RowVersion += 1;
        //        context.SubmitChanges();
        //        return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
        //    }
        //    catch
        //    {
        //        return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}

        ///// <summary> Nghia: Chuyển từ code sang kiểu dài (cộng các Region lại) </summary>
        //public static string Convert_to_fullRegion(string _RegionCode)
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        if (!string.IsNullOrEmpty(_RegionCode))
        //        {
        //            var query = from tb in context.Regions
        //                        where tb.RegionCode == _RegionCode
        //                        select tb;
        //            if (query.Count() > 0)
        //            {
        //                Region item = query.First();
        //                ExtRegion extRegion = new ExtRegion(item, context);
        //                string str = extRegion.ToLongRegion(", ");
        //                return str;
        //            }
        //            else return "";
        //        }
        //        else return "";
        //    }
        //    catch
        //    {
        //        return "";
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}

        ///// <summary> nghia: lấy thông qua level </summary>
        //public static List<RegionForCombo> GetBy_RegionLevel(int RegionLevel)
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        var query = from tb in context.Regions
        //                    where tb.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
        //                    && tb.RegionLevel == RegionLevel
        //                    orderby tb.Ordinal ascending
        //                    select new RegionForCombo { RegionCode = tb.RegionCode, RegionName = tb.RegionName, ParentCode = tb.ParentCode }
        //                    ;
        //        var l = query.ToList();
        //        return l;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}

        ///// <summary> nghia: lấy thông qua parentcode, code trống => null </summary>
        //public static List<RegionForCombo> GetByParentCode(string parentcode, bool hasAll = false)
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

        //        List<RegionForCombo> list = new List<RegionForCombo>();
        //        if (!string.IsNullOrEmpty(parentcode))
        //        {
        //            var query = from dt in context.Regions
        //                        where dt.ParentCode.Trim().Equals(parentcode.Trim())
        //                        orderby dt.Ordinal ascending
        //                        select new RegionForCombo { RegionCode = dt.RegionCode, RegionName = dt.RegionName, ParentCode = dt.ParentCode }
        //                        ;
        //            list = query.ToList();
        //        }

        //        if (hasAll)
        //        {
        //            list.Insert(0, new RegionForCombo { RegionCode = "", RegionName = "Tất cả" });
        //        }
        //        return list;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}

        ///// <summary> Nghia: chứa đựng, so sánh theo RegionCode </summary>
        //public static bool Contains(DatabaseDataContext context, string parentCode, string childCode)
        //{
        //    if (context == null)
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //    var query = from tb in context.Regions
        //                     where tb.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
        //                     && tb.RegionCode == childCode
        //                     select tb;
        //    while(query.Count() > 0)
        //    {
        //        Region Region = query.First();
        //        if (Region.ParentCode == parentCode)
        //            return true;
        //        query = from tb in context.Regions
        //                where tb.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
        //                && tb.RegionCode == Region.ParentCode
        //                select tb;
        //    }
        //    return false;
        //}

        ///// <summary> Nghia: chứa đựng, so sánh theo RegionCode </summary>
        //public static string GetName_byCode(string RegionCode)
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        var query = from tb in context.Regions
        //                    where tb.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
        //                    && tb.RegionCode == RegionCode
        //                    orderby tb.Ordinal ascending
        //                    select tb.RegionName;
        //        return query.First();
        //    }
        //    catch
        //    {
        //        return "";
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}
        ///// <summary>linh/// </summary>

        //public IEnumerable<ComboItem> GetForCombo(bool defaultOption = false, bool allOption = false)
        //{
        //    try
        //    {
        //        DatabaseDataContext context = null;
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        var Region = context.Regions
        //    .Where(p => p.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE && p.RegionLevel == 4)
        //    .Select(p => new ComboItem { ID = Convert.ToInt32(p.RegionCode.Trim()), Text = p.RegionName.TrimStart().Trim() });

        //        List<ComboItem> items = Region.ToList();
        //        if (!allOption)
        //            items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
        //        if (defaultOption)
        //            items.Insert(0, new ComboItem { ID = 1, Text = String.Empty });
        //        return items;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}
        //public IEnumerable<ComboItem> GetForComboLevel3(bool defaultOption = false, bool allOption = false)
        //{
        //    try
        //    {
        //        DatabaseDataContext context = null;
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        var Region = context.Regions
        //    .Where(p => p.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE && p.RegionLevel == 3)
        //    .Select(p => new ComboItem { ID = Convert.ToInt32(p.RegionCode.Trim()), Text = p.RegionName.TrimStart().Trim() });

        //        List<ComboItem> items = Region.ToList();
        //        if (!allOption)
        //            items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
        //        if (defaultOption)
        //            items.Insert(0, new ComboItem { ID = 1, Text = String.Empty });
        //        return items;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}
        //public static List<RegionSE> Get(RegionSE re)
        //{
        //    try
        //    {
        //        DatabaseDataContext context = null;
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        var query = from dt in context.Regions
        //                    where dt.RegionLevel == 4
        //                    select new RegionSE { Regionname=dt.RegionName.TrimStart().Trim()};
        //      return query.ToList();


        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}

        public static List<Region> GetAll(int _pProjectID, int _pOutletTypeID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            join outlet in context.Outlets on item.ID equals outlet.RegionID
                            where (item.ProjectID == _pProjectID)
                            && item.Status != 2
                            && (outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)
                            && outlet.Status != 2
                            && outlet.Promotion == true
                            select item;

                return query.ToList().OrderBy(p => p.RegionName).Distinct().ToList();
            }
            catch
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static List<a> GetAll2(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            where (item.ProjectID == _pProjectID || _pProjectID == 0)
                            select new a { t = item.RegionName.ToString() };

                return query.ToList();
            }
            catch
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static string RemoveUnicode(string text)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",  
            "đ",  
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",  
            "í","ì","ỉ","ĩ","ị",  
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",  
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",  
            "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",  
            "d",  
            "e","e","e","e","e","e","e","e","e","e","e",  
            "i","i","i","i","i",  
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",  
            "u","u","u","u","u","u","u","u","u","u","u",  
            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]);
                text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return text;
        }

        public static string GetSimple(int _pRegionID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            where item.ID == _pRegionID
                            select item.RegionName;
                string name = RemoveUnicode(query.FirstOrDefault()).ToUpper().Replace(" ", "");
                return name;
            }
            catch
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public IEnumerable<ComboItem> GetRegionName(string _pRegionType, int _pProjectID, int _OutletTypeID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from region in context.Regions
                            join outlet in context.Outlets on region.ID equals outlet.RegionID
                            where (region.Type == _pRegionType.Trim() || _pRegionType.Trim() == "Tất Cả")
                            && (outlet.ProjectID == _pProjectID)
                            && (outlet.OutletTypeID == _OutletTypeID || _OutletTypeID == 0 || _OutletTypeID == -1)
                            select new ComboItem
                            {
                                ID = region.ID,
                                Value = region.RegionName,
                                Text = region.RegionName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList().Distinct(new ComparerRegionData()).ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetRegionNameSaleSup(string _pRegionType, int _pProjectID, int _OutletTypeID, string _pListSaleID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                List<string> result = _pListSaleID.Split('|').ToList();
                var query = from region in context.Regions
                            join outlet in context.Outlets on region.ID equals outlet.RegionID
                            join saleoutlet in context.Sale_Outlets on outlet.ID equals saleoutlet.OutletID
                            join salesup in context.Sale_Sups on saleoutlet.SaleID equals salesup.SaleID
                            where (region.Type == _pRegionType.Trim() || _pRegionType.Trim() == "Tất Cả")
                            && (outlet.ProjectID == _pProjectID)
                            && (outlet.OutletTypeID == _OutletTypeID || _OutletTypeID == 0 || _OutletTypeID == -1)
                            && result.Contains(saleoutlet.SaleID.ToString())
                            select new ComboItem
                            {
                                ID = region.ID,
                                Value = region.RegionName,
                                Text = region.RegionName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList().Distinct(new ComparerRegionData()).ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }

        public static List<RegionOutlet> CountOutletInRegion(int _pProjectID, int _pOutletTypeID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            join outlet in context.Outlets on item.ID equals outlet.RegionID
                            where (item.ProjectID == _pProjectID)
                             && item.Status != 2
                            && (outlet.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)
                            && outlet.Status != 2
                            && outlet.Promotion == true
                            select item;
                var listOutlet = context.Outlets.Where(p => p.Status != 2 && p.Promotion == true && (p.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0) && ((p.ProjectID == _pProjectID))).ToList();
                var list = query.ToList().Distinct().ToList();
                List<RegionOutlet> listRegion = new List<RegionOutlet>();
                foreach (var i in list)
                {
                    RegionOutlet simple = new RegionOutlet();
                    simple.RegionID = i.ID;
                    simple.NunberOutlet = 0;
                    var dk1 = listOutlet.Where(p => p.RegionID == i.ID && (p.TimeEnd.GetValueOrDefault().Date <= DateTime.Now.Date && p.TimeBegin.GetValueOrDefault().Date < DateTime.Now.Date));
                    if (dk1.Count() > 0)
                    {
                        int soluongoutlet = 0;
                        int numberngay = 0;
                        soluongoutlet += dk1.Count();
                        foreach (var x in dk1)
                        {
                            string a = (x.TimeEnd.GetValueOrDefault().Date - x.TimeBegin.GetValueOrDefault().Date).TotalDays.ToString();
                            numberngay += int.Parse(a);
                        }
                        simple.NunberOutlet = numberngay;
                    }
                    var dk2 = listOutlet.Where(p => p.RegionID == i.ID && (p.TimeEnd.GetValueOrDefault().Date >= DateTime.Now.Date && DateTime.Now.Date > p.TimeBegin.GetValueOrDefault().Date));
                    if (dk2.Count() > 0)
                    {
                        int soluongoutlet = 0;
                        int numberngay = 0;
                        soluongoutlet += dk2.Count();
                        foreach (var x in dk2)
                        {
                            string a = (DateTime.Now.Date - x.TimeBegin.GetValueOrDefault().Date).TotalDays.ToString();
                            numberngay += int.Parse(a);
                        }
                        simple.NunberOutlet = numberngay;
                    }
                    listRegion.Add(simple);
                }


                return listRegion;
            }
            catch
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static List<Region> getByProject(int pProjectID)
        {
            List<Region> listRP = new List<Region>();
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            where item.ProjectID == pProjectID
                            select item;
                listRP = query.ToList();

                return listRP;

            }
            catch (Exception ex)
            {
                return listRP;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class a { public string t { get; set; } }

    public class RegionOutlet
    {
        public int RegionID { get; set; }
        public int NunberOutlet { get; set; }
    }
    class CompareComboItemByText : IEqualityComparer<ComboItem>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(ComboItem x, ComboItem y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.Text == y.Text;
            // return x.AssignID == y.AssignID;
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(ComboItem item)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(item, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashName = item.Text == null ? 0 : item.Text.GetHashCode();
            //Calculate the hash code for the product.
            return hashName;
            // return hashAssignID;
        }
    }
}
