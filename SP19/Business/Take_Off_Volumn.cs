﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Take_Off_Volumn
    {
        //public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string _messageSystemError, string _pAppCode, List<Take_Off_Volumn> _pList, ref MainResult result)
        //{
        //    DatabaseDataContext context = null;

        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
        //        {

        //            //string _id = String.Empty;
        //            foreach (var i in _pList)
        //            {
        //                var query = from comment in context.Take_Off_Volumns
        //                            where comment.DeviceDateTime.Date == i.DeviceDateTime.Date
        //                            && comment.OutletID == i.OutletID
        //                            && comment.ProductID == i.ProductID
        //                            select comment;
        //                if (query.Count() > 0)
        //                {
        //                    var simple = query.FirstOrDefault();
        //                    simple.Number = i.Number;
        //                    simple.LastUpdatedBy = i.CreatedBy;
        //                    simple.LastUpdatedDateTime = DateTime.Now;
        //                    simple.RowVersion += 1;
        //                    context.SubmitChanges();
        //                }
        //                else
        //                {
        //                    context.Take_Off_Volumns.InsertOnSubmit(i);
        //                    context.SubmitChanges();

        //                }
        //            }
        //            //result.Data = _id.Remove(_id.Length - 1);
        //            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
        //            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
        //            return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
        //        }
        //        else
        //        {
        //            //result.Data = "-1";
        //            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
        //            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
        //            return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
        //        }
        //    }


        //    catch (Exception ex)
        //    {
        //        _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
        //        //result.Data = "-1";
        //        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
        //        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR);
        //        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(string pAppCode, List<Take_Off_Volumn> list, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy ds hàng bán của outlet
                    var queryTakeOff = from item in context.Take_Off_Volumns
                                       where item.OutletID == list.First().OutletID
                                       select item;
                    var listTemp = queryTakeOff.ToList();

                    // chạy vòng lặp list thêm
                    foreach (var entity in list)
                    {
                        // kiểm tra xem dữ liệu có chưa
                        var query = listTemp.Where(p =>
                            p.DeviceDateTime.Date == entity.DeviceDateTime.Date
                                && p.ProductID == entity.ProductID
                                && p.CreatedBy == entity.CreatedBy).ToList();

                        if (query.Count() > 0) // nếu có thì update số lượng
                        {
                            var simple = query.FirstOrDefault();
                            simple.Number = entity.Number;
                            simple.LastUpdatedBy = entity.CreatedBy;
                            simple.LastUpdatedDateTime = DateTime.Now;
                            simple.RowVersion += 1;
                            context.SubmitChanges(); // thực thi câu lệnh
                        }
                        else  // nếu chưa có thì thêm mới
                        {
                            context.Take_Off_Volumns.InsertOnSubmit(entity); // thêm
                            context.SubmitChanges(); // thực thi câu lệnh

                        }
                    }
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageError, int _pProjectID, int _pOutletType, ref List<Take_Off_VolumnEXT> list, Take_Off_VolumnSE se)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                //Khoi tao ngay
                //lay ngay hien tai tru di 7

                var query = from item in context.Take_Off_Volumns
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.Outlet.ProjectID == _pProjectID)
                            && (item.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                            && item.Outlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.Product.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.OutletID == se.OutletTakeOffSE || se.OutletTakeOffSE == null || se.OutletTakeOffSE == 0)
                            && (item.DeviceDateTime.Date >= se.DateFromTakeOffSE.Date && item.DeviceDateTime.Date <= se.DateToTakeOffSE.Date)
                            select item;
                List<Take_Off_VolumnEXT> result = query
                              .GroupBy(l => new { l.DeviceDateTime.Date, l.Product.BrandID })
                              .Select(cl => new Take_Off_VolumnEXT
                              {
                                  Dates = cl.First().DeviceDateTime.Date,
                                  Brand = cl.First().Product.BrandID.ToString(),
                                  TotalNumber = cl.Sum(c => c.Number),
                              }).ToList();
                list = result.ToList().OrderByDescending(p => p.Dates).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                GC.Collect();
                context = null;
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchDate(ref string _messageError, int _pProjectID, int _pOutletType, ref List<DateTime> list, Take_Off_VolumnSE se)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                //Khoi tao ngay
                //lay ngay hien tai tru di 7

                var query = from item in context.Take_Off_Volumns
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.Outlet.ProjectID == _pProjectID)
                            && (item.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                            && item.Outlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.Product.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.OutletID == se.OutletTakeOffSE || se.OutletTakeOffSE == null || se.OutletTakeOffSE == 0)
                            && (item.DeviceDateTime.Date >= se.DateFromTakeOffSE.Date && item.DeviceDateTime.Date <= se.DateToTakeOffSE.Date)
                            select item.DeviceDateTime.Date;
                list = query.ToList().Distinct().ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                GC.Collect();
                context = null;
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageError, int _pProjectID, int _pOutletType, Take_Off_VolumnSE se, ref List<Take_Off_VolumnOutletForSum> list)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                var query = from item in context.Take_Off_Volumns
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.Outlet.ProjectID == _pProjectID)
                            && (item.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0 || _pOutletType == -1)
                            && item.Outlet.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.Product.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.OutletID == se.OutletTakeOffSE || se.OutletTakeOffSE == null || se.OutletTakeOffSE == 0)
                            && (item.Outlet.RegionID == se.DistrictAttendance || se.DistrictAttendance == null || se.DistrictAttendance == 0)
                            && (item.ServerDateTime.Date >= se.DateFromTakeOffSE.Date && item.ServerDateTime.Date <= se.DateToTakeOffSE.Date)
                            select item;
                var l1 = query.ToList();
                List<Take_Off_VolumnOutletForSum> result = l1
                            .GroupBy(l => new { l.ProductID, l.OutletID })
                            .Select(cl => new Take_Off_VolumnOutletForSum
                            {
                                OutletID = cl.FirstOrDefault().Outlet.ID,
                                OutletName = cl.FirstOrDefault().Outlet.OutletName,
                                RegionType = cl.FirstOrDefault().Outlet.Region.Type,
                                GroupID = cl.FirstOrDefault().Outlet.GroupID,
                                ProductID = cl.FirstOrDefault().ProductID,
                                TotalNumber = cl.Sum(c => c.Number),
                            }).ToList();
                list = result.ToList();
                //list = result.ToList().OrderByDescending(p => p.DeviceDateTime.Date).ToList();
                //list = query.ToList().OrderByDescending(p => p.DeviceDateTime.Date).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                GC.Collect();
                context = null;
            }
        }
    }


    public class Take_Off_VolumnSE
    {
        [UIHint("_ListOutlet")]
        [Display(Name = "Siêu thị")]
        public long OutletTakeOffSE { get; set; }
        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionTakeOffSE { get; set; }

        [UIHint("_District")]
        [Display(Name = "Tỉnh")]
        public int DistrictAttendance { get; set; }

        [UIHint("_Sale")]
        [Display(Name = "Sale")]
        public long SaleReportSE { get; set; }

        private DateTime _DateFromTakeOffSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Ngày Bắt Đầu")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateFromTakeOffSE
        {
            get { return _DateFromTakeOffSE; }
            set
            {
                if (value != null)
                    _DateFromTakeOffSE = value.Date;
            }
        }

        private DateTime _DateToTakeOffSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Ngày Kết Thúc")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateToTakeOffSE
        {
            get { return _DateToTakeOffSE; }
            set
            {
                if (value != null)
                    _DateToTakeOffSE = value.Date;
            }
        }
    }
    public class Take_Off_VolumnEXT
    {
        public DateTime Dates { get; set; }
        public string OutletID { get; set; }
        public string Brand { get; set; }
        public int TotalNumber { get; set; }
    }
    public class StockForDate
    {
        public DateTime Dates { get; set; }
        public string OutletID { get; set; }
        public int TotalNumber { get; set; }
    }

    public class Take_Off_VolumnOutletDate
    {
        public DateTime Dates { get; set; }
        public int OutletID { get; set; }
        public string OutletName { get; set; }
        public string RegionType { get; set; }
        public string City { get; set; }
        public int GroupID { get; set; }
    }

    public class Take_Off_VolumnOutletForSum
    {
        public int ProductID { get; set; }
        public int OutletID { get; set; }
        public string OutletName { get; set; }
        public string RegionType { get; set; }
        public string City { get; set; }
        public int GroupID { get; set; }
        public int TotalNumber { get; set; }
    }
}
