﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Team
    {
        public IEnumerable<ComboItem> GetAllForCombo(int _pProjectID, int _pOutletType, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Teams
                            join teamoutlet in context.Team_Outlets on item.ID equals teamoutlet.TeamID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (teamoutlet.Outlet.ProjectID == _pProjectID)
                            && (teamoutlet.Outlet.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.TeamName,
                                Text = item.TeamName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }

        public IEnumerable<ComboItem> GetAllForCombo2(bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Teams
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.TeamName,
                                Text = item.TeamName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }

        public IEnumerable<ComboItem> GetAllForComboProfile(bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Teams
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.TeamName,
                                Text = item.TeamName
                            };
                List<ComboItem> items = query.ToList();
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetTeamForRegion(string _pRegionID, int _pProjectID, int _OutletTypeID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Teams
                            join teamoutlet in context.Team_Outlets on item.ID equals teamoutlet.TeamID
                            where (teamoutlet.Outlet.Region.Type == _pRegionID || _pRegionID == "Tất Cả")
                            && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            && teamoutlet.Outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            && (teamoutlet.Outlet.ProjectID == _pProjectID)
                            && (teamoutlet.Outlet.OutletTypeID == _OutletTypeID || _OutletTypeID == 0)
                            select new ComboItem
                            {
                                ID = item.ID,
                                Value = item.TeamName,
                                Text = item.TeamName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList().Distinct().ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }

        public IEnumerable<ComboItem> GetTeamForDistrict(int _pDistrictID, int _pProjectID, int _OutletTypeID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Teams
                            join teamoutlet in context.Team_Outlets on item.ID equals teamoutlet.TeamID
                            where (teamoutlet.Outlet.Region.ID == _pDistrictID || _pDistrictID == 0)
                            && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            && teamoutlet.Outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            && (teamoutlet.Outlet.ProjectID == _pProjectID)
                            && (teamoutlet.Outlet.OutletTypeID == _OutletTypeID || _OutletTypeID == 0)
                            select new ComboItem
                            {
                                ID = item.ID,
                                Value = item.TeamName,
                                Text = item.TeamName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList().Distinct().ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }

        public IEnumerable<ComboItem> GetDistrictForRegion(string _pRegionID, int _pProjectID, int _OutletTypeID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Regions
                            join outlet in context.Outlets on item.ID equals outlet.RegionID
                            where (outlet.Region.Type == _pRegionID || _pRegionID == "Tất Cả")
                            && item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            && outlet.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
                            && (outlet.ProjectID == _pProjectID)
                            && (outlet.OutletTypeID == _OutletTypeID || _OutletTypeID == 0)
                            select new ComboItem
                            {
                                ID = item.ID,
                                Value = item.ID.ToString(),
                                Text = item.RegionName
                            };
                List<ComboItem> list = new List<ComboItem>();
                list.AddRange(query.ToList().Distinct(new CompareComboItemByText()).ToList());
                if (query.ToList().Count() != 0)
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                }
                else
                {
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                }

                return list;
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }
        public static List<Team_Outlet> GetAllForSale(int _pProjectID, int _pSaleID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            join teamoutlet in context.Team_Outlets on item.ID equals teamoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == -1)
                            && saleoutlet.SaleID == _pSaleID
                            select teamoutlet;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Team_Outlet> GetAllForSaleSup(int _pProjectID, int _pSaleSupID, string _pListSaleID)
        {
            DatabaseDataContext context = null;
            try
            {
                List<string> result = _pListSaleID.Split('|').ToList();
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            join salesup in context.Sale_Sups on saleoutlet.SaleID equals salesup.SaleID
                            join teamoutlet in context.Team_Outlets on item.ID equals teamoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            && salesup.SaleSupID == _pSaleSupID
                            && result.Contains(saleoutlet.SaleID.ToString())
                            select teamoutlet;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Team_Outlet> GetAll(int _pProjectID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join teamoutlet in context.Team_Outlets on item.ID equals teamoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select teamoutlet;
                var list = query.ToList().OrderBy(p => p.Team.TeamName).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Team_Outlet> SearchOutlet(int _pProjectID, int _pOutletType, int _pOutletID, string _pRegionID, string _SaleID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            join teamoutlet in context.Team_Outlets on item.ID equals teamoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && saleoutlet.Status != 2
                            && (item.ProjectID == _pProjectID || _pProjectID == 0)
                            && (item.ID == _pOutletID || _pOutletID == 0)
                            && (item.Region.Type == _pRegionID || _pRegionID == "Tất Cả" || _pRegionID == null || _pRegionID == "0")
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            && (saleoutlet.SaleID == int.Parse(_SaleID) || _SaleID == "0")
                            select teamoutlet;
                var list = query.ToList().OrderBy(p => p.Team.TeamName).ToList(); 
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Team_Outlet> SearchOutletForSale(int _pProjectID, int _pSaleID, int _pOutletID, string _pRegionID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            join teamoutlet in context.Team_Outlets on item.ID equals teamoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && saleoutlet.Status != 2
                             && (item.ProjectID == _pProjectID)
                            && (item.ID == _pOutletID || _pOutletID == 0)
                            && (item.Region.Type == _pRegionID || _pRegionID == null || _pRegionID == "0" || _pRegionID == "Tất Cả")
                            && saleoutlet.SaleID == _pSaleID
                            select teamoutlet;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Team_Outlet> SearchOutletForSaleSup(int _pProjectID, int _pSaleSupID, int _pOutletID, string _pRegionID, string _pListSaleID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                List<string> result = _pListSaleID.Split('|').ToList();
                var query = from item in context.Outlets
                            join saleoutlet in context.Sale_Outlets on item.ID equals saleoutlet.OutletID
                            join salesup in context.Sale_Sups on saleoutlet.SaleID equals salesup.SaleID
                            join teamoutlet in context.Team_Outlets on item.ID equals teamoutlet.OutletID
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && saleoutlet.Status != 2
                            && (item.ProjectID == _pProjectID)
                            && (item.ID == _pOutletID || _pOutletID == 0)
                            && (item.Region.Type == _pRegionID || _pRegionID == null || _pRegionID == "0" || _pRegionID == "Tất Cả")
                            && salesup.SaleSupID == _pSaleSupID
                            && result.Contains(saleoutlet.SaleID.ToString())
                            select teamoutlet;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        //public static User getteambyuser(int p)
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        var query = from item in context.Users
        //                    where item.Status == (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE
        //                    && item.ID == p
        //                    select item;

        //        return query.SingleOrDefault();
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        context = null;
        //    }
        //}
    }

    
}
