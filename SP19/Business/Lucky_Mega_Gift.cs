﻿using Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Lucky_Mega_Gift
    {
        public IEnumerable<ComboItem> GetAllForComboMega(DateTime _date, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var querydate = from item in context.spturnMega(_date.Date).ToList()
                                select item;
                if (querydate.Count() > 0)
                {
                    var querydaphan = from item in context.Lucky_Mega_Set_Gift_To_Outlets
                                      where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                      && ((item.IsHaveWin == false && item.DateTrue.Date >= DateTime.Now.Date) || (item.IsHaveWin == true)) 
                                      select item;
                    if (querydaphan.Count() > 0)
                    {
                        var simple = querydaphan.ToList();
                        var query = from item in context.Lucky_Mega_Gifts
                                    where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                    && item.TurnID == querydate.FirstOrDefault().ID
                                    && item.IsGift == true
                                    select new Common.ComboItem
                                {
                                    ID = item.ID,
                                    Value = item.Name,
                                    Text = item.Name
                                };
                        var l = query.ToList().Where(item => !simple.Any(item2 => item2.GiftID == item.ID));
                        List<ComboItem> list = new List<ComboItem>();
                        list.AddRange(l.Distinct().OrderBy(p => p.ID).ToList());                       
                        if (allOption)
                            list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                        if (defaultOption)
                            list.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                        return list;
                    }
                    else
                    {
                        var simple = querydaphan.ToList();
                        var query = from item in context.Lucky_Mega_Gifts
                                    where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                    && item.TurnID == querydate.FirstOrDefault().ID
                                    && item.IsGift == true
                                    select new Common.ComboItem
                                    {
                                        ID = item.ID,
                                        Value = item.Name,
                                        Text = item.Name
                                    };
                        List<ComboItem> items = query.ToList().OrderBy(p => p.ID).ToList();
                        if (allOption)
                            items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                        if (defaultOption)
                            items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                        return items;
                    }
                }
                else
                {
                    List<ComboItem> items = new List<ComboItem>();
                    if (allOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                    if (defaultOption)
                        items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                    return items;
                }
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetAllMega(DateTime _date, string _pOutletID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (_date == null)
                {
                    _date = DateTime.Now;
                }
                var querydate = from item in context.spturnMega(_date.Date).ToList()
                                select item;
                if (querydate.Count() > 0)
                {
                    //qua da phan
                    var querydaphan = from item in context.Lucky_Mega_Set_Gift_To_Outlets
                                      where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                      && item.OutletID == int.Parse(_pOutletID)
                                      select item;
                    if (querydaphan.Count() > 0)
                    {
                        var simple = querydaphan.ToList();
                        var query = from item in context.Lucky_Mega_Gifts
                                    where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                    && item.TurnID == querydate.ToList().OrderByDescending(p => p.ID).FirstOrDefault().ID
                                    && item.IsGift == true
                                    select new ComboItem
                                    {
                                        ID = item.ID,
                                        Value = item.Name,
                                        Text = item.Name
                                    };
                        var l = query.ToList().Where(item => !simple.Any(item2 => item2.GiftID == item.ID)); 
                        List<ComboItem> list = new List<ComboItem>();
                        list.AddRange(l.Distinct().ToList());
                        if (query.ToList().Count() != 0)
                        {
                            list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                        }
                        else
                        {
                            list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                        }

                        return list;
                    }
                    else
                    {
                        var query = from item in context.Lucky_Mega_Gifts
                                    where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                    && item.TurnID == querydate.ToList().OrderByDescending(p => p.ID).FirstOrDefault().ID
                                    && item.IsGift == true
                                    select new ComboItem
                                    {
                                        ID = item.ID,
                                        Value = item.Name,
                                        Text = item.Name
                                    };
                        List<ComboItem> list = new List<ComboItem>();
                        list.AddRange(query.ToList().Distinct().ToList());
                        if (query.ToList().Count() != 0)
                        {
                            list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                        }
                        else
                        {
                            list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                        }

                        return list;
                    }

                }
                else
                {
                    List<ComboItem> list = new List<ComboItem>();
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                    return list;
                }
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }


        public IEnumerable<ComboItem> GetGift(DateTime _date)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var querydate = from item in context.spturnMega(_date.Date).ToList()
                                select item;
                if (querydate.Count() > 0)
                {
                      var querydaphan = from item in context.Lucky_Mega_Set_Gift_To_Outlets
                                      where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                      && ((item.IsHaveWin == false && item.DateTrue.Date >= DateTime.Now.Date)||(item.IsHaveWin== true)) 
                                      select item;
                      if (querydaphan.Count() > 0)
                      {
                          var simple = querydaphan.ToList();
                          var query = from item in context.Lucky_Mega_Gifts
                                      where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                      && item.TurnID == querydate.ToList().OrderByDescending(p => p.ID).FirstOrDefault().ID
                                      && item.IsGift == true
                                      select new ComboItem
                                      {
                                          ID = item.ID,
                                          Value = item.Name,
                                          Text = item.Name
                                      };
                          var l = query.ToList().Where(item => !simple.Any(item2 => item2.GiftID == item.ID));
                          List<ComboItem> _list = new List<ComboItem>();
                          _list.AddRange(l.Distinct().OrderBy(p => p.ID).ToList());      
                          if (query.ToList().Count() != 0)
                          {
                              _list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                          }
                          else
                          {
                              _list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                          }

                          return _list;
                      }
                      else
                      {
                          var query = from item in context.Lucky_Mega_Gifts
                                      where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                      && item.TurnID == querydate.ToList().OrderByDescending(p => p.ID).FirstOrDefault().ID
                                      && item.IsGift == true
                                      select new ComboItem
                                      {
                                          ID = item.ID,
                                          Value = item.Name,
                                          Text = item.Name
                                      };
                          var list = query.ToList();
                          List<ComboItem> _list = new List<ComboItem>();
                          _list.AddRange(list.Distinct().OrderBy(p => p.ID).ToList());
                          if (query.ToList().Count() != 0)
                          {
                              _list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                          }
                          else
                          {
                              _list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                          }

                          return _list;
                      }
                }
                else
                {
                    List<ComboItem> list = new List<ComboItem>();
                    list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                    return list;
                }
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }
        public IEnumerable<ComboItem> GetTurn(DateTime _date)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var querydate = from item in context.spturnMega(_date.Date).ToList()
                                select new ComboItem
                                {
                                    ID = item.ID,
                                    Value = item.Name,
                                    Text = item.Name
                                };
                var _list = querydate.ToList();
                if (querydate.Count() > 0)
                {
                    List<ComboItem> items = _list.OrderBy(p => p.Text).ToList();
                    return items;
                }
                else
                {
                    List<ComboItem> items = new List<ComboItem>();
                    items.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                    return items;
                }
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }

        public object GetOutletNotGift(string _GiftID, int _pProjectID, int _pOutletType)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                //qua da phan
                var querydaphan = from item in context.Lucky_Mega_Set_Gift_To_Outlets
                                  where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                  && ((item.GiftID == int.Parse(_GiftID) && item.IsHaveWin == true)|| (item.IsHaveWin == false && item.DateTrue.Date>= DateTime.Now.Date))
                                  select item;
                if (querydaphan.Count() > 0)
                {
                    var simple = querydaphan.ToList();
                    var query = from item in context.Outlets
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                  && (item.ProjectID == _pProjectID)
                                 && (item.Chanel.ChanelName == "Mega Market")
                                  && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                                select new Common.ComboItem
                                {
                                    ID = item.ID,
                                    Value = item.OutletName,
                                    Text = item.OutletName
                                };
                    var l = query.ToList().Where(item => !simple.Any(item2 => item2.OutletID == item.ID)); ;
                    List<ComboItem> list = new List<ComboItem>();
                    list.AddRange(l.Distinct().ToList());
                    if (query.ToList().Count() != 0)
                    {
                        list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                    }
                    else
                    {
                        list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                    }

                    return list;
                }
                else
                {
                    var query = from item in context.Outlets
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                  && (item.ProjectID == _pProjectID)
                                  && (item.Chanel.ChanelName == "Mega Market")
                                  && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                                select new Common.ComboItem
                                {
                                    ID = item.ID,
                                    Value = item.OutletName,
                                    Text = item.OutletName
                                };
                    List<ComboItem> list = new List<ComboItem>();
                    list.AddRange(query.ToList().Distinct().ToList());
                    if (query.ToList().Count() != 0)
                    {
                        list.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                    }
                    else
                    {
                        list.Insert(0, new ComboItem { ID = 0, Text = "Không Tìm Thấy" });
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
            }
        }
    }
}
