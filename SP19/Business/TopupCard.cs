﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Net.Http;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Business
{
    public partial class TopupCard
    {
        public static bool sendTopup(string phoneNumber, string phoneType,int outletId, int userId, string userType, string secretKey, ref MainResult result)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if(secretKey != Common.Global.secrectKeyTopup){
                    result.Status = -1;
                    result.Description = "Lỗi xác thực. Vui lòng thử lại.";
                    return false;
                }

                var queryUser = from item in context.User_Apps
                                where item.ID == userId
                                && item.Status != 2
                                && item.UserType == "SP"
                                select item;
                if (queryUser.Count() == 0)
                {
                    result.Status = -1;
                    result.Description = "User không tồn tại. Vui lòng thử lại.";
                    return false;
                }

                var maxAmountPurchase = from item in context.Log_TopupCards
                                        where item.Phone == phoneNumber
                                        && item.State == true
                                        && item.CreatedDateTime.Value.Date == DateTime.Now.Date
                                        select item;
                if (maxAmountPurchase.Count() >= 2)
                {
                    result.Status = -1;
                    result.Description = "Số điện thoại đã nhận đủ số lượng card trong ngày. Vui lòng thử lại.";
                    return false;
                }

                string transactionId = Guid.NewGuid().ToString();
                string checksum = CodeMD5.MD5Hash(phoneNumber + "." + phoneType + "." + transactionId + "." + userId + "." + userType + ".aU1hcmtQYXkvMS4xLWRldkAyMDE5");
                Log_TopupCard log = new Log_TopupCard();
                log.OutletID = outletId;
                log.UserID = userId;
                log.UserType = userType;
                log.Money = 20000;
                log.Phone = phoneNumber;
                log.PhoneType = phoneType;
                log.TransactionID = transactionId;
                log.CreatedDateTime = DateTime.Now;
                string DATA = string.Format("phoneNumber={0}&phoneType={1}&transactionId={2}&userId={3}&userType={4}&checksum={5}", phoneNumber, phoneType, transactionId, userId.ToString(), userType,checksum);

                string URL = "https://pay.imark.com.vn/api/topup.php";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = DATA.Length;
                using (Stream webStream = request.GetRequestStream())
                using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
                {
                    requestWriter.Write(DATA);
                }

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                    using (StreamReader responseReader = new StreamReader(webStream))
                    {
                        string response = responseReader.ReadToEnd();
                        dynamic data = JObject.Parse(response);
                        bool state = data.status;
                        if (state)
                        {
                            log.State = state;
                            log.Message = "Thành công";
                            log.TraceID = data.traceId;
                            context.Log_TopupCards.InsertOnSubmit(log);
                            context.SubmitChanges();
                            result.Status = 1;
                            result.Description = "Thành công.";
                        }
                        else
                        {
                            log.State = state;
                            log.Message = "Không thành công. " + data.errors[0].message;
                            context.Log_TopupCards.InsertOnSubmit(log);
                            context.SubmitChanges();
                            result.Status = -1;
                            result.Description = "Không thành công. " + data.errors[0].message;
                            return false;
                        }
                        
                    }
                }
                catch (Exception e)
                {
                    log.State = false;
                    log.Message = "Lỗi xác thực. Vui lòng thử lại. " + e.Message;
                    context.Log_TopupCards.InsertOnSubmit(log);
                    context.SubmitChanges();
                    result.Status = -1;
                    result.Description = "Lỗi xác thực. Vui lòng thử lại.";
                    return false;
                }
                return true;


            }
            catch (Exception ex)
            {
                result.Status = -1;
                result.Description = "Hệ thống đang bảo trì. Vui lòng thử lại.";
                return false;
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
    }


    public class ParamsTopup
    {
        public string phoneNumber { get; set; }
        public string phoneType { get; set; }
        public string transactionId { get; set; }
        public string userId { get; set; }
        public string userType { get; set; }
        public string checksum { get; set; }


    }
}
