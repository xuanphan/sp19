﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class AttendanceTracking_Image
    {
        /// <summary>WS
        /// </summary>

        

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW addNewSimple(string pAppCode, long pAttendanceTrackingID, long pImageID, ref AddAttendanceTrackingImageResult result)
        {
            DatabaseDataContext context = null;
           
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.Trim().ToLower() != Common.Global.AppCode.Trim().ToLower())
                {
                    // gán giá trị cho phần trả về
                    result.Data = 0;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }

                // kiểm tra xem có dữ liệu chấm công + hình ảnh này chưa => nhắm tránh dup
                var query = from item in context.AttendanceTracking_Images
                            where item.AttendanceTrackingID == pAttendanceTrackingID
                            && item.ImageID == pImageID
                            select item;
                // nếu có thì thôi trả về ID có rồi
                if (query.Count() > 0)
                {
                    // gán giá trị cho phần trả về
                    result.Data = query.FirstOrDefault().ID;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else  // chưa có thì thêm mới
                {
                    AttendanceTracking_Image temp = new AttendanceTracking_Image();
                    temp.AttendanceTrackingID = pAttendanceTrackingID;
                    temp.ImageID = pImageID;
                    context.AttendanceTracking_Images.InsertOnSubmit(temp); // thêm
                    context.SubmitChanges(); // thực thi câu lệnh
                    // gán giá trị cho phần trả về
                    result.Data = temp.ID;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = 0;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW addNew(ref string _messagererror, AttendanceTracking_Image temp)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.AttendanceTracking_Images
                            where item.AttendanceTrackingID == temp.AttendanceTrackingID
                            && item.ImageID == temp.ImageID
                            select item;
                if (query.Count() > 0)
                {
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {                   
                    context.AttendanceTracking_Images.InsertOnSubmit(temp);
                    context.SubmitChanges();
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
}

