﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class ReportProduct
    {
        //public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ReportProduct report, string _userTeamID, string _AppCode, ref ReportProductResult result)
        //{
        //    DatabaseDataContext context = null;
        //    context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //    try
        //    {
        //        if (_AppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
        //        {
        //            var queryimage = from images in context.ReportProducts
        //                             where images.SessionCode == report.SessionCode
        //                             && images.UserTeamID == long.Parse(_userTeamID)
        //                             select images;
        //            if (queryimage.Count() > 0)
        //            {
        //                var simple = queryimage.FirstOrDefault();
        //                simple.Number = report.Number;
        //                simple.UserTeamID = long.Parse(_userTeamID);
        //                simple.LastUpdatedDatetime = DateTime.Now;
        //                simple.LastUpdatedBy = int.Parse(_userTeamID);
        //                simple.RowVersion += 1;
        //                context.SubmitChanges();
        //                result.ReportProductID = simple.ID;
        //                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
        //                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
        //                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
        //            }
        //            report.UserTeamID = long.Parse(_userTeamID);
        //            report.CreatedBy = int.Parse(_userTeamID);
        //            report.CreatedDatetime = DateTime.Now;
        //            report.RowVersion = 1;
        //            report.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
        //            context.ReportProducts.InsertOnSubmit(report);
        //            context.SubmitChanges();
        //            result.ReportProductID = report.ID;
        //            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
        //            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
        //            return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
        //        }
        //        else
        //        {
        //            result.ReportProductID = 0;
        //            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
        //            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
        //            return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        string MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
        //        result.ReportProductID = 0;
        //        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
        //        result.Description = ex.Message;
        //        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}

        //public static List<View_SoNgayProduct> GetDate()
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        var query2 = from item in context.View_SoNgayProducts
        //                     select item;
        //        return query2.ToList().OrderByDescending(p=>p.Expr1.GetValueOrDefault().Date).Take(7).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}

        //public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(ref string MessageSystemError, ref List<ReportSanLuongEXT> list)
        //{
        //    DatabaseDataContext context = null;
        //    try
        //    {
        //        context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
        //        var query = from item in context.View_ReportSanLuongs
        //                    select new ReportSanLuongEXT
        //                    {
        //                        Date = item.Expr1.GetValueOrDefault(),
        //                        OutletID = int.Parse(item.ID.ToString()),
        //                        OutletName = item.Name,
        //                        ProductID = item.ProductID.GetValueOrDefault(),
        //                        Number = item.number.GetValueOrDefault(),
        //                    };
        //        var listquery = query.ToList().Distinct(new ComparerSanLuong()).ToList();
        //        list = listquery.ToList();
        //        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
        //        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
        //    }
        //    finally
        //    {
        //        context = null;
        //        GC.Collect();
        //    }
        //}


        //Ham Group

        //Ham Group

        class ComparerSanLuong : IEqualityComparer<ReportSanLuongEXT>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(ReportSanLuongEXT x, ReportSanLuongEXT y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.OutletID == y.OutletID && x.Date.Date == y.Date.Date && x.ProductID == y.ProductID;
                // return x.AssignID == y.AssignID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(ReportSanLuongEXT item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashoutletid = item.OutletID == null ? 0 : item.OutletID.GetHashCode();
                int hashdate = item.Date.Date == null ? 0 : item.Date.Date.GetHashCode();
                int hashproduct = item.ProductID == null ? 0 : item.ProductID.GetHashCode();
                //Calculate the hash code for the product.
                return hashoutletid ^ hashdate ^ hashproduct;
                // return hashAssignID;
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT ReportExcelGiftUse(ref string _messageError, ref List<ReportGiftOutEXT> list, int _pOutletTypeID, int _pProjectID, ReportSanLuongSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var querygift = from gift in context.Gifts
                                join brandset in context.Brand_Set_Details on gift.ID equals brandset.GiftID
                                where gift.ProjectID == _pProjectID
                                && brandset.Brand_Set.OutletTypeID == _pOutletTypeID
                                && gift.LastUpdatedBy != null
                                select gift;
                var _listgift = querygift.ToList().Distinct().OrderBy(p => p.LastUpdatedBy).ToList();
                //for gift
                foreach (var z in _listgift)
                {
                    var name = context.Brand_Set_Details.Where(p => p.GiftID == z.ID && p.Brand_Set.OutletTypeID == _pOutletTypeID).Select(p => p.Brand_Set);
                    var query = from k in context.ThongKeQuaOutlet(se.DateFromReport, se.DateToReport, z.ID, _pProjectID, _pOutletTypeID).ToList()
                                select new ReportGiftOutEXT
                                {
                                    BrandName = name.FirstOrDefault().Brand.BrandName,
                                    OutletName = k.OutletName,
                                    Name = z.GiftName,
                                    Number = k.tong.GetValueOrDefault()
                                };
                    var a = query;
                    list = list.Union(a).ToList();
                }
                //for tiếp brand set
                var querybrand = from brand in context.Brand_Sets
                                 where brand.Brand.ProjectID == _pProjectID
                                 && brand.OutletTypeID == _pOutletTypeID
                                 select brand;
                var _brand = querybrand.ToList();
                foreach (var b in _brand)
                {
                    string nameset = "    Set Chính";
                    if (b.IsDefault == false)
                    {
                        nameset = "    Set Phụ";
                        var query = from k in context.ThongKeSetOutletSumUse(se.DateFromReport, se.DateToReport, b.ID, _pProjectID, _pOutletTypeID, b.Brand.BrandName).ToList()
                                    select new ReportGiftOutEXT
                                    {
                                        BrandName = b.Brand.BrandName,
                                        OutletName = k.OutletName,
                                        Name = nameset,
                                        Number = k.tongsetphu.GetValueOrDefault()
                                    };
                        var a = query.ToList();
                        list = list.Union(a).ToList();
                    }
                    else
                    {
                        var query = from k in context.ThongKeSetOutletSumUse(se.DateFromReport, se.DateToReport, b.ID, _pProjectID, _pOutletTypeID, b.Brand.BrandName).ToList()
                                    select new ReportGiftOutEXT
                                    {
                                        BrandName = b.Brand.BrandName,
                                        OutletName = k.OutletName,
                                        Name = nameset,
                                        Number = k.tongsetchinh.GetValueOrDefault()
                                    };
                        var a = query.ToList();
                        list = list.Union(a).ToList();
                    }

                }

                //for tiếp brand set strongbow
                var querybrandfalse = from brand in context.Product_Gifts
                                      where brand.Product.Brand.ProjectID == _pProjectID
                                      select brand;
                var _brandfalse = querybrandfalse.ToList();

                var querybrandfalseset = from brand in context.Product_Gifts
                                         where brand.Product.Brand.ProjectID == _pProjectID
                                         && brand.Product.Brand.BrandCode != "BR07"
                                         select brand.Product.Brand;
                var _brandfalseset = querybrandfalseset.ToList().Distinct().ToList();

                foreach (var bf in _brandfalseset)
                {
                    var query = from k in context.ThongKeSetOutletSumUse(se.DateFromReport, se.DateToReport, bf.ID, _pProjectID, _pOutletTypeID, bf.BrandName).ToList()
                                select new ReportGiftOutEXT
                                {
                                    BrandName = bf.BrandName,
                                    OutletName = k.OutletName,
                                    Name = "   Set Chính",
                                    Number = k.tong.GetValueOrDefault()
                                };
                    var a = query.ToList();
                    list = list.Union(a).ToList();
                }

                foreach (var bf in _brandfalse)
                {
                    var query = from k in context.ThongKeQuaOutlet(se.DateFromReport, se.DateToReport, bf.GiftID, _pProjectID, _pOutletTypeID).ToList()
                                select new ReportGiftOutEXT
                                {
                                    BrandName = bf.Product.Brand.BrandName,
                                    OutletName = k.OutletName,
                                    Name = bf.Gift.GiftName,
                                    Number = k.tong.GetValueOrDefault()
                                };
                    var a = query.ToList();
                    list = list.Union(a).ToList();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT ReportExcelGift(ref string _messageError, ref List<ReportGiftOutEXT> list, int _pOutletTypeID, int _pProjectID, ReportSanLuongSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var querygift = from gift in context.Gifts
                                join brandset in context.Brand_Set_Details on gift.ID equals brandset.GiftID
                                where gift.ProjectID == _pProjectID
                                && brandset.Brand_Set.OutletTypeID == _pOutletTypeID
                                && gift.LastUpdatedBy != null
                                select gift;
                var _listgift = querygift.ToList().Distinct().OrderBy(p => p.LastUpdatedBy).ToList();
                //for gift
                foreach (var z in _listgift)
                {
                    var name = context.Brand_Set_Details.Where(p => p.GiftID == z.ID && p.Brand_Set.OutletTypeID == _pOutletTypeID).Select(p => p.Brand_Set);
                    var query = from k in context.ThongKeQuaOutlet(se.DateFromReport, se.DateToReport, z.ID, _pProjectID, _pOutletTypeID).ToList()
                                select new ReportGiftOutEXT
                                {
                                    BrandName = name.FirstOrDefault().Brand.BrandName,
                                    OutletName = k.OutletName,
                                    Name = z.GiftName,
                                    Number = k.tong.GetValueOrDefault()
                                };
                    var a = query;
                    list = list.Union(a).ToList();
                }
                //for tiếp brand set
                var querybrand = from brand in context.Brand_Sets
                                 where brand.Brand.ProjectID == _pProjectID
                                 && brand.OutletTypeID == _pOutletTypeID
                                 select brand;
                var _brand = querybrand.ToList();
                foreach (var b in _brand)
                {
                    string nameset = "    Set Chính";
                    if (b.IsDefault == false)
                    {
                        nameset = "    Set Phụ";
                        var query = from k in context.ThongKeSetOutletSum(se.DateFromReport, se.DateToReport, b.ID, _pProjectID, _pOutletTypeID, b.Brand.BrandName).ToList()
                                    select new ReportGiftOutEXT
                                    {
                                        BrandName = b.Brand.BrandName,
                                        OutletName = k.OutletName,
                                        Name = nameset,
                                        Number = k.tongsetphu.GetValueOrDefault()
                                    };
                        var a = query.ToList();
                        list = list.Union(a).ToList();
                    }
                    else
                    {
                        var query = from k in context.ThongKeSetOutletSum(se.DateFromReport, se.DateToReport, b.ID, _pProjectID, _pOutletTypeID, b.Brand.BrandName).ToList()
                                    select new ReportGiftOutEXT
                                    {
                                        BrandName = b.Brand.BrandName,
                                        OutletName = k.OutletName,
                                        Name = nameset,
                                        Number = k.tongsetchinh.GetValueOrDefault()
                                    };
                        var a = query.ToList();
                        list = list.Union(a).ToList();
                    }

                }

                //for tiếp brand set strongbow
                var querybrandfalse = from brand in context.Product_Gifts
                                      where brand.Product.Brand.ProjectID == _pProjectID
                                      select brand;
                var _brandfalse = querybrandfalse.ToList();

                var querybrandfalseset = from brand in context.Product_Gifts
                                         where brand.Product.Brand.ProjectID == _pProjectID
                                            && brand.Product.Brand.BrandCode != "BR05"
                                         select brand.Product.Brand;
                var _brandfalseset = querybrandfalseset.ToList().Distinct().ToList();

                foreach (var bf in _brandfalseset)
                {
                    var query = from k in context.ThongKeSetOutletSum(se.DateFromReport, se.DateToReport, bf.ID, _pProjectID, _pOutletTypeID, bf.BrandName).ToList()
                                select new ReportGiftOutEXT
                                {
                                    BrandName = bf.BrandName,
                                    OutletName = k.OutletName,
                                    Name = "   Set Chính",
                                    Number = k.tong.GetValueOrDefault()
                                };
                    var a = query.ToList();
                    list = list.Union(a).ToList();
                }

                foreach (var bf in _brandfalse)
                {
                    var query = from k in context.ThongKeQuaOutlet(se.DateFromReport, se.DateToReport, bf.GiftID, _pProjectID, _pOutletTypeID).ToList()
                                select new ReportGiftOutEXT
                                {
                                    BrandName = bf.Product.Brand.BrandName,
                                    OutletName = k.OutletName,
                                    Name = bf.Gift.GiftName,
                                    Number = k.tong.GetValueOrDefault()
                                };
                    var a = query.ToList();
                    list = list.Union(a).ToList();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT ReportSoVongQuay(ref string _messageError, ref List<ReportSoLanQuay> list, int _pOutletTypeID, int _pProjectID, ReportSanLuongSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var querytotal = from item in context.ReportVongquay(_pOutletTypeID, _pProjectID, se.DateFromReport.Date, se.DateToReport.Date)
                                 select item;
                var _list = querytotal.ToList();
                var query = from item in context.ReportVongquay(_pOutletTypeID, _pProjectID, se.DateFromReport.Date, se.DateToReport.Date)
                            select new ReportSoLanQuay
                            {
                                OutletID = int.Parse(item.OutletID.ToString()),
                                OutletName = item.OutletName,
                                Total = _list.Where(p => p.OutletID == item.OutletID).Sum(p => p.tổng_lần_quay_quà).GetValueOrDefault()
                            };
                list = query.ToList().Distinct(new ComparerVongQuay()).ToList();
                foreach (var i in list)
                {
                    i.list = new List<DetailBrand>();
                    var simple = _list.Where(p => p.OutletID == i.OutletID);
                    if (simple.Count() > 0)
                    {
                        foreach (var e in simple)
                        {
                            DetailBrand detail = new DetailBrand();
                            detail.BranID = e.ID;
                            detail.Number = e.tổng_lần_quay_quà.GetValueOrDefault();
                            i.list.Add(detail);
                        }
                    }
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        class ComparerVongQuay : IEqualityComparer<ReportSoLanQuay>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(ReportSoLanQuay x, ReportSoLanQuay y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.OutletID == y.OutletID;
                // return x.AssignID == y.AssignID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(ReportSoLanQuay item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashoutletid = item.OutletID == null ? 0 : item.OutletID.GetHashCode();
                //Calculate the hash code for the product.
                return hashoutletid;
                // return hashAssignID;
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT ReportExcelSale(ref string _messageError, ref List<ReportGiftOutEXT> list, int _pOutletTypeID, int _pProjectID, ReportSanLuongSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var querydate = from item in context.Customers
                                where item.Outlet.OutletTypeID == _pOutletTypeID
                                && item.Outlet.ProjectID == _pProjectID
                                && (item.DeviceDateTime.Date <= se.DateFromReport.Date && item.DeviceDateTime.Date >= se.DateToReport.Date)
                                select item.DeviceDateTime.Date;
                var _listdate = querydate.ToList().Distinct().ToList();
                List<ReportGiftOutEXT> _list = new List<ReportGiftOutEXT>();
                foreach (var i in _listdate)
                {
                    var queryproduct = from product in context.Products
                                       where product.Brand.ProjectID == _pProjectID
                                       && product.IsTakeOffVolume == true
                                       && product.RowVersion == 1
                                       select product;
                    var _listproduct = queryproduct.ToList().Distinct().OrderBy(p => p.Ordinal).ToList();
                    //for gift
                    foreach (var z in _listproduct)
                    {
                        if (z.Brand.BrandName == "Larue")
                        {
                            z.ProductName = z.Brand.BrandName;
                        }
                        if (z.ProductCode == "SP006")
                        {
                            z.ProductName = " SB 10s";
                        }
                        if (z.ProductCode == "SP007" || z.ProductCode == "SP008" || z.ProductCode == "SP009" || z.ProductCode == "SP010")
                        {
                            z.ProductName = " SB 24s";
                        }
                        var query = from k in context.ThongKeSaleVolumeOutlet(i.Date.Date, z.LastUpdatedBy, _pProjectID, _pOutletTypeID).ToList()
                                    select new ReportGiftOutEXT
                                    {
                                        BrandName = z.Brand.BrandName,
                                        OutletName = k.OutletName,
                                        Date = i.Date.Date,
                                        Name = z.ProductName,
                                        Number = k.tong.GetValueOrDefault()
                                    };
                        var a = query.ToList();
                        _list = _list.Union(a).ToList();
                    }
                }
                list = _list;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT ReportExcelSaleSum(ref string _messageError, ref List<ReportGiftOutEXT> list, int _pOutletTypeID, int _pProjectID, ReportSanLuongSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);                
                List<ReportGiftOutEXT> _list = new List<ReportGiftOutEXT>();
                    var queryproduct = from product in context.Products
                                       where product.Brand.ProjectID == _pProjectID
                                       && product.IsTakeOffVolume == true
                                       && product.RowVersion == 1
                                       select product;
                    var _listproduct = queryproduct.ToList().Distinct().OrderBy(p => p.Ordinal).ToList();
                    //for gift
                    foreach (var z in _listproduct)
                    {
                        if (z.Brand.BrandName == "Larue")
                        {
                            z.ProductName = z.Brand.BrandName;
                        }
                        if (z.ProductCode == "SP006")
                        {
                            z.ProductName = " SB 10s";
                        }
                        if (z.ProductCode == "SP007" || z.ProductCode == "SP008" || z.ProductCode == "SP009" || z.ProductCode == "SP010")
                        {
                            z.ProductName = " SB 24s";
                        }
                        var query = from k in context.ThongKeSaleVolumeOutletSum(se.DateFromReport.Date,se.DateToReport.Date, z.LastUpdatedBy, _pProjectID, _pOutletTypeID).ToList()
                                    select new ReportGiftOutEXT
                                    {
                                        BrandName = z.Brand.BrandName,
                                        OutletName = k.OutletName,
                                        Name = z.ProductName,
                                        Number = k.tong.GetValueOrDefault()
                                    };
                        var a = query.ToList();
                        _list = _list.Union(a).ToList();
                    }
                list = _list;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class ReportSanLuongEXT
    {
        public string OutletName { get; set; }
        public int OutletID { get; set; }
        public DateTime Date { get; set; }
        public long ProductID { get; set; }
        public int Number { get; set; }
    }

    public class ReportGiftOutEXT
    {
        public string OutletName { get; set; }
        public int OutletID { get; set; }
        public DateTime Date { get; set; }
        public string BrandName { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
    }

    public class ReportSoLanQuay
    {
        public string OutletName { get; set; }
        public int OutletID { get; set; }
        public List<DetailBrand> list { get; set; }
        public int Total { get; set; }
    }

    public class DetailBrand
    {
        public int BranID { get; set; }
        public int Number { get; set; }
    }
    public class ReportSanLuongSE
    {
        private DateTime _DateFromReport = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Từ Ngày")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateFromReport
        {
            get { return _DateFromReport; }
            set
            {
                if (value != null)
                    _DateFromReport = value.Date;
            }
        }

        private DateTime _DateToReport = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Đến Ngày")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateToReport
        {
            get { return _DateToReport; }
            set
            {
                if (value != null)
                    _DateToReport = value.Date;
            }
        }
    }
}
