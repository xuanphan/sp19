﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class POSM_Report
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(string _pAppCode, List<POSM_Report> list, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    var query = from item in context.POSM_Reports
                                where item.OutletID == list.First().OutletID
                                select item;
                    var listExist = query.ToList();
                    foreach (var entity in list)
                    {
                        // kiểm tra xem có tồn tại không
                        var read = listExist.Where(p => p.POSMID == entity.POSMID && p.DeviceDateTime.Date == entity.DeviceDateTime.Date).FirstOrDefault();
                        if (read != null) // nếu có update số mới
                        {
                            var simple = query.FirstOrDefault();
                            simple.Number = entity.Number;
                            simple.ServerDateTime = DateTime.Now;
                            simple.LastUpdatedBy = entity.CreatedBy;
                            simple.LastUpdatedDateTime = DateTime.Now;
                            simple.RowVersion += 1;
                            context.SubmitChanges(); // thực thi câu lệnh
                        }
                        else  // chưa có thì tạo mới
                        {

                            context.POSM_Reports.InsertOnSubmit(entity); // thêm
                            context.SubmitChanges(); // thực thi câu lệnh
                        }
                    }
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }


        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageError, int _pProjectID, int _pOutletType, ref List<POSMEXT> list, POSMSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.POSM_Reports
                            where item.DeviceDateTime.Date == se.DatePOSM.Date
                            && (item.Outlet.Region.Type == se.RegionPOSM || se.RegionPOSM == "Tất Cả" || se.RegionPOSM == null)
                            && (item.OutletID == se.OutletPOSM || se.OutletPOSM == 0 || se.OutletPOSM == null)
                            && (item.Outlet.ProjectID == _pProjectID)
                            && (item.POSM.OutletTypeID == _pOutletType || _pOutletType == 0)
                            && (item.Outlet.RegionID == se.DistrictAttendance || se.DistrictAttendance == null || se.DistrictAttendance == 0)
                            select new POSMEXT
                            {
                                OutletID = item.OutletID,
                                OutletName = item.Outlet.OutletName,
                                datetime = item.DeviceDateTime,
                                list = context.POSM_Reports.Where(p => p.OutletID == item.OutletID && p.DeviceDateTime.Date == item.DeviceDateTime.Date).ToList()
                            };

                list = query.ToList().Distinct(new ComparerPOSM()).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        //Ham Group
        class ComparerPOSM : IEqualityComparer<POSMEXT>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(POSMEXT x, POSMEXT y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.OutletID == y.OutletID;
                // return x.AssignID == y.AssignID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(POSMEXT item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashproductid = item.OutletID == null ? 0 : item.OutletID.GetHashCode();
                //Calculate the hash code for the product.
                return hashproductid;
                // return hashAssignID;
            }
        }
    }
    public class POSMSE
    {

        private DateTime _DatePOSM = DateTime.Now;
        [Required(ErrorMessage = "{0} Not Empty.")]
        [Display(Name = "Ngày")]
        [DataType(DataType.Date)]
        public DateTime DatePOSM
        {
            get { return _DatePOSM; }
            set
            {
                if (value != null)
                    _DatePOSM = value.Date;
            }
        }

        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionPOSM { get; set; }

        [UIHint("_listOutlet")]
        [Display(Name = "Siêu Thị")]
        public long OutletPOSM { get; set; }

        [UIHint("_District")]
        [Display(Name = "Tỉnh")]
        public int DistrictAttendance { get; set; }


    }
    public class POSMEXT
    {
        public long OutletID { get; set; }
        public DateTime datetime { get; set; }
        public string OutletName { get; set; }
        public List<POSM_Report> list { get; set; }
    }

    public class POSMDetails
    {
        public long POSMID { get; set; }
        public int Number { get; set; }
    }

    public class POSMEXTREPORT
    {
        public string Link { get; set; }
        public int Number { get; set; }
    }
}
