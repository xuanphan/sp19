﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Brewery
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(string _pAppCode, int pProjectID, ref BreweryResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    var query = from item in context.Breweries
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.ProjectID == pProjectID
                                select new SimpleBrewery
                                {
                                    ID = item.ID,
                                    BreweryName = item.BreweryName,
                                    BreweryDescription = item.BreweryDescription
                                };
                    // gán giá trị cho phần trả về
                    result.Data = query.ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public IEnumerable<ComboItem> GetAllForCombo(int _pProjectID, bool defaultOption = false, bool allOption = true)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (_pProjectID == 0)
                {
                    _pProjectID = 1;
                }
                var query = from item in context.Breweries
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            select new Common.ComboItem
                            {
                                ID = item.ID,
                                Value = item.BreweryName,
                                Text = item.BreweryName
                            };
                List<ComboItem> items = query.ToList().OrderBy(p => p.Text).ToList();
                if (allOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = "Tất Cả" });
                if (defaultOption)
                    items.Insert(0, new ComboItem { ID = 0, Text = String.Empty });
                return items;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
        public object GetAll(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Breweries
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && (item.ProjectID == _pProjectID)
                            select item;
                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
            finally
            {
                context = null;
            }
        }
    }
    public class SimpleBrewery
    {
        private int? _ID;
        public int? ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _BreweryNamee;
        public string BreweryName
        {
            get { return _BreweryNamee; }
            set { _BreweryNamee = value; }
        }
        private string _BreweryDescription;
        public string BreweryDescription
        {
            get { return _BreweryDescription; }
            set { _BreweryDescription = value; }
        }

        public SimpleBrewery() { }

        public SimpleBrewery(int ID, string BreweryName, string BreweryDescription)
        {
            this.BreweryName = BreweryName;
            this.ID = ID;
            this.BreweryDescription = BreweryDescription;
        }
    }
}
