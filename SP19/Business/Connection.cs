﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public partial class Connection
    {
        public static string CreateImagePath(string rootPath, string rootUrl, string directoryName, string filepath, out string imageUrl)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string imagePath = String.Empty;
                imageUrl = String.Empty;
                imagePath = rootPath.ToStringWithSuffix("/", directoryName, filepath);
                imageUrl = "https://" + rootUrl.ToStringWithSuffix("/", "WS", directoryName, filepath);

                return imageUrl;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
        }
        public static string CreateImagePathProfile(string rootPath, string rootUrl, string directory, string directoryName, string filepath, out string imageUrl)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string imagePath = String.Empty;
                imageUrl = String.Empty;
                imagePath = rootPath.ToStringWithSuffix("/", directoryName, filepath);
                imageUrl = "https://" + rootUrl.ToStringWithSuffix("/", "WS", directory, directoryName, filepath);

                return imageUrl;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
        }
        public static string CreateImagePathWebApp(string rootPath, string rootUrl, string directoryName, string filepath, out string imageUrl)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string imagePath = String.Empty;
                imageUrl = String.Empty;
                imagePath = rootPath.ToStringWithSuffix("/", directoryName, filepath);
                imageUrl = "https://" + rootUrl.ToStringWithSuffix("/", "AppWeb", directoryName, filepath);

                return imageUrl;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
        }

        public static string CreateImagePathWeb(string rootPath, string rootUrl, string directoryName, string filepath, out string imageUrl)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string imagePath = String.Empty;
                imageUrl = String.Empty;
                imagePath = rootPath.ToStringWithSuffix("/", directoryName, filepath);
                imageUrl = "http://" + rootUrl.ToStringWithSuffix("/", "Report", directoryName, filepath);

                return imageUrl;
            }
            catch (Exception ex)
            {
                throw ExceptionUtils.ThrowCustomException(ex);
            }
        }
    }
}

