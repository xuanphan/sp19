﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class DataKeyDriver
    {
        public static string addMonthOfYear(DataKeyDriver entity)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                context.DataKeyDrivers.InsertOnSubmit(entity);
                context.SubmitChanges();
                return "Ok";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static List<DataKeyDriver> getListMonthOfYear(int year)
        {
            DatabaseDataContext context = null;
            List<DataKeyDriver> listRP = new List<DataKeyDriver>();
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.DataKeyDrivers
                            where item.CapDo == 0
                            && item.NgayLam.Date.Year == year
                            select item;
                listRP = query.ToList();
                return listRP;
            }
            catch (Exception ex)
            {
                return listRP;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static string addDaysOfMonth(List<DataKeyDriver> listDetail)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                context.DataKeyDrivers.InsertAllOnSubmit(listDetail);
                context.SubmitChanges();
                return "Ok";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static DataKeyDriver getByDayOfMonth(DateTime dateTime)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.DataKeyDrivers
                            where item.CapDo == 1
                            && item.NgayLam.Date == dateTime.Date
                            select item;
                if (query.Count() > 0)
                {
                    return query.FirstOrDefault();
                }
                else
                {
                    return null;
                }
               
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
}
