﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class POSM
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(string _pAppCode, string _pOutletID, ref POSMResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu
                    //var query = from item in context.POSMs
                    //            join outletposm in context.Outlet_POSMs on item.ID equals outletposm.POSMID
                    //            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                    //            && outletposm.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                    //            && outletposm.OutletID == int.Parse(_pOutletID)
                    //            select new SimplePOSM
                    //            {
                    //                ID = item.ID,
                    //                BrandID = item.BrandID,
                    //                OutletTypeID = item.OutletTypeID,
                    //                POSMName = item.POSMName,
                    //                POSMDescription = item.POSMDescription,
                    //                POSMAvatar = item.POSMAvatar
                    //            };
                    var queryOutlet = from item in context.Outlets
                                      where item.ID == int.Parse(_pOutletID)
                                      select item;
                    var entityOutlet = queryOutlet.FirstOrDefault();
                    var queryOutletBrand = from item in context.Outlet_Brands
                                           where item.OutletID == int.Parse(_pOutletID)
                                           && item.Status != 2
                                           select item.Brand;
                    var listTemp = queryOutletBrand.ToList();
                    List<SimplePOSM> listRP = new List<SimplePOSM>();
                    foreach (var item in listTemp)
                    {
                        var listPOSM = item.POSMs.Where(p => p.Status != 2 && p.OutletTypeID == entityOutlet.OutletTypeID).Select(o => new SimplePOSM
                        {
                            ID = o.ID,
                            BrandID = o.BrandID,
                            OutletTypeID = o.OutletTypeID,
                            POSMName = o.POSMName,
                            POSMDescription = o.POSMDescription,
                            POSMAvatar = o.POSMAvatar
                        }).ToList();

                        listRP.AddRange(listPOSM);
                    }
                    // gán giá trị cho phần trả về
                    result.Data = listRP.ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }



        public static List<POSM> GetForProject(int _pProjectID, int _pOutletTypeID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.POSMs
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.Brand.ProjectID == _pProjectID
                            && (item.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)
                            select item;

                var list = query.ToList().Distinct(new ComparerGroupPOSM()).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<POSM> GetForProjectdelta(int _pProjectID, int _pOutletTypeID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.POSMs
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.Brand.ProjectID == _pProjectID
                            && (item.OutletTypeID == _pOutletTypeID || _pOutletTypeID == 0)
                            select item;

                var list = query.ToList().Distinct(new ComparerGroupPOSM()).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        class ComparerGroupPOSM : IEqualityComparer<POSM>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(POSM x, POSM y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.POSMType == y.POSMType && x.BrandID == y.BrandID;
                // return x.AssignID == y.AssignID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(POSM item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashproductid = item.POSMType == null ? 0 : item.POSMType.GetHashCode();
                int hashbrand = item.BrandID == null ? 0 : item.BrandID.GetHashCode();
                //Calculate the hash code for the product.
                return hashproductid ^ hashbrand;
                // return hashAssignID;
            }
        }

        public static List<POSM> getByProject(int pProjectID, int pOutletTypeID)
        {
            List<POSM> listRP = new List<POSM>();
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.POSMs
                            where item.Brand.ProjectID == pProjectID
                            && item.OutletTypeID == pOutletTypeID
                            select item;
                listRP = query.ToList();

                return listRP;

            }
            catch (Exception ex)
            {
                return listRP;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class SimplePOSM
    {
        private int? _ID;
        public int? ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        private int? _BrandID;
        public int? BrandID
        {
            get { return _BrandID; }
            set { _BrandID = value; }
        }
        private int? _OutletTypeID;
        public int? OutletTypeID
        {
            get { return _OutletTypeID; }
            set { _OutletTypeID = value; }
        }

        private string _POSMName;
        public string POSMName
        {
            get { return _POSMName; }
            set { _POSMName = value; }
        }
        private string _POSMDescription;
        public string POSMDescription
        {
            get { return _POSMDescription; }
            set { _POSMDescription = value; }
        }
        private string _POSMAvatar;
        public string POSMAvatar
        {
            get { return _POSMAvatar; }
            set { _POSMAvatar = value; }
        }

        public SimplePOSM() { }

        public SimplePOSM(int ID, int BrandID, int OutletTypeID, string POSMName, string POSMDescription, string POSMAvatar)
        {
            this.ID = ID;
            this.BrandID = BrandID;
            this.OutletTypeID = OutletTypeID;
            this.POSMName = POSMName;
            this.POSMDescription = POSMDescription;
            this.POSMAvatar = POSMAvatar;
        }
    }

}