﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.EnterpriseServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Warehouse_Requirement_Set
    {
        public enum StatusStockE
        {
            OOS = 1,
            ALERT = 2,
            ORDER = 3,
            SAFE = 4,
            AWAY = 0
        }

        public class Warehouse_Requirement_Set_Status
        {
            public const string NEW = "YÊU CẦU MỚI";// "Chỉ đếm số lượng hàng hóa trong line, Ụ, kệ trưng này bia tết";
            public const string CONFRIM = "HOÀN THÀNH";//"Số lượng đã bao gồm hàng hóa trong kho";
            public const string KHOCONFRIM = "ĐANG VẬN CHUYỂN";//"Số lượng đã bao gồm hàng hóa trong kho";
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(string _pAppCode, IList<WarehouseRequirementSetData> listEntity, ref WarehouseRequirementSetResult result)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // kiểm tra có dữ liệu tải lên không
                    if (listEntity.Count() > 0)
                    {
                        // kiểm tra mã yêu cầu có tồn tại không
                        var query = from item in context.Warehouse_Requirement_Sets
                                    where item.RequirementCode == listEntity.First().pRequirementCode
                                    && item.OutletID == listEntity.First().pOutletID
                                    select item;

                        var queryKho = from item in context.Kho_Outlets
                                       where item.OutletID == listEntity.First().pOutletID
                                       select item;


                        if (query.Count() > 0) //nếu như nó trả về đã tồn tại yêu cầu 
                        {
                            // gán giá trị cho phần trả về
                            result.Data = 0;
                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA);
                            return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                        }
                        else //tạo mới
                        {
                            // khởi tạo object master
                            Warehouse_Requirement_Set temp = new Warehouse_Requirement_Set();
                            // gán dữ liệu
                            temp.OutletID = listEntity.First().pOutletID;
                            temp.RequirementCode = listEntity.First().pRequirementCode;
                            temp.DeviceTokenSup = listEntity.First().pDeviceToken;
                            temp.RequirementDateTime = Common.ConvertUtils.ConvertStringToLongDate(listEntity.First().pRequirementDateTime);
                            temp.CreatedBy = listEntity.First().pUserID;
                            temp.CreatedDateTime = DateTime.Now;
                            temp.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            temp.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            context.Warehouse_Requirement_Sets.InsertOnSubmit(temp); // thêm 
                            context.SubmitChanges(); // thực thi yêu cầu

                            // khởi tạo list object detail
                            List<Warehouse_Requirement_Set_Detail> listRP = new List<Warehouse_Requirement_Set_Detail>();
                            foreach (var item in listEntity)
                            {
                                // khởi tạo object
                                Warehouse_Requirement_Set_Detail tempDetail = new Warehouse_Requirement_Set_Detail();
                                // gán dữ liệu
                                tempDetail.WarehouseRequirementSetID = temp.ID;
                                tempDetail.BrandSetID = item.pBrandSetID;
                                tempDetail.Number = item.pNumber;
                                tempDetail.CreatedBy = item.pUserID;
                                tempDetail.CreatedDateTime = DateTime.Now;
                                tempDetail.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                                tempDetail.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                                // add vào list
                                listRP.Add(tempDetail);
                            }


                            context.Warehouse_Requirement_Set_Details.InsertAllOnSubmit(listRP); // thêm list
                            context.SubmitChanges(); // thực thi câu lệnh

                            // bắn 1 thông báo = FCM xuống kho cung cấp set qua cho siêu thị
                            var listTemp = queryKho.ToList();
                            string message = "Có đơn yêu cầu giao set quà mã " + temp.RequirementCode + " từ siêu thị " + temp.Outlet.OutletName + ".";
                            foreach (var item in listTemp)
                            {
                                // send sms
                                FCM.SendSMS(item.User_App.DeviceToken, message);
                                // tạo data thông báo thêm vào db
                                User_App_Notification tempNotifi = new User_App_Notification();
                                // gán dữ liệu
                                tempNotifi.Title = "Yêu cầu giao thêm quà";
                                tempNotifi.Message = message;
                                tempNotifi.UserID = item.KhoID;
                                tempNotifi.DeviceToken = item.User_App.DeviceToken;
                                tempNotifi.CreatedDateTime = DateTime.Now;
                                tempNotifi.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                                // xử lý thêm
                                Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = User_App_Notification.add(tempNotifi);
                            }

                            // gán giá trị cho phần trả về
                            result.Data = temp.ID;
                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                            return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                        }
                    }
                    else // trả về lỗi không có dữ liệu tải lên
                    {
                        // gán giá trị cho phần trả về
                        result.Data = 0;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA;
                        result.Description = "Không có dữ liệu tải lên.";
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA;
                    }

                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = 0;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = 0;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW supConfirm( string pAppCode, long WarehouseRequirementSetID, int userID, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {

                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu yêu cầu
                    var temp = context.Warehouse_Requirement_Sets.Where(p => p.ID == WarehouseRequirementSetID).FirstOrDefault();
                    if (temp == null) // nếu không có báo lỗi không có dữ liệu
                    {
                        // gán giá trị cho phần trả về
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA;
                    }
                    if (temp.LastUpdatedBy == null) // nếu có nhưng kho chưa xác nhận chuyển hàng thì báo lỗi 
                    {
                        // gán giá trị cho phần trả về
                        result.Status = 6;
                        result.Description = "Kho chưa xác nhận chuyển hàng.";
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_DATA;
                    }

                    // ok thì cập nhật dữ liệu
                    int userIDKho = temp.LastUpdatedBy.Value;
                    temp.LastUpdatedBy = userID;
                    temp.LastUpdatedDateTime = DateTime.Now;
                    temp.Status = (int)Common.STATUS_CODE.ROW_STATUS.COMPLETE;
                    temp.RowVersion++;
                    context.SubmitChanges();// thực thi câu lệnh

                    // cập nhật xong thì send 1 firebase về device KHO chuyển hàng báo đã nhận hàng thành công
                    // send sms
                    string message = "Đơn yêu cầu giao set quà mã " + temp.RequirementCode + " đã được siêu thị xác nhận nhận hàng.";
                    FCM.SendSMS(temp.DeviceTokenKho, message);
                    // tạo object thông báo lưu vào db
                    User_App_Notification tempNotifi = new User_App_Notification();
                    // gán dữ liệu
                    tempNotifi.Title = "Yêu cầu giao thêm quà";
                    tempNotifi.Message = message;
                    tempNotifi.UserID = userIDKho;
                    tempNotifi.DeviceToken = temp.DeviceTokenKho;
                    tempNotifi.CreatedDateTime = DateTime.Now;
                    tempNotifi.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    // xử lý thêm thông báo
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = User_App_Notification.add(tempNotifi);

                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW khoConfirm(string pAppCode, long WarehouseRequirementSetID, int userID, string pDeviceToken, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu và cập nhật status đang giao set quà cho siêu thị
                    var temp = context.Warehouse_Requirement_Sets.Where(p => p.ID == WarehouseRequirementSetID).FirstOrDefault();
                    temp.DeviceTokenKho = pDeviceToken;
                    temp.LastUpdatedBy = userID;
                    temp.LastUpdatedDateTime = DateTime.Now;
                    temp.Status = 2;
                    temp.RowVersion++;
                    context.SubmitChanges(); // thực thi câu lệnh

                    // send sms
                    string message = "Đơn yêu cầu giao set quà mã " + temp.RequirementCode + " đã được kho xác nhận giao hàng.";
                    FCM.SendSMS(temp.DeviceTokenSup, message);
                    // tạo object thông báo lưu vào db
                    User_App_Notification tempNotifi = new User_App_Notification();
                    // gán dữ liệu
                    tempNotifi.Title = "Yêu cầu giao thêm quà";
                    tempNotifi.Message = message;
                    tempNotifi.UserID = temp.CreatedBy;
                    tempNotifi.DeviceToken = temp.DeviceTokenSup;
                    tempNotifi.CreatedDateTime = DateTime.Now;
                    tempNotifi.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    // xử lý
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = User_App_Notification.add(tempNotifi);

                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW khoConfirmWeb(ref string messageSystemError, string pAppCode, long WarehouseRequirementSetID, int userID, string pDeviceToken, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {

                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    var temp = context.Warehouse_Requirement_Sets.Where(p => p.ID == WarehouseRequirementSetID).FirstOrDefault();
                    temp.DeviceTokenKho = pDeviceToken;
                    temp.LastUpdatedBy = userID;
                    temp.LastUpdatedDateTime = DateTime.Now;
                    temp.Status = 2;
                    temp.RowVersion++;
                    context.SubmitChanges();

                    // send sms
                    string message = "Đơn yêu cầu giao set quà mã " + temp.RequirementCode + " đã được kho xác nhận giao hàng.";
                    //FCM.SendSMS(temp.DeviceTokenSup, message);
                    User_App_Notification tempNotifi = new User_App_Notification();
                    tempNotifi.Title = "Yêu cầu giao thêm quà";
                    tempNotifi.Message = message;
                    tempNotifi.UserID = temp.CreatedBy;
                    tempNotifi.DeviceToken = temp.DeviceTokenSup;
                    tempNotifi.CreatedDateTime = DateTime.Now;
                    tempNotifi.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = User_App_Notification.add(tempNotifi);


                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + messageSystemError;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT getALL(string pAppCode, int pOutletID, ref GetWarehouseRequirementResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu
                    var query = from item in context.Warehouse_Requirement_Sets
                                where item.OutletID == pOutletID
                                && (item.Status != 3)
                                select item;
                    // lấy dữ liệu sang format theo class SimpleWarehouseRequirementSet
                    var listTemp = query.ToList().Select(o => new SimpleWarehouseRequirementSet { OutletID = o.OutletID, WarehouseRequirementSetID = o.ID, RequirementCode = o.RequirementCode, RequirementDateTime = o.RequirementDateTime.ToString("dd/MM/yyyy HH:mm:ss"), Status = o.Status, ListDetail = new List<SimpleWarehouseRequirementSetDetail>() }).ToList();
                    // chạy vòng lặp để lấy dữ liệu cho ListDetail
                    foreach (var item in listTemp)
                    {
                        var listTempDetail = context.Warehouse_Requirement_Set_Details.Where(p => p.WarehouseRequirementSetID == item.WarehouseRequirementSetID).Select(o => new SimpleWarehouseRequirementSetDetail { BrandSetID = o.BrandSetID, Number = o.Number }).ToList();
                        item.ListDetail = listTempDetail;
                    }
                    // gán giá trị cho phần trả về
                    result.Data = listTemp;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;


                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT getALLBySup(string pAppCode, int pSupID, ref GetWarehouseRequirementResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu chưa được kho chưa xác nhận giao hoặc đang trong trạng thái chuyển hàng
                    var query = from item in context.Warehouse_Requirement_Sets
                                where item.CreatedBy == pSupID
                                && (item.Status != 3)
                                select item;
                    if (query.Count() > 0) // nếu có
                    {
                        // tạo list theo format class SimpleWarehouseRequirementSet với dữ liệu lấy được
                        var listTemp = query.ToList().Select(o => new SimpleWarehouseRequirementSet { OutletID = o.OutletID, WarehouseRequirementSetID = o.ID, RequirementCode = o.RequirementCode, RequirementDateTime = o.RequirementDateTime.ToString("dd/MM/yyyy HH:mm:ss"), Status = o.Status, ListDetail = new List<SimpleWarehouseRequirementSetDetail>() }).ToList();
                        // chạy vòng lặp để lấy dữ liệu chi tiết trong yêu cầu
                        foreach (var item in listTemp)
                        {
                            var listTempDetail = context.Warehouse_Requirement_Set_Details
                                .Where(p => p.WarehouseRequirementSetID == item.WarehouseRequirementSetID)
                                .Select(o => new SimpleWarehouseRequirementSetDetail
                                {
                                    BrandSetID = o.BrandSetID,
                                    Number = o.Number
                                }).ToList();
                            item.ListDetail = listTempDetail;
                        }
                        // gán giá trị cho phần trả về
                        result.Data = listTemp;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    }
                    else // không có  thì trả về không có dữ liệu
                    {
                        // gán giá trị cho phần trả về
                        result.Data = null;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                        result.Description = "Không có dữ liệu.";
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_DATA;
                    }
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }



        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageError, int _pProjectID, int _pOutletType, ref List<View_Warehouse_Request> list, RequirementSetSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_Warehouse_Requests
                            where (item.Type == se.RegionRequirementSetSE || se.RegionRequirementSetSE == "Tất Cả" || se.RegionRequirementSetSE == null)
                            && (item.OutletID == se.OutletRequirementSetSE || se.OutletRequirementSetSE == 0 || se.OutletRequirementSetSE == null)
                            && (item.RequirementDateTime.Date == se.DateRequirementSetSE.Date)
                            && (item.ProjectID == _pProjectID)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == 0)
                            select item;

                list = query.ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchSup(ref string _messageError, int _pProjectID, int _pOutletType, int _pOutletID, ref List<View_Warehouse_Request> list, RequirementSetSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_Warehouse_Requests
                            where item.OutletID == _pOutletID
                            select item;

                list = query.ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNewSup(ref string _messageError, string _brandsetID, string _Numberset, string _outletID, string _Code, int _pProjectID, int _pOutletType, int _UserID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                string[] arayid = _brandsetID.Remove(0, 1).Split('|');
                string[] araynumber = _Numberset.Remove(0, 1).Split('|');


                var query = from item in context.Warehouse_Requirement_Sets
                            where item.RequirementCode.Trim() == _Code.Trim()
                            && item.OutletID == int.Parse(_outletID)
                            select item;
                if (query.Count() > 0) //nếu như nó update stock
                {
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.EXIT_DATA;
                }
                else //tạo mới
                {
                    Warehouse_Requirement_Set temp = new Warehouse_Requirement_Set();
                    temp.OutletID = int.Parse(_outletID);
                    temp.RequirementCode = _Code;
                    temp.DeviceTokenSup = "";
                    temp.RequirementDateTime = DateTime.Now;
                    temp.CreatedBy = _UserID;
                    temp.CreatedDateTime = DateTime.Now;
                    temp.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    temp.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    context.Warehouse_Requirement_Sets.InsertOnSubmit(temp);
                    context.SubmitChanges();
                    List<Warehouse_Requirement_Set_Detail> listRP = new List<Warehouse_Requirement_Set_Detail>();
                    for (int i = 0; i < arayid.Count(); i++)
                    {
                        if (araynumber[i].ToString() != String.Empty)
                        {
                            Warehouse_Requirement_Set_Detail tempDetail = new Warehouse_Requirement_Set_Detail();
                            tempDetail.WarehouseRequirementSetID = temp.ID;
                            tempDetail.BrandSetID = int.Parse(arayid[i]);
                            tempDetail.Number = int.Parse(araynumber[i]);
                            tempDetail.CreatedBy = _UserID;
                            tempDetail.CreatedDateTime = DateTime.Now;
                            tempDetail.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            tempDetail.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                            listRP.Add(tempDetail);
                        }
                    }

                    context.Warehouse_Requirement_Set_Details.InsertAllOnSubmit(listRP);
                    context.SubmitChanges();
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class WarehouseRequirementSetData
    {
        public int pUserID { get; set; }
        public int pOutletID { get; set; }
        public string pRequirementCode { get; set; }
        public string pRequirementDateTime { get; set; }
        public int pBrandSetID { get; set; }
        public int pNumber { get; set; }
        public string pDeviceToken { get; set; }
    }

    public class SimpleWarehouseRequirementSet
    {
        public long WarehouseRequirementSetID { get; set; }
        public int OutletID { get; set; }
        public string RequirementCode { get; set; }
        public string RequirementDateTime { get; set; }
        public int Status { get; set; }
        public List<SimpleWarehouseRequirementSetDetail> ListDetail { get; set; }


    }
    public class SimpleWarehouseRequirementSetDetail
    {
        public int BrandSetID { get; set; }
        public int Number { get; set; }
    }


    public class RequirementSetSE
    {
        [UIHint("_ListOutlet")]
        [Display(Name = "Siêu Thị")]
        public long OutletRequirementSetSE { get; set; }

        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionRequirementSetSE { get; set; }


        private DateTime _DateRequirementSetSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not empty.")]
        [Display(Name = "Ngày")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateRequirementSetSE
        {
            get { return _DateRequirementSetSE; }
            set
            {
                if (value != null)
                    _DateRequirementSetSE = value.Date;
            }
        }

    }
}
