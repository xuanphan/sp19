﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Profile
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll( string _pAppCode, string _pProjectID, ref GetProfileResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu
                    var query = from item in context.Profiles
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                select new SimpleProfile
                                {
                                    ID = item.ID,
                                    ProjectID = item.ProjectID,
                                    ProfileCode = item.ProfileCode,
                                    ProfileName = item.ProfileName,
                                    ProfilePhone = item.ProfilePhone,
                                    ProfileAddress = item.ProfileAddress,
                                    Avatar = item.Avatar,
                                    Experience = item.Experience,
                                    IsEmergency = item.IsEmergency
                                };

                    // gán giá trị cho phần trả về
                    var list = query.ToList();
                    result.Data = list;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }

            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public class StatusEmergencyProfile
        {
            public static string True = "Khẩn Cấp";
            public static string False = "Bình Thường";
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string MessageError, int _pProjectID, int _pOutletType, int _pSaleID, ProfileSE se, ref List<ProfileEXT> list)
        {
            DatabaseDataContext context = null;
            try
            {

                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (se.ProfileCode != null)
                {
                    se.ProfileCode = se.ProfileCode.Replace("\t", "").TrimEnd();
                }
                var query = from item in context.spProfileem(se.ProfileCode, se.ProfileRegionName, se.ProfileRegion, _pProjectID, _pOutletType, _pSaleID)
                            select new ProfileEXT
                            {
                                ProjectName = item.ProjectCode,
                                ProjectID = item.ProjectID,
                                RegionName = item.RegionName,
                                ProfileName = item.ProfileName,
                                ProfileCode = item.ProfileCode,
                                ID = item.ID,
                                Phone = item.ProfilePhone,
                                Address = item.ProfileAddress,
                                Status = item.IsEmergency,
                                StatusName = item.IsEmergency != true ? StatusEmergencyProfile.False : StatusEmergencyProfile.True,
                                FilePath = item.Avatar,
                                Experience = item.Experience,
                                EmergencyName = item.EmergencyName,
                                StartDateTime = item.StartDateTime,
                                EndDateTime = item.EndDateTime
                            };
                var l = query.ToList();
                foreach (var i in l)
                {
                    i.khancap = new List<string>();
                    var simple = l.Where(p => p.ID == i.ID);
                    if (simple.Count() > 0)
                    {
                        foreach (var u in simple)
                        {
                            if (u.StartDateTime == null)
                            {
                            }
                            else if (u.EndDateTime == null)
                            {
                                i.khancap.Add(u.EmergencyName + "-" + u.StartDateTime.GetValueOrDefault().ToString("HH:mm"));
                            }
                            else
                            {
                                i.khancap.Add(u.EmergencyName + "-" + u.StartDateTime.GetValueOrDefault().ToString("HH:mm") + "-" + u.EndDateTime.GetValueOrDefault().ToString("HH:mm"));
                            }
                        }
                    }

                }
                list = l.OrderByDescending(p => p.Status == true).Distinct(new ComparerProfileView()).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchSaleSup(ref string MessageError, int _pProjectID, int _pOutletType, int _pSaleID, string _pListSaleID, ProfileSE se, ref List<ProfileEXT> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                if (se.ProfileCode != null)
                {
                    se.ProfileCode = se.ProfileCode.Replace("\t", "").TrimEnd();
                }
                var query = from item in context.spProfileSaleSupem(se.ProfileCode, se.ProfileRegionName, se.ProfileRegion, _pProjectID, _pOutletType, _pListSaleID.Replace('|', ','))
                            select new ProfileEXT
                            {
                                ProjectName = item.ProjectCode,
                                ProjectID = item.ProjectID,
                                RegionName = item.RegionName,
                                ProfileName = item.ProfileName,
                                ProfileCode = item.ProfileCode,
                                ID = item.ID,
                                Phone = item.ProfilePhone,
                                Address = item.ProfileAddress,
                                Status = item.IsEmergency,
                                StatusName = item.IsEmergency != true ? StatusEmergencyProfile.False : StatusEmergencyProfile.True,
                                FilePath = item.Avatar,
                                Experience = item.Experience,
                                EmergencyName = item.EmergencyName,
                                StartDateTime = item.StartDateTime,
                                EndDateTime = item.EndDateTime
                            };

                var l = query.ToList();
                foreach (var i in l)
                {
                    i.khancap = new List<string>();
                    var simple = l.Where(p => p.ID == i.ID);
                    if (simple.Count() > 0)
                    {
                        foreach (var u in simple)
                        {
                            if (u.StartDateTime == null)
                            {

                            }
                            else if (u.EndDateTime == null)
                            {
                                i.khancap.Add(u.EmergencyName + "-" + u.StartDateTime.GetValueOrDefault().ToString("HH:mm"));
                            }
                            else
                            {
                                i.khancap.Add(u.EmergencyName + "-" + u.StartDateTime.GetValueOrDefault().ToString("HH:mm") + "-" + u.EndDateTime.GetValueOrDefault().ToString("HH:mm"));
                            }
                        }
                    }

                }
                list = l.OrderByDescending(p => p.Status == true).Distinct(new ComparerProfileView()).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetSimple(ref string MessageError, string _pCode, ref ProfileEXT _simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);

                var query = from item in context.Profiles
                            join profile_Discipline in context.Profile_Disciplines on item.ID equals profile_Discipline.ProfileID
                            where item.ProfileCode == _pCode
                             && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select new ProfileEXT
                            {
                                ProfileName = item.ProfileName,
                                ProfileCode = item.ProfileCode,
                                RegionName = item.Region.RegionName,
                                Avatar = item.Avatar,
                                ID = item.ID,
                                Phone = item.ProfilePhone,
                                Address = item.ProfileAddress,
                                FilePath = item.Avatar,
                                Experience = item.Experience,
                                Discipline = string.Join(",", context.Profile_Disciplines.Where(p => p.ProfileID == item.ID && p.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE).Select(p => p.Description).ToList())
                            };

                _simple = query.FirstOrDefault();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetSimple(ref string MessageError, string _pCode, string _pCompany, string _pPassWord, ref ProfileEXT _simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                //get projectID

                var query = from item in context.Profiles
                            //join profile_Discipline in context.Profile_Disciplines on item.ID equals profile_Discipline.ProfileID
                            where item.ProfileCode.Replace(" ", "").Replace("-", "_").ToUpper() == _pCode.Replace(" ", "").Replace("-", "_").ToUpper()
                            && item.ProjectCode.Trim().ToLower() == _pCompany.Trim().ToLower()
                            && item.PassWord.Trim() == _pPassWord.Trim()
                            && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select new ProfileEXT
                            {
                                ProfileName = item.ProfileName,
                                ProfileCode = item.ProfileCode,
                                RegionName = item.Region.RegionName,
                                Avatar = item.Avatar,
                                ID = item.ID,
                                Phone = item.ProfilePhone,
                                Address = item.ProfileAddress,
                                FilePath = item.Avatar,
                                Experience = item.Experience,
                                Discipline = string.Join(",", context.Profile_Disciplines.Where(p => p.ProfileID == item.ID && p.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE).Select(p => p.Description).ToList())
                            };

                _simple = query.FirstOrDefault();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetSimple(ref string MessageError, long _pid, ref ProfileEXT _simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Profiles
                            //join profile_Discipline in context.Profile_Disciplines on item.ID equals profile_Discipline.ProfileID
                            where item.ID == _pid
                             && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select new ProfileEXT
                            {
                                ProfileName = item.ProfileName,
                                ProfileCode = item.ProfileCode,
                                RegionName = item.Region.RegionName,
                                Avatar = item.Avatar,
                                ID = item.ID,
                                Phone = item.ProfilePhone,
                                Address = item.ProfileAddress,
                                FilePath = item.Avatar,
                                Experience = item.Experience,
                                Discipline = string.Join(",", context.Profile_Disciplines.Where(p => p.ProfileID == item.ID && p.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE).Select(p => p.Description).ToList())
                            };

                _simple = query.FirstOrDefault();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetSimpleEmergency(ref string MessageError, long _id, ref List<Profile_Emergency> _simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Profiles
                            join profile_Emergencies in context.Profile_Emergencies on item.ID equals profile_Emergencies.ProfileID
                            where item.ID == _id
                            && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && profile_Emergencies.CreatedDateTime.Date == DateTime.Now.Date
                            select profile_Emergencies;

                _simple = query.ToList().OrderByDescending(p => p.CreatedDateTime.Date).ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Profile_Emergency GetSimpleEmergency(long _id, ref Profile_Emergency _simple)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Profiles
                            join profile_Emergencies in context.Profile_Emergencies on item.ID equals profile_Emergencies.ProfileID
                            where item.ID == _id
                            && item.CreatedDateTime.Date == DateTime.Now.Date
                            && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            select profile_Emergencies;

                _simple = query.ToList().OrderByDescending(p => p.CreatedDateTime.Date).FirstOrDefault();
                return _simple;
            }
            catch (Exception ex)
            {
                string MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string _messageSystemError)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var queryproject = from item in context.Profiles
                                   where item.ProfileCode.Trim() == this.ProfileCode.Trim()
                                    && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                     && item.ProjectID == this.ProjectID
                                   select item;
                if (queryproject.Count() > 0)
                {
                    //co thong tin update
                    var simple = queryproject.FirstOrDefault();
                    simple.ProfileCode = this.ProfileCode;
                    simple.ProfileName = this.ProfileName;
                    simple.ProfilePhone = this.ProfilePhone;
                    simple.ProfileAddress = this.ProfileAddress;
                    simple.Avatar = this.Avatar;
                    simple.Experience = this.Experience;
                    simple.RegionID = this.RegionID;
                    simple.IsEmergency = this.IsEmergency;
                    simple.PassWord = this.PassWord;
                    simple.ProjectID = this.ProjectID;
                    simple.LastUpdatedBy = this.CreatedBy;
                    simple.LastUpdatedDateTime = DateTime.Now;
                    simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    simple.RowVersion += 1;
                }
                else
                {
                    //tao moi
                    context.Profiles.InsertOnSubmit(this);
                }
                context.SubmitChanges();

                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;


            }

            catch (Exception ex)
            {
                string MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string _messageSystemError, Profile profile)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var queryproject = from item in context.Profiles
                                   where item.ProfileCode.Trim() == profile.ProfileCode.Trim()
                                    && item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                     && item.ProjectID == profile.ProjectID
                                   select item;
                if (queryproject.Count() > 0)
                {
                    //co thong tin update
                    var simple = queryproject.FirstOrDefault();
                    simple.ProfileCode = profile.ProfileCode;
                    simple.ProfileName = profile.ProfileName;
                    simple.ProfilePhone = profile.ProfilePhone;
                    simple.ProfileAddress = profile.ProfileAddress;
                    simple.Avatar = profile.Avatar;
                    simple.Experience = profile.Experience;
                    simple.RegionID = profile.RegionID;
                    simple.IsEmergency = profile.IsEmergency;
                    simple.PassWord = profile.PassWord;
                    simple.ProjectID = profile.ProjectID;
                    simple.LastUpdatedBy = profile.CreatedBy;
                    simple.LastUpdatedDateTime = DateTime.Now;
                    simple.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    simple.RowVersion += 1;
                }
                else
                {
                    //tao moi
                    context.Profiles.InsertOnSubmit(profile);
                }
                context.SubmitChanges();
                _messageSystemError = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;


            }

            catch (Exception ex)
            {
                _messageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
        class ComparerProfileView : IEqualityComparer<ProfileEXT>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(ProfileEXT x, ProfileEXT y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.ID == y.ID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(ProfileEXT item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashtteamid = item.ID == null ? 0 : item.ID.GetHashCode();
                //Calculate the hash code for the product.
                return hashtteamid;
            }
        }

        public string GetProjectCode(int pProjectID)
        {
            DatabaseDataContext context = null;

            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var queryproject = from item in context.Profiles
                                   where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                     && item.ProjectID == pProjectID
                                   select item.ProjectCode;
                string _name = queryproject.FirstOrDefault();
                return _name;
            }

            catch (Exception ex)
            {
                string MessageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return "";
            }
            finally
            {
                context.Dispose();
                context = null;
                GC.Collect();
            }
        }
    }

    public class SimpleProfile
    {
        private int? _ID;
        public int? ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private int? _ProjectID;
        public int? ProjectID
        {
            get { return _ProjectID; }
            set { _ProjectID = value; }
        }
        private string _ProfileCode;
        public string ProfileCode
        {
            get { return _ProfileCode; }
            set { _ProfileCode = value; }
        }
        private string _ProfileName;
        public string ProfileName
        {
            get { return _ProfileName; }
            set { _ProfileName = value; }
        }
        private string _ProfilePhone;
        public string ProfilePhone
        {
            get { return _ProfilePhone; }
            set { _ProfilePhone = value; }
        }
        private string _ProfileAddress;
        public string ProfileAddress
        {
            get { return _ProfileAddress; }
            set { _ProfileAddress = value; }
        }
        private string _Avatar;
        public string Avatar
        {
            get { return _Avatar; }
            set { _Avatar = value; }
        }
        private string _Experience;
        public string Experience
        {
            get { return _Experience; }
            set { _Experience = value; }
        }

        private bool _IsEmergency;
        public bool IsEmergency
        {
            get { return _IsEmergency; }
            set { _IsEmergency = value; }
        }
        public SimpleProfile() { }

        public SimpleProfile(int ID, int ProjectID, string ProfileCode, string ProfilePhone, string ProfileAddress, string Avatar, string Experience, bool IsEmergency)
        {
            this.ProjectID = ProjectID;
            this.ID = ID;
            this.ProfileCode = ProfileCode;
            this.ProfilePhone = ProfilePhone;
            this.ProfileAddress = ProfileAddress;
            this.Avatar = Avatar;
            this.Experience = Experience;
            this.IsEmergency = IsEmergency;
        }
    }

    public class ProfileSE
    {
        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string ProfileRegion { get; set; }

        [UIHint("_RegionName")]
        [Display(Name = "Tỉnh")]
        public int ProfileRegionName { get; set; }

        [Display(Name = "Mã Nhân Viên")]
        public string ProfileCode { get; set; }
    }

    public class ProfileEXT : Profile
    {
        public string ProjectName { get; set; }
        public string Code { get; set; }
        public string TeamName { get; set; }
        public string ProfileName { get; set; }
        public string OutletName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string FilePath { get; set; }
        public string Experience { get; set; }
        public bool Status { get; set; }
        public string Discipline { get; set; }
        public string StatusName { get; set; }
        public string RegionName { get; set; }
        public string EmergencyName { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public List<string> khancap { get; set; }

        public Profile_Emergency profile_Emergency { get; set; }
    }
}
