﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Brand_Set_Detail
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(string _pAppCode,int pProjectID, ref Brand_Set_DetailResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu chi tiết quà theo projectID 
                    var query = from item in context.Brand_Set_Details
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.Brand_Set.Brand.ProjectID == pProjectID
                                orderby item.BrandSetID, item.GiftID ascending
                                select new SimpleBrandSetDetail
                                {
                                    BrandSetDetailID = item.ID,
                                    BrandSetID = item.BrandSetID,
                                    GiftID = item.GiftID,
                                    Number = item.Number,
                                    Percentage = float.Parse(item.Percentage.ToString())
                                };

                    // gán giá trị cho phần trả về
                    result.Data = query.ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
    }
    public class SimpleBrandSetDetail
    {
        private int _BrandSetDetailID;
        public int BrandSetDetailID
        {
            get { return _BrandSetDetailID; }
            set { _BrandSetDetailID = value; }
        }

        private int _BrandSetID;
        public int BrandSetID
        {
            get { return _BrandSetID; }
            set { _BrandSetID = value; }
        }
        private int _GiftID;
        public int GiftID
        {
            get { return _GiftID; }
            set { _GiftID = value; }
        }
        private int _Number;
        public int Number
        {
            get { return _Number; }
            set { _Number = value; }
        }
        private float _Percentage;
        public float Percentage
        {
            get { return _Percentage; }
            set { _Percentage = value; }
        }

        public SimpleBrandSetDetail() { }

        public SimpleBrandSetDetail(int BrandSetDetailID, int BrandSetID, int GiftID, int Number, float Percentage)
        {
            this.BrandSetDetailID = BrandSetDetailID;
            this.BrandSetID = BrandSetID;
            this.GiftID = GiftID;
            this.Number = Number;
            this.Percentage = Percentage;
        }
    }
}
