﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Gift
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT GetAll(string _pAppCode, int pProjectID, ref GiftResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    // lấy dữ liệu
                    var query = from item in context.Gifts
                                where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                                && item.ProjectID == pProjectID
                                select new SimpleGift
                                {
                                    ID = item.ID,
                                    GiftCode = item.GiftCode,
                                    GiftName = item.GiftName,
                                    FilePath = item.FilePath,
                                    IsWin = item.IsWin,
                                    IsTopupCard = item.IsTopupCard.HasValue?item.IsTopupCard.Value:false
                                };

                    // gán giá trị cho phần trả về
                    result.Data = query.ToList();
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Data = null;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Data = null;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR) + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }

        public static List<Gift> GetAll(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Gifts
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.ProjectID == _pProjectID
                            select item;
                var list = query.ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public static List<Gift> GetAllReport(int _pProjectID)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Gifts
                            where item.Status != (int)Common.STATUS_CODE.ROW_STATUS.DEACTIVE
                            && item.ProjectID == _pProjectID
                            && item.CreatedBy != 2
                            select item;
                var list = query.ToList().OrderBy(p => p.RowVersion).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class SimpleGift
    {
        private int? _ID;
        public int? ID
        {
            get { return _ID; }
            set { _ID = value; }
        }


        private string _GiftCode;
        public string GiftCode
        {
            get { return _GiftCode; }
            set { _GiftCode = value; }
        }
        private string _GiftName;
        public string GiftName
        {
            get { return _GiftName; }
            set { _GiftName = value; }
        }
        private string _FilePath;
        public string FilePath
        {
            get { return _FilePath; }
            set { _FilePath = value; }
        }

        private bool _IsWin;
        public bool IsWin
        {
            get { return _IsWin; }
            set { _IsWin = value; }
        }

        private bool _IsTopupCard;
        public bool IsTopupCard
        {
            get { return _IsTopupCard; }
            set { _IsTopupCard = value; }
        }

        public SimpleGift() { }

        public SimpleGift(int ID, string GiftCode, string GiftName, string FilePath, bool IsWin, bool IsTopupCard)
        {
            this.GiftCode = GiftCode;
            this.ID = ID;
            this.GiftName = GiftName;
            this.FilePath = FilePath;
            this.IsWin = IsWin;
            this.IsTopupCard = IsTopupCard;
        }
    }
}
