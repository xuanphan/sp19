﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class StockFestive
    {
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE Update(ref string MessageSystemError, string _AppCode, string _UserTeamID, ref UpdateStockResult result)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {
                if (_AppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {
                    //Lấy userID
                    var queryuser = from item in context.User_Teams
                                    where item.ID == long.Parse(_UserTeamID)
                                    select item.UserID;


                    LogStock log = new LogStock();
                    //safe update roi
                    var query = from stock in context.StockFestives
                                where stock.OutletID == this.OutletID
                                && stock.ProductID == this.ProductID
                                 && stock.Type == this.Type
                                && stock.DateTimeDevice.GetValueOrDefault().Date == this.DateTimeDevice.GetValueOrDefault().Date
                                select stock;
                    if (query.Count() > 0) //nếu như nó update stock
                    {
                        var simplestock = query.FirstOrDefault();
                        simplestock.Number = this.Number;
                        simplestock.LastUpdatedBy = long.Parse(queryuser.FirstOrDefault().ToString());
                        simplestock.LastUpdatedDateTime = DateTime.Now;
                        simplestock.RowVersion += 1;

                        context.SubmitChanges();
                        result.StockID = simplestock.ID;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
                    }
                    else //tạo mới
                    {

                        this.SessionCode = _SessionCode;
                        this.CreatedBy = int.Parse(queryuser.FirstOrDefault().ToString());
                        this.CreatedDate = DateTime.Now;
                        this.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                        this.RowVersion = 1;
                        context.StockFestives.InsertOnSubmit(this);
                        context.SubmitChanges();


                        result.StockID = this.ID;
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS);
                        return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SUCCESS;
                    }
                }
                else
                {
                    result.StockID = -1;
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.NOT_EXIT_APP;
                }
            }


            catch (Exception ex)
            {
                MessageSystemError = Common.ExceptionUtils.ExceptionToMessage(ex);
                result.StockID = -1;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
                result.Description = ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageError, ref List<StockFestiveEXT> list, StockFestiveSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_Festives
                            where ((item.DateTimeDevice != null ? item.DateTimeDevice.GetValueOrDefault().Date : DateTime.Now.Date) == se.DateFestiveSE.Date || item.DateTimeDevice == null)
                            && (item.Expr1 == se.AreaFestiveSE || se.AreaFestiveSE == "All" || se.AreaFestiveSE == null)
                            && (item.ID == se.OutletFestiveSE || se.OutletFestiveSE == 0 || se.OutletFestiveSE == null || se.OutletFestiveSE == 0)
                            select new StockFestiveEXT
                            {
                                OutletID = item.ID,
                                OutletName = item.Name,
                                account = item.Accounts,
                                city = item.City,
                                datetime = item.DateTimeDevice != null ? item.DateTimeDevice : null,
                            };

                list = query.ToList().Distinct(new ComparerFestive()).ToList();

                foreach (var i in list)
                {
                    i.list = new List<StockFestiveDetails>();
                    var queryposm = context.View_Festives.Where(p => p.ID == i.OutletID &&
                      ((p.DateTimeDevice != null ? p.DateTimeDevice.GetValueOrDefault().Date : DateTime.Now.Date) == se.DateFestiveSE.Date || p.DateTimeDevice == null)
                            && (p.Expr1 == se.AreaFestiveSE || se.AreaFestiveSE == "All" || se.AreaFestiveSE == null)).ToList();
                    foreach (var u in queryposm)
                    {
                        StockFestiveDetails simple = new StockFestiveDetails();
                        simple.OutletID = u.ID;
                        simple.Type = u.type.GetValueOrDefault();
                        simple.ProductID = u.ProductID.GetValueOrDefault();
                        simple.Number = u.Number.GetValueOrDefault();
                        i.list.Add(simple);
                    }
                }

                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        class ComparerFestive : IEqualityComparer<StockFestiveEXT>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(StockFestiveEXT x, StockFestiveEXT y)
            {

                //Check whether the compared objects reference the same data.
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null.
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal.
                return x.OutletID == y.OutletID;
                // return x.AssignID == y.AssignID;
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(StockFestiveEXT item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                int hashproductid = item.OutletID == null ? 0 : item.OutletID.GetHashCode();
                //Calculate the hash code for the product.
                return hashproductid;
                // return hashAssignID;
            }
        }
    }
    public class StockFestiveSE
    {

        private DateTime _DateFestiveSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not Empty.")]
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public DateTime DateFestiveSE
        {
            get { return _DateFestiveSE; }
            set
            {
                if (value != null)
                    _DateFestiveSE = value.Date;
            }
        }

        [UIHint("_areaCity")]
        [Display(Name = "Area")]
        public string AreaFestiveSE { get; set; }

        [UIHint("_listOutlet")]
        [Display(Name = "Outlet")]
        public long OutletFestiveSE { get; set; }


    }
    public class StockFestiveEXT
    {
        public long OutletID { get; set; }
        public DateTime? datetime { get; set; }
        public string OutletName { get; set; }
        public string city { get; set; }
        public string account { get; set; }
        public List<StockFestiveDetails> list { get; set; }
    }

    public class StockFestiveDetails
    {
        public long OutletID { get; set; }
        public long ProductID { get; set; }
        public int Type { get; set; }
        public double Number { get; set; }
    }
}
