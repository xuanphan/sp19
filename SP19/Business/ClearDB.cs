﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class ClearDB
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT delete()
        {
            DatabaseDataContext context = null;
            string a = "";
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var queryAttendance = from item in context.AttendanceTrackings
                            where item.Team_Outlet.Outlet.ProjectID == 2
                            && item.Team_Outlet.Outlet.OutletTypeID == 1
                            && item.Team_Outlet.Outlet.ChanelID != 22
                            select item;
                var queryIslane = from item in context.Brewery_Brand_IsLands
                                      where item.Outlet.ProjectID == 2
                                      && item.Outlet.OutletTypeID == 1
                                      && item.Outlet.ChanelID != 22
                                      select item;

                var queryCustomer = from item in context.Customers
                                  where item.Outlet.ProjectID == 2
                                  && item.Outlet.OutletTypeID == 2
                                  && item.Outlet.ChanelID != 22
                                  select item;

                var queryProfileEmer = from item in context.Profile_Emergencies
                                    where item.Outlet.ProjectID == 2
                                    && item.Outlet.OutletTypeID == 1
                                    && item.Outlet.ChanelID != 22
                                    select item;

                var queryOlCurrentgift = from item in context.Outlet_Current_Gifts
                                       where item.Outlet.ProjectID == 2
                                       && item.Outlet.OutletTypeID == 1
                                       && item.Outlet.ChanelID != 22
                                       select item;

                var queryPOSM = from item in context.POSM_Reports
                                         where item.Outlet.ProjectID == 2
                                         && item.Outlet.OutletTypeID == 1
                                         && item.Outlet.ChanelID != 22
                                         select item;

                var queryStock = from item in context.Stocks
                                where item.Outlet.ProjectID == 2
                                && item.Outlet.OutletTypeID == 1
                                && item.Outlet.ChanelID != 22
                                select item;
                var queryTakeOfVolumn = from item in context.Take_Off_Volumns
                                 where item.Outlet.ProjectID == 2
                                 && item.Outlet.OutletTypeID == 1
                                 && item.Outlet.ChanelID != 22
                                 select item;
                var queryOutlet = from item in context.Outlets
                                        where item.ProjectID == 2
                                        && item.OutletTypeID == 1
                                        && item.ChanelID != 22
                                        select item;

                //update 
                //var listOutlet = queryOutlet.ToList();
                //listOutlet.All(o => { o.DeviceID = null; o.DeviceToken = null; o.IsOnline = false; return true; });
                //context.SubmitChanges();



                // xoas nhieubang
                var listAtt = queryAttendance.ToList();
                var listCustomer = queryCustomer.ToList();

                // xoa thang
                var listIslane = queryIslane.ToList();
                var listProfileEmer = queryProfileEmer.ToList();
                var listOutletCurrentgift = queryOlCurrentgift.ToList();
                var listPOSM = queryPOSM.ToList();
                var listStock = queryStock.ToList();
                var listTakeOfVolumn = queryTakeOfVolumn.ToList();
               
                // xoas thawng
                //context.Brewery_Brand_IsLands.DeleteAllOnSubmit(listIslane);
                //context.Profile_Emergencies.DeleteAllOnSubmit(listProfileEmer);
                //context.POSM_Reports.DeleteAllOnSubmit(listPOSM);
                //context.Stocks.DeleteAllOnSubmit(listStock);
                //context.Take_Off_Volumns.DeleteAllOnSubmit(listTakeOfVolumn);
                context.Outlet_Current_Gifts.DeleteAllOnSubmit(listOutletCurrentgift);
                context.SubmitChanges();

                // xoas nhieu bang
                //foreach (var item in listAtt)
                //{
                //    context.AttendanceTracking_Images.DeleteAllOnSubmit(item.AttendanceTracking_Images);
                //    context.SubmitChanges();
                //    context.AttendanceTrackings.DeleteOnSubmit(item);
                //    context.SubmitChanges();
                //}

                foreach (var item in listCustomer)
                {
                    context.Customer_Gifts.DeleteAllOnSubmit(item.Customer_Gifts);
                    context.Customer_Images.DeleteAllOnSubmit(item.Customer_Images);
                    context.Customer_Products.DeleteAllOnSubmit(item.Customer_Products);
                    context.Customers.DeleteOnSubmit(item);
                    context.SubmitChanges();
                }

                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {

            }
        }
    }
}
