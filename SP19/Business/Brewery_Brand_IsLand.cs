﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public partial class Brewery_Brand_IsLand
    {
        public static Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(string _pAppCode, List<Brewery_Brand_IsLand> list, ref MainResult result)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                // kiểm tra xem appcode truyền lên có đúng không
                if (_pAppCode.ToLower().Trim() == Common.Global.AppCode.ToLower().Trim())
                {

                    foreach (var entity in list)
                    {
                        // kiểm tra xem đã có trong db chưa
                        var query = from item in context.Brewery_Brand_IsLands
                                    where item.OutletID == entity.OutletID
                                    && item.BreweryBrandID == entity.BreweryBrandID
                                    && item.DeviceDateTime.GetValueOrDefault().Date == entity.DeviceDateTime.GetValueOrDefault().Date
                                    select item;
                        // tồn tại update
                        if (query.Count() > 0)
                        {
                            var simple = query.FirstOrDefault();
                            simple.InsideColumn = entity.InsideColumn;
                            simple.InsidePalletColumn = entity.InsidePalletColumn;
                            simple.OutsideColumn = entity.OutsideColumn;
                            simple.LastUpdatedDateTime = DateTime.Now;
                            simple.LastUpdatedBy = entity.CreatedBy;
                            simple.RowVersion += 1;
                            context.SubmitChanges(); // thực thi câu lệnh
                        }
                        //-- tạo mới
                        else
                        {
                            context.Brewery_Brand_IsLands.InsertOnSubmit(entity); // thêm
                            context.SubmitChanges(); // thực thi câu lệnh
                        }
                    }
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                }
                else
                {
                    // gán giá trị cho phần trả về
                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP);
                    return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.NOT_EXIT_APP;
                }

            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR) + " " + ex.Message;
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context.Connection.Close();
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW AddNew(ref string _messageSystemError, Brewery_Brand_IsLand brewery_Brand_IsLand)
        {
            DatabaseDataContext context = null;
            context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
            try
            {

                var query = from item in context.Brewery_Brand_IsLands
                            where item.OutletID == brewery_Brand_IsLand.OutletID
                            && item.BreweryBrandID == brewery_Brand_IsLand.BreweryBrandID
                            && item.DeviceDateTime.GetValueOrDefault().Date == brewery_Brand_IsLand.DeviceDateTime.GetValueOrDefault().Date
                            select item;
                // tồn tại update
                if (query.Count() > 0)
                {
                    var simple = query.FirstOrDefault();
                    simple.InsideColumn = brewery_Brand_IsLand.InsideColumn;
                    simple.InsidePalletColumn = brewery_Brand_IsLand.InsidePalletColumn;
                    simple.OutsideColumn = brewery_Brand_IsLand.OutsideColumn;
                    simple.LastUpdatedDateTime = DateTime.Now;
                    simple.LastUpdatedBy = brewery_Brand_IsLand.CreatedBy;
                    simple.RowVersion += 1;
                    context.SubmitChanges();
                }
                //-- tạo mới
                else
                {
                    context.Brewery_Brand_IsLands.InsertOnSubmit(brewery_Brand_IsLand);
                    context.SubmitChanges();
                }
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
            }
            catch (Exception ex)
            {
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT Search(ref string _messageError, int _pProjectID, int _pSaleID, int _pOutletType, ref List<IslandEXT> list, IsLandSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_Islands
                            where (item.DeviceDateTime.GetValueOrDefault().Date >= se.DateIsLandSE.Date && item.DeviceDateTime.GetValueOrDefault().Date <= se.DateIsLandToSE.Date)
                            && (item.Type == se.RegionIsLandSE || se.RegionIsLandSE == "Tất Cả" || se.RegionIsLandSE == null)
                            && (item.BreweryID == se.BreweryIsLandSE || se.BreweryIsLandSE == 0 || se.BreweryIsLandSE == null)
                            && (item.ProjectID == _pProjectID)
                            && (item.SaleID == _pSaleID || _pSaleID == -1 || _pSaleID == 0)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == -1 || _pOutletType == 0)
                            orderby item.RegionID, item.BreweryID, item.BreweryName
                            select new IslandEXT
                            {
                                DateTimeDevice = item.DeviceDateTime.GetValueOrDefault().Date,
                                RegionName = item.RegionName,
                                OutLetName = item.OutletName,
                                OutLetID = item.OutletID,
                                BreweryName = item.BreweryName,
                                BrandName = item.BrandName,
                                Outside = item.OutsideColumn,
                                Column = item.InsideColumn,
                                Pallet = item.InsidePalletColumn,
                                //HalfPallet = item.InsideHalfPalletColumn
                            };

                List<IslandEXT> listTmp = query.ToList();
                var listGroup = listTmp.GroupBy(l => new { l.DateTimeDevice.Date, l.OutLetID });
                foreach (var item in listGroup)
                {
                    var listByOutLet = listTmp.Where(a => a.OutLetID == item.Key.OutLetID && a.DateTimeDevice.Date == item.Key.Date).ToList();
                    var filtered = (from a in listByOutLet
                                    group a by a.BrandName
                                        into grp
                                        select grp.OrderByDescending(b => b.OutLetID).FirstOrDefault()
                       ).ToList();
                    list.AddRange(filtered);
                }

                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchSaleSup(ref string _messageError, int _pProjectID, int _pSaleSupID, string _pListSaleID, ref List<IslandEXT> list, IsLandSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                List<string> result = _pListSaleID.Split('|').ToList();
                var query = from item in context.View_IsLandSaleSups
                            where (item.DeviceDateTime.GetValueOrDefault().Date >= se.DateIsLandSE.Date && item.DeviceDateTime.GetValueOrDefault().Date <= se.DateIsLandToSE.Date)
                            && (item.Type == se.RegionIsLandSE || se.RegionIsLandSE == "Tất Cả" || se.RegionIsLandSE == null)
                            && (item.BreweryID == se.BreweryIsLandSE || se.BreweryIsLandSE == 0 || se.BreweryIsLandSE == null)
                            && (item.ProjectID == _pProjectID)
                            && (item.SaleSupID == _pSaleSupID)
                             && result.Contains(item.SaleID.ToString())
                            orderby item.RegionID, item.BreweryID, item.BreweryName
                            select new IslandEXT
                            {
                                DateTimeDevice = item.DeviceDateTime.GetValueOrDefault().Date,
                                RegionName = item.RegionName,
                                OutLetName = item.OutletName,
                                OutLetID = item.OutletID,
                                BreweryName = item.BreweryName,
                                BrandName = item.BrandName,
                                Outside = item.OutsideColumn,
                                Column = item.InsideColumn,
                                Pallet = item.InsidePalletColumn
                            };

                List<IslandEXT> listTmp = query.ToList();
                var listGroup = listTmp.GroupBy(l => new { l.DateTimeDevice.Date, l.OutLetID });
                foreach (var item in listGroup)
                {
                    var listByOutLet = listTmp.Where(a => a.OutLetID == item.Key.OutLetID && a.DateTimeDevice.Date == item.Key.Date).ToList();
                    var filtered = (from a in listByOutLet
                                    group a by a.BrandName
                                        into grp
                                        select grp.OrderByDescending(b => b.OutLetID).FirstOrDefault()
                       ).ToList();
                    list.AddRange(filtered);
                }

                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchSale(ref string _messageError, int _pProjectID, int _pSaleID, int _pOutletType, ref List<IslandEXT> list, IsLandSE se)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.View_Islands
                            where (item.DeviceDateTime.GetValueOrDefault().Date >= se.DateIsLandSE.Date && item.DeviceDateTime.GetValueOrDefault().Date <= se.DateIsLandToSE.Date)
                            && (item.Type == se.RegionIsLandSE || se.RegionIsLandSE == "Tất Cả" || se.RegionIsLandSE == null)
                            && (item.BreweryID == se.BreweryIsLandSE || se.BreweryIsLandSE == 0 || se.BreweryIsLandSE == null)
                            && (item.ProjectID == _pProjectID)
                            && (item.SaleID == _pSaleID || _pSaleID == -1)
                            && (item.OutletTypeID == _pOutletType || _pOutletType == -1 || _pOutletType == 0)
                            orderby item.RegionID, item.BreweryID, item.BreweryName
                            select new IslandEXT
                            {
                                DateTimeDevice = item.DeviceDateTime.GetValueOrDefault().Date,
                                RegionName = item.RegionName,
                                OutLetName = item.OutletName,
                                OutLetID = item.OutletID,
                                BreweryName = item.BreweryName,
                                BrandName = item.BrandName,
                                Outside = item.OutsideColumn,
                                Column = item.InsideColumn,
                                Pallet = item.InsidePalletColumn
                            };

                List<IslandEXT> listTmp = query.ToList();
                var listGroup = listTmp.GroupBy(l => new { l.DateTimeDevice.Date, l.OutLetID });
                foreach (var item in listGroup)
                {
                    var listByOutLet = listTmp.Where(a => a.OutLetID == item.Key.OutLetID && a.DateTimeDevice.Date == item.Key.Date).ToList();
                    var filtered = (from a in listByOutLet
                                    group a by a.BrandName
                                        into grp
                                        select grp.OrderByDescending(b => b.OutLetID).FirstOrDefault()
                       ).ToList();
                    list.AddRange(filtered);
                }

                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }

        public Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT SearchSup(ref string _messageError, int _OutletID, int _pProjectID, int _pSupID, ref List<Brewery_Brand_IsLand> list)
        {
            DatabaseDataContext context = null;
            try
            {
                context = new DatabaseDataContext(Common.Global.GetConnectionInfo().ConnectionStringSQL);
                var query = from item in context.Brewery_Brand_IsLands
                            join sup in context.Sup_Outlets on item.OutletID equals sup.OutletID
                            where sup.SupID == _pSupID
                            && item.CreatedDateTime.Date == DateTime.Now.Date
                            && item.OutletID == _OutletID
                            select item;
                list = query.ToList();
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
            }
            catch (Exception ex)
            {
                _messageError = Common.ExceptionUtils.ExceptionToMessage(ex);
                return Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
            }
            finally
            {
                context = null;
                GC.Collect();
            }
        }
    }
    public class IsLandSE
    {

        private DateTime _DateIsLandSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not Empty.")]
        [Display(Name = "Từ Ngày")]
        [DataType(DataType.Date)]
        public DateTime DateIsLandSE
        {
            get { return _DateIsLandSE; }
            set
            {
                if (value != null)
                    _DateIsLandSE = value.Date;
            }
        }
        private DateTime _DateIsLandToSE = DateTime.Now;
        [Required(ErrorMessage = "{0} Not Empty.")]
        [Display(Name = "Đến Ngày")]
        [DataType(DataType.Date)]
        public DateTime DateIsLandToSE
        {
            get { return _DateIsLandToSE; }
            set
            {
                if (value != null)
                    _DateIsLandToSE = value.Date;
            }
        }

        [UIHint("_Region")]
        [Display(Name = "Vùng")]
        public string RegionIsLandSE { get; set; }

        [UIHint("_Brewery")]
        [Display(Name = "Nhà Máy Bia")]
        public long BreweryIsLandSE { get; set; }


    }
    public class IslandEXT
    {
        public DateTime DateTimeDevice { get; set; }
        public string RegionName { get; set; }
        public int OutLetID { get; set; }
        public string OutLetName { get; set; }
        public string BreweryName { get; set; }
        public string BrandName { get; set; }
        public int Outside { get; set; }
        public int Column { get; set; }
        public int Pallet { get; set; }
        public int HalfPallet { get; set; }

        public int Total
        {
            get
            {
                return Outside + Column + Pallet * 4 + HalfPallet * 8;
            }
        }
    }
}
