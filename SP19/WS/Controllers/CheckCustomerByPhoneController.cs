﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class CheckCustomerByPhoneController : ApiController
    {
        // kiểm tra sđt khách hàng có quay vòng quay trong ngày  (bỏ)
        public async Task<IHttpActionResult> Postcheck(CheckCustomerByPhoneModel model)
        {
            MainResult result = new MainResult();
            //Common.ERROR_CODE.ERROR_CODE_ITEMS_CHECK_CHANGE_GIFT error = Customer.checkChangeGift(model.pAppCode, int.Parse(model.pOutletID), model.pCustomerPhone, ref result);
            return Ok(result);
        }
    }
}
