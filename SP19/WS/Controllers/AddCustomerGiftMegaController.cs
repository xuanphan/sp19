﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using WS.Models;

namespace WS.Controllers
{
    public class AddCustomerGiftMegaController : ApiController
    {
        // thêm khách hàng trúng quà vòng quay mega
        public async Task<IHttpActionResult> Postaddgiftmega(AddCustomerGiftMegaModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            try
            {
                // parse chuỗi json ra object
                JavaScriptSerializer jss = new JavaScriptSerializer();
                WSCustomerGiftMegaDetail item = jss.Deserialize<WSCustomerGiftMegaDetail>(model.pData);

                // khởi tạo object 
                Customer_Gift_Mega entity = new Customer_Gift_Mega();
                // gán dữ liệu
                entity.CustomerID = item.pCustomerID;
                entity.GiftID = item.pGiftID;
                entity.CreatedBy = item.pTeamOutletID;
                entity.CreatedDateTime = DateTime.Now;
                entity.Status = 1;
                entity.RowVersion = 1;

                // vào xử lý
                Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = LuckyMega.addGiftMega(model.pAppCode,entity, ref result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = -1;
                result.Description = "Lỗi: " + ex.Message;
                return Ok(result);
            }

        }
    }
}
