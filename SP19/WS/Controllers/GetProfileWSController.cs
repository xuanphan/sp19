﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using System.Net.Http;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class GetProfileWSController : ApiController
    {
        // lấy danh sách nhân viên theo project
        public async Task<IHttpActionResult> PostProfile(ProfileModel model)
        {
            // khởi tạo trả về
            GetProfileResult result = new GetProfileResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Profile.GetAll( model.pAppCode, model.pProjectID, ref result);
            return Ok(result);
        }
    }
}
