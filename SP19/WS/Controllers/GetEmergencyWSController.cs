﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetEmergencyWSController : ApiController
    {
        // GET: /Brand/
        // lấy danh sách các loại khẩn cấp nhân viên
        public async Task<IHttpActionResult> GetAll(string pAppCode, string pProjectID)
        {
            // khởi tạo trả về
            EmergencyResult result = new EmergencyResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Emergency.GetAll(pAppCode, int.Parse(pProjectID), ref result);
            return Ok(result);
        }
    }
}
