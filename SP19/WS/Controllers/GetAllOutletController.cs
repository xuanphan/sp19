﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;
using Business;


namespace WS.Controllers
{
    public class GetAllOutletController : ApiController
    {
        // lấy danh sách outlet thuộc quyền quản lý của KHO  (dùng cho app KHO)
        public async Task<IHttpActionResult> Postgetall(GetAllOutletModel model)
        {
            // khởi tạo trả về
            GetAllOutletResult result = new GetAllOutletResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Outlet.getAllWS(model.pAppCode, int.Parse(model.pProjectID),int.Parse(model.pUserID), ref result);
            return Ok(result);
        }
    }
}
