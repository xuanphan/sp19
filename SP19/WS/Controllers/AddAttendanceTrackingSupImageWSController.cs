﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;
using Business;

namespace WS.Controllers
{
    public class AddAttendanceTrackingSupImageWSController : ApiController
    {
        // chấm công sup + hình ảnh
        public async Task<IHttpActionResult> Postattimagesup(AddAttendanceTrackingSUPImageWSModel model)
        {
            // khởi tạo trả về
            AddAttendanceTrackingSupImageResult result = new AddAttendanceTrackingSupImageResult();
            // vào xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = AttendanceTracking_Sup_Image.addNewSimple(model.pAppCode, long.Parse(model.pAttendanceTrackingSupID), long.Parse(model.pImageID), ref result);
            return Ok(result);
        }
    }
}
