﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;

namespace WS.Controllers
{
    public class AddWarehouseRequirementSetWSController : ApiController
    {
        //
        // GET: /AddCustomer/
        // thêm yêu cầu giao thêm set quà
        public async Task<IHttpActionResult> PostData(WarehouseRequirementSetModel model)
        {
            //khởi tạo trả về
            WarehouseRequirementSetResult result = new WarehouseRequirementSetResult();
            // Parse chuỗi json ra list object
            IList<WarehouseRequirementSetData> listEntity = Common.JsonUtils.DeserializeToList<WarehouseRequirementSetData>(model.pData);
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Warehouse_Requirement_Set.AddNew(model.pAppCode, listEntity, ref result);

            return Ok(result);
        }
    }
}