﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class SendWinMegaNowController : ApiController
    {
        // sét quà trúng trực tiếp cho vòng quay mega cho siêu thị
        public async Task<IHttpActionResult> GetSendNow(string pOutletID ,string pGiftID)
        {
            // xử lý
            string a = Outlet.sendWinMegaNow(pOutletID, pGiftID);
            return Ok(a);
        }
    }
}
