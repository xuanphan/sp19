﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetNotificationWSController : ApiController
    {
        // GET: /Brand/
        // lấy ds thông báo của siêu thị
        public async Task<IHttpActionResult> GetAll(string pAppCode, string pProjectID,string pOutletTypeID,string pOutletID)
        {
            // khởi tạo trả về
            NotificationResult result = new NotificationResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Notification.GetAll(pAppCode, int.Parse(pProjectID), int.Parse(pOutletTypeID), int.Parse(pOutletID), ref result);
            return Ok(result);
        }
    }
}
