﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class GetWarehouseRequirementWSController : ApiController
    {
        // lấy danh sách yêu cầu giao thêm set quà theo siêu thị : APP KHO
        public async Task<IHttpActionResult> Postgetall(GetWarehouseRequirementModel model)
        {
            // khởi tạo trả về
            GetWarehouseRequirementResult result = new GetWarehouseRequirementResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Warehouse_Requirement_Set.getALL(model.pAppCode,int.Parse(model.pOutletID), ref result);
            return Ok(result);
        }
    }
}
