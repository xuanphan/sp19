﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;

namespace WS.Controllers
{
    public class GetVersionController : ApiController
    {
        //
        // GET: /UpdateSoft/
        // thêm log ghi nhận version hiện tại của app
        public async Task<IHttpActionResult> PostUpdate(VersionModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // tạo object thêm vào db
            Business.Log_Update log = new Business.Log_Update();
            // gán dữ liệu
            log.OutletID = long.Parse(model.pOutletID);
            log.Version = model.pVersion;
            log.CreatedBy = 1;
            log.CreatedDateTime = DateTime.Now;
            log.Status = 1;
            log.RowVersion = 1;
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = log.AddNew( model.pAppCode, ref result);
            return Ok(result);
        }
    }
}