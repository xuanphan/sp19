﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Business;
using System.Threading.Tasks;

namespace WS.Controllers
{
    public class UpdateCurrentSetForSUPController : ApiController
    {
        // add set quà đang chạy hiện tại của outlet theo sup : APP SUP
        // vì sup chỉ quay 1 set quà duy nhất nên không có chuyện thay đổi set quà như bên app SP
        public async Task<IHttpActionResult> Getupdate()
        {
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Sup_Outlet_Brand_Set_Used.update();
            return Ok(error);
        }
    }
}
