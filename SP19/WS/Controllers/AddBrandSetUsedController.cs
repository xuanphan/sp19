﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;
using Business;

namespace WS.Controllers
{
    public class AddBrandSetUsedController : ApiController
    {
        // thêm số lượng set quà quay hết tại outlet
        public async Task<IHttpActionResult> Postaddbrandsetused(AddBrandSetUsedModel model)
        {
            //khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db 
            List<Brand_Set_Used> list = new List<Brand_Set_Used>();
            // Parse chuỗi json ra list object
            IList<BrandSetUsedData> listEntity = Common.JsonUtils.DeserializeToList<BrandSetUsedData>(model.pData);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in listEntity)
            {
                Brand_Set_Used temp = new Brand_Set_Used();
                temp.OutletID = item.pOutletID;
                temp.BrandSetID = item.pBrandSetID;
                temp.Number = item.pNumber;
                temp.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(item.pDeviceDateTime);
                temp.CreatedBy = item.pTeamOutletID;
                temp.CreatedDateTime = DateTime.Now;
                temp.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                temp.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                list.Add(temp);
            }
            // vào xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Brand_Set_Used.add(model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}
