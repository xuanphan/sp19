﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetGiftWSController : ApiController
    {
        // GET: /Brand/
        // lấy danh sách thông tin quà đổi theo project ID 
        public async Task<IHttpActionResult> GetAll(string pAppCode, string pProjectID)
        {
            // khởi tạo trả về
            GiftResult result = new GiftResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Gift.GetAll(pAppCode, int.Parse(pProjectID), ref result);
            return Ok(result);
        }
    }
}
