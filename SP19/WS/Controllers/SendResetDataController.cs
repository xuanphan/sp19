﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class SendResetDataController : ApiController
    {
        // bắn 1 firebase trực tiếp xuống siêu thị để xóa dữ liệu quay quà mega (nhằm xóa list quay quà mega và làm cho sthi đó không thể trúng quà)
        public async Task<IHttpActionResult> GetResetData(string pOutletID)
        {
            // xử lý
            string a = Outlet.resetData(pOutletID);
            return Ok(a);
        }
    }
}
