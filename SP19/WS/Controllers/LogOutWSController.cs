﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class LogOutWSController : ApiController
    {
        // đăng xuất 
        public async Task<IHttpActionResult> Postupdate(LogOutWSModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Outlet.logOut(model.pAppCode, model.pOutletID, ref result);
            return Ok(result);
        }
    }
}
