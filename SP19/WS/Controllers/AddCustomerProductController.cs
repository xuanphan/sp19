﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;
using Newtonsoft.Json;
namespace WS.Controllers
{
    public class AddCustomerProductController : ApiController
    {
        //
        // GET: /AddCustomer/
        // thêm danh sách sản phẩm khách hàng mua
        public async Task<IHttpActionResult> PostData(CustomerProductModel model)
        {
            //khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db 
            List<Customer_Product> list = new List<Customer_Product>();
            // Parse chuỗi json ra list object
            CustomerProductDetail[] items = JsonConvert.DeserializeObject<CustomerProductDetail[]>(model.pData);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in items)
            {
                Customer_Product entity = new Business.Customer_Product();
                entity.ProductID = int.Parse(item.pProductID);
                entity.CustomerID = long.Parse(item.pCustomerID);
                entity.Number = item.pNumber;
                entity.CreatedBy = int.Parse(item.pTeamOutletID);
                entity.CreatedDateTime = DateTime.Now;
                entity.Status = 1;
                entity.RowVersion = 1;
                list.Add(entity);
            }
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Customer_Product.AddNew(model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}