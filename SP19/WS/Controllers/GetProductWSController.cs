﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetProductWSController : ApiController
    {
        // GET: /Brand/
        // lấy danh sách sản phẩm theo
        public async Task<IHttpActionResult> GetAll(string pAppCode, string pProjectID,string pOutletID)
        {
            // khởi tạo trả về
            ProductResult result = new ProductResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Product.GetAll(pAppCode, int.Parse(pProjectID), pOutletID, ref result);
            return Ok(result);
        }
    }
}
