﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    // lấy danh sách hình layout cho các vòng quay may mắn theo project tải lên
    public class GetBackgroundWSController : ApiController
    {
        // GET: /Brand/
        public async Task<IHttpActionResult> GetAll(string pAppCode,string pProjectID)
        {
            // khởi tạo trả về
            BackgroundResult result = new BackgroundResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Background.GetAll(pAppCode, pProjectID, ref result);
            return Ok(result);
        }
    }
}
