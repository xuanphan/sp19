﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetBrandWSController : ApiController
    {
        // GET: /Brand/
        // lấy danh sách thông tin các nhãn hàng chạy thuộc Project
        public async Task<IHttpActionResult> GetAll(string pAppCode, string pProjectID)
        {
            // khởi tạo trả về
            BrandResult result = new BrandResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Brand.GetAllWS(pAppCode, int.Parse(pProjectID), ref result);
            return Ok(result);
        }
    }
}
