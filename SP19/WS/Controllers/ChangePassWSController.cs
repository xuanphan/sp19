﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    // đổi mật khẩu
    public class ChangePassWSController : ApiController
    {
        public async Task<IHttpActionResult> PostChangePass(ChangePassModel model)
        {
            //khởi tạo trả về
            ChangePassResult result = new ChangePassResult();

            // xử lý
            Business.User user = new Business.User();
            Common.ERROR_CODE.ERROR_CODE_CHANGE_PASS_WORD error = user.ChangePassWord(model.pUserID, model.pAppcode, model.pOldPassword, model.pNewPassword, ref result);
            return Ok(result);
        }
    }
}
