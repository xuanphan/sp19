﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;

namespace WS.Controllers
{
    public class AddAttendanceTrackingSUPWSController : ApiController
    {
        //
        // GET: /AttendanceTracking/
        // chấm công SUP
        public async Task<IHttpActionResult> PostDatasup(AttendanceTrackingSupModel model)
        {
            // khởi tạo trả về
            AddAttendanceTrackingSUPResult result = new AddAttendanceTrackingSUPResult();
            // khởi tạo object
            AttendanceTracking_Sup attendancetrackingsup = new AttendanceTracking_Sup();
            try
            {
                //gán dữ liệu
                attendancetrackingsup.SupID = int.Parse(model.pSupID);
                attendancetrackingsup.OutletID = int.Parse(model.pOutletID);
                attendancetrackingsup.AttendanceTrackingCode = model.pAttendanceTrackingCode;
                attendancetrackingsup.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(model.pDeviceDateTime);
                attendancetrackingsup.TimePointType = model.pTimePointType;
                attendancetrackingsup.LatGPS = double.Parse(model.pLatGPS);
                attendancetrackingsup.LongGPS = double.Parse(model.pLongGPS);
                // gán xong vào xử lý
                Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = attendancetrackingsup.AddNew(model.pAppCode, ref result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Data = 0;
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = ex.Message;
                return Ok(result);
            }
        }
    }
}