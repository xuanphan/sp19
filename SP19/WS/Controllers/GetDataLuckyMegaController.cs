﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    // lấy thông tin vòng quay mega
    public class GetDataLuckyMegaController : ApiController
    {
        public async Task<IHttpActionResult> Postgetdata(GetDataLuckyMegaModel model)
        {
            // khởi tạo trả về
            GetDataMegaResult result = new GetDataMegaResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = LuckyMega.getData(model.pAppCode, ref result);
            return Ok(result);
        }
    }
}
