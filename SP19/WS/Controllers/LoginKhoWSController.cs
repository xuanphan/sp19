﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class LoginKhoWSController : ApiController
    {
      // đăng nhập dùng cho APP KHO
        public async Task<IHttpActionResult> Postloginkho(LoginKhoWSModel model)
        {
            // khởi tạo trả về
            LoginKhoResult result = new LoginKhoResult();
            // xử lý
            Business.User user = new Business.User();
            Common.ERROR_CODE.ERROR_CODE_LOGIN error = user.LoginKho(model.pUserName, model.pPassWord, model.pUserType, model.pAppCode,model.pDeviceToken, ref result);
            return Ok(result);
        }
    }
}
