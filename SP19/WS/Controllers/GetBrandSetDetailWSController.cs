﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetBrandSetDetailWSController : ApiController
    {
        // GET: /Brand/
        // lấy danh sách chi tiết quà theo từng set
        public async Task<IHttpActionResult> GetAll(string pAppCode, string pProjectID)
        {
            // khởi tạo trả về
            Brand_Set_DetailResult result = new Brand_Set_DetailResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Brand_Set_Detail.GetAll(pAppCode, int.Parse(pProjectID), ref result);
            return Ok(result);
        }
    }
}
