﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;
using Business;

namespace WS.Controllers
{
    public class GetGiftUsedByOutletController : ApiController
    {
        // lấy danh sách quà đã sử dụng trong ngày tại siêu thị: dùng cho APP SUP
        public async Task<IHttpActionResult> Postthongkequa(GetGiftUsedByOutletModel model)
        {
            // khởi tạo trả về
            GetGiftUsedByOutletResult result = new GetGiftUsedByOutletResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Sup_Outlet_Current_Gift.thongkequa(model.pAppCode, model.pOutletID, ref result);
            return Ok(result);
        }
    }
}
