﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;
using System.IO;
using System.Web;

namespace WS.Controllers
{
    public class GetUpdateVersionKHOController : ApiController
    {
        // GET api/Login
        // get link file apk về để update app KHO
        public async Task<IHttpActionResult> GetUpdateVersion(string pAppCode, string pProjectID)
        {
            // khởi tạo trả về
            UpdateVersionResult result = new UpdateVersionResult();
            try
            {
                string realPath;
                // lấy danh sách file từ host/VersionMobileKHO/ + projectID truyền lên (ví dụ 1, 2, 3 ....)
                realPath = System.Web.HttpContext.Current.Server.MapPath(("~/VersionMobileKHO" + "/" + pProjectID));
                DirectoryInfo d = new DirectoryInfo(realPath);
                List<VersionDetail> listfile = new List<VersionDetail>();
                // lấy danh sách file apk trong đường dẫn
                foreach (var file in d.GetFiles("*.apk"))
                {
                    VersionDetail version = new VersionDetail();
                    string[] namefile = file.ToString().Split('_');
                    version.Version = namefile[1];
                    version.Link = file.Name.ToString();
                    listfile.Add(version);
                }
                if (listfile.Count() > 0)// nếu có trả về link file
                {
                    result.Data = realPath + '/' + listfile.ToList().OrderByDescending(p => p.Version).First().Link;
                    String RelativePath = @"~\" + result.Data.Replace(HttpContext.Current.Request.PhysicalApplicationPath, String.Empty);
                    RelativePath = RelativePath.Replace(@"~\", "/");
                    RelativePath = RelativePath.Replace('\\', '/');
                    string vPath = "https://sp.imark.com.vn/WS" + RelativePath;
                    result.Data = vPath;
                }
                else // nếu không trả về thư mục rỗng
                {
                    result.Data = "Folder Empty!";
                }
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS;
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = ex.Message;
                return Ok(result);
            }
        }
    }
}