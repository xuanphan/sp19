﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;
using Business;


namespace WS.Controllers
{
    public class GetProfileByCodeController : ApiController
    {
        // lấy thông tin nhân viên theo code (bỏ)
        public async Task<IHttpActionResult> Postgetprofile(GetProfileByCodeModel model)
        {
            GetProfileByCodeResult result = new GetProfileByCodeResult();
            //Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Profile.getByCode(model.pAppCode, int.Parse(model.pProjectID), model.pProfileCode, ref result);
            return Ok(result);
        }
    }
}
