﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetPOSMWSController : ApiController
    {
        // GET: /Brand/
        // lấy danh sách POSM theo outlet
        public async Task<IHttpActionResult> GetAll(string pAppCode, string pOutletID)
        {
            // khởi tạo trả về
            POSMResult result = new POSMResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = POSM.GetAll(pAppCode, pOutletID, ref result);
            return Ok(result);
        }
    }
}
