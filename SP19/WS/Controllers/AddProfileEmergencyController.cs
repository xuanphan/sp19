﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;
using WS.Models;
using Newtonsoft.Json;

namespace WS.Controllers
{
    public class AddProfileEmergencyController : ApiController
    {
        // thêm trạng thái khẩn cấp cho nhân viên
        public async Task<IHttpActionResult> PostProfileEmergency(AddProflieEmergencyModel model)
        {
            // khởi tạo trả về
            AddProfileEmergencyResult result = new AddProfileEmergencyResult();

            // khởi tạo object thêm vào db
            Profile_Emergency entity = new Profile_Emergency();
            entity.ProfileEmergencyCode = model.pProflieEmergencyCode;
            entity.OutletID = int.Parse(model.pOutletID);
            entity.EmergencyID = int.Parse(model.pEmergencyID);
            entity.StartDateTime = Common.ConvertUtils.ConvertStringToLongDate(model.pStartDateTime);
            entity.CreatedBy = int.Parse(model.pTeamOutletID);
            entity.CreatedDateTime = DateTime.Now;
            entity.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
            entity.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;

            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = entity.AddNew(model.pAppCode, model.pProfileCode, int.Parse(model.pProjectID), ref result);

            return Ok(result);
        }
    }


}
