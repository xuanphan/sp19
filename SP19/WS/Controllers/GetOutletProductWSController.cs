﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetOutletProductWSController : ApiController
    {
        // GET: /Brand/
        public async Task<IHttpActionResult> GetAll(string pAppCode, string pOutletID)
        {
            string messageSystemError = String.Empty;
            OutletProductResult result = new OutletProductResult();
            Outlet_Product outlet_Product = new Outlet_Product();
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = outlet_Product.GetAll(ref messageSystemError, pAppCode, pOutletID, ref result);
            return Ok(result);
        }
    }
}
