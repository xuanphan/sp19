﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class AddCustomerTotalDialMegaController : ApiController
    {
        // thêm số vòng quay mega đã quay của khách hàng
        public async Task<IHttpActionResult> Postaddtotaldial(AddCustomerTotalDialMegaModel model)
        {
            //khởi tạo trả về
            MainResult result = new MainResult();
            try
            { 
                // khởi tạo list thêm vào db 
                List<Customer_Total_Dial_Mega> list = new List<Customer_Total_Dial_Mega>();
                // Parse chuỗi json ra list object
                IList<WSAddCustomerTotalDialMegaDetail> listEntity = Common.JsonUtils.DeserializeToList<WSAddCustomerTotalDialMegaDetail>(model.pData);
                // chạy vòng lặp list object vừa parse để add vào list thêm
                foreach (var item in listEntity)
                {
                    Customer_Total_Dial_Mega entity = new Customer_Total_Dial_Mega();
                    entity.CustomerID = item.pCustomerID;
                    entity.NumberDial = item.pNumber;
                    entity.CreatedBy = item.pTeamOutletID;
                    entity.CreatedDateTime = DateTime.Now;
                    entity.Status = 1;
                    entity.RowVersion = 1;
                    list.Add(entity);
                }
                Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = LuckyMega.addNumberDialMega(model.pAppCode,list, ref result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Status = -1;
                result.Description = "Lỗi: " + ex.Message;
                return Ok(result);
            }
           
        }
    }
}
