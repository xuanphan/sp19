﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;
using Business;

namespace WS.Controllers
{
    public class AddAttendanceTrackingImageWSController : ApiController
    {
        // chấm công nhân viên + hình ảnh
        public async Task<IHttpActionResult> Postattimage(AddAttendanceTrackingImageWSModel model)
        {
            // khởi tạo trả về
            AddAttendanceTrackingImageResult result = new AddAttendanceTrackingImageResult();
            // vào xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = AttendanceTracking_Image.addNewSimple(model.pAppCode, long.Parse(model.pAttendanceTrackingID), long.Parse(model.pImageID), ref result);
            return Ok(result);
        }
    }
}
