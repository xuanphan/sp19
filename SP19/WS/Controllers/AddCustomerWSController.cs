﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
namespace WS.Controllers
{
    public class AddCustomerWSController : ApiController
    {
        //
        // GET: /AddCustomer/
        // thêm khách hàng
        public async Task<IHttpActionResult> PostData(CustomerModel model)
        {
            // khởi tạo trả về
            AddCustomerResult result = new AddCustomerResult();
            // khởi tạo object thêm vào db
            Business.Customer customer = new Business.Customer();
            // parse chuỗi json ra object
            JavaScriptSerializer jss = new JavaScriptSerializer();
            CustomerDetail customerDetail = jss.Deserialize<CustomerDetail>(model.pData);

            // gán dữ liệu
            customer.OutletID = customerDetail.pOutletID;
            customer.CustomerCode = customerDetail.pCustomerCode;
            customer.CustomerName = customerDetail.pCustomerName;
            customer.CustomerPhone = customerDetail.pCustomerPhone;
            customer.BillNumber = customerDetail.pBillNumber;
            customer.Address = customerDetail.pAddress;
            customer.Note = customerDetail.pNote;
            customer.Sex = customerDetail.pSex;
            customer.Email = customerDetail.pEmail;
            customer.YearOfBirth = customerDetail.pYearOfBirth;
            customer.ReasonBuy = customerDetail.pReasonBuy;
            customer.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(customerDetail.pDeviceDateTime);
            customer.ServerDateTime = DateTime.Now;
            customer.CreatedBy = customerDetail.pTeamOutletID;
            customer.CreatedDateTime = DateTime.Now;
            customer.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
            customer.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
            //vào xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = customer.AddNew(model.pAppCode, ref result);
            return Ok(result);
        }
    }
}