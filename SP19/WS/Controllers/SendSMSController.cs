﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;

namespace WS.Controllers
{
    public class SendSMSController : ApiController
    {
        public static readonly string BrandName = "HEINEKEN_VN";
        public async Task<IHttpActionResult> PostSendSMS(MOModel mo)
        {
            MTResult result = new MTResult();
            string messageSystemError = String.Empty;
            string SessionCode = String.Empty;
            string SourcePhone = String.Empty;
            string SourceSMS = String.Empty;
            string SMSMsg = String.Empty;
            DateTime ReceivedTime = DateTime.Now;

            string returnMsg = String.Empty;

            try
            {
                var consumerip = String.Empty;
                // Kiem tra request thuoc local hay khong
                consumerip = HttpContext.Current.Request.UserHostAddress;
                if (!Common.VNPT_IP.IP.Contains(consumerip))
                {
                    Log_Raw_SM raw = new Log_Raw_SM();
                    raw.cpid = mo.cpId;
                    raw.data = mo.data;
                    raw.CreateDateTime = DateTime.Now;
                    raw.SourceIP = consumerip;
                    raw.AddNew(ref messageSystemError, raw);

                    result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR_IP;
                    result.SMS = Common.EnumUtils.stringValueOf(Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR_IP);
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR_IP);
                    result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR_IP, Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR_IP, messageSystemError);
                    return Ok(result);
                }
                else
                {
                    Log_Raw_SM raw = new Log_Raw_SM();
                    raw.cpid = mo.cpId;
                    raw.data = mo.data;
                    raw.CreateDateTime = DateTime.Now;
                    raw.SourceIP = consumerip;
                    raw.AddNew(ref messageSystemError, raw);

                    //giai ma hoa chuoi gui den
                    var decryptedtext = Common.EncryptionUtility.Decrypt(mo.data, Common.ContentProvider.Encryptkey);

                    string[] plaintextInArray = Common.StringUtils.SubStringInArray(Common.StringUtils.SplitDataString(decryptedtext));
                    SessionCode = plaintextInArray[0].Trim().ToUpper();
                    SourcePhone = plaintextInArray[1].Trim().ToUpper();
                    SourceSMS = plaintextInArray[2].Trim().ToUpper();
                    SMSMsg = plaintextInArray[3].Trim().ToUpper();
                    
                    ReceivedTime = Common.ConvertUtils.ConvertStringToLongDateReport(plaintextInArray[4].Replace("\0", "").Trim());

                    // log data da giai ma
                    Log_Data_SM log = new Log_Data_SM();                    
                    log.SessionCode = SessionCode;
                    log.Phone = SourcePhone;
                    log.SNSMS = SourceSMS;
                    log.SMSSource = SMSMsg;
                    log.ReceivedTime = ReceivedTime;
                    

                    String tmp = plaintextInArray[3].Trim().ToUpper();                 

                    string[] containOfMsg = tmp.Split(' ');                              


                    if (containOfMsg[1].ToString() == "INFO") // tra cuu diem
                    {
                        if (containOfMsg.Length > 2) // sai cu phap
                        {
                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                            result.SMS = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                            result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS, Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS, messageSystemError);

                            log.ErrorCodeType = "ERROR_CODE_INVALID";
                            log.ErrorCode = (int)Business.ReturnMessage.ERROR_CODE_INVALID.INVALID;
                            log.DescriptionError = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                            Log_Data_SM.AddDataSMS(ref messageSystemError, log);
                            return Ok(result);
                        }
                        else
                        {
                            Business.ReturnMessage.ERROR_CODE_INFO errorCode = ReturnMessage.ERROR_CODE_INFO.SUCCESS;
                            Customer.GetCustomerInfo(ref messageSystemError, SourcePhone, ref returnMsg, ref errorCode);

                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                            result.SMS = returnMsg;
                            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                            result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS, Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS, messageSystemError);

                            log.ErrorCodeType = "ERROR_CODE_INFO";
                            log.ErrorCode = (int) errorCode;
                            log.DescriptionError = returnMsg;
                            Log_Data_SM.AddDataSMS(ref messageSystemError, log);
                        }
                    }
                    else if (containOfMsg[1].ToString() == "DOI") // doi qua
                    {
                        int quantiy = int.Parse(containOfMsg[2]);
                        string address = string.Join(" ", containOfMsg, 3, containOfMsg.Length - 3);

                        if (quantiy <= 0) // ngan ngua cheat quantity
                        {
                            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                            result.SMS = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                            result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, messageSystemError);

                            log.ErrorCodeType = "ERROR_CODE_INVALID";
                            log.ErrorCode = (int)Business.ReturnMessage.ERROR_CODE_INVALID.INVALID;
                            log.DescriptionError = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                            Log_Data_SM.AddDataSMS(ref messageSystemError, log);

                            return Ok(result);
                        }

                        ReturnMessage.ERROR_CODE_EXCHANGE errorCode = ReturnMessage.ERROR_CODE_EXCHANGE.SUCCESS;
                        Log_Exchange_Present exchange = new Log_Exchange_Present();
                        exchange.Phone = SourcePhone;
                        exchange.Quantity = quantiy;
                        exchange.Description = address;
                        exchange.DateTime = ReceivedTime;
                        Customer.AddExchangePresent(ref messageSystemError, exchange, ref returnMsg, ref errorCode);

                        log.ErrorCodeType = "ERROR_CODE_EXCHANGE";
                        log.ErrorCode = (int)errorCode;
                        log.DescriptionError = returnMsg;
                        Log_Data_SM.AddDataSMS(ref messageSystemError, log);

                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                        result.SMS = returnMsg;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                        result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS, Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS, messageSystemError);

                    }
                    else if (containOfMsg[0] != "HNKTET")   // Sai tu khoa
                    {
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                        result.SMS = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                        result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, messageSystemError);

                        log.ErrorCodeType = "ERROR_CODE_INVALID";
                        log.ErrorCode = (int) Business.ReturnMessage.ERROR_CODE_INVALID.INVALID;
                        log.DescriptionError = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);

                        Log_Data_SM.AddDataSMS(ref messageSystemError, log);
                    }
                    else // tich luy diem
                    {
                        for (int i = 0; i < containOfMsg.Length; i++)// Trim cac phan tu
                        {
                            containOfMsg[i] = containOfMsg[i].Trim();
                            if (containOfMsg[i] == String.Empty)    // phat hien co ky tu rong
                            {
                                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                                result.SMS = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                                result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, messageSystemError);

                                log.ErrorCodeType = "ERROR_CODE_INVALID";
                                log.ErrorCode = (int)Business.ReturnMessage.ERROR_CODE_INVALID.INVALID;
                                log.DescriptionError = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                                Log_Data_SM.AddDataSMS(ref messageSystemError, log);

                                return Ok(result); 
                            }
                        }    

                        if (containOfMsg.Length == 1 || containOfMsg.Length > 4) // sai cu phap nhan tin
                        {
                            result.Status = (int) Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                            result.SMS = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                            result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS);
                            result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, messageSystemError);

                            log.ErrorCodeType = "ERROR_CODE_INVALID";
                            log.ErrorCode = (int) Business.ReturnMessage.ERROR_CODE_INVALID.INVALID;
                            log.DescriptionError = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                            Log_Data_SM.AddDataSMS(ref messageSystemError, log);

                            return Ok(result);
                        }

                      
                        List<Customer> list = new List<Customer>();
                        for (int i = 1; i < containOfMsg.Length; i++)
                        {
                            Customer cus = new Customer();
                            cus.Phone = SourcePhone;
                            cus.LuckyCode = containOfMsg[i].ToString();
                            cus.DateTime = ReceivedTime;
                            list.Add(cus);
                        }

                        ReturnMessage.ERROR_CODE_SAVE_UP errrorCode = ReturnMessage.ERROR_CODE_SAVE_UP.NOT_EXIST;

                        Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Customer.AddNew(ref messageSystemError, list, ref returnMsg, ref errrorCode);
                        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                        result.SMS = returnMsg;
                        result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                        result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, messageSystemError);

                        log.ErrorCodeType = "ERROR_CODE_SAVE_UP";
                        log.ErrorCode = (int) errrorCode;
                        log.DescriptionError = returnMsg;
                        Log_Data_SM.AddDataSMS(ref messageSystemError, log);
                    }
                }

                if (DateTime.Now >= DeadLine.GetDeadLineTime)
                {
                    result.Status = (int) Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SUCCESS;
                    result.SMS = DeadLine.GetDeadLineMessage;
                    result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS);
                    result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SUCCESS, messageSystemError);
                }

            }
            catch (Exception ex)
            {
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.SMS = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                result.Description = Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR);
                result.GetResultStatus((int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR, Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR, ex.ToString());

                Log_Data_SM log = new Log_Data_SM();
                log.SessionCode = SessionCode;
                log.Phone = SourcePhone;
                log.SNSMS = SourceSMS;
                log.SMSSource = SMSMsg;
                log.ReceivedTime = ReceivedTime;

                log.ErrorCodeType = "ERROR_CODE_INVALID";
                log.ErrorCode = (int)Business.ReturnMessage.ERROR_CODE_INVALID.INVALID;
                log.DescriptionError = Common.EnumUtils.stringValueOf(Business.ReturnMessage.ERROR_CODE_INVALID.INVALID);
                Log_Data_SM.AddDataSMS(ref messageSystemError, log);                       
            }
            return Ok(result);
        }
    }
}
