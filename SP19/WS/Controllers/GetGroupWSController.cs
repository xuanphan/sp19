﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetGroupWSController : ApiController
    {
        // GET: /Brand/
        // lấy danh sách nhóm siêu thị
        public async Task<IHttpActionResult> GetAll(string pAppCode,string pProjectID)
        {
            // khởi tạo trả về
            GroupResult result = new GroupResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Group.GetAll(pAppCode, int.Parse(pProjectID), ref result);
            return Ok(result);
        }
    }
}
