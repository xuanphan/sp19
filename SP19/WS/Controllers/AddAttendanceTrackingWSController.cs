﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;

namespace WS.Controllers
{
    public class AddAttendanceTrackingWSController : ApiController
    {
        //
        // GET: /AttendanceTracking/
        // chấm công nhân viên
        public async Task<IHttpActionResult> PostData(AttendanceTrackingModel model)
        {
            AddNewResult result = new AddNewResult();
            // khởi tạo object
            AttendanceTracking attendancetracking = new AttendanceTracking();
            try
            {
                //gán dữ liệu từ model
                attendancetracking.TeamOutletID = int.Parse(model.pTeamOutletID);
                attendancetracking.AttendanceTrackingCode = model.pAttendanceTrackingCode;
                attendancetracking.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(model.pDeviceDateTime);
                attendancetracking.TimePointType = model.pTimePointType;
                attendancetracking.LatGPS = double.Parse(model.pLatGPS);
                attendancetracking.LongGPS = double.Parse(model.pLongGPS);
                attendancetracking.NumberPG = model.pNumberPG;
                // gán xong vào xử lý để add vào db
                Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = attendancetracking.AddNew(model.pAppCode, ref result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Data = "-1";
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = ex.Message;
                return Ok(result);
            }
        }
    }
}