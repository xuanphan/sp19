﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;
using Business;

namespace WS.Controllers
{
    public class GetOutletBrandWSController : ApiController
    {
        // lấy danh sách các nhãn hàng chạy theo outlet
        public async Task<IHttpActionResult> Postgetoutletbrand(GetOutletBrandWSModel model)
        {
            // khởi tạo trả về
            GetOutletBrandResult result = new GetOutletBrandResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Outlet_Brand.getByOutlet(model.pAppCode, int.Parse(model.pOutletID), ref result);
            return Ok(result);
        }
    }
}
