﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class CheckAccessTokenController : ApiController
    {
        public async Task<IHttpActionResult> PostCheckAccessToken(CheckAccessTokenModel model)
        {
            CheckAccessTokenResult result = new CheckAccessTokenResult();
            Business.User user = new Business.User();
            Common.ERROR_CODE.ERROR_CODE_LOGIN error = user.checkAccessToken(model.pToken, ref result);
            return Ok(result);
        }
    }
}
