﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class AddFolderToDriverController : ApiController
    {
        // xử lý tạo folder trên gg driver
        public async Task<IHttpActionResult> Postaddfolder()
        {
            string[] scopes = new string[] { DriveService.Scope.Drive,
                             DriveService.Scope.DriveFile};
            var clientId = Common.Global.ClientIDDriver;
            var clientSecret = Common.Global.KeySecretDriver;

            var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(new ClientSecrets
            {
                ClientId = clientId,
                ClientSecret = clientSecret
            },
            scopes,
                                                                    Environment.UserName,
                                                                    CancellationToken.None,
                                                                    new FileDataStore("MyAppsToken")).Result;


            DriveService service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "application Name",
            });

            // add project
            //var folder = new Google.Apis.Drive.v3.Data.File();
            //folder.Name = SP19;
            //folder.Description = "project2019";
            //folder.MimeType = "application/vnd.google-apps.folder";
            //var request = service.Files.Create(folder);
            //   request.Fields = "id";
            //   var fileFol = request.Execute();
            //   string ID = fileFol.Id

            // add năm
            //List<string> paren = new List<string>();
            //paren.Add(Common.Global.FolderIDProject);
            //var folder = new Google.Apis.Drive.v3.Data.File();
            //folder.Name = 2019;
            //folder.Description = "monthofyear2019";
            //folder.MimeType = "application/vnd.google-apps.folder";
            //folder.Parents = paren;
            //var request = service.Files.Create(folder);
            //   request.Fields = "id";
            //   var fileFol = request.Execute();
            //   string ID = fileFol.Id


            // add foder tháng 
            //List<string> paren = new List<string>();
            //paren.Add(Common.Global.FolderIDNam2019);
            //for (int i = 1; i <= 12; i++)
            //{
            //    var folder = new Google.Apis.Drive.v3.Data.File();
            //    folder.Name = i+"";
            //    folder.Description = "monthofyear2019";
            //    folder.MimeType = "application/vnd.google-apps.folder";
            //    folder.Parents = paren;

            //    var request = service.Files.Create(folder);
            //    request.Fields = "id";
            //    var fileFol = request.Execute();
            //    string ID = fileFol.Id;
            //    DateTime dateMonth = new DateTime(2019, i, 1);
            //    DataKeyDriver entity = new DataKeyDriver();
            //    entity.NgayLam = dateMonth;
            //    entity.CapDo = 0;
            //    entity.KeyFolder = ID;
            //    string a = DataKeyDriver.addMonthOfYear(entity);
            //}


            // forder ngày theo tháng
            //List<DataKeyDriver> listMonth = DataKeyDriver.getListMonthOfYear(2019);
            //foreach (var item in listMonth)
            //{
            //    List<string> paren = new List<string>();
            //    paren.Add(item.KeyFolder);
            //    List<DataKeyDriver> listDetail = new List<DataKeyDriver>();
            //    for (int i = 1; i <= DateTime.DaysInMonth(item.NgayLam.Year, item.NgayLam.Month); i++)
            //    {
            //        var folder = new Google.Apis.Drive.v3.Data.File();
            //        folder.Name = i + "";
            //        folder.Description = "dayofmonth2019";
            //        folder.MimeType = "application/vnd.google-apps.folder";
            //        folder.Parents = paren;
            //        var request = service.Files.Create(folder);
            //        request.Fields = "id";
            //        var fileFol = request.Execute();
            //        string ID = fileFol.Id;

            //        DataKeyDriver entity = new DataKeyDriver();
            //        entity.NgayLam = new DateTime(item.NgayLam.Year, item.NgayLam.Month, i);
            //        entity.CapDo = 1;
            //        entity.KeyFolder = ID;
            //        listDetail.Add(entity);
            //    }
            //    string a = DataKeyDriver.addDaysOfMonth(listDetail);
            //}



            return Ok("Ok");

        }

    }
}
