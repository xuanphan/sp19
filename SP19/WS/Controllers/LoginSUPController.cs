﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;
using WS.Models;

namespace WS.Controllers
{
    public class LoginSUPController : ApiController
    {
        // đăng nhập app SUP
        public async Task<IHttpActionResult> Postloginsup(LoginSUPModel model)
        {
            // khởi tạo trả về
            LoginSUPResult result = new LoginSUPResult();
            Business.User user = new Business.User();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_LOGIN error = user.LoginSUP(model.pUserName, model.pPassWord, model.pUserType, model.pAppCode, model.pDeviceToken, ref result);
            return Ok(result);

        }
    }
}
