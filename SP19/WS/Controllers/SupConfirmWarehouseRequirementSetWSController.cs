﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;

namespace WS.Controllers
{
    public class SupConfirmWarehouseRequirementSetWSController : ApiController
    {
        //
        // GET: /AddCustomer/
        // SUP xác nhận nhận hàng từ kho giao đến thành công
        public async Task<IHttpActionResult> PostData(SupConfirmWarehouseRequirementSetWSModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Warehouse_Requirement_Set.supConfirm( model.pAppCode, long.Parse(model.pWarehouseRequirementID), int.Parse(model.pUserID), ref result);
            return Ok(result);
        }
    }
}