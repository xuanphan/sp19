﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;

namespace WS.Controllers
{
    public class AddPOSMWSController : ApiController
    {
        //
        // GET: /AddCustomer/
        // thêm báo cáo số lượng POSM theo outlet
        public async Task<IHttpActionResult> PostData(POSMModel model)
        {
            //khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db 
            List<POSM_Report> list = new List<POSM_Report>();
            // Parse chuỗi json ra list object
            string json = model.pData.Replace("\n", "").Replace(" ", String.Empty);
            POSMDetail[] items = JsonConvert.DeserializeObject<POSMDetail[]>(json);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in items)
            {
                POSM_Report entity = new POSM_Report();
                entity.OutletID = int.Parse(item.pOutletID);
                entity.Number = int.Parse(item.pNumber);
                entity.POSMID = int.Parse(item.pPOSMID);
                entity.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(item.pDeviceDateTime);
                entity.ServerDateTime = DateTime.Now;
                entity.CreatedBy = int.Parse(item.pTeamOutletID);
                entity.CreatedDateTime = DateTime.Now;
                entity.Status = 1;
                entity.RowVersion = 1;
                list.Add(entity);
            }

            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = POSM_Report.AddNew(model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}