﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class UpdateCurrentGiftSUPWSController : ApiController
    {
        // cập nhật quà hiện tại của outlet theo sup : APP SUP
        public async Task<IHttpActionResult> Postupdate(UpdateCurrentGiftSUPWSModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // Parse chuỗi json ra list object
            IList<OutletCurrenrGiftSUPDetail> listEntity = Common.JsonUtils.DeserializeToList<OutletCurrenrGiftSUPDetail>(model.pData);
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE error = Sup_Outlet_Current_Gift.update(model.pAppCode, listEntity, ref result);

            return Ok(result);
        }
    }
}
