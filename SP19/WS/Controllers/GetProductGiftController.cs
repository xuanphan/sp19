﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace WS.Controllers
{
    public class GetProductGiftController : ApiController
    {
        // lấy danh sách quà đổi trực tiếp theo sản phẩm
        public async Task<IHttpActionResult> Getall(string pAppCode, string pProjectID)
        {
            // khởi tạo trả về
            GetProductGiftResult result = new GetProductGiftResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Product_Gift.getAll(pAppCode, int.Parse(pProjectID), ref result);
            return Ok(result);
        }
    }
}
