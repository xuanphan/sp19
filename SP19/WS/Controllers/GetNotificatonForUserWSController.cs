﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;
using WS.Models;

namespace WS.Controllers
{
    public class GetNotificatonForUserWSController : ApiController
    {
        // lấy danh sách thông báo đến user
        public async Task<IHttpActionResult> Postgetbyuser(GetNotificatonForUserWSModel model)
        {
            // khởi tạo trả về
            GetNotificationForUserResult  result= new GetNotificationForUserResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = User_App_Notification.getByID(model.pAppCode, int.Parse(model.pUserID), ref result);
            return Ok(result);
        }
    }
}
