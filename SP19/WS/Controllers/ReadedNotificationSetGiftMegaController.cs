﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class ReadedNotificationSetGiftMegaController : ApiController
    {
        // xác nhận đã nhận được thông báo từ  set quà trúng mega
        public async Task<IHttpActionResult> Postreaded(ReadedNotificationSetGiftMegaModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = LuckyMega.setReaded(model.pAppCode,model.pOutletID,model.pGiftID,model.pTeamOutletID,ref result);
            return Ok(result);
        }
    }
}
