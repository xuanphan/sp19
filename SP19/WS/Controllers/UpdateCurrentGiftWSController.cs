﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class UpdateCurrentGiftWSController : ApiController
    {
        // cập nhật quà hiện tại trong set theo outlet : APP SP
        public async Task<IHttpActionResult> Postupdate(UpdateCurrentGiftWSModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // Parse chuỗi json ra list object
            IList<OutletCurrenrGiftDetail> listEntity = Common.JsonUtils.DeserializeToList<OutletCurrenrGiftDetail>(model.pData);
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_UPDATE error = Outlet_Current_Gift.update(model.pAppCode, listEntity, ref result);

            return Ok(result);
        }
    }
}
