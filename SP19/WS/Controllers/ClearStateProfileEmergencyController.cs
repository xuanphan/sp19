﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class ClearStateProfileEmergencyController : ApiController
    {
        // chạy tự động chuyển trạng thái khẩn cấp nhân viên về bình thường (set chạy tự động schedule task trên plesk)
        public async Task<IHttpActionResult> GetClearState()
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Profile_Emergency.clear(ref result);
            return Ok(result);
        }
    }
}
