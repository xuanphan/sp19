﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{

    public class GetWarehouseRequirementBySupWSController : ApiController
    {
        // lấy danh sách các yêu cầu giao thêm set quà chưa nhận : APP SUP
        public async Task<IHttpActionResult> Postgetall(GetWarehouseRequirementBySupWSModel model)
        {
            // khởi tạo trả về
            GetWarehouseRequirementResult result = new GetWarehouseRequirementResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Warehouse_Requirement_Set.getALLBySup(model.pAppCode,int.Parse(model.pSupID), ref result);
            return Ok(result);
        }
    }
}
