﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Business;
using System.Threading.Tasks;
using WS.Models;


namespace WS.Controllers
{
    public class AddBrandSetUsedSUPWSController : ApiController
    {
        // thêm số lượng set quà quay hết tại outlet đối với app SUP
        public async Task<IHttpActionResult> Postaddbrandsetused(AddBrandSetUsedSUPModel model)
        {
            //khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db 
            List<Sup_Outlet_Brand_Set_Used> list = new List<Sup_Outlet_Brand_Set_Used>();
            // Parse chuỗi json ra list object
            IList<BrandSetUsedSUPData> listEntity = Common.JsonUtils.DeserializeToList<BrandSetUsedSUPData>(model.pData);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in listEntity)
            {
                Sup_Outlet_Brand_Set_Used temp = new Sup_Outlet_Brand_Set_Used();
                temp.SupID = item.pSupID;
                temp.OutletID = item.pOutletID;
                temp.BrandSetID = item.pBrandSetID;
                temp.Number = item.pNumber;
                temp.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(item.pDeviceDateTime);
                temp.CreatedBy = item.pSupID;
                temp.CreatedDateTime = DateTime.Now;
                temp.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                temp.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                list.Add(temp);
            }
            // vào xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Sup_Outlet_Brand_Set_Used.add(model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}
