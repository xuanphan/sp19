﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;
using Business;

namespace WS.Controllers
{
    public class AddOutletImageForSupWSController : ApiController
    {
        // thêm danh sách hình ảnh chụp tại outlet đối với app SUP
        public async Task<IHttpActionResult> Postaddoutletimage(AddOutletImageForSupWSModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db
            List<Outlet_Image_Of_Sup> list = new List<Outlet_Image_Of_Sup>();
            // Parse chuỗi json ra list object
            IList<OutletImageForSupDetail> listEntity = Common.JsonUtils.DeserializeToList<OutletImageForSupDetail>(model.pData);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in listEntity)
            {
                Outlet_Image_Of_Sup entity = new Outlet_Image_Of_Sup();
                entity.OutletID = item.pOutletID;
                entity.ImageID = item.pImageID;
                entity.ImageType = item.pImageType;
                entity.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDateReport(item.pDeviceDateTime);
                entity.CreatedBy = item.pSupID;
                entity.CreatedDateTime = DateTime.Now;
                entity.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                entity.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                list.Add(entity);
            }
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Outlet_Image_Of_Sup.add(model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}
