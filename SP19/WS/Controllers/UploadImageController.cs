﻿using Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using WS.Models;
using Google.Apis.Drive.v3;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using Google.Apis.Drive.v3.Data;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;


namespace WS.Controllers
{
    public class UploadImageController : Controller
    {
        // tải hình ảnh lên hệ thống
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult UploadFile()
        {
            return View();
        }

        //[HttpPost]
        //public JsonResult UploadFile(string rootUrl, ImageModel model)
        //{
        //    AddImageResult result = new AddImageResult();
        //    try
        //    {
        //        string _FilePath = String.Empty;
        //        string imageUrl = String.Empty;
        //        string imagePath = String.Empty;
        //        string imageName = String.Empty;

        //        HttpPostedFileBase file = Request.Files["file"];
        //        var tmp = file.InputStream;

        //        long Id_Return = -1;
        //        string list = String.Empty;
        //        string namefilepath = String.Empty;
        //        namefilepath = model.pFileName;
        //        string _messagererror = String.Empty;
        //        _FilePath = model.pTeamOutletID + "/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + model.pImageType + "/";
        //        DataKeyDriver folder = DataKeyDriver.getByDayOfMonth(DateTime.Now);
        //        if (folder == null)
        //        {
        //            result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
        //            result.Description = "Không tìm thấy folder lưu hình.";
        //            return this.Json(result);
        //        }
        //        for (int i = 0; i < Request.Files.Count; i++)
        //        {
        //        //    string[] scopes = new string[] { DriveService.Scope.Drive,
        //        //                 DriveService.Scope.DriveFile};
        //        //    var clientId = Common.Global.ClientIDDriver;
        //        //    var clientSecret = Common.Global.KeySecretDriver;

        //        //    var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(new ClientSecrets
        //        //    {
        //        //        ClientId = clientId,
        //        //        ClientSecret = clientSecret
        //        //    },
        //        //    scopes,
        //        //                                                            "FIHU",
        //        //                                                            CancellationToken.None,
        //        //                                                            new FileDataStore(Request.PhysicalApplicationPath + "MyAppsToken")).Result;


        //        //    DriveService service = new DriveService(new BaseClientService.Initializer()
        //        //    {
        //        //        HttpClientInitializer = credential,
        //        //        ApplicationName = "application Name",
        //        //    });

        //        //    string LinkImage = uploadFile(folder.KeyFolder, service, tmp, Request.Files[0].FileName, Request.Files[0].ContentType, ref _messagererror);
        //        //    if (LinkImage != "-1")
        //        //    {
        //        //        Image fieldImage = new Image();
        //        //        fieldImage.ImageCode = model.pImageCode;
        //        //        fieldImage.FileName = Request.Files[0].FileName;
        //        //        fieldImage.FilePath = LinkImage;
        //        //        fieldImage.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(model.pDeviceDateTime);
        //        //        fieldImage.LatGPS = double.Parse(model.pLatGPS);
        //        //        fieldImage.LongGPS = double.Parse(model.pLongGPS);
        //        //        Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = fieldImage.AddNew(ref  _messagererror, model.pTeamOutletID, model.pAppCode, ref result);
        //        //    }
        //        //    else
        //        //    {
        //        //        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
        //        //        result.Description = _messagererror + "\n";
        //        //    }
        //        //}
        //        //return this.Json(result);

        //            Business.Connection.CreateImagePath(Request.PhysicalApplicationPath, Request.Url.Authority, "Image2018", _FilePath, out imageUrl);
        //            imagePath = Request.PhysicalApplicationPath + "/Image2018/" + _FilePath;
        //            Directory.CreateDirectory(imagePath);

        //            if (System.IO.File.Exists(imagePath + model.pFileName))
        //            {
        //                namefilepath = CodeMD5.MD5Hash(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss tt")) + model.pTeamOutletID + "-" + model.pFileName;

        //                Request.Files[i].SaveAs(imagePath + namefilepath);
        //                Image fieldImage = new Image();
        //                fieldImage.ImageCode = model.pImageCode;
        //                fieldImage.FileName = namefilepath;
        //                fieldImage.FilePath = imageUrl.ToStringWithSuffix("", namefilepath);
        //                fieldImage.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(model.pDeviceDateTime);
        //                fieldImage.LatGPS = double.Parse(model.pLatGPS);
        //                fieldImage.LongGPS = double.Parse(model.pLongGPS);

        //                Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = fieldImage.AddNew(ref  _messagererror, model.pTeamOutletID, model.pAppCode, ref result);
        //                list += Id_Return.ToString() + "|";
        //            }
        //            else
        //            {
        //                Request.Files[i].SaveAs(imagePath + namefilepath);
        //                Image fieldImage = new Image();
        //                fieldImage.ImageCode = model.pImageCode;
        //                fieldImage.FileName = namefilepath;
        //                fieldImage.FilePath = imageUrl.ToStringWithSuffix("", namefilepath);
        //                fieldImage.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(model.pDeviceDateTime);
        //                fieldImage.LatGPS = double.Parse(model.pLatGPS);
        //                fieldImage.LongGPS = double.Parse(model.pLongGPS);

        //                Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = fieldImage.AddNew(ref  _messagererror, model.pTeamOutletID, model.pAppCode, ref result);
        //                list += Id_Return.ToString() + "|";
        //            }
        //        }
        //        return this.Json(result);


        //    }
        //    catch (Exception ex)
        //    {
        //        result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
        //        result.Description = ex.Message;
        //        return this.Json(result);
        //    }
        //}


        [HttpPost]
        public JsonResult UploadFile(string rootUrl, ImageModel model)
        {
            // khởi tạo trả về
            AddImageResult result = new AddImageResult();
            try
            {
                string _FilePath = String.Empty;
                string imageUrl = String.Empty;
                string imagePath = String.Empty;
                 
                // lấy file từ biến file tải lên
                HttpPostedFileBase file = Request.Files["file"];

                string namefilepath = model.pFileName;
                byte[] data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, (int)file.InputStream.Length);

                Stream bakupStream = new MemoryStream(data);

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    // tạo path lưu hình
                    Business.Connection.CreateImagePath(Request.PhysicalApplicationPath, Request.Url.Authority, "Image2018" + "/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day, _FilePath, out imageUrl);
                    imagePath = Request.PhysicalApplicationPath + "/Image2018/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day +"/" + _FilePath;
                    Directory.CreateDirectory(imagePath);

                    if (System.IO.File.Exists(imagePath + model.pFileName))
                    {
                        namefilepath = model.pTeamOutletID + "-" + model.pFileName;
                    }

                    // khởi tạo object thêm vào db
                    Image fieldImage = new Image();
                    // gán dữ liệu
                    fieldImage.ImageCode = model.pImageCode;
                    fieldImage.FileName = namefilepath;
                    fieldImage.FilePath = imageUrl.ToStringWithSuffix("", namefilepath);
                    fieldImage.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(model.pDeviceDateTime);
                    fieldImage.LatGPS = double.Parse(model.pLatGPS);
                    fieldImage.LongGPS = double.Parse(model.pLongGPS);
                    fieldImage.CreatedBy = int.Parse(model.pTeamOutletID);
                    fieldImage.CreatedDateTime = DateTime.Now;
                    fieldImage.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    fieldImage.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                    // vào xử lý để thêm vào db
                    Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = fieldImage.AddNewDriver(fieldImage, model.pAppCode, ref result);

                    // lưu hình vào server
                    using (var fileStream = System.IO.File.Create(imagePath + namefilepath))
                    {
                        bakupStream.Seek(0, SeekOrigin.Begin);
                        bakupStream.CopyTo(fileStream);
                    }
                    // tải thẳng hình lên gg driver . hiện tại đóng vì chưa tối ưu hóa được
                    //Task.Run(() =>
                    //{
                    //    try
                    //    {
                    //        var client = new RestClient("http://gdrive250.imark.com.vn");
                    //        var request = new RestRequest("UploadGDrive/UploadFile", Method.POST);
                    //        request.AddParameter("pProjectName", "SPDB");
                    //        request.AddParameter("pTeamUserID", model.pTeamOutletID);
                    //        request.AddFile("file", data, model.pFileName, file.ContentType);

                    //        IRestResponse response = client.Execute(request);
                    //        var content = response.Content;

                    //        GDriverImageModel jsonResult = JsonConvert.DeserializeObject<GDriverImageModel>(content);
                    //        if (jsonResult.Status == 1)
                    //        {
                    //            Image.UpdateImage(fieldImage, jsonResult.Link);
                    //            System.IO.File.Delete(imagePath + namefilepath);
                    //        }
                    //        else
                    //        {
                    //            //using (var fileStream = System.IO.File.Create(imagePath + namefilepath))
                    //            //{
                    //            //    bakupStream.Seek(0, SeekOrigin.Begin);
                    //            //    bakupStream.CopyTo(fileStream);
                    //            //}
                    //        }
                    //    }
                    //    catch
                    //    {
                    //        //using (var fileStream = System.IO.File.Create(imagePath + namefilepath))
                    //        //{
                    //        //    bakupStream.Seek(0, SeekOrigin.Begin);
                    //        //    bakupStream.CopyTo(fileStream);
                    //        //}
                    //    }
                    //});
                }
                return this.Json(result);
            }
            catch (Exception ex)
            {
                // gán giá trị cho phần trả về
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description =Common.EnumUtils.stringValueOf((Enum)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR)+ ex.Message;
                return this.Json(result);
            }
        }

        public static string uploadFile(string parentid, DriveService _service, Stream stream, string filename, string mimeType, ref string _messagererror)
        {
            Permission perm = new Permission();
            perm.Role = "reader";
            perm.Type = "anyone";

            var body = new Google.Apis.Drive.v3.Data.File();
            List<string> parent = new List<string>();
            parent.Add(parentid);

            body.Name = System.IO.Path.GetFileName(filename);
            body.Description = "image";
            body.MimeType = mimeType;
            body.Parents = parent;
            //body.Permissions = listPermission;


            FilesResource.CreateMediaUpload request;
            try
            {
                request = _service.Files.Create(body, stream, body.MimeType);
                request.Fields = "id";
                request.Upload();

                var file = request.ResponseBody;
                var link = file.Id;

                _service.Permissions.Create(perm, parentid).Execute();

                return "https://drive.google.com/uc?id=" + link;
            }
            catch (Exception ex)
            {
                _messagererror = ex.Message;
                return "-1";
            }
        }
    }
}
