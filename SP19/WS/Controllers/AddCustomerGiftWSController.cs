﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;
using Newtonsoft.Json;
namespace WS.Controllers
{
    public class AddCustomerGiftWSController : ApiController
    {
        //
        // GET: /AddCustomer/
        // thêm danh sách quà trúng của khách hàng
        public async Task<IHttpActionResult> PostData(CustomerGiftModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db 
            List<Customer_Gift> list = new List<Customer_Gift>();
            // Parse chuỗi json ra list object
            CustomerGiftDetail[] items = JsonConvert.DeserializeObject<CustomerGiftDetail[]>(model.pData);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in items)
            {
                Customer_Gift entity = new Customer_Gift();
                entity.CustomerID = long.Parse(item.pCustomerID);
                entity.GiftID = int.Parse(item.pGiftID);
                entity.NumberGift = int.Parse(item.pNumberGift);
                entity.CreatedBy = int.Parse(item.pTeamOutletID);
                entity.CreatedDateTime = DateTime.Now;
                entity.Status = 1;
                entity.RowVersion = 1;
                list.Add(entity);
            }
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Customer_Gift.AddNew( model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}