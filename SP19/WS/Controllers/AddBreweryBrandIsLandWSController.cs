﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;

namespace WS.Controllers
{
    public class AddBreweryBrandIsLandWSController : ApiController
    {
        //
        // GET: /AddCustomer/
        // thêm báo cáo island (ụ kệ)
        public async Task<IHttpActionResult> PostData(BreweryBrandIsLandModel model)
        {
            //khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db
            List<Brewery_Brand_IsLand> list = new List<Brewery_Brand_IsLand>();
            // Parse chuỗi json ra list object
            IsLandDetail[] items = JsonConvert.DeserializeObject<IsLandDetail[]>(model.pData);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in items)
            {
                Brewery_Brand_IsLand entity = new Brewery_Brand_IsLand();
                entity.OutletID = int.Parse(item.pOutletID);
                entity.BreweryBrandID = int.Parse(item.pBreweryBrandID);
                entity.OutsideColumn = int.Parse(item.pOutsideColumn);
                entity.InsideColumn = int.Parse(item.pInsideColumn);
                entity.InsidePalletColumn = int.Parse(item.pInsidePalletColumn);
                entity.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(item.pDeviceDateTime);
                entity.CreatedBy = int.Parse(item.pSupID);
                entity.CreatedDateTime = DateTime.Now;
                entity.Status = 1;
                entity.RowVersion = 1;
                list.Add(entity);
            }
            // vào xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Brewery_Brand_IsLand.AddNew(model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}