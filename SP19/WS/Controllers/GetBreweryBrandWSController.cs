﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetBreweryBrandWSController : ApiController
    {
        // GET: /Brand/
        // lấy danh sách các nhãn hàng chạy theo các nhà máy beer (báo cáo island) dùng cho app SUP
        public async Task<IHttpActionResult> GetAll(string pAppCode,string pProjectID)
        {
            // khởi tạo trả về
            Brewery_BrandResult result = new Brewery_BrandResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Brewery_Brand.GetAll(pAppCode, int.Parse(pProjectID), ref result);
            return Ok(result);
        }
    }
}
