﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;
using Business;

namespace WS.Controllers
{
    // hoàn thành khẩn cấp cho nhân viên
    public class CompleteProfileEmergencyController : ApiController
    {
        public async Task<IHttpActionResult> PostProfileEmergency(CompleteProfileEmergencyModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Profile_Emergency.complete(model.pAppCode, long.Parse(model.pProfileEmergencyID), int.Parse(model.pTeamOutletID), ref result);
            return Ok(result);
        }
    }
}
