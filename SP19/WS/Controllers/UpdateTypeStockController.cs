﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using WS.Models;

namespace WS.Controllers
{
    public class UpdateTypeStockController : ApiController
    {
        // cập nhật lại loại báo cáo tồn kho của outlet theo ngày
        public async Task<IHttpActionResult> Postupdate(UpdateTypeStockModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // parse chuối json ra object
            JavaScriptSerializer jss = new JavaScriptSerializer();
            UpdateTypeDetail updateTypeDetail = jss.Deserialize<UpdateTypeDetail>(model.pData);
            // vào xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Stock.updateType(model.pAppCode, updateTypeDetail.pDeviceDateTime, updateTypeDetail.pOutletID, updateTypeDetail.pNumberType, ref result);
            return Ok(result);
        }
    }
}
