﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Business;
using System.Threading.Tasks;

namespace WS.Controllers
{
    public class ClearDatabaseController : ApiController
    {
        // xóa dữ liệu trên database (chỉ dùng khi test)
        public async Task<IHttpActionResult> GetClearDataP2P()
        {
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = ClearDB.delete();
            return Ok();
        }
    }
}
