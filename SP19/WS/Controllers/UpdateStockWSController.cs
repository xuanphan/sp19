﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;
using Newtonsoft.Json;
namespace WS.Controllers
{
    public class UpdateStockWSController : ApiController
    {
        //
        // GET: /UpdateStock/
        // GET api/<controller>
        // tải danh sách báo cáo tồn kho của các sản phẩm theo outlet
        public async Task<IHttpActionResult> PostData(StockModel model)
        {
            //khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db 
            List<Stock> list = new List<Stock>();
            // Parse chuỗi json ra list object
            StockDetailWS[] items = JsonConvert.DeserializeObject<StockDetailWS[]>(model.pData);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in items)
            {
                Stock entity = new Stock();
                entity.StockCode = item.pStockCode;
                entity.OutletID = int.Parse(item.pOutletID.ToString());
                entity.ProductID = int.Parse(item.pProductID);
                entity.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(item.pDeviceDateTime);
                entity.ServerDateTime = DateTime.Now;
                entity.NumberSP = int.Parse(item.pNumber);
                entity.NumberType = int.Parse(item.pNumberType);
                entity.StatusOOS = 0;
                entity.CreatedBy = int.Parse(item.pTeamOutletID);
                entity.CreatedDateTime = DateTime.Now;
                entity.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                entity.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                list.Add(entity);
            }
            // vào xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Stock.AddNew(model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}
