﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;
using Newtonsoft.Json;
namespace WS.Controllers
{
    public class AddOutletEmergencyWSController : ApiController
    {
        //
        // GET: /AddCustomer/
        // báo cáo khẩn cấp cho outlet
        public async Task<IHttpActionResult> PostData(OutletEmergencyModel model)
        {
            //khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db 
            List<Outlet_Emergency> list = new List<Outlet_Emergency>();
            // Parse chuỗi json ra list object
            OutletEmergencyDetail[] items = JsonConvert.DeserializeObject<OutletEmergencyDetail[]>(model.pData);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in items)
            {
                Outlet_Emergency entity = new Outlet_Emergency();
                entity.OutletID = int.Parse(item.pOutletID);
                entity.Suggestion = item.pSuggestion;
                entity.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(item.pDeviceDateTime);
                entity.CreatedBy = int.Parse(item.pTeamOutletID);
                entity.CreatedDateTime = DateTime.Now;
                entity.Status = 1;
                entity.RowVersion = 1;
                list.Add(entity);
            }

            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Outlet_Emergency.AddNew(model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}