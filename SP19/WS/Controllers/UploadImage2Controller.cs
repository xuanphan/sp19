﻿using Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using WS.Models;
using Google.Apis.Drive.v3;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using Google.Apis.Drive.v3.Data;


namespace WS.Controllers
{
    public class UploadImage2Controller : Controller
    {

        // api test
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult UploadFile()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UploadFile(string rootUrl, ImageModel model)
        {
            AddImageResult result = new AddImageResult();
            try
            {
                string _FilePath = String.Empty;
                string imageUrl = String.Empty;
                string imagePath = String.Empty;    
                HttpPostedFileBase file = Request.Files["file"];

                string namefilepath = model.pFileName;
                byte[] data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, (int)file.InputStream.Length);

                Stream bakupStream = new MemoryStream(data);


               
                
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    Business.Connection.CreateImagePath(Request.PhysicalApplicationPath, Request.Url.Authority, "Images" + "/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day, _FilePath, out imageUrl);
                    imagePath = Request.PhysicalApplicationPath + "/Images/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + _FilePath;
                    Directory.CreateDirectory(imagePath);
                }
                return this.Json(result);

                //    Business.Connection.CreateImagePath(Request.PhysicalApplicationPath, Request.Url.Authority, "Image2018", _FilePath, out imageUrl);
                //    imagePath = Request.PhysicalApplicationPath + "/Image2018/" + _FilePath;
                //    Directory.CreateDirectory(imagePath);

                //    if (System.IO.File.Exists(imagePath + model.pFileName))
                //    {
                //        namefilepath = CodeMD5.MD5Hash(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss tt")) + model.pTeamOutletID + "-" + model.pFileName;

                //        Request.Files[i].SaveAs(imagePath + namefilepath);
                //        Image fieldImage = new Image();
                //        fieldImage.ImageCode = model.pImageCode;
                //        fieldImage.FileName = namefilepath;
                //        fieldImage.FilePath = imageUrl.ToStringWithSuffix("", namefilepath);
                //        fieldImage.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(model.pDeviceDateTime);
                //        fieldImage.LatGPS = double.Parse(model.pLatGPS);
                //        fieldImage.LongGPS = double.Parse(model.pLongGPS);

                //        Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = fieldImage.AddNew(ref  _messagererror, model.pTeamOutletID, model.pAppCode, ref result);
                //        list += Id_Return.ToString() + "|";
                //    }
                //    else
                //    {
                //        Request.Files[i].SaveAs(imagePath + namefilepath);
                //        Image fieldImage = new Image();
                //        fieldImage.ImageCode = model.pImageCode;
                //        fieldImage.FileName = namefilepath;
                //        fieldImage.FilePath = imageUrl.ToStringWithSuffix("", namefilepath);
                //        fieldImage.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(model.pDeviceDateTime);
                //        fieldImage.LatGPS = double.Parse(model.pLatGPS);
                //        fieldImage.LongGPS = double.Parse(model.pLongGPS);

                //        Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = fieldImage.AddNew(ref  _messagererror, model.pTeamOutletID, model.pAppCode, ref result);
                //        list += Id_Return.ToString() + "|";
                //    }
                //}
                //return this.Json(result);


            }
            catch (Exception ex)
            {
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW.SYSTEM_ERROR;
                result.Description = ex.Message;
                return this.Json(result);
            }
        }

        public static string uploadFile(string parentid, DriveService _service, Stream stream, string filename, string mimeType, ref string _messagererror)
        {
            Permission perm = new Permission();
            perm.Role = "reader";
            perm.Type = "anyone";

            var body = new Google.Apis.Drive.v3.Data.File();
            List<string> parent = new List<string>();
            parent.Add(parentid);

            body.Name = System.IO.Path.GetFileName(filename);
            body.Description = "image";
            body.MimeType = mimeType;
            body.Parents = parent;
            //body.Permissions = listPermission;


            FilesResource.CreateMediaUpload request;
            try
            {
                request = _service.Files.Create(body, stream, body.MimeType);
                request.Fields = "id";
                request.Upload();

                var file = request.ResponseBody;
                var link = file.Id;

                _service.Permissions.Create(perm, parentid).Execute();

                return "https://drive.google.com/uc?id=" + link;
            }
            catch (Exception ex)
            {
                _messagererror = ex.Message;
                return "-1";
            }
        }
    }
}
