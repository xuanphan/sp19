﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;

namespace WS.Controllers
{
    public class UpdateChangeSetWSController : ApiController
    {
        //
        // GET: /AttendanceTracking/
        // cập nhật thay đổi set quà hiện tại của outlet
        public async Task<IHttpActionResult> PostData(UpdateSetModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            try
            {
                Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Outlet_Requirement_Change_Set.complete( model.pAppCode, long.Parse(model.pRequimentChangeSetID), ref result);
                return Ok(result);
            }
            catch (Exception ex)
            {
                // xảy ra catch lúc chưa xử lý thì báo lỗi
                result.Status = (int)Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT.SYSTEM_ERROR;
                result.Description = ex.Message;
                return Ok(result);
            }
        }
    }
}