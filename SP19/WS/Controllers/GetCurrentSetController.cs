﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;
using Business;

namespace WS.Controllers
{
    public class GetCurrentSetController : ApiController
    {
        // lấy danh sách set quà đang chạy của outlet đối với APP SP
        public async Task<IHttpActionResult> Postgetcurrent(GetCurrentSetModel model)
        {
            // khởi tạo trả về
            GetCurrentSetResult result = new GetCurrentSetResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Outlet_Current_Set.getByOutlet(model.pAppCode,int.Parse(model.pOutletID),ref result);
            return Ok(result);
        }
    }
}
