﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetBreweryWSController : ApiController
    {
        // GET: /Brand/
        // lấy danh sách các nhà máy beer (báo cáo island) dùng cho app SUP
        public async Task<IHttpActionResult> GetAll(string pAppCode,string pProjectID)
        {
            // khởi tạo trả về
            BreweryResult result = new BreweryResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Brewery.GetAll(pAppCode, int.Parse(pProjectID), ref result);
            return Ok(result);
        }
    }
}
