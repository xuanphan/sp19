﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;
using Newtonsoft.Json;
namespace WS.Controllers
{
    public class AddCustomerImageWSController : ApiController
    {
        //
        // GET: /AddCustomer/
        // thêm hình ảnh hóa đơn của khách hàng
        public async Task<IHttpActionResult> PostData(CustomerImageModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db 
            List<Customer_Image> list = new List<Customer_Image>();
            // Parse chuỗi json ra list object
            string json = model.pData.Replace("\n", "").Replace(" ", String.Empty);
            CustomerImageDetail[] items = JsonConvert.DeserializeObject<CustomerImageDetail[]>(json);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in items)
            {
                Customer_Image entity = new Business.Customer_Image();
                //them data vao outlet
                entity.ImageID = long.Parse(item.pImageID);
                entity.CustomerID = long.Parse(item.pCustomerID);
                entity.CreatedBy = int.Parse(item.pTeamOutletID);
                entity.CreatedDateTime = DateTime.Now;
                entity.Status = 1;
                entity.RowVersion = 1;
                list.Add(entity);
            }
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Customer_Image.AddNew(model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}