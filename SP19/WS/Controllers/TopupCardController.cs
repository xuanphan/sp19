﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class TopupCardController : ApiController
    {
        public async Task<IHttpActionResult> PostTopup(TopupCardModel model)
        {
           MainResult result = new MainResult();
             string phoneNumber  = model.phoneNumber;
         string phoneType  = model.phoneType;
         int outletId = model.outletId != null ? int.Parse(model.outletId) : 0;
         int userId  = int.Parse(model.userId);
         string userType  = model.userType;
         string secretKey  = model.secretKey;
         bool intResult = TopupCard.sendTopup(phoneNumber, phoneType,outletId, userId, userType, secretKey, ref result);
            return Ok(result);
        }
    }
}
