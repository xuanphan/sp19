﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Business;

namespace WS.Controllers
{
    public class GetBrandSetWSController : ApiController
    {
        // GET: /Brand/
        // lấy danh sách các set quà chạy theo các brand thuộc projectID truyền lên
        public async Task<IHttpActionResult> GetAll(string pAppCode, string pProjectID)
        {
            // khởi tạo trả về
            Brand_SetResult result = new Brand_SetResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Brand_Set.GetAllWS(pAppCode, int.Parse(pProjectID), ref result);
            return Ok(result);
        }
    }
}
