﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Business;
using System.Threading.Tasks;
using WS.Models;

namespace WS.Controllers
{
    public class LoginWSController : ApiController
    {
        // đăng nhập app SP
        public async Task<IHttpActionResult> PostLogin(LoginModel model)
        {
            // khởi tạo trả về
            LoginResult result = new LoginResult();
            Business.User user = new Business.User();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_LOGIN error = user.LoginSP(model.pUserName, model.pPassWord, model.pUserType, model.pAppCode, model.pDeviceToken,string.IsNullOrEmpty(model.pDeviceID)?"":model.pDeviceID, ref result);
            return Ok(result);
        }
    }
}
