﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;
using Newtonsoft.Json;

namespace WS.Controllers
{
    public class AddTakeOffVolumnController : ApiController
    {
        // GET api/<controller>
        // thêm báo cáo bán hàng theo outlet
        public async Task<IHttpActionResult> PostData(TakeOffVolumnModel model)
        {
            //khởi tạo trả về
            MainResult result = new MainResult();
            // khởi tạo list thêm vào db 
            List<Take_Off_Volumn> list = new List<Take_Off_Volumn>();
            // Parse chuỗi json ra list object
            TakeOffVolumnDetail[] items = JsonConvert.DeserializeObject<TakeOffVolumnDetail[]>(model.pData);
            // chạy vòng lặp list object vừa parse để add vào list thêm
            foreach (var item in items)
            {
                Take_Off_Volumn entity = new Take_Off_Volumn();
                entity.TakeOffVolumnCode = item.pTakeOffVolumnCode;
                entity.OutletID = int.Parse(item.pOutletID);
                entity.ProductID = int.Parse(item.pProductID);
                entity.DeviceDateTime = Common.ConvertUtils.ConvertStringToLongDate(item.pDeviceDateTime);
                entity.ServerDateTime = DateTime.Now;
                entity.Number = int.Parse(item.pNumber);
                entity.CreatedBy = int.Parse(item.pTeamOutletID);
                entity.CreatedDateTime = DateTime.Now;
                entity.Status = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                entity.RowVersion = (int)Common.STATUS_CODE.ROW_STATUS.ACTIVE;
                list.Add(entity);
            }
            // vào xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Take_Off_Volumn.AddNew(model.pAppCode, list, ref result);
            return Ok(result);
        }
    }
}