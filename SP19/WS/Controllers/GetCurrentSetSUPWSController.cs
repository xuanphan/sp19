﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS.Models;

namespace WS.Controllers
{
    public class GetCurrentSetSUPWSController : ApiController
    {
        // lấy danh sách set quà đang chạy của outlet của sup quay quà : đối với APP SUP
        public async Task<IHttpActionResult> Postgetcurrentsup(GetCurrentSetSUPModel model)
        {
            // Khởi tạo trả về
            GetCurrentSetResult result = new GetCurrentSetResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error = Outlet_Current_Set.getByOutletSUP(model.pAppCode, int.Parse(model.pOutletID),int.Parse(model.pSupID), ref result);
            return Ok(result);
        }
    }
}
