﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Business;
using System.Threading.Tasks;

namespace WS.Controllers
{
    public class GetProductSUPController : ApiController
    {
        // lấy danh sách sản phẩm dùng cho app SUP
        public async Task<IHttpActionResult> Getforsup(string pAppCode, string pProjectID)
        {
            // khởi tạo trả về
            GetProductSUPResult result = new GetProductSUPResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_SELECT error1 = Product.getAllForSup(pAppCode, pProjectID, ref result);
            return Ok(result);
        }
    }
}
