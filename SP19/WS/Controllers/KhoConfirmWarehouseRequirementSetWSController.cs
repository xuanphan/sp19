﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WS.Models;

namespace WS.Controllers
{
    public class KhoConfirmWarehouseRequirementSetWSController : ApiController
    {
        //
        // GET: /AddCustomer/
        // kho xác nhận giao hàng cho yêu cầu thêm set qua từ siêu thị
        public async Task<IHttpActionResult> PostData(KhoConfirmWarehouseRequirementSetWSModel model)
        {
            // khởi tạo trả về
            MainResult result = new MainResult();
            // xử lý
            Common.ERROR_CODE.ERROR_CODE_ITEMS_ADDNEW error = Warehouse_Requirement_Set.khoConfirm( model.pAppCode, long.Parse(model.pWarehouseRequirementID), int.Parse(model.pUserID), model.pDeviceToken, ref result);
            return Ok(result);
        }
    }
}