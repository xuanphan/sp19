﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class LoginSUPModel
    {
        public string pAppCode { get; set; }
        public string pUserName { get; set; }
        public string pPassWord { get; set; }
        public string pUserType { get; set; }
        public string pDeviceToken { get; set; }
    }
}