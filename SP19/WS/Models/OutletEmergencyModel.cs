﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class OutletEmergencyModel
    {
    
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }
    public class OutletEmergencyDetail
    {
        public string pOutletID { get; set; }
        public string pSuggestion { get; set; }
        public string pTeamOutletID { get; set; }
        public string pDeviceDateTime { get; set; }
    }
}
