﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class WarehouseRequirementSetDetailModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }
        public class WarehouseRequirementSetDetail
        {
            public string pOutletID { get; set; }
            public string pTeamOutletID { get; set; }
            public string pWarehouseRequirementSetID { get; set; }
            public string pBrandSetID { get; set; }
            public string pNumber { get; set; }
        }
    
}
