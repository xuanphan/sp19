﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class AddBrandSetUsedModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }

    public class BrandSetUsedData
    {
        public int pTeamOutletID { get; set; }
        public int pOutletID { get; set; }
        public int pBrandSetID { get; set; }
        public int pNumber { get; set; }
        public string pDeviceDateTime { get; set; }
    }
}