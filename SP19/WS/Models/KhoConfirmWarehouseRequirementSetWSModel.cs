﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class KhoConfirmWarehouseRequirementSetWSModel
    {
        public string pAppCode { get; set; }
        public string pWarehouseRequirementID { get; set; }
        public string pUserID { get; set; }
        public string pDeviceToken { get; set; }
    }
}