﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class AttendanceTrackingSupModel
    {
        public string pAppCode { get; set; }
        public string pSupID { get; set; }
        public string pOutletID { get; set; }
        public string pAttendanceTrackingCode { get; set; }
        public string pDeviceDateTime { get; set; }
        public string pTimePointType { get; set; }
        public string pLatGPS { get; set; }
        public string pLongGPS { get; set; }
    }
}