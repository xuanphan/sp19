﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class GetProfileByCodeModel
    {
        public string pAppCode { get; set; }
        public string pProjectID { get; set; }
        public string pProfileCode { get; set; }
    }
}