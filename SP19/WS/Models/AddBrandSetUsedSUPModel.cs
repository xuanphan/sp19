﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class AddBrandSetUsedSUPModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }

    public class BrandSetUsedSUPData
    {
        public int pSupID { get; set; }
        public int pOutletID { get; set; }
        public int pBrandSetID { get; set; }
        public int pNumber { get; set; }
        public string pDeviceDateTime { get; set; }
    }
}