﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class TopupCardModel
    {
        public string phoneNumber { get; set; }
        public string phoneType { get; set; }
        public string outletId { get; set; }
        public string userId { get; set; }
        public string userType { get; set; }
        public string secretKey { get; set; }
    }
}