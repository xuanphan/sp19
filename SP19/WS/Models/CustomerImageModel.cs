﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class CustomerImageModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }
    public class CustomerImageDetail
    {
        public string pTeamOutletID { get; set; }
        public string pCustomerID { get; set; }
        public string pImageID { get; set; }
    }

}