﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class GetAllOutletModel
    {
        public string pAppCode { get; set; }
        public string pProjectID { get; set; }
        public string pUserID { get; set; }
    }
}