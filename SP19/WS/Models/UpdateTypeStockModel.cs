﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class UpdateTypeStockModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }
    public class UpdateTypeDetail
    {
        public int pNumberType { get; set; }
        public int pOutletID { get; set; }
        public string pDeviceDateTime { get; set; }
    }
}