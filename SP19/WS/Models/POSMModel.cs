﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class POSMModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }
        public class POSMDetail
        {
            public string pOutletID { get; set; }
            public string pTeamOutletID { get; set; }
            public string pPOSMID { get; set; }
            public string pNumber { get; set; }
            public string pDeviceDateTime { get; set; }
        }
        
    
}