﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class AddAttendanceTrackingSUPImageWSModel
    {
        public string pAppCode { get; set; }
        public string pAttendanceTrackingSupID { get; set; }
        public string pImageID { get; set; }
    }
}