﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class AttendanceTrackingModel
    {
        public string pAppCode { get; set; }
        public string pAttendanceTrackingCode { get; set; }
        public string pTeamOutletID { get; set; }
        public string pDeviceDateTime { get; set; }
        public string pTimePointType { get; set; }
        public string pLatGPS { get; set; }
        public string pLongGPS { get; set; }
        public int pNumberPG { get; set; }
    }
}