﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class ReadedNotificationSetGiftMegaModel
    {
        public string pAppCode { get; set; }
        public string pOutletID { get; set; }
        public string pGiftID  { get; set; }
        public string pTeamOutletID { get; set; }
    }
}