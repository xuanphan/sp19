﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class GDriverImageModel
    {
        public string Link { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
    }
}