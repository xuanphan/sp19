﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class ChangePassModel
    {
        public string pAppcode { get; set; }
        public string pUserID { get; set; }
        public string pOldPassword { get; set; }
        public string pNewPassword { get; set; }
    }
}