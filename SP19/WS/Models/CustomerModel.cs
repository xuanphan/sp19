﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class CustomerModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }
    public class CustomerDetail
    {
        public int pOutletID { get; set; }
        public int pTeamOutletID { get; set; }
        public string pCustomerName { get; set; }
        public string pCustomerCode { get; set; }
        public string pCustomerPhone { get; set; }
        public string pBillNumber { get; set; }
        public string pDeviceDateTime { get; set; }
        public string pAddress { get; set; }
        public string pNote { get; set; }
        public string pSex { get; set; }
        public string pEmail { get; set; }
        public string pYearOfBirth { get; set; }
        public string pReasonBuy { get; set; }
    }
}
