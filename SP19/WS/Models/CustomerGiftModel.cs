﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class CustomerGiftModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }

    public class CustomerGiftDetail
    {
        public string pTeamOutletID { get; set; }
        public string pCustomerID { get; set; }
        public string pGiftID { get; set; }
        public string pNumberGift { get; set; }
    }
}