﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class CheckCustomerByPhoneModel
    {
        public string pAppCode { get; set; }
        public string pOutletID { get; set; }
        public string pCustomerPhone { get; set; }
    }
}