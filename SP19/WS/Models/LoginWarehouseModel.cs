﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class LoginWarehouseModel
    {
        public string pAppCode { get; set; }
        public string pUserName { get; set; }
        public string pPassWord { get; set; }
        public string pDeviceToken { get; set; }
        public string pDeviceID { get; set; }
    }
}