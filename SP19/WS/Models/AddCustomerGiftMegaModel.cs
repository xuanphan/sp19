﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class AddCustomerGiftMegaModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }
    public class WSCustomerGiftMegaDetail
    {
        public int pCustomerID { get; set; }
        public int pGiftID { get; set; }
        public int pTeamOutletID { get; set; }
    }
}