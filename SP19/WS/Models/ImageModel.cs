﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class ImageModel
    {
        public string pImageCode { get; set; }
        public string pAppCode { get; set; }
        public string pFileName { get; set; }
        public string pDeviceDateTime { get; set; }
        public string pLatGPS { get; set; }
        public string pLongGPS { get; set; }
        public string pTeamOutletID { get; set; }
        public string pImageType { get; set; }
    }
}