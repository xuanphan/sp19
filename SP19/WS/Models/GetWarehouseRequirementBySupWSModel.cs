﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class GetWarehouseRequirementBySupWSModel
    {
        public string pAppCode { get; set; }
        public string pSupID { get; set; }
    }
}