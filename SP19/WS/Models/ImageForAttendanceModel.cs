﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class ImageForAttendanceModel
    {
        public string pAppCode { get; set; }

        public string pData { get; set; }
       
    }
    public class ImageForAttendanceDetail
    {
        public string pAttendanceID { get; set; }
        public string pImageID { get; set; }
    }
}