﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class AddAttendanceTrackingImageWSModel
    {
        public string pAppCode { get; set; }
        public string pAttendanceTrackingID { get; set; }
        public string pImageID { get; set; }
    }
}