﻿namespace WS.Models
{
    public class AddProflieEmergencyModel
    {
        public string pAppCode { get; set; }
        public string pProjectID { get; set; }
        public string pProflieEmergencyCode { get; set; }
        public string pTeamOutletID { get; set; }
        public string pProfileCode { get; set; }
        public string pEmergencyID { get; set; }
        public string pOutletID { get; set; }
        public string pStartDateTime { get; set; }

    }

}