﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class BreweryBrandIsLandModel
    {
          public string pAppCode { get; set; }

          public string pData { get; set; }
       
    }
    public class IsLandDetail
    {
        public string pOutletID { get; set; }
        public string pSupID { get; set; }
        public string pBreweryBrandID { get; set; }
        public string pOutsideColumn { get; set; }
        public string pInsideColumn { get; set; }
        public string pInsidePalletColumn { get; set; }
        public string pDeviceDateTime { get; set; }

    }
}
