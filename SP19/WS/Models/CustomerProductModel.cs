﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class CustomerProductModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }
    public class CustomerProductDetail
    {
        public string pTeamOutletID { get; set; }
        public string pCustomerID { get; set; }
        public string pProductID { get; set; }
        public int pNumber { get; set; }
    }
}