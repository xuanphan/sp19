﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class GetCurrentSetModel
    {
        public string pAppCode { get; set; }
        public string pOutletID { get; set; }
    }
}