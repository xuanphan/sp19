﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Models
{
    public class AddCustomerTotalDialMegaModel
    {
        public string pAppCode { get; set; }
        public string pData { get; set; }
    }
    public class WSAddCustomerTotalDialMegaDetail
    {
        public int pCustomerID { get; set; }
        public int pNumber { get; set; }
        public int pTeamOutletID { get; set; }
    }
}