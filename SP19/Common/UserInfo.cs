﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class Globaled
    {
        public static int InfoOutletID;
    }


    public class UserInfo
    {
        //user_teamID của user Đăng nhập
        public string TeamOutletID { get; set; }
        //tên của user đăng nhập
        public string OutletID { get; set; }
        public string OutletName { get; set; }
        public string DisplayName { get; set; }
        public string OutletTypeID { get; set; }
        public string ProjectID { get; set; }
        public UserInfo() { }

    }
    /// <summary>
    /// LoginInfo class
    /// </summary>
    /// <author>Nguyen Minh Tien</author>
    public class LoginInfo
    {
        public int UserID { get; set; }
        public int OutletTypeID { get; set; }
        public int ProjectID { get; set; }
        public string ListSaleID { get; set; }
        public int SaleID { get; set; }
        public int SaleSupID { get; set; }
        public int UserRole { get; set; }
        public String UserName { get; set; }
        public string Password { get; set; }
        public ERROR_CODE.ERROR_CODE_LOGIN LoginResult { get; set; }
        public UserInfo UserInfo { get; set; }
        public LoginInfo() { }
        public LoginInfo(String username, String password)
        {
            UserName = username;
            Password = password;
        }
        //----------------------Trạng thái bắt buộc khi trả về
        public int Status { get; set; }

        public string Description { get; set; }

        public long teamOutletID { get; set; }
    }
}
