﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class AutoMake
    {
        /// <summary> Mã tính theo prefixText### </summary>
        /// <param name="listID">danh sách id</param>
        /// <param name="prefixText">mã kiểm tra tăng</param>
        /// <param name="checkPrefix">kiểm tra tăng theo mã hay ko</param>
        /// <returns></returns>
        public static string NextCode(IEnumerable<string> listID, string prefixText, bool checkPrefix = true, int fistNumberLenght = 3)
        {
            //nếu rỗng => về 1
            if (listID.Count() <= 0)
            {
                string num = "";
                for (int i = 0; i < fistNumberLenght - 1; i++)
                {
                    num += "0";
                }
                return prefixText + num + "1";
            }

            Int64 nextNum = 0;
            // lấy những giá trị có chứa prefixText, sắp xếp nó
            listID = listID.Where(l => !string.IsNullOrEmpty(l));               //bỏ rỗng mới xét contain được
            if (checkPrefix)
                listID = listID.Where(l => l.Contains(prefixText));             // chọn những dòng có prefix text
            listID = listID.OrderBy(l => l);
            string maxNum = listID.Last().Remove(0, prefixText.Length);         // tìm số lớn nhất
            Int64.TryParse(maxNum, out nextNum);
            nextNum++;

            //kích cỡ số 0
            int zeroLength = maxNum.Length - nextNum.ToString().Length;
            string zeroNumber = "";
            if (zeroLength > 0)
            {
                for (int j = 0; j < zeroLength; j++)
                {
                    zeroNumber += "0";
                }
                return prefixText + zeroNumber + nextNum.ToString();
            }
            return prefixText + nextNum;
        }

        /// <summary> Mã HardCopy tính theo yyyyMMdd##### tăng dần </summary>
        public static string NextDateCode(IEnumerable<string> listCode)
        {
            string prefixToday = DateTime.Now.ToString("yyyyMMdd");

            //bỏ giá trị null, tránh lỗi khi contain phía dưới
            var list = from l in listCode
                       where !string.IsNullOrEmpty(l)
                       select l;

            if (list.Count() <= 0)
                return prefixToday + "001";
            else
                return NextCode(list, prefixToday, false);
        }
    }
}
