﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

namespace Common
{
    public class XMLHelper
    {
        public static void CreateXMLNode<T>(XmlDocument _document, T _addItem)
        {
            XmlNode fatherNode = _document.CreateNode(XmlNodeType.Element, typeof(T).Name, "");

            IList<PropertyInfo> propertyInfos = typeof(T).GetProperties();
            for (int i = 0; i < propertyInfos.Count; i++)
            {
                PropertyInfo propertyInfo = propertyInfos[i];
                string propertyName = propertyInfo.Name; // tên properties

                object objValue = propertyInfo.GetValue(_addItem, null);
                string strValue = MakeValueFriendly(objValue); // giá trị

                string type = propertyInfo.PropertyType.ToString(); // loại của giá trị
                // thêm
                XmlNode currNode = _document.CreateNode("element", propertyName, "");
                currNode.InnerText = strValue;

                XmlAttribute currAtribute = _document.CreateAttribute("Type");
                currAtribute.Value = type;

                currNode.Attributes.Append(currAtribute);
                fatherNode.AppendChild(currNode);
            }

            XmlElement root = _document.DocumentElement;
            root.AppendChild(fatherNode);
        }

        /// <summary>
        /// Lưu đè thông tin
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_document"></param>
        /// <param name="_addItem"></param>
        public static void OverrideXMLNode<T>(XmlDocument _document, T _addItem)
        {
            
        }

        /// <summary> convert Object => string </summary>
        private static string MakeValueFriendly(object value)
        {
            // value = null => ""
            if (value == null) return "";
            if (value is Nullable && ((INullable)value).IsNull) return "";

            // value = datetime => yyyy-MM-dd  HH:mm:ss
            if (value is DateTime)
            {
                if (((DateTime)value).TimeOfDay.TotalSeconds == 0)
                    return ((DateTime)value).ToString("yyyy-MM-dd");
                return ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
            }

            string output = value.ToString();
            // chứa "," hay ""\" sửa lại
            if (output.Contains(",") || output.Contains("\""))
                output = '"' + output.Replace("\"", "\"\"") + '"';

            return output;
        }
    }
}
