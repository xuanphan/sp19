﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.XML
{
    public class XMLInfo
    {
        public string FileName { get; set; }

        public string RootName { get; set; }
    }
}
