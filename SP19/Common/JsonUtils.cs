﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public partial class JsonUtils
    {
        public static IList<T> DeserializeToList<T>(string jsonString)
        {
            var array = JArray.Parse(jsonString);

            IList<T> objectsList = new List<T>();
            foreach (var item in array)
            {
                try
                {
                    objectsList.Add(item.ToObject<T>());
                }

                catch (Exception ex)
                {

                }

            }
            return objectsList;
        }

        
    }
}
