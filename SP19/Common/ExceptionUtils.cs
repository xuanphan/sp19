﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Common
{
    public class ExceptionUtils
    {
        public static string ExceptionToMessage(Exception ex)
        {
            if (ex is TimeoutException)
            {
                return "Thời gian thực hiện quá lâu.";
            }
            return ex.Message;
        }
        public static string DontHaveRightMessage()
        {
            return "Bạn chưa được phân quyền sử dụng chức năng này!";
        }
        public static CustomException ThrowCustomException(Exception ex)
        {
            if (ex is CustomException)
                return new CustomException(ex.Message, null, EnumTypeCollection.EnumError.LogicError);
            return new CustomException("Lỗi hệ thống.", ex, EnumTypeCollection.EnumError.SystemError);
        }
        public static string ExceptionToDetailMessage(string functionName, CustomException ex)
        {
            return String.Format("Error at function [{0}]: {1}", functionName, ex.DetailMessage);
        }



        [Serializable]
        public class CustomException : Exception
        {
            public bool Debug = true;

            public EnumTypeCollection.EnumError ErrorType { get; set; }

            public string ErrorArg { get; set; }

            public string DetailMessage
            {
                get
                {
                    if (this.InnerException != null)
                        return this.InnerException.ToString();
                    return this.Message;
                }
            }

            public CustomException()
                : base() { }

            public CustomException(string errorMessage, Exception ex, EnumTypeCollection.EnumError errorType)
                : base(errorMessage, ex)
            {
                this.ErrorType = errorType;
            }

            public CustomException(string message)
                : base(message) { }

            public CustomException(string message, params string[] errorArgs)
                : base(message)
            {
                foreach (var item in errorArgs)
                {
                    ErrorArg += "_" + item;
                }
            }

            public CustomException(string format, params object[] args)
                : base(string.Format(format, args)) { }

            public CustomException(string message, Exception innerException)
                : base(message, innerException) { }

            public CustomException(string format, Exception innerException, params object[] args)
                : base(string.Format(format, args), innerException) { }

            protected CustomException(SerializationInfo info, StreamingContext context)
                : base(info, context) { }

            public string GetMessage()
            {
                if (Debug)
                {
                    return DetailMessage;
                }
                else
                    return Message;
            }
        }
    }
}
