﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public partial class Cell
    {
        public static string GetCell( ICell cell)
        {
            string cellValue= String.Empty;
            switch (cell.CellType)
            {
                case CellType.String:
                    cellValue = cell.StringCellValue;
                    break;
                case CellType.Formula:
                    cellValue = cell.NumericCellValue.ToString();
                    break;
                case CellType.Numeric:
                    cellValue = DateUtil.IsCellDateFormatted(cell)
                        ? cell.DateCellValue.ToString()
                        : cell.NumericCellValue.ToString();
                    break;
                case CellType.Blank:
                    cellValue = "";
                    break;
                case CellType.Boolean:
                    cellValue = cell.BooleanCellValue.ToString();
                    break;
            }
            return cellValue;
        }
    }
}
