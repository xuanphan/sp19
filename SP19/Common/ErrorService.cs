﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Common
{
    public partial class ErrorService
    {
        public enum WebService_ROLE
        {
            [DescriptionAttribute("ThanhCong.")]
            THANHCONG = 0,
            [DescriptionAttribute("Lỗi, Giao dịch đang xử lý kiểm tra lại với NCC.")]
            LOIDANGKIEMTRALAI = 11,
            [DescriptionAttribute("Lỗi, Cho xu ly.")]
            LOIDANGXULY = 99,
            [DescriptionAttribute("Lỗi, Hệ thống.")]
            LOIHETHONG = 1,
            [DescriptionAttribute("Lỗi, Số dư của bạn không đủ.")]
            LOISODUKHONGDU = 5,
            [DescriptionAttribute("Lỗi, Tham số truyền vào không đúng.")]
            LOITHAMSOKHONGDUNG = 6,
            [DescriptionAttribute("Hệ thống lỗi không cập nhật được giao dịch.")]
            LOIKHONGCAPNHATDCGIAODICH = 7,
            [DescriptionAttribute("Lỗi, Sai mã checksum.")]
            LOISAIMACHECKSUM = 8,
            [DescriptionAttribute("IP chưa được phép truy cập.")]
            SAIIP = 9,
            [DescriptionAttribute("Lỗi, Hệ thống đã tồn tại mã giao dịch này.")]
            TONTAIMAGIAODICH = 10,
            [DescriptionAttribute("Lỗi, Hiện tại mệnh giá này đang khóa.")]
            MENHGIANAYDANGKHOA = 16
        }

    }
}
