﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class WSUtils
    {
        public static int Decode(string _WSString, ref string _value)
        {
            string[] array = _WSString.Split('|');
            if (array.Count() > 1)
            {
                _value = array[1];
            }
            return ConvertUtils.ToInt32(array[0]);
        }

        public enum RETURN_STATUS
        {
            SUCCESS = 0,
        }
    }
}
