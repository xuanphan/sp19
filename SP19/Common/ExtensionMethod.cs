﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public static class ExtensionMethod
    {
        public static string ToStringWithPrefix(this string s, string prefix, string spliter = "")
        {
            if (String.IsNullOrEmpty(spliter))
                return String.Format("{0}{1}", prefix, s);
            return String.Format("{0}{1}{2}", prefix, spliter, s);
        }

        public static string ToStringWithSuffix(this string s, string spliter = "_", params string[] suffixs)
        {
            string result = s;
            foreach (var item in suffixs)
            {
                result = result + spliter + item;
            }
            return result;
        }
    }
}
