﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class MESSAGE_TITLE
    {
        public const string INFO = "Thông báo";
        public const string WARNING = "Cảnh báo";
        public const string ERROR = "Lỗi";
    }

  
    public class AUTH_MESSAGE_STRING
    {
        public const string WRONG_USERNAME = "Username incorect.";
        public const string WRONG_PASSWORD = "Password incorect.";
        public const string DUPLICATE_USERNAME = "Username not exist.";
        public const string UNVALID_REGISTER_USER = "Thông tin đăng ký không hợp lệ!";
        public const string UNVALID_OUTLET_USER = "Thông tin siêu thị không hợp lệ!";
        public const string UNVALID_LOGIN_USER = "Username or Password incorect!";
    }

}
