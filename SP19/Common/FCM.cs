﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
//using System.Net.http;
//using System.Net.Http.Headers;
using System.Text;

namespace Common
{
    public class FCM
    {
        private const string URL = "https://sp.imark.com.vn/SMSSP19/api/AddBrandName";

        public static string SendSMS(string tokenDevice, string message)
        {
            try
            {
                string url = @"https://fcm.googleapis.com/fcm/send";
                WebRequest tRequest = WebRequest.Create(url);
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data = new
                {
                    to = tokenDevice,
                    notification = new
                    {
                        body = message,
                        title = "Thông báo"
                    },
                };
                string jsons = Newtonsoft.Json.JsonConvert.SerializeObject(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(jsons);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", Common.Global.Serverkey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", Common.Global.SenderID));
                tRequest.ContentLength = byteArray.Length;
                tRequest.ContentType = "application/json";
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String responseFromFirebaseServer = tReader.ReadToEnd();
                                return responseFromFirebaseServer;
                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                return null;
            }
        }

        public static string SendSMSChangeSet(string tokenDevice, List<SimpleChangeSetResult> listRP)
        {
            try
            {
                string url = @"https://fcm.googleapis.com/fcm/send";
                WebRequest tRequest = WebRequest.Create(url);
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data = new
                {
                    to = tokenDevice,
                    //notification = new
                    //{
                    //    body = "Thông báo đổi set quà",
                    //    title = "Thông báo"
                    //},
                    data = new
                    {
                        Message = "Thông báo đổi set quà",
                        Title = "Thông báo",
                        ListBrand = JsonConvert.SerializeObject(listRP)
                    }
                };
                string jsons = Newtonsoft.Json.JsonConvert.SerializeObject(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(jsons);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", Common.Global.Serverkey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", Common.Global.SenderID));
                tRequest.ContentLength = byteArray.Length;
                tRequest.ContentType = "application/json";
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String responseFromFirebaseServer = tReader.ReadToEnd();
                                return responseFromFirebaseServer;
                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                return null;
            }
        }

        public static string SendSMSSetGiftMega(string tokenDevice, SimpleGiftMegaResult listRP)
        {
            try
            {
                string url = @"https://fcm.googleapis.com/fcm/send";
                WebRequest tRequest = WebRequest.Create(url);
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data = new
                {
                    to = tokenDevice,
                    //notification = new
                    //{
                    //    body = "Thông báo đổi set quà",
                    //    title = "Thông báo"
                    //},
                    data = new
                    {
                        ListGiftMega = JsonConvert.SerializeObject(listRP)
                    }
                };
                string jsons = Newtonsoft.Json.JsonConvert.SerializeObject(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(jsons);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", Common.Global.Serverkey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", Common.Global.SenderID));
                tRequest.ContentLength = byteArray.Length;
                tRequest.ContentType = "application/json";
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String responseFromFirebaseServer = tReader.ReadToEnd();
                                return responseFromFirebaseServer;
                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                return null;
            }
        }

        public static string SendResetData(string tokenDevice)
        {
            try
            {
                string url = @"https://fcm.googleapis.com/fcm/send";
                WebRequest tRequest = WebRequest.Create(url);
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data = new
                {
                    to = tokenDevice,
                    data = new
                    {
                        TypeNotifi = 11
                    }
                };
                string jsons = Newtonsoft.Json.JsonConvert.SerializeObject(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(jsons);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", Common.Global.Serverkey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", Common.Global.SenderID));
                tRequest.ContentLength = byteArray.Length;
                tRequest.ContentType = "application/json";
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String responseFromFirebaseServer = tReader.ReadToEnd();
                                return responseFromFirebaseServer;
                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                return null;
            }
        }

        public static string SendClearRef(string tokenDevice)
        {
            try
            {
                string url = @"https://fcm.googleapis.com/fcm/send";
                WebRequest tRequest = WebRequest.Create(url);
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data = new
                {
                    to = tokenDevice,
                    data = new
                    {
                        TypeNotifi = 10
                    }
                };
                string jsons = Newtonsoft.Json.JsonConvert.SerializeObject(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(jsons);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", Common.Global.Serverkey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", Common.Global.SenderID));
                tRequest.ContentLength = byteArray.Length;
                tRequest.ContentType = "application/json";
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String responseFromFirebaseServer = tReader.ReadToEnd();
                                return responseFromFirebaseServer;
                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                return null;
            }
        }

        public static string SendSetWinMegaNow(string tokenDevice, List<int> listGiftID)
        {
            try
            {
                string url = @"https://fcm.googleapis.com/fcm/send";
                WebRequest tRequest = WebRequest.Create(url);
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data = new
                {
                    to = tokenDevice,
                    data = new
                    {
                        TypeNotifi = 9,
                        listGiftID = JsonConvert.SerializeObject(listGiftID)
                    }
                };
                string jsons = Newtonsoft.Json.JsonConvert.SerializeObject(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(jsons);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", Common.Global.Serverkey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", Common.Global.SenderID));
                tRequest.ContentLength = byteArray.Length;
                tRequest.ContentType = "application/json";
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String responseFromFirebaseServer = tReader.ReadToEnd();
                                return responseFromFirebaseServer;
                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                return null;
            }
        }

        public static string GenerateRandomOTP(int iOTPLength, string[] saAllowedCharacters)
        {

            string sOTP = String.Empty;

            string sTempChars = String.Empty;

            Random rand = new Random();

            for (int i = 0; i < iOTPLength; i++)
            {

                int p = rand.Next(0, saAllowedCharacters.Length);

                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];

                sOTP += sTempChars;

            }

            return sOTP;

        }        

        public static void GetPOSTResponse(string message, string phone)
        {
            using (WebClient client = new WebClient())
            {

                byte[] response =
                client.UploadValues("https://sp.imark.com.vn/SMSSP19/api/AddBrandName", new NameValueCollection()
                   {
                       { "pKey", "78GjSKi781jHgoPmaldiABhqwY87" },
                       { "pPhone", phone },
                        { "pMessage", message }
                   });

                string result = System.Text.Encoding.UTF8.GetString(response);
            }
        }

        public static string SendSMSToList(List<string> ListTokens, string title, string message)
        {

            try
            {
                string url = @"https://fcm.googleapis.com/fcm/send";
                WebRequest tRequest = WebRequest.Create(url);
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                foreach (var item in ListTokens)
                {


                    var data = new
                    {
                        to = item,
                        //notification = new
                        //{
                        //    body = "Thông báo đổi set quà",
                        //    title = "Thông báo"
                        //},
                        data = new
                        {
                            Message = message,
                            Title = title,
                            TypeNotifi = 4
                        }
                    };
                    string jsons = Newtonsoft.Json.JsonConvert.SerializeObject(data);

                    Byte[] byteArray = Encoding.UTF8.GetBytes(jsons);
                    tRequest.Headers.Add(string.Format("Authorization: key={0}", Common.Global.Serverkey));
                    tRequest.Headers.Add(string.Format("Sender: id={0}", Common.Global.SenderID));
                    tRequest.ContentLength = byteArray.Length;
                    tRequest.ContentType = "application/json";
                    using (Stream dataStream = tRequest.GetRequestStream())
                    {
                        dataStream.Write(byteArray, 0, byteArray.Length);

                        using (WebResponse tResponse = tRequest.GetResponse())
                        {
                            using (Stream dataStreamResponse = tResponse.GetResponseStream())
                            {
                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String responseFromFirebaseServer = tReader.ReadToEnd();

                                }
                            }

                        }
                    }
                }

                return "thành công";
            }

            catch (Exception ex)
            {
                return "Lỗi";
            }
        }
    }
    public class SimpleChangeSetResult
    {
        public long RequimentChangeSetID { get; set; }
        public int BrandID { get; set; }
        public int BrandSetID { get; set; }
        public int NumberUsed { get; set; }
    }

    public class SimpleGiftMegaResult
    {
        public int GiftID { get; set; }
        public int OutletID { get; set; }
        public string DateTrue { get; set; }
    }
    public class SimplePhoneResult
    {
        public string key { get; set; }
        public string Message { get; set; }
        public string Phone { get; set; }
    }
}
