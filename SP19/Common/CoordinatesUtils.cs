﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class CoordinatesUtils
    {
        private double _Lat = 0;
        public double Lat
        {
            get { return _Lat; }
            set { _Lat = value; }
        }

        private double _Lng = 0;
        public double Lng
        {
            get { return _Lng; }
            set { _Lng = value; }
        }

        public CoordinatesUtils() { }
        public CoordinatesUtils(double? Lat, double? Lng)
        {
            this.Lat = ConvertUtils.ToDouble(Lat);
            this.Lng = ConvertUtils.ToDouble(Lng);
        }

        public static bool CompareGPS(CoordinatesUtils ItemCompare, CoordinatesUtils ItemToCompare, double metterDistance)
        {
            double latCompare = ItemCompare.Lat;
            double longCompare = ItemCompare.Lng;
            double latToCompare = ItemToCompare.Lat;
            double longToCompare = ItemToCompare.Lng;

            double pi80 = (Math.PI) / 180;
            latCompare *= pi80;
            longCompare *= pi80;
            latToCompare *= pi80;
            longToCompare *= pi80;

            double r = 6372.797; // mean radius of Earth in km
            double dlat = latToCompare - latCompare;
            double dlng = longToCompare - longCompare;
            double a = Math.Sin(dlat / 2) * Math.Sin(dlat / 2) + Math.Cos(latCompare) * Math.Cos(latToCompare) * Math.Sin(dlng / 2) * Math.Sin(dlng / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double km = r * c;
            double temp = km * 1000;
            return (temp <= metterDistance) ? true : false;
        }
    }
}
