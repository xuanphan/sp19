﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class ComboItem
    {
        private long _ID = 0;
        public long ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        private string _ID1 = String.Empty;
        public string ID1
        {
            get { return _ID1; }
            set { _ID1 = value; }
        }
        private string _Value = String.Empty;
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        private string _Text = String.Empty;
        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        public ComboItem()
        { 
        
        }

        public ComboItem(string Value, string Text)
        {
            _Value = Value;
            _Text = Text;
        }

        public ComboItem(long ID, string Value, string Text)
        {
            _ID = ID;
            _Value = Value;
            _Text = Text;
        }
    }
}
