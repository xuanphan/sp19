﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using System.ComponentModel;
namespace Common.ERROR_CODE
{
    /// <summary>
    /// ERROR_CODE_LOGIN
    /// </summary>
    /// <author>Nguyen Minh Tien</author>
    public enum ERROR_CODE_LOGIN 
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        [DescriptionAttribute("Sai mật khẩu.")]
        WRONG_PASS = 6,
        [DescriptionAttribute("Đang có thiết bị đang sử dụng tài khoản này.")]
        IS_LOGIN = 7,
        [DescriptionAttribute("Chưa có phân công team.")]
        NOT_ASSIGN_TEAM = 8,
        [DescriptionAttribute("Sai tên đăng nhập hoặc loại đăng nhập.")]
        WRONG_USERNAME = 3,
        [DescriptionAttribute("Dữ Liệu Không Tồn Tại.")]
        NOT_EXIST_DATA = 2,
        [DescriptionAttribute("Sai số Imei.")]
        WRONG_IMEI = 4,
        [DescriptionAttribute("Lỗi hệ thống.")]
        SYSTEM_ERROR = -1,
        [DescriptionAttribute("App không tồn tại.")]
        NOT_EXIST_APP = 5
    }

    public enum ERROR_CODE_CHANGE_PASS_WORD
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        [DescriptionAttribute("Mật khẩu cũ không đúng.")]
        WRONG_PASS = 6,
        [DescriptionAttribute("Sai tên đăng nhập.")]
        WRONG_USERNAME = 3,
        [DescriptionAttribute("App không tồn tại.")]
        NOT_EXIST_APP = 5,
        [DescriptionAttribute("Data không tồn tại.")]
        NOT_EXIST_DATA = 4,
        [DescriptionAttribute("Lỗi hệ thống.")]
        SYSTEM_ERROR = -1,
    }
}
