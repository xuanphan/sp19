﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using System.ComponentModel;
namespace Common.ERROR_CODE
{
    public enum ERROR_CODE_ITEMS_ADDNEW
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,

        [DescriptionAttribute("Lỗi hệ Thống.")]
        SYSTEM_ERROR = -1,

        [DescriptionAttribute("Dữ liệu đã tồn tại.")]
        EXIT_DATA = 2,

        [DescriptionAttribute("Dữ liệu không tồn tại.")]
        NOT_EXIT_DATA = 4,

        [DescriptionAttribute("App không tồn tại.")]
        NOT_EXIT_APP = 5,

        [DescriptionAttribute("Dữ liệu đã được Update.")]
        UPDATE_DATA = 3,
        DUPPLICATE = 0

    }
    public enum ERROR_CODE_ITEMS_UPDATE
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        [DescriptionAttribute("Dữ liệu đã tồn tại.")]
        EXIT_DATA = 2,
        [DescriptionAttribute("Không có dữ liệu tải lên hoặc sai formart.")]
        NOT_EXIST_DATA_OR_WRONG_FORMAT = 3,
        [DescriptionAttribute("Dữ liệu không tồn tại.")]
        NOT_EXIST_DATA = 4,
        [DescriptionAttribute("App không tồn tại.")]
        NOT_EXIST_APP = 5,
        [DescriptionAttribute("Lỗi hệ thống.")]
        SYSTEM_ERROR = -1,
        DUPPLICATE = 0
    }
    public enum ERROR_CODE_ITEMS_GET_BY_ID
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        SYSTEM_ERROR = -1,
        [DescriptionAttribute("Dữ liệu không tồn tại.")]
        DO_NOT_EXIST
    }
    public enum ERROR_CODE_ITEMS_CHECK_DUPPLICATE
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        SYSTEM_ERROR = -1
    }

    public enum ERROR_CODE_ITEMS_CHECK_CHANGE_GIFT
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        [DescriptionAttribute("Số điện thoại khách hàng đã đổi quà 2 lần tại đây trong hôm nay.")]
        EXIST_CHANGE_GIFT_ON_DAY = 2,
        [DescriptionAttribute("App không tồn tại.")]
        NOT_EXIST_APP = 5,
        [DescriptionAttribute("Lỗi hệ thống.")]
        SYSTEM_ERROR = -1
    }
    public enum ERROR_CODE_ITEMS_SELECT
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        [DescriptionAttribute("Dữ liệu đã tồn tại.")]
        EXIT_DATA = 2,
        [DescriptionAttribute("App không tồn tại.")]
        NOT_EXIT_APP = 5,
        [DescriptionAttribute("Dữ liệu không tồn tại.")]
        NOT_EXIT_DATA = 4,
        [DescriptionAttribute("Dữ liệu chưa phân công.")]
        NOT_EXIT_DATA_ASSIGN = 7,
        [DescriptionAttribute("Dữ liệu không tồn tại.")]
        DO_NOT_EXIST = 3,
        [DescriptionAttribute("Lỗi. ")]
        SYSTEM_ERROR = -1
    }
}
