﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.ERROR_CODE
{
    public class ERROR_STRING
    {
        public const string NOT_HAVE_PRIVACY = "The current user dose not have privacy.";
        public const string LOST_USER_SESSION = "The current user is out of session.";
        public const string USERNAME_IS_EXIST = "Tài khoản đăng nhập đã tồn tại.";
        public const string NOT_EXIST_SESSIONCODE = "SessionCode is not exist.";
    }
}
