﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using System.ComponentModel;
namespace Common.ERROR_CODE
{
    public enum ERROR_CODE_SIMPLE
    {
        [DescriptionAttribute("Thành công.")]
        SUCCESS = 1,
        [DescriptionAttribute("Lỗi.")]
        ERROR = 2,
        [DescriptionAttribute("App Không tồn tại.")]
        NOT_EXIST_APP = 5,
        [DescriptionAttribute("Dữ liệu không tồn tại.")]
        NOT_EXIT_DATA = 4,
        [DescriptionAttribute("Lỗi hệ thống.")]
        SYSTEM_ERROR = -1
    }
}
