﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class MESSAGE_STRING
    {
        public class MESSAGE_TITLE
        {
            public const string INFO = "Thông báo";
            public const string WARNING = "Cảnh báo";
            public const string ERROR = "Lỗi";
        }


        /// <summary> Xác nhận: rỗng, đã tồn tại </summary>
        public class Validate
        {
            public const string NOT_NULL = "Không để trống giá trị này";
            public const string IS_EXIST = "Giá trị đã tồn tại";
            public const string ONLY_NUM = "Chỉ nhập số";
            
        }

        public class MessageError
        {
            public static string NotNull(string _name)
            {
                return "Không để trống '" + _name + "'";
            }
            public static string IsExist(string _name)
            {
                return "'" + _name + "' đã tồn tại";
            }
        }

        public class AddNew
        {
            public static string Success(string _name)
            {
                return "Tạo mới '" + _name + "' thành công";
            }
            public static string Fail(string _name, string _error)
            {
                return "Tạo mới '" + _name + "' thất bại. Lỗi: " + _error;
            }
        }

        public class Webservice
        {
            public static string Success(string _name)
            {
                return "Gởi webservice '" + _name + "' thành công";
            }
            public static string Fail(string _name, string _error)
            {
                return "Gởi webservice '" + _name + "' thất bại. Lỗi: " + _error;
            }
        }

        public class Update
        {
            public static string Success(string _name)
            {
                return "Cập nhật '" + _name + "' thành công";
            }
            public static string Fail(string _name, string _error)
            {
                return "Cập nhật '" + _name + "' thất bại. Lỗi: " + _error;
            }
        }

        public class Import
        {
            public static string ReadRowError(int _rowIndex, string _error)
            {
                _rowIndex++;
                return "Đọc file lỗi, dòng " + _rowIndex + "\n" + _error;
            }
        }
    }
}
