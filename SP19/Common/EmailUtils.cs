﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;

namespace Common
{
    public class EmailUtils
    {
        public static string SendSimpleMail(string emailTo,string subject, List<KEYWORD> listKey)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(MailServerInfoImark._userName);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    string html = File.ReadAllText(DirectoryUtils.GetPathByFolder("\\Common\\MailTemplate.html", Global.ProjectName));

                    foreach (var key in listKey)
                    {
                        Regex re = new Regex(key.KEY);
                        MatchCollection mc = re.Matches(html);
                        foreach (Match m in mc)
                        {
                            html = html.Replace(m.Value, key.DESCRIPTION);
                        }
                    }

                    mail.Body = html;
                    mail.IsBodyHtml = true;
                    // Can set to false, if you are sending pure text.

                    //mail.Attachments.Add(new Attachment("C:\\SomeFile.txt"));
                    //mail.Attachments.Add(new Attachment("C:\\SomeZip.zip"));

                    using (SmtpClient smtp = new SmtpClient(MailServerInfoImark._mailServer, MailServerInfoImark._port))
                    {
                        smtp.Credentials = new NetworkCredential(MailServerInfoImark._userName, MailServerInfoImark._password);
                        smtp.EnableSsl = MailServerInfoImark._EnableSsl;
                        smtp.Send(mail);
                    }
                }
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return "ERROR :" + ex.ToString();
            }
        }
    }

    public class KEYWORD
    {
        public string KEY { get; set; }
        public string DESCRIPTION { get; set; }
    }
}
