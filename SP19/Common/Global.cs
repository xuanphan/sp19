﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Configuration;

namespace Common
{
    public class Global
    {
        public static bool CheckUserPrivacy = true;
        public static string AppCode = "IDS";
        public static string ProjectName = "SP19";
        public static string Serverkeytoken = "XHlzP8bs-7L5Cr8W0ZvI26oL898BiS";
        public static string secrectKeyTopup = "ciub6yO3YDiEsP5FkTki4R1unEeesjNcOyKWm-QLsSvzd1z0A1MeRET_n4a6_ZeNF5b_UduMleNz7Yfr76R_6K2YRH94UDNxXA2GjYG8njVEduXjdnV5TUtEZaaUcO";
        public static string Serverkey = "AAAACF-kI6Q:APA91bHx6OL9z6Mo_emuFbDQ7jP3JRv9VbsIF7qiyX6bTTPVxp-M_TjWhVK8wB1aZ-h10nzuGZsx7QGv0jU9AY84l9sVJCIUSpGZzT4YSjH6a4-lpXunyGaTRFpar6Z2CtHGczW4mSMc";
        public static string SenderID = "35964330916";
        public static string LegacyKey = "AIzaSyAr_juGpf5rWkmDLnMPR09h2MakYEVKMio";


        public static string ClientIDDriver = "476571903861-5ps7rganie9eb2e8638lftash4q8bh4b.apps.googleusercontent.com";
        public static string KeySecretDriver = "XHlzP8bs-7L5Cr8W0ZvI26oL";
        public static string FolderIDProject = "1YNN0IqW6DIXbBQ6j-TA3U5wOjf81I5KT";
        public static string FolderIDNam2019 = "10JGZ3knGm5yJHSUSCH-dJFIWtbGO6ps8";

        public enum UserType { FORM, WEB };
        public static string DoNotHavePrivacy = "Bạn chưa được phân quyền cho chức năng này.";
        public static ConnectionInfo ConnectionInfo = new ConnectionInfo(@"(local)",@"QLDT");
        public static MailServerInfo MailServerInfo_ = new MailServerInfo();
        public static String HostDomain = "http://localhost";
        private static int PageSize_ = 10;
        public static int PageSize
        {
            get
            {
                return PageSize_;
            }
            set
            {
                   PageSize_ = value;
            }
        }

        public static string ProductName = "Project Manager";
        public static System.Nullable<System.DateTime> ParseDateTime(string datetime)
        {
            try
            {
                string[] datetimes = datetime.Split("/".ToCharArray());
                if (datetimes == null || datetimes.Length != 3)
                {
                    return null;
                }              


                //String format = "yyyy/MM/dd";
                //DateTime temp = DateTime.ParseExact(datetime, format, CultureInfo.InvariantCulture);

                //System.Nullable<System.DateTime> ret = new DateTime(temp.Year, temp.Month, temp.Day, 12, 0, 0);
                System.Nullable<System.DateTime> ret = new DateTime(int.Parse(datetimes[0]), int.Parse(datetimes[1]), int.Parse(datetimes[2]), 12, 0, 0);
                                
                return ret;
            }
            catch 
            {
                return null;
            }
        }

        public static String DateTimeToString(DateTime datetime)
        {
            try
            {
                //String ret = datetime.ToString("d", CultureInfo.CreateSpecificCulture("zh-TW"));
                String ret = datetime.ToString("yyyy/MM/dd");
                return ret;
            }
            catch
            {
                return null;
            }
        }

        public static String ParseYearMonth(DateTime datetime)
        {
            try
            {
                String ret = datetime.ToString("yyyy/MM");
                return ret;
            }
            catch
            {
                return null;
            }
        }
        public static System.Nullable<System.DateTime> MakeDateTimeToFirstTimeInDay(System.Nullable<System.DateTime> input)
        {
            try
            {
                if (input != null)
                {
                    System.Nullable<System.DateTime> a = new DateTime(input.Value.Year, input.Value.Month, input.Value.Day, 0, 0, 0);
                    return a;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        public static System.Nullable<System.DateTime> MakeDateTimeToLastTimeInDay(System.Nullable<System.DateTime> input)
        {
            try
            {
                if (input != null)
                {
                    System.Nullable<System.DateTime> a = new DateTime(input.Value.Year, input.Value.Month, input.Value.Day, 23, 59, 59);
                    return a;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        public static System.Nullable<System.DateTime> MakeDateTimeToFirst(System.Nullable<System.DateTime> input)
        {
            try
            {
                if (input != null)
                {
                    System.Nullable<System.DateTime> a = new DateTime(input.Value.Year, input.Value.Month, 1,0,0,0);
                    return a;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        public static System.Nullable<System.DateTime> MakeDateTimeToLast(System.Nullable<System.DateTime> input)
        {
            try
            {
                if (input != null)
                {
                    System.Nullable<System.DateTime> a = new DateTime(input.Value.Year, input.Value.Month, DateTime.DaysInMonth(input.Value.Year,input.Value.Month),23,59,59);
                    return a;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public static String DefaultFromDate()
        {
            DateTime tmpDate = new DateTime(DateTime.Now.Year,1,1);
            return Common.Global.DateTimeToString(tmpDate);
        }

        public static String DefaultToDate()
        {
            DateTime tmpDate = new DateTime(DateTime.Now.Year,12, 31);
            return Common.Global.DateTimeToString(tmpDate);
        }

        public static String encodeBase64(String value)
        {
            try
            {
                if (value == null)
                    value = "";
                return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(value), Base64FormattingOptions.None);
            }
            catch /*(Exception e)*/
            {
                return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(""), Base64FormattingOptions.None);
            }
        }

        public static ConnectionInfo GetConnectionInfo()
        {
            string ServerName = ConfigurationManager.AppSettings["ServerName"].ToString();
            string DatabaseName = ConfigurationManager.AppSettings["DatabaseName"].ToString();
            string UserName = ConfigurationManager.AppSettings["UserName"].ToString();
            string Password = ConfigurationManager.AppSettings["Password"].ToString();
            string Authentication = ConfigurationManager.AppSettings["Authentication"].ToString();
            //ConfigurationManager.AppSettings["Timeout"]
            ConnectionInfo.ServerName = ServerName;
            ConnectionInfo.DatabaseName = DatabaseName;
            ConnectionInfo.UserName = UserName;
            ConnectionInfo.Password = Password;
            ConnectionInfo.Authentication = (Authentication == "") ? Common.ConnectionInfo.WIN_AUTHENTICATION_MODE : Common.ConnectionInfo.SQL_AUTHENTICATION_MODE;
            return ConnectionInfo;
        }

        
        #region Nghia
        /// <summary>
        /// lấy connection string ở đây, vì app.config có thể chỉnh sửa
        /// </summary>
        /// <returns></returns>
        public static ConnectionInfo GetConnectionInfoForm()
        {
            ConnectionInfo.ServerName = ServerName;
            ConnectionInfo.DatabaseName = DatabaseName;
            ConnectionInfo.UserName = UserName;
            ConnectionInfo.Password = Password;
            ConnectionInfo.Authentication = (Authentication == "") ? Common.ConnectionInfo.WIN_AUTHENTICATION_MODE : Common.ConnectionInfo.SQL_AUTHENTICATION_MODE;
            return ConnectionInfo;
        }

        public static ConnectionInfo GetConnectionInfo(string serverName, string databaseName, string userName, string password, string authentication)
        {
            ConnectionInfo.ServerName = serverName;
            ConnectionInfo.DatabaseName = databaseName;
            ConnectionInfo.UserName = userName;
            ConnectionInfo.Password = password;
            ConnectionInfo.Authentication = (authentication == "") ? Common.ConnectionInfo.WIN_AUTHENTICATION_MODE : Common.ConnectionInfo.SQL_AUTHENTICATION_MODE;
            return ConnectionInfo;
        }

        public static string ServerName = "221.132.37.39";
        public static string DatabaseName = "NANHA2015";
        public static string UserName = "nhat";
        public static string Password = "123Imark";
        public static string Authentication = "SQL";
        #endregion
    }
}
