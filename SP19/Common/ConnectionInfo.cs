﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;
namespace Common
{
	using System.IO;
	public class ConnectionInfo
	{
		#region Const

		public const string SQL_AUTHENTICATION_MODE = @"SQL Server Authentication";

		public const string WIN_AUTHENTICATION_MODE = @"Windows Authentication";

		#endregion

		#region Members
        private int timeout = 20; // 20s
		private string _serverName = "testserver";
		private string _databaseName = "QLDT";
		private string _userName = "sa";
		private string _password = "@a123456";
		private string _authentication = SQL_AUTHENTICATION_MODE;
		#endregion

		#region Properties
		
		public string ServerName
		{
			get { return _serverName; }
			set { _serverName = value; }
		}

		public string DatabaseName
		{
			get { return _databaseName; }
			set { _databaseName = value; }
		}

		public string UserName
		{
			get { return _userName; }
			set { _userName = value; }
		}

		public string Password
		{
			get { return _password; }
			set { _password = value; }
		}

		public string Authentication
		{
			get { return _authentication; }
			set { _authentication = value; }
		}
		
		public string ConnectionStringSQL
		{
			get
			{
				if (Authentication == "SQL Server Authentication")
                    return string.Format("Data Source={0};Initial Catalog={1};Connect Timeout={2};User Id={3};Password={4};", ServerName, DatabaseName, timeout.ToString(), UserName, Password);
				else
                    return string.Format("Initial Catalog={0};Connect Timeout={1};Data Source={2}; Integrated Security=SSPI", DatabaseName, timeout.ToString(), ServerName);
			}
		}
		
		public string ConnectionString
		{
			get
			{
				if (Authentication == "SQL Server Authentication")
					return string.Format("Provider=SQLOLEDB;Data Source={0};Initial Catalog={1};User Id={2};Password={3};", ServerName, DatabaseName, UserName, Password);
				else
					return string.Format("Provider=SQLOLEDB;Initial Catalog={0};Data Source={1}; Integrated Security=SSPI", DatabaseName, ServerName);
			}
		}
		
		#endregion Properties

		#region Constructor
		
		/// <summary>
		/// Connection uses windows account to authenticate
		/// </summary>		
		public ConnectionInfo(string serverName, string database)
		{
			this.ServerName = serverName;
			this.DatabaseName = database;
			_authentication = WIN_AUTHENTICATION_MODE;
		}

		/// <summary> 
		///  Connection uses sql server account to authenticate
		/// </summary>
		public ConnectionInfo(string serverName, string databaseName, string userName, string password)
		{
			_serverName = serverName;
			_databaseName = databaseName;
			_userName = userName;
			_password = password;
			_authentication = SQL_AUTHENTICATION_MODE;
		}
		
		#endregion

		#region Public Methods

		public bool TestConnection()
		{
			OleDbConnection con = null;
			try
			{
				con = new OleDbConnection(ConnectionString);
				con.Open();
				return true;
			}
			catch
			{
				return false;
			}
			finally
			{
				if (con != null)
				{
					if (con.State == ConnectionState.Open)
						con.Close();

					con.Dispose();
					con = null;
				}
			}
		}
        /*
		public static bool LoadConnection(string connFile, out ConnectionInfo connInfo)
		{
			var filename = connFile;
			if (!File.Exists(filename))
			{
				connInfo = null;
				return false;
			}
			try
			{
				using (var stream = new StreamReader(filename))
				{
					var auth = Utils.GiaiMa(stream.ReadLine());
					var host = Utils.GiaiMa(stream.ReadLine());
					var db = Utils.GiaiMa(stream.ReadLine());
					var user = Utils.GiaiMa(stream.ReadLine());
					var pass = Utils.GiaiMa(stream.ReadLine());
					if (auth == ConnectionInfo.SQL_AUTHENTICATION_MODE)
						connInfo = new ConnectionInfo(host, db, user, pass);
					else
						connInfo = new ConnectionInfo(host, db);
				}
				return true;
			}
			catch
			{
				connInfo = null;
				return false;
			}
		}

		public static bool SaveConnection(string connFile, ConnectionInfo connInfo)
		{
			var filename = connFile;
			if (connInfo == null)
				return false;
			try
			{
				using (var stream = new StreamWriter(filename))
				{
					stream.WriteLine(Utils.MaHoa(connInfo.Authentication));
					stream.WriteLine(Utils.MaHoa(connInfo.ServerName));
					stream.WriteLine(Utils.MaHoa(connInfo.DatabaseName));
					if (connInfo.Authentication == ConnectionInfo.SQL_AUTHENTICATION_MODE)
					{
						stream.WriteLine(Utils.MaHoa(connInfo.UserName));
						stream.WriteLine(Utils.MaHoa(connInfo.Password));
					}
					else
					{
						stream.WriteLine(string.Empty);
						stream.WriteLine(string.Empty);
					}
				}
				return true;
			}
			catch
			{
				return false;
			}
		}
        */
		#endregion

	}
}
