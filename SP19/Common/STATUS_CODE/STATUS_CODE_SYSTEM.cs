﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace Common.STATUS_CODE
{
    public class IMPORT_CODE
    {
        public const string SUCCESS = "Success";
        public const string UPDATE = "Update";
        public const string SKIP = "Skip";
    }

    public class HISTORY_ACTION_CODE
    {
        public const string INSERT = "Insert";
        public const string UPDATE = "Update";
        public const string ORGINAL = "Orginal";
    }

    public class ROLE_CODE
    {
        public const string ROOT = "Root";
        public const string ADMIN = "Admin";
        public const string PGLeader = "PGLeader";
        public const string PG = "PG";
    }
    //-----------------------Phan quyen user
    public enum USER_ROLE
    {
        [DescriptionAttribute("System.")]
        SYSTEM = 1,
        [DescriptionAttribute("Administrator.")]
        ADMINISTRATOR = 2,
        [DescriptionAttribute("Audit.")]
        AUDIT = 3,
        [DescriptionAttribute("Client.")]
        CLIENT = 4
    }

    //----------------------Status recode
    public enum ROW_STATUS
    {
        [DescriptionAttribute("Active.")]
        ACTIVE = 1,
        [DescriptionAttribute("Complete.")]
        COMPLETE = 3,
        [DescriptionAttribute("Deactive.")]
        DEACTIVE = 2
    }

    /// <summary> Phân biệt User WEB vs FORM </summary>
    public enum USER_ROW_STATUS
    {
        [DescriptionAttribute("Form Login")]
        FORM_LOGIN = 3,
        [DescriptionAttribute("Web Login")]
        WEB_LOGIN = 4,
    }


    public enum SURVEY_STATUS
    {
        [DescriptionAttribute("Start.")]
        START_SURVEY = 1,
        [DescriptionAttribute("End.")]
        END_SURVEY = 2
    }

    public enum OFFLINE_STATUS
    {
        WAIT_UPLOAD = 1,
        UPLOADED = 2,
    }
}
