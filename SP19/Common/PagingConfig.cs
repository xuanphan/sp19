﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Common
{
    public class PagingConfig
    {
        private int _PageSize = 100;
        public int PageSize
        {
            get { return _PageSize; }
            set { _PageSize = value; }
        }

        private int _CurrentPage = 1;
        public int CurrentPage
        {
            get { return _CurrentPage; }
            set { _CurrentPage = value; }
        }

        public int SkipRecord
        {
            get { return (CurrentPage - 1) * _PageSize; }
        }
    }
}
