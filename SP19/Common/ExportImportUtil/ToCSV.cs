﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using System.IO;
using System.Data.SqlTypes;

namespace Common.ExportImportUtil
{
    public class ExportCSV<T> where T : class
    {
        public List<T> Objects;

        /// <summary> Truyền List 
        /// vd: string logPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\log.csv";
        /// Common.ExportImportUtil.ExportCSV<PublicCode.Log.MakeLuckyLog> csv = new Common.ExportImportUtil.ExportCSV<PublicCode.Log.MakeLuckyLog>(listLog);
        /// csv.ExportToFile(logPath);
        /// </summary>
        public ExportCSV(List<T> objects)
        {
            Objects = objects;
        }

        public string Export()
        {
            return Export(true);
        }

        public string Export(bool includeHeaderLine)
        {
            StringBuilder sb = new StringBuilder();
            //Get properties using reflection.
            IList<PropertyInfo> propertyInfos = typeof(T).GetProperties();

            if (includeHeaderLine)
            {
                //add header line.
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    string propertyName = propertyInfo.Name;
                    sb.Append(propertyName).Append(",");
                }
                sb.Remove(sb.Length - 1, 1).AppendLine(); // bỏ , cuối
            }

            //add value for each property.
            foreach (T obj in Objects)
            {
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    object objValue = propertyInfo.GetValue(obj, null);
                    string strValue = MakeValueCsvFriendly(objValue);
                    sb.Append(strValue).Append(",");
                }
                sb.Remove(sb.Length - 1, 1).AppendLine(); // bỏ , cuối
            }

            return sb.ToString();
        }

        //export to a file.
        public void ExportToFile(string path)
        {
            string value = Export();
            File.WriteAllText(path, value, Encoding.UTF8);
        }

        //export as binary data.
        public byte[] ExportToBytes()
        {
            string value = Export();
            return Encoding.UTF8.GetBytes(value);
        }

        /// <summary> convert Object => string </summary>
        private string MakeValueCsvFriendly(object value)
        {
            // value = null => ""
            if (value == null) return "";
            if (value is Nullable && ((INullable)value).IsNull) return "";

            // value = datetime => yyyy-MM-dd  HH:mm:ss
            if (value is DateTime)
            {
                if (((DateTime)value).TimeOfDay.TotalSeconds == 0)
                    return ((DateTime)value).ToString("yyyy-MM-dd");
                return ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
            }

            string output = value.ToString();
            // chứa "," hay ""\" sửa lại
            if (output.Contains(",") || output.Contains("\""))
                output = '"' + output.Replace("\"", "\"\"") + '"';

            return output;
        }
    }
}
