﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Common
{
    public class DirectoryUtils
    {
        public static string GetPathByFolder(string FolderName,string RootFolderName)
        {
            string path = Directory.GetCurrentDirectory();
            string FullName = String.Empty;
            string RootDrive = Directory.GetDirectoryRoot(path);
            while (!FullName.Equals(RootFolderName))
            {
                FullName = Directory.GetParent(path).Name;
                path = Directory.GetParent(path).FullName;
                if (FullName.Equals(RootDrive))
                    return String.Empty;
            }
            string pathReturn = path + FolderName;
            return pathReturn;
        }
    }
}
