﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class PagingParam
    {
        private int _PageFrom = int.MinValue;
        public int PageFrom
        {
            get { return _PageFrom; }
            set { _PageFrom = value; }
        }

        private int _PageTo = int.MaxValue;
        public int PageTo
        {
            get { return _PageTo; }
            set { _PageTo = value; }
        }

        private int _PageSize = int.MaxValue;
        public int PageSige
        {
            get { return _PageSize; }
            set { _PageSize = value; }
        }
    }
}
