﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class CardUtils
    {
        public static Dictionary<string, string> ChangeToValue(string _value_In)
        {
            Dictionary<string, string> returnValue = new Dictionary<string, string>();
            // chuyển ; => cách nhóm, : => cách giá trị
            string[] arrTemp = _value_In.Split(';');
            arrTemp = arrTemp.Where(item => !string.IsNullOrEmpty(item)).ToArray();

            foreach (var item in arrTemp)
            {
                string[] arrValue = item.Split(':');
                if (arrValue.Length >= 2)
                {
                    returnValue.Add(arrValue[0], arrValue[1]);
                }
            }

            return returnValue;
        }
    }
}
