﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    class ObjectUtils
    {
        /// <summary> Gán dữ liệu đến dữ liệu </summary>
        public static void CopyObject<T>(T from, T to)
        {
            Type tto = typeof(T);
            System.Reflection.PropertyInfo[] pFrom = tto.GetProperties();
            foreach (System.Reflection.PropertyInfo p in pFrom)
            {
                System.Reflection.PropertyInfo tmp = tto.GetProperty(p.Name);
                //bỏ qua null
                if (tmp == null || p.GetValue(from, null) == null) continue;

                if (!p.GetValue(from, null).GetType().FullName.Contains("System")
                    || p.GetValue(from, null).GetType().FullName.Contains("Business"))
                    continue;
                tmp.SetValue(to, p.GetValue(from, null), null);
            }

        }

        /// <summary> So sánh phần tử mảng này có thuộc mảng kia ko </summary>
        public static bool CompareStringArray(string[] arr1, string[] arr2)
        {
            if (arr1.Count() > 0 && arr2.Count() > 0)
            {
                foreach (string str1 in arr1)
                {
                    if (arr2.Contains(str1))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
