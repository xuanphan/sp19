﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;

namespace Common
{
    public class FileUtils
    {
        public static void Rename(string FileName, string DirectoryPath, string NewName)
        {
            try
            {
                string currFilePath = DirectoryPath + "/" + FileName;
                FileInfo file = new FileInfo(currFilePath);
                file.MoveTo(DirectoryPath + "/" + NewName);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        
        public static void Save(string FilePath, Stream Stream)
        {
            try
            {
                if (Stream.Length == 0) return;
                // Create a FileStream object to write a stream to a file
                using (FileStream fileStream = System.IO.File.Create(FilePath, (int)Stream.Length))
                {
                    // Fill the bytes[] array with the stream data
                    byte[] bytesInStream = new byte[Stream.Length];
                    Stream.Read(bytesInStream, 0, (int)bytesInStream.Length);

                    // Use FileStream object to write to the specified file
                    fileStream.Write(bytesInStream, 0, bytesInStream.Length);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static void Save(string FilePath, byte[] ImageByte)
        {
            try
            {
                File.WriteAllBytes(FilePath, ImageByte);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Delete(string FilePath)
        {
            try
            {
                FileInfo file = new FileInfo(FilePath);
                file.Delete();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private string GetExtensionFile(string FileName)
        {
            string[] arr = FileName.Split('.');
            return arr[arr.Length - 1];
        }

        public static byte[] GetBytesFromPath(string FilePath)
        {
            FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            byte[] bytes = br.ReadBytes((int)fs.Length);
            fs.Flush();
            fs.Close();
            return bytes;
        }

        #region Nghia
        /// <summary> Mở file </summary>
        public static bool Open(string FileName)
        {
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = FileName;
                process.Start();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }

    public class ImageUtils : FileUtils
    {
        public static byte[] CreateThumbnail(string ImagePath, int MaxHeight, int MaxWidth, string Extension)
        {
            System.Drawing.Image img = System.Drawing.Image.FromFile(ImagePath);
            Extension = Extension.ToLower();
            byte[] buffer = null;
            try
            {
                //int width = img.Size.Width;
                //int height = img.Size.Height;
                //bool doWidthResize = (MaxWidth > 0 && width > MaxWidth && width > MaxHeight);
                //bool doHeightResize = (MaxHeight > 0 && height > MaxHeight && height > MaxWidth);
                ////only resize if the image is bigger than the max
                //if (doWidthResize || doHeightResize)
                //{
                //    int iStart;
                //    Decimal divider;
                //    if (doWidthResize)
                //    {
                //        iStart = width;
                //        divider = Math.Abs((Decimal)iStart / (Decimal)MaxWidth);
                //        width = MaxWidth;
                //        height = (int)Math.Round((Decimal)(height / divider));
                //    }
                //    else
                //    {
                //        iStart = height;
                //        divider = Math.Abs((Decimal)iStart / (Decimal)MaxHeight);
                //        height = MaxHeight;
                //        width = (int)Math.Round((Decimal)(width / divider));
                //    }
                //}

                System.Drawing.Image newImg = img.GetThumbnailImage(1024, 768, null, new System.IntPtr());
                try
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        if (Extension.IndexOf("jpg") > -1)
                        {
                            newImg.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                        else if (Extension.IndexOf("png") > -1)
                        {
                            newImg.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        }
                        else if (Extension.IndexOf("gif") > -1)
                        {
                            newImg.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                        }
                        else // if (extension.IndexOf("bmp") > -1)
                        {
                            newImg.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                        }
                        buffer = ms.ToArray();
                    }
                }
                finally
                {
                    newImg.Dispose();
                }
            }
            catch(Exception ex)
            {
                System.Drawing.Image imNoimage = System.Drawing.Image.FromFile("/images/noimage.gif");
                try
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        imNoimage.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                        buffer = ms.ToArray();
                    }
                }
                finally
                {
                    imNoimage.Dispose();
                }
                throw ex;
            }

            finally
            {
                img.Dispose();
            }

            return buffer;
        }

        public static Image ByteArrayToImage(byte[] ImageByte)
        {
            MemoryStream ms = new MemoryStream(ImageByte);
            Image image = Image.FromStream(ms);
            return image;
        }

        /// <summary> resize image </summary>
        public static Image ResizeImage(Image imgToResize, int maxWidth, int maxHeight)
        {
            // canh lại maxHeight
            int resizeWidth = imgToResize.Width;
            int resizeHeight = imgToResize.Height;
            double aspect = (double)resizeWidth / resizeHeight;

            if (resizeWidth > maxWidth)
            {
                resizeWidth = maxWidth;
                resizeHeight = (int)(resizeWidth / aspect);
            }
            if (resizeHeight > maxHeight)
            {
                aspect = (double)resizeWidth / resizeHeight;
                resizeHeight = maxHeight;
                resizeWidth = (int)(resizeHeight * aspect);
            }
            Size size = new Size(resizeWidth, resizeHeight);
            Bitmap bitmap = new Bitmap(imgToResize, size);
            return (Image)bitmap;
        }

        /// <summary> resize image </summary>
        public static Bitmap ResizeImage(Bitmap imgToResize, int maxWidth, int maxHeight)
        {
            // canh lại maxHeight
            int resizeWidth = imgToResize.Width;
            int resizeHeight = imgToResize.Height;
            double aspect = (double)resizeWidth / resizeHeight;

            if (resizeWidth > maxWidth)
            {
                resizeWidth = maxWidth;
                resizeHeight = (int)(resizeWidth / aspect);
            }
            if (resizeHeight > maxHeight)
            {
                aspect = (double)resizeWidth / resizeHeight;
                resizeHeight = maxHeight;
                resizeWidth = (int)(resizeHeight * aspect);
            }
            Size size = new Size(resizeWidth, resizeHeight);
            Bitmap bitmap = new Bitmap(imgToResize, size);
            return bitmap;
        }
    }
}
