﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class StringUtils
    {
        /// <summary>
        /// Viết ngắn gọn lại 1 chuỗi
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static string toShort(string longString)
        {
            longString = longString.Trim();
            string[] array = longString.Split(' ');
            string sortString = "";

            foreach (string str in array)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    sortString += str.First();
                }
            }

            return sortString;
        }
    }
}
